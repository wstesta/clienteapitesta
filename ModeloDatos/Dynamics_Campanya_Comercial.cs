//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ModeloDatos
{
    using System;
    using System.Collections.Generic;
    
    public partial class Dynamics_Campanya_Comercial
    {
        public string OdataEtag { get; set; }
        public string eve_fecha_contacto { get; set; }
        public string eve_reasignacion { get; set; }
        public string C_eve_inquilino_principal_id_value { get; set; }
        public string createdon { get; set; }
        public string eve_name { get; set; }
        public string importsequencenumber { get; set; }
        public string statecode { get; set; }
        public string eve_estado_recuperacion_adicional { get; set; }
        public string eve_fecha_cambio_motivo_1 { get; set; }
        public string eve_fecha_cambio_motivo_2 { get; set; }
        public string eve_tipo_recuperacion { get; set; }
        public string C_ownerid_value { get; set; }
        public string modifiedon { get; set; }
        public string C_eve_codigo_contrato_id_value { get; set; }
        public string eve_comentarios_motivo_finalizacion { get; set; }
        public string versionnumber { get; set; }
        public string eve_motivo_inquilino { get; set; }
        public string timezoneruleversionnumber { get; set; }
        public string eve_comentarios_estado_recuperacion { get; set; }
        public string C_modifiedby_value { get; set; }
        public string eve_tipo_envio_renovacion { get; set; }
        public string statuscode { get; set; }
        public string eve_estado_recuperacion { get; set; }
        public string eve_fecha_envio_renovacion_historico { get; set; }
        public string eve_motivo_finalizacion_contrato_anterior_2 { get; set; }
        public string eve_campanya_comercialid { get; set; }
        public string eve_motivo_finalizacion_contrato_anterior_1 { get; set; }
        public string C_createdby_value { get; set; }
        public string C_owningbusinessunit_value { get; set; }
        public string eve_motivo_finalizacion_contrato_adicional { get; set; }
        public string C_owninguser_value { get; set; }
        public string eve_equipo_encargado_recuperacion { get; set; }
        public string eve_motivo_no_recuperacion_1 { get; set; }
        public string eve_cierre_automatico_recuperacion { get; set; }
        public string eve_otros_motivos_i { get; set; }
        public string eve_email_recuperacion_inquilino_2 { get; set; }
        public string eve_fecha_fin_contrato { get; set; }
        public string eve_cierre_expediente { get; set; }
        public string eve_fecha_finalizacion_recuperacion { get; set; }
        public string eve_otros_motivos_ii { get; set; }
        public string eve_inquilino_recuperacion_3 { get; set; }
        public string overriddencreatedon { get; set; }
        public string eve_inquilino_recuperacion_1 { get; set; }
        public string eve_fecha_cierre_automatico { get; set; }
        public string C_modifiedonbehalfby_value { get; set; }
        public string eve_email_recuperacion_inquilino_3 { get; set; }
        public string eve_codigo_postal_destino { get; set; }
        public string eve_poblacion_destino { get; set; }
        public string eve_email_recuperacion_inquilino_1 { get; set; }
        public string eve_clasificacion_cambio_vivienda { get; set; }
        public string utcconversiontimezonecode { get; set; }
        public string eve_motivo_no_recuperacion_2 { get; set; }
        public string C_createdonbehalfby_value { get; set; }
        public string eve_telefono_recuperacion_1 { get; set; }
        public string eve_telefono_recuperacion_3 { get; set; }
        public string eve_telefono_recuperacion_2 { get; set; }
        public string eve_inquilino_recuperacion_2 { get; set; }
        public string C_owningteam_value { get; set; }
    }
}
