﻿using System;
using Newtonsoft.Json;
using ModeloDatos;

namespace ClienteAPITesta
{
    public class Promocion
    {
        [JsonProperty("@odata.etag")]
        public string OdataEtag { get; set; }
        public string eve_provincia { get; set; }
        public string eve_carencia_garaje_comunidad { get; set; }
        public string eve_protocolo_suministros { get; set; }
        public string eve_direccion { get; set; }
        public string eve_titularidad_ccpp { get; set; }
        public string eve_calendario_citas_personalizado { get; set; }
        public string eve_restringir_envio_notificaciones_impagados { get; set; }
        public string eve_ivatrastero { get; set; }
        public string eve_restringir_acceso_portal { get; set; }
        public string eve_ms_flow { get; set; }
        public string eve_promocionid { get; set; }
        public string eve_activar_restriccion_visualizacion_portal { get; set; }
        public string eve_carencia_vivienda_comunidad { get; set; }
        public string eve_carencia_trastero_comunidad { get; set; }
        public string eve_estado_promocion { get; set; }
        public string _ownerid_value { get; set; }
        public string createdon { get; set; }
        public string eve_nombre { get; set; }
        public string eve_poblacion { get; set; }
        public string eve_habilitarrenovacion_opcioncompra { get; set; }
        public string eve_equipo_deuda { get; set; }
        public string eve_ivaviviendapersonajuridica { get; set; }
        public string eve_equipo_comercial { get; set; }
        public string eve_reserva_vi { get; set; }
        public string eve_activar_restriccion_acceso_portal { get; set; }
        public string statuscode { get; set; }
        public string versionnumber { get; set; }
        public string emailaddress { get; set; }
        public string eve_carencia_garaje_ibi { get; set; }
        public string eve_descripcionpromocion { get; set; }
        public string eve_ivavivienda { get; set; }
        public string eve_visualizacion_portal { get; set; }
        public string eve_ivagarajepersonajuridica { get; set; }
        public string eve_ivagaraje { get; set; }
        public string eve_sociedad_origen { get; set; }
        public string modifiedon { get; set; }
        public string eve_equipo_comercializadora { get; set; }
        public string eve_asset_manager { get; set; }
        public string _eve_sociedadid_value { get; set; }
        public string eve_nombre_comercializadora { get; set; }
        public string _modifiedby_value { get; set; }
        public string eve_reserva_ga_tr { get; set; }
        public string _createdby_value { get; set; }
        public string eve_calendario_citas_personalizado_renovacion { get; set; }
        public string _owninguser_value { get; set; }
        public string eve_plazovivienda { get; set; }
        public string eve_actualizacionpropietario { get; set; }
        public string eve_no_anejos_disponibles { get; set; }
        public string eve__carencia_vivienda_ibi { get; set; }
        public string eve_nombre_comercializadora_reservas { get; set; }
        public string eve_carencia_trastero_ibi { get; set; }
        public string statecode { get; set; }
        public string _owningbusinessunit_value { get; set; }
        public string eve_ivatrasteropersonajuridica { get; set; }
        public string eve_tipologia_viviendas { get; set; }
        public string eve_plazogaraje { get; set; }
        public string eve_agua_fria { get; set; }
        public string eve_plantillaborradorviviendanovacion { get; set; }
        public string eve_email_gestor_acompanamiento_suministroiii { get; set; }
        public string eve_email_llaves_3 { get; set; }
        public string _eve_responsablefirmaid_value { get; set; }
        public string eve_delegacion { get; set; }
        public string importsequencenumber { get; set; }
        public string eve_telefono_comercializadora_reservas { get; set; }
        public string eve_parking_bicicleta { get; set; }
        public string utcconversiontimezonecode { get; set; }
        public string overriddencreatedon { get; set; }
        public string eve_email_comercializadora_reservasiii { get; set; }
        public string eve_mensajerestriccionfactura { get; set; }
        public string eve_email_validacion_seguros_ii { get; set; }
        public string eve_padel { get; set; }
        public string eve_electricidad { get; set; }
        public string eve_email_centro_custodio_3 { get; set; }
        public string eve_email_comercializadora_reservas { get; set; }
        public string eve_email_centro_custodio_2 { get; set; }
        public string eve_email_centro_custodio_1 { get; set; }
        public string eve_sala_social { get; set; }
        public string eve_email_llaves_2 { get; set; }
        public string eve_email_validacion_contrato_ii { get; set; }
        public string eve_ascensor { get; set; }
        public string eve_mailbox_lockerbox { get; set; }
        public string eve_email_llaves_1 { get; set; }
        public string eve_direccion_postal_api { get; set; }
        public string eve_email_gestor_energetico_ii { get; set; }
        public string eve_portfolio { get; set; }
        public string eve_ivalocalpersonajuridica { get; set; }
        public string eve_plantillarenovacionfirma { get; set; }
        public string eve_vigilancia_24h { get; set; }
        public string eve_plantillarenovacionviviendanovacion { get; set; }
        public string eve_email_gestor_energetico_i { get; set; }
        public string eve_gimnasio { get; set; }
        public string timezoneruleversionnumber { get; set; }
        public string eve_email_validacion_seguros_i { get; set; }
        public string _owningteam_value { get; set; }
        public string eve_agua_caliente { get; set; }
        public string eve_portero_fisico { get; set; }
        public string eve_carencia_local_ibi { get; set; }
        public string _modifiedonbehalfby_value { get; set; }
        public string eve_plantillaborradorgaraje { get; set; }
        public string eve_nombre_gestor_energetico { get; set; }
        public string eve_jardin { get; set; }
        public string eve_ivalocal { get; set; }
        public string eve_email_gestor_acompanamiento_suministros_i { get; set; }
        public string eve_email_validacion_contrato_iii { get; set; }
        public string eve_zona_infantil { get; set; }
        public string eve_plantillarenovacion { get; set; }
        public string _createdonbehalfby_value { get; set; }
        public string eve_telefono_comercializadora { get; set; }
        public string eve_carencia_local_comunidad { get; set; }
        public string eve_restringir_envio_notificaciones { get; set; }
        public string eve_agua_caliente_placas_solares { get; set; }
        public string eve_acceso_fibra_optica { get; set; }
        public string eve_reserva_lo { get; set; }
        public string eve_acepta_mascotas { get; set; }
        public string eve_equipo_financiero { get; set; }
        public string eve_zona_juegos { get; set; }
        public string eve_certificado_energetico { get; set; }
        public string eve_recarga_vehiculos_electricos { get; set; }
        public string eve_concepto_supuesto { get; set; }
        public string eve_climatizacion { get; set; }
        public string eve_plantillarenovaciongarajenovacion { get; set; }
        public string eve_calefaccion { get; set; }
        public string eve_plazolocal { get; set; }
        public string eve_email_validacion_contrato_iv { get; set; }
        public string eve_email_validacion_contrato_i { get; set; }
        public string eve_fecharestriccionfactura { get; set; }
        public string eve_equipo_tecnico { get; set; }
        public string eve_nombre_gestor_acompanamiento_suministros { get; set; }
        public string eve_energia_solar { get; set; }
        public string eve_email_gestor_energetico_iii { get; set; }
        public string eve_plazotrastero { get; set; }
        public string eve_email_gestor_acompanamiento_suministrosii { get; set; }
        public string eve_piscina { get; set; }
        public string eve_zona_deportiva { get; set; }
        public string eve_gestor_energetico { get; set; }
        public string eve_reforma_zzcc { get; set; }
        public string eve_email_comercializadora_reservasii { get; set; }
        public string eve_plantillaborradorgarajenovacion { get; set; }
        public string eve_nombre_centro_custodio { get; set; }
        public string eve_email_comercializadora_reservasiv { get; set; }
        public string eve_plantillarenovaciongaraje { get; set; }
        public string eve_fecha_hora_bloqueo { get; set; }
        public string eve_supuesto { get; set; }
        public string eve_perimetro_inconcert { get; set; }
        public string eve_caracteristicas_comerciales { get; set; }
        public string eve_observaciones_comerciales { get; set; }
        public string eve_promocion_fecha_renovacion { get; set; }
        public string data_dynamics { get; set; }

public Promocion() { }

        public Dynamics_Promociones Convertir2BD()
        {
            Dynamics_Promociones d_promocion = new Dynamics_Promociones();
            d_promocion.OdataEtag = OdataEtag;
            d_promocion.eve_provincia = eve_provincia;
            d_promocion.eve_carencia_garaje_comunidad = eve_carencia_garaje_comunidad;
            d_promocion.eve_protocolo_suministros = eve_protocolo_suministros;
            d_promocion.eve_direccion = eve_direccion;
            d_promocion.eve_titularidad_ccpp = eve_titularidad_ccpp;
            d_promocion.eve_calendario_citas_personalizado = eve_calendario_citas_personalizado;
            d_promocion.eve_restringir_envio_notificaciones_impagados = eve_restringir_envio_notificaciones_impagados;
            d_promocion.eve_ivatrastero = eve_ivatrastero;
            d_promocion.eve_restringir_acceso_portal = eve_restringir_acceso_portal;
            d_promocion.eve_ms_flow = eve_ms_flow;
            d_promocion.eve_promocionid = eve_promocionid;
            d_promocion.eve_activar_restriccion_visualizacion_portal = eve_activar_restriccion_visualizacion_portal;
            d_promocion.eve_carencia_vivienda_comunidad = eve_carencia_vivienda_comunidad;
            d_promocion.eve_carencia_trastero_comunidad = eve_carencia_trastero_comunidad;
            d_promocion.eve_estado_promocion = eve_estado_promocion;
            d_promocion.C_ownerid_value = _ownerid_value;
            d_promocion.createdon = createdon;
            d_promocion.eve_nombre = eve_nombre;
            d_promocion.eve_poblacion = eve_poblacion;
            d_promocion.eve_habilitarrenovacion_opcioncompra = eve_habilitarrenovacion_opcioncompra;
            d_promocion.eve_equipo_deuda = eve_equipo_deuda;
            d_promocion.eve_ivaviviendapersonajuridica = eve_ivaviviendapersonajuridica;
            d_promocion.eve_equipo_comercial = eve_equipo_comercial;
            d_promocion.eve_reserva_vi = eve_reserva_vi;
            d_promocion.eve_activar_restriccion_acceso_portal = eve_activar_restriccion_acceso_portal;
            d_promocion.statuscode = statuscode;
            d_promocion.versionnumber = versionnumber;
            d_promocion.emailaddress = emailaddress;
            d_promocion.eve_carencia_garaje_ibi = eve_carencia_garaje_ibi;
            d_promocion.eve_descripcionpromocion = eve_descripcionpromocion;
            d_promocion.eve_ivavivienda = eve_ivavivienda;
            d_promocion.eve_visualizacion_portal = eve_visualizacion_portal;
            d_promocion.eve_ivagarajepersonajuridica = eve_ivagarajepersonajuridica;
            d_promocion.eve_ivagaraje = eve_ivagaraje;
            d_promocion.eve_sociedad_origen = eve_sociedad_origen;
            d_promocion.modifiedon = modifiedon;
            d_promocion.eve_equipo_comercializadora = eve_equipo_comercializadora;
            d_promocion.eve_asset_manager = eve_asset_manager;
            d_promocion.C_eve_sociedadid_value = _eve_sociedadid_value;
            d_promocion.eve_nombre_comercializadora = eve_nombre_comercializadora;
            d_promocion.C_modifiedby_value = _modifiedby_value;
            d_promocion.eve_reserva_ga_tr = eve_reserva_ga_tr;
            d_promocion.C_createdby_value = _createdby_value;
            d_promocion.eve_calendario_citas_personalizado_renovacion = eve_calendario_citas_personalizado_renovacion;
            d_promocion.C_owninguser_value = _owninguser_value;
            d_promocion.eve_plazovivienda = eve_plazovivienda;
            d_promocion.eve_actualizacionpropietario = eve_actualizacionpropietario;
            d_promocion.eve_no_anejos_disponibles = eve_no_anejos_disponibles;
            d_promocion.eve__carencia_vivienda_ibi = eve__carencia_vivienda_ibi;
            d_promocion.eve_nombre_comercializadora_reservas = eve_nombre_comercializadora_reservas;
            d_promocion.eve_carencia_trastero_ibi = eve_carencia_trastero_ibi;
            d_promocion.statecode = statecode;
            d_promocion.C_owningbusinessunit_value = _owningbusinessunit_value;
            d_promocion.eve_ivatrasteropersonajuridica = eve_ivatrasteropersonajuridica;
            d_promocion.eve_tipologia_viviendas = eve_tipologia_viviendas;
            d_promocion.eve_plazogaraje = eve_plazogaraje;
            d_promocion.eve_agua_fria = eve_agua_fria;
            d_promocion.eve_plantillaborradorviviendanovacion = eve_plantillaborradorviviendanovacion;
            d_promocion.eve_email_gestor_acompanamiento_suministroiii = eve_email_gestor_acompanamiento_suministroiii;
            d_promocion.eve_email_llaves_3 = eve_email_llaves_3;
            d_promocion.C_eve_responsablefirmaid_value = _eve_responsablefirmaid_value;
            d_promocion.eve_delegacion = eve_delegacion;
            d_promocion.importsequencenumber = importsequencenumber;
            d_promocion.eve_telefono_comercializadora_reservas = eve_telefono_comercializadora_reservas;
            d_promocion.eve_parking_bicicleta = eve_parking_bicicleta;
            d_promocion.utcconversiontimezonecode = utcconversiontimezonecode;
            d_promocion.overriddencreatedon = overriddencreatedon;
            d_promocion.eve_email_comercializadora_reservasiii = eve_email_comercializadora_reservasiii;
            d_promocion.eve_mensajerestriccionfactura = eve_mensajerestriccionfactura;
            d_promocion.eve_email_validacion_seguros_ii = eve_email_validacion_seguros_ii;
            d_promocion.eve_padel = eve_padel;
            d_promocion.eve_electricidad = eve_electricidad;
            d_promocion.eve_email_centro_custodio_3 = eve_email_centro_custodio_3;
            d_promocion.eve_email_comercializadora_reservas = eve_email_comercializadora_reservas;
            d_promocion.eve_email_centro_custodio_2 = eve_email_centro_custodio_2;
            d_promocion.eve_email_centro_custodio_1 = eve_email_centro_custodio_1;
            d_promocion.eve_sala_social = eve_sala_social;
            d_promocion.eve_email_llaves_2 = eve_email_llaves_2;
            d_promocion.eve_email_validacion_contrato_ii = eve_email_validacion_contrato_ii;
            d_promocion.eve_ascensor = eve_ascensor;
            d_promocion.eve_mailbox_lockerbox = eve_mailbox_lockerbox;
            d_promocion.eve_direccion_postal_api = eve_direccion_postal_api;
            d_promocion.eve_email_llaves_1 = eve_email_llaves_1;
            d_promocion.eve_email_gestor_energetico_ii = eve_email_gestor_energetico_ii;
            d_promocion.eve_portfolio = eve_portfolio;
            d_promocion.eve_ivalocalpersonajuridica = eve_ivalocalpersonajuridica;
            d_promocion.eve_plantillarenovacionfirma = eve_plantillarenovacionfirma;
            d_promocion.eve_vigilancia_24h = eve_vigilancia_24h;
            d_promocion.eve_plantillarenovacionviviendanovacion = eve_plantillarenovacionviviendanovacion;
            d_promocion.eve_email_gestor_energetico_i = eve_email_gestor_energetico_i;
            d_promocion.eve_gimnasio = eve_gimnasio;
            d_promocion.timezoneruleversionnumber = timezoneruleversionnumber;
            d_promocion.eve_email_validacion_seguros_i = eve_email_validacion_seguros_i;
            d_promocion.C_owningteam_value = _owningteam_value;
            d_promocion.eve_agua_caliente = eve_agua_caliente;
            d_promocion.eve_portero_fisico = eve_portero_fisico;
            d_promocion.eve_carencia_local_ibi = eve_carencia_local_ibi;
            d_promocion.C_modifiedonbehalfby_value = _modifiedonbehalfby_value;
            d_promocion.eve_plantillaborradorgaraje = eve_plantillaborradorgaraje;
            d_promocion.eve_nombre_gestor_energetico = eve_nombre_gestor_energetico;
            d_promocion.eve_jardin = eve_jardin;
            d_promocion.eve_ivalocal = eve_ivalocal;
            d_promocion.eve_email_gestor_acompanamiento_suministros_i = eve_email_gestor_acompanamiento_suministros_i;
            d_promocion.eve_email_validacion_contrato_iii = eve_email_validacion_contrato_iii;
            d_promocion.eve_zona_infantil = eve_zona_infantil;
            d_promocion.eve_plantillarenovacion = eve_plantillarenovacion;
            d_promocion.C_createdonbehalfby_value = _createdonbehalfby_value;
            d_promocion.eve_telefono_comercializadora = eve_telefono_comercializadora;
            d_promocion.eve_carencia_local_comunidad = eve_carencia_local_comunidad;
            d_promocion.eve_restringir_envio_notificaciones = eve_restringir_envio_notificaciones;
            d_promocion.eve_agua_caliente_placas_solares = eve_agua_caliente_placas_solares;
            d_promocion.eve_acceso_fibra_optica = eve_acceso_fibra_optica;
            d_promocion.eve_reserva_lo = eve_reserva_lo;
            d_promocion.eve_acepta_mascotas = eve_acepta_mascotas;
            d_promocion.eve_equipo_financiero = eve_equipo_financiero;
            d_promocion.eve_zona_juegos = eve_zona_juegos;
            d_promocion.eve_certificado_energetico = eve_certificado_energetico;
            d_promocion.eve_recarga_vehiculos_electricos = eve_recarga_vehiculos_electricos;
            d_promocion.eve_concepto_supuesto = eve_concepto_supuesto;
            d_promocion.eve_climatizacion = eve_climatizacion;
            d_promocion.eve_plantillarenovaciongarajenovacion = eve_plantillarenovaciongarajenovacion;
            d_promocion.eve_calefaccion = eve_calefaccion;
            d_promocion.eve_plazolocal = eve_plazolocal;
            d_promocion.eve_email_validacion_contrato_iv = eve_email_validacion_contrato_iv;
            d_promocion.eve_email_validacion_contrato_i = eve_email_validacion_contrato_i;
            d_promocion.eve_fecharestriccionfactura = eve_fecharestriccionfactura;
            d_promocion.eve_equipo_tecnico = eve_equipo_tecnico;
            d_promocion.eve_nombre_gestor_acompanamiento_suministros = eve_nombre_gestor_acompanamiento_suministros;
            d_promocion.eve_energia_solar = eve_energia_solar;
            d_promocion.eve_email_gestor_energetico_iii = eve_email_gestor_energetico_iii;
            d_promocion.eve_plazotrastero = eve_plazotrastero;
            d_promocion.eve_email_gestor_acompanamiento_suministrosii = eve_email_gestor_acompanamiento_suministrosii;
            d_promocion.eve_piscina = eve_piscina;
            d_promocion.eve_zona_deportiva = eve_zona_deportiva;
            d_promocion.eve_gestor_energetico = eve_gestor_energetico;
            d_promocion.eve_reforma_zzcc = eve_reforma_zzcc;
            d_promocion.eve_email_comercializadora_reservasii = eve_email_comercializadora_reservasii;
            d_promocion.eve_nombre_centro_custodio = eve_nombre_centro_custodio;
            d_promocion.eve_plantillaborradorgarajenovacion = eve_plantillaborradorgarajenovacion;
            d_promocion.eve_email_comercializadora_reservasiv = eve_email_comercializadora_reservasiv;
            d_promocion.eve_plantillarenovaciongaraje = eve_plantillarenovaciongaraje;
            d_promocion.eve_fecha_hora_bloqueo = eve_fecha_hora_bloqueo;
            d_promocion.eve_supuesto = eve_supuesto;
            d_promocion.eve_perimetro_inconcert = eve_perimetro_inconcert;
            d_promocion.eve_caracteristicas_comerciales = eve_caracteristicas_comerciales;
            d_promocion.eve_observaciones_comerciales = eve_observaciones_comerciales;
            d_promocion.eve_promocion_fecha_renovacion = eve_promocion_fecha_renovacion;


            return d_promocion;
        }
    }
}
