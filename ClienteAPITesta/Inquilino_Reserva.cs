﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using ModeloDatos;

namespace ClienteAPITesta
{
    public class Inquilinos_Reserva
    {
        [JsonProperty("@odata.etag")]
        public string OdataEtag { get; set; }
        public string eve_estado_civil { get; set; }
        public string statecode { get; set; }
        public string eve_nacionalidad { get; set; }
        public string eve_asnef { get; set; }
        public string eve_documento_identidad { get; set; }
        public string eve_coeficiente_parcialidad { get; set; }
        public string createdon { get; set; }
        public string eve_name { get; set; }
        public string eve_consentimiento_reserva_terceras { get; set; }
        public string eve_consentimiento_reserva_grupo { get; set; }
        public string eve_movil { get; set; }
        public string _ownerid_value { get; set; }
        public string eve_situacion_laboral { get; set; }
        public string eve_inquilinos_reservaid { get; set; }
        public string modifiedon { get; set; }
        public string emailaddress { get; set; }
        public string versionnumber { get; set; }
        public string eve_tipo_documento_identidad { get; set; }
        public string eve_telefono_movil_sin_prefijo { get; set; }
        public string timezoneruleversionnumber { get; set; }
        public string eve_fecha_nacimiento { get; set; }
        public string _modifiedby_value { get; set; }
        public string statuscode { get; set; }
        public string eve_nombreinquilino { get; set; }
        public string eve_posee_otro_inmueble { get; set; }
        public string _createdby_value { get; set; }
        public string eve_gdpr { get; set; }
        public string _owningbusinessunit_value { get; set; }
        public string eve_extranjero { get; set; }
        public string eve_telefono { get; set; }
        public string _owninguser_value { get; set; }
        public string eve_apellidosinquilino { get; set; }
        public string eve_sexo { get; set; }
        public string eve_horas_jornada_laboral { get; set; }
        public string eve_tipo_jornada_laboral { get; set; }
        public string eve_tipo_contrato_laboral { get; set; }
        public string overriddencreatedon { get; set; }
        public string importsequencenumber { get; set; }
        public string _modifiedonbehalfby_value { get; set; }
        public string eve_direccion { get; set; }
        public string eve_cuentabancariaingreso { get; set; }
        public string eve_poblacion { get; set; }
        public string eve_bic { get; set; }
        public string eve_horas_trabajo_semanales { get; set; }
        public string utcconversiontimezonecode { get; set; }
        public string _createdonbehalfby_value { get; set; }
        public string eve_codigo_postal { get; set; }
        public string _owningteam_value { get; set; }
        public Inquilinos_Reserva() { }
        public Dynamics_Inquilinos_Reservas Convertir2BD() 
        {
            Dynamics_Inquilinos_Reservas d_ir = new Dynamics_Inquilinos_Reservas();
            d_ir.OdataEtag = OdataEtag;
            d_ir.eve_estado_civil = eve_estado_civil;
            d_ir.statecode = statecode;
            d_ir.eve_nacionalidad = eve_nacionalidad;
            d_ir.eve_asnef = eve_asnef;
            d_ir.eve_documento_identidad = eve_documento_identidad;
            d_ir.eve_coeficiente_parcialidad = eve_coeficiente_parcialidad;
            d_ir.createdon = createdon;
            d_ir.eve_name = eve_name;
            d_ir.eve_consentimiento_reserva_terceras = eve_consentimiento_reserva_terceras;
            d_ir.eve_consentimiento_reserva_grupo = eve_consentimiento_reserva_grupo;
            d_ir.eve_movil = eve_movil;
            d_ir.C_ownerid_value = _ownerid_value;
            d_ir.eve_situacion_laboral = eve_situacion_laboral;
            d_ir.eve_inquilinos_reservaid = eve_inquilinos_reservaid;
            d_ir.modifiedon = modifiedon;
            d_ir.emailaddress = emailaddress;
            d_ir.versionnumber = versionnumber;
            d_ir.eve_tipo_documento_identidad = eve_tipo_documento_identidad;
            d_ir.eve_telefono_movil_sin_prefijo = eve_telefono_movil_sin_prefijo;
            d_ir.timezoneruleversionnumber = timezoneruleversionnumber;
            d_ir.eve_fecha_nacimiento = eve_fecha_nacimiento;
            d_ir.C_modifiedby_value = _modifiedby_value;
            d_ir.statuscode = statuscode;
            d_ir.eve_nombreinquilino = eve_nombreinquilino;
            d_ir.eve_posee_otro_inmueble = eve_posee_otro_inmueble;
            d_ir.C_createdby_value = _createdby_value;
            d_ir.eve_gdpr = eve_gdpr;
            d_ir.C_owningbusinessunit_value = _owningbusinessunit_value;
            d_ir.eve_extranjero = eve_extranjero;
            d_ir.eve_telefono = eve_telefono;
            d_ir.C_owninguser_value = _owninguser_value;
            d_ir.eve_apellidosinquilino = eve_apellidosinquilino;
            d_ir.eve_sexo = eve_sexo;
            d_ir.eve_horas_jornada_laboral = eve_horas_jornada_laboral;
            d_ir.eve_tipo_jornada_laboral = eve_tipo_jornada_laboral;
            d_ir.eve_tipo_contrato_laboral = eve_tipo_contrato_laboral;
            d_ir.overriddencreatedon = overriddencreatedon;
            d_ir.importsequencenumber = importsequencenumber;
            d_ir.C_modifiedonbehalfby_value = _modifiedonbehalfby_value;
            d_ir.eve_direccion = eve_direccion;
            d_ir.eve_cuentabancariaingreso = eve_cuentabancariaingreso;
            d_ir.eve_poblacion = eve_poblacion;
            d_ir.eve_bic = eve_bic;
            d_ir.eve_horas_trabajo_semanales = eve_horas_trabajo_semanales;
            d_ir.utcconversiontimezonecode = utcconversiontimezonecode;
            d_ir.C_createdonbehalfby_value = _createdonbehalfby_value;
            d_ir.eve_codigo_postal = eve_codigo_postal;
            d_ir.C_owningteam_value = _owningteam_value;
            return d_ir;
        }
    }
}
