﻿using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Configuration;
using ModeloDatos;
using log4net;
using log4net.Config;
using System.Linq;

namespace ClienteAPITesta
{
    class Program
    {

        public static string _url = ConfigurationManager.AppSettings.Get("URLCRM");
        public static string _userName = ConfigurationManager.AppSettings.Get("USERCRM");
        public static string _password = ConfigurationManager.AppSettings.Get("PASSCRM");
        public static string _clientId = ConfigurationManager.AppSettings.Get("CLIENTCRM");
        public static string _apiVersion = ConfigurationManager.AppSettings.Get("VAPICRM");

        public static readonly ILog logger = LogManager.GetLogger(typeof(Program));

        static void Main(string[] args)
        {
            XmlConfigurator.Configure();
            logger.Info("INICIO");
            
            Program p = new Program();
            try
            {
                if (args.Length > 0)
                {
                    foreach (var item in args)
                    {
                        logger.Info($"Con parámetro: {item}");
                        p.ActualizarAlguno(p, item);
                    }
                }
                else
                {
                    p.ActualizarTodo(p, true);
                }
                logger.Info("FINAL");
            }
            catch (Exception e)
            {
                Email emailenv = new Email();
                emailenv.InicializarGestorCorreo();
                string mensaje = $"El proceso finaliza con error: {e.Message} \n {e.InnerException} \n {e.StackTrace}";
                logger.Error(mensaje);
                string destinatarios = Email._destinatarios;
                string asunto = Email._asunto;
                emailenv.EnviarCorreo(destinatarios, asunto, mensaje);
            }
        }
        private void ActualizarTodo(Program p, bool soloinsertarContactos)
        {
            logger.Info("ActualizarTodo. Los contactos solo se insertan: " + soloinsertarContactos);
            IEnumerable<string> datos;
            string entidad = "eve_campanya_comercials";
            datos = p.ObtenerDatosEntidad(entidad);
            p.UpsertCampanya_comercial(datos);

            entidad = "eve_sociedads";
            datos = p.ObtenerDatosEntidad(entidad);
            p.UpsertSociedades(datos);

            entidad = "eve_contratos";
            datos = p.ObtenerDatosEntidad(entidad);
            p.UpsertContratos(datos);

            entidad = "eve_reservas";
            datos = p.ObtenerDatosEntidad(entidad);
            p.UpsertReservas(datos);

            entidad = "eve_inmuebleanejoscomercials";
            datos = p.ObtenerDatosEntidad(entidad);
            p.UpsertInmuebleAnejoComercial(datos);

            entidad = "eve_participacioncontratos";
            datos = p.ObtenerDatosEntidad(entidad);
            p.UpsertParticipacionContratos(datos);

            entidad = "eve_renovacions";
            datos = p.ObtenerDatosEntidad(entidad);
            p.UpsertRenovaciones(datos);

            entidad = "eve_inquilinos_reservas";
            datos = p.ObtenerDatosEntidad(entidad);
            p.UpsertInquilino_Reserva(datos);

            entidad = "eve_promocions";
            datos = p.ObtenerDatosEntidad(entidad);
            p.UpsertPromociones(datos);

            entidad = "accounts";
            datos = p.ObtenerDatosEntidad(entidad);
            if (soloinsertarContactos)
            {
                p.InsertContacts(datos);
            }
            else
            {
                p.UpsertContacts(datos);
            }

            entidad = "eve_inmuebles";
            datos = p.ObtenerDatosEntidad(entidad);
            p.UpsertProducts(datos);
        }
        private void ActualizarAlguno(Program p, string entidad)
        {
            IEnumerable<string> datos;
            switch (entidad)
            {
                case "eve_campanya_comercials":
                case "campanya_comercial":
                    datos = p.ObtenerDatosEntidad("eve_campanya_comercials");
                    p.UpsertCampanya_comercial(datos);
                    break;
                case "eve_sociedads":
                case "sociedades":
                    datos = p.ObtenerDatosEntidad("eve_sociedads");
                    p.UpsertSociedades(datos);
                    break;
                case "eve_contratos":
                case "contratos":
                    datos = p.ObtenerDatosEntidad("eve_contratos");
                    p.UpsertContratos(datos);
                    break;
                case "eve_reservas":
                case "reservas":
                    datos = p.ObtenerDatosEntidad("eve_reservas");
                    p.UpsertReservas(datos);
                    break;
                case "eve_inmuebleanejoscomercials":
                case "inmuebleanejoscomerciales":
                    datos = p.ObtenerDatosEntidad("eve_inmuebleanejoscomercials");
                    p.UpsertInmuebleAnejoComercial(datos);
                    break;
                case "eve_participacioncontratos":
                case "participacioncontratos":
                    datos = p.ObtenerDatosEntidad("eve_participacioncontratos");
                    p.UpsertParticipacionContratos(datos);
                    break;
                case "eve_renovacions":
                case "renovaciones":
                    datos = p.ObtenerDatosEntidad("eve_renovacions");
                    p.UpsertRenovaciones(datos);
                    break;
                case "eve_inquilinos_reservas":
                case "inquilinos_reservas":
                    datos = p.ObtenerDatosEntidad("eve_inquilinos_reservas");
                    p.UpsertInquilino_Reserva(datos);
                    break;
                case "eve_promocions":
                case "promociones":
                    datos = p.ObtenerDatosEntidad("eve_promocions");
                    p.UpsertPromociones(datos);
                    break;
                case "actualizar_contactos":
                    datos = p.ObtenerDatosEntidad("accounts");
                    p.UpsertContacts(datos);
                    break;
                case "insertar_contactos":
                    datos = p.ObtenerDatosEntidad("accounts");
                    p.InsertContacts(datos);
                    break;
                case "eve_inmuebles":
                case "productos":
                    datos = p.ObtenerDatosEntidad("eve_inmuebles");
                    p.UpsertProducts(datos);
                    break;
                case "todo":
                    ActualizarTodo(p, false);
                    break;
                default:
                    logger.Info("No es un parámetro válido");
                    break;
            }
        }
        #region LecturaEscritura
        private void UpsertContacts(IEnumerable<string> datos)
        {
            int inserciones = 0;
            int actualizados = 0;
            int total = 0;
            Inquilino inquilino = null;
            Contact contact = null;
            if (null != datos)
            {
                var lotes = Utils.Batch<string>(datos, 100);
                int num_lotes = lotes.Count<IEnumerable<string>>();
                logger.Info("UpsertContacts. Número de lotes: " + num_lotes);  
                int contador_lotes = 0;
                foreach (var lote in lotes)
                {
                    foreach (var item in lote)
                    {
                        using (InconcertOnline2 db = new InconcertOnline2())
                        {
                            inquilino = JsonConvert.DeserializeObject<Inquilino>(item, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                            Contact contactDB = db.Contacts.Find("Testa", inquilino.name);
                            if (null != contactDB)
                            {
                                contact = UpdateContact(inquilino);
                                JToken jsonBD = JsonConvert.DeserializeObject<JToken>(contactDB.CustomData);
                                logger.Info("Datos BD: " + contactDB.CustomData);
                                JToken jsonCRM = JsonConvert.DeserializeObject<JToken>(contact.CustomData);
                                logger.Info("Datos CRM: " + contact.CustomData);
                                JObject customdatafinal = Utils.MergeJsons(jsonBD, jsonCRM);
                                string customdata = JsonConvert.SerializeObject(customdatafinal);
                                logger.Info("Datos después de la fusión: " + customdata);
                                contact.CustomData = customdata;
                                if(LlamadaSPSaveContact(db, contact))
                                {
                                    ActualizarContactoEnMAS(contact.ID);
                                    actualizados++;
                                }
                            }
                            else
                            {
                                contact = CreateContact(inquilino);
                                if (LlamadaSPSaveContact(db, contact))
                                {
                                    if (LlamadaSPSaveContactEvent(db, contact))
                                    {
                                        inserciones++;
                                    }
                                }
                            }

                        }
                        total++;
                    }
                    contador_lotes++;
                    logger.Info("Lote número: " + contador_lotes);
                }
                logger.Info($"Contactos tratados {total}, actualizados {actualizados}, insertados {inserciones}");
            }
        }
        private void InsertContacts(IEnumerable<string> datos)
        {
            int inserciones = 0;
            int total = 0;
            Inquilino inquilino = null;
            Contact contact = null;
            if (null != datos)
            {
                var lotes = Utils.Batch(datos, 100);
                int num_lotes = lotes.Count();
                logger.Info("InsertContacts. Número de lotes: " + num_lotes);
                int contador_lotes = 0;
                foreach (var lote in lotes)
                {
                    foreach (var item in lote)
                    {
                        using (InconcertOnline2 db = new InconcertOnline2())
                        {
                            inquilino = JsonConvert.DeserializeObject<Inquilino>(item, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                            Contact contactDB = db.Contacts.Find("Testa", inquilino.name);
                            if (null == contactDB)
                            {
                                contact = CreateContact(inquilino);
                                if(LlamadaSPSaveContact(db, contact))
                                {
                                    if(LlamadaSPSaveContactEvent(db, contact))
                                    {
                                        inserciones++;
                                    }
                                }
                            }
                        }
                        total++;
                    }
                    contador_lotes++;
                    logger.Info("Lote número: " + contador_lotes);
                }
                logger.Info($"Contactos revisados: {total}, insertados: {inserciones}");
            }
        }
        private bool LlamadaSPSaveContactEvent(InconcertOnline2 db, Contact contact)
        {
            bool respuesta = false;
            Guid guid = Guid.NewGuid();
            try
            {
                db.SaveContactEvent(contact.Instance, contact.ID, "created", guid.ToString(), "Default", "process", string.Empty, string.Empty, string.Empty, string.Empty,
                                        string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, DateTime.Now, false);
                respuesta = true;
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                logger.Error(e.InnerException);
                logger.Error(e.StackTrace);
            }
            return respuesta;
        }
        private bool LlamadaSPSaveContact(InconcertOnline2 db, Contact contact)
        {
            bool respuesta = false;
            try
            {
                db.SaveContact(contact.Instance, contact.ID, contact.Email, contact.Title, contact.Firstname, contact.Lastname, contact.Score, contact.Stage, contact.Language,
                contact.Owner, contact.Company, contact.Position, contact.Phone, contact.Mobile, contact.Fax, contact.Website, contact.Address1, contact.Address2, contact.Country,
                contact.State, contact.City, contact.Zip, contact.Facebook, contact.Twitter, contact.Skype, contact.GooglePlus, contact.Linkedin, contact.Instagram, contact.Comments,
                contact.ClientComments, contact.CustomData, contact.Membership, contact.Blacklist, contact.ReferredDate, contact.ReferredByContactId, contact.ReferredAtCampaignId,
                contact.ReferredAtInteractionId, contact.LastTrackingId, contact.LastFingerprint, contact.HadDuplicateDetected, contact.FirstClickEventDate, contact.FirstClickEventId,
                contact.FirstClickEventCampaign, contact.LastClickEventDate, contact.LastClickEventId, contact.LastClickEventCampaign, contact.FirstVisitEventDate, contact.FirstVisitEventId,
                contact.FirstVisitEventCampaign, contact.LastVisitEventDate, contact.LastVisitEventId, contact.LastVisitEventCampaign, contact.FirstConversionEventDate, contact.FirstConversionEventId,
                contact.FirstConversionEventCampaign, contact.FirstConversionEventUrl, contact.FirstConversionEventContentType, contact.FirstConversionEventContentId,
                contact.FirstConversionEventVariantId, contact.FirstConversionEventPopupId, contact.FirstConversionEventFormId, contact.LastConversionEventDate, contact.LastConversionEventId,
                contact.LastConversionEventCampaign, contact.LastConversionEventUrl, contact.LastConversionEventContentType, contact.LastConversionEventContentId, contact.LastConversionEventVariantId,
                contact.LastConversionEventPopupId, contact.LastConversionEventFormId, contact.FirstCallEventDate, contact.FirstCallEventId, contact.FirstCallEventCampaign,
                contact.FirstCallEventCcCampaign, contact.FirstCallEventAgent, contact.FirstCallEventDisposition, contact.FirstCallEventResult, contact.FirstCallEventOutOfHour,
                contact.LastCallEventDate, contact.LastCallEventId, contact.LastCallEventCampaign, contact.LastCallEventCcCampaign, contact.LastCallEventAgent, contact.LastCallEventDisposition,
                contact.LastCallEventResult, contact.LastCallEventOutOfHour, contact.FirstManagedCallEventDate, contact.FirstManagedCallEventId, contact.FirstManagedCallEventCampaign,
                contact.FirstManagedCallEventCcCampaign, contact.FirstManagedCallEventAgent, contact.FirstManagedCallEventManagementResult, contact.LastManagedCallEventDate,
                contact.LastManagedCallEventId, contact.LastManagedCallEventCampaign, contact.LastManagedCallEventCcCampaign, contact.LastManagedCallEventAgent,
                contact.LastManagedCallEventManagementResult, contact.FirstGoalEventDate, contact.FirstGoalEventId, contact.FirstGoalEventCampaign, contact.FirstGoalEventCcCampaign,
                contact.FirstGoalEventAgent, contact.FirstGoalEventManagementResult, contact.LastGoalEventDate, contact.LastGoalEventId, contact.LastGoalEventCampaign,
                contact.LastGoalEventCcCampaign, contact.LastGoalEventAgent, contact.LastGoalEventManagementResult, contact.LastEventDate, contact.LastEventType, contact.LastEventId,
                contact.LastEventCampaign, contact.CreatedDate, contact.CreatedOutOfHour, contact.CreatedByCampaignId, contact.CreatedByCampaignCategory, contact.CreatedByCampaignClient,
                contact.CreatedByUserId, contact.CreatedByEventType, contact.CreatedByEventId, contact.CreatedBySource, contact.CreatedBySourceId, contact.CreatedByClickSource,
                contact.CreatedByClickId, contact.CreatedByClickCampaign, contact.CreatedByClickAdGroup, contact.CreatedByClickKeyword, contact.CreatedByClickAdPosition, contact.Blocked,
                false);
                respuesta = true;
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                logger.Error(e.InnerException);
                logger.Error(e.StackTrace);
            }
            return respuesta;
        }
        private Contact CreateContact(Inquilino inquilino)
        {
            CustomDataContacto customData = new CustomDataContacto(
                inquilino.accountid, 
                inquilino.eve_nif_nie, 
                inquilino.eve_numero, 
                inquilino.eve_portal, 
                inquilino.eve_escalera, 
                inquilino.eve_piso, 
                inquilino.eve_puerta, 
                inquilino.eve_codigopostal, 
                inquilino.eve_consentimiento_grupo, 
                inquilino.eve_consentimiento_terceras, 
                inquilino.eve_origen_consentimiento, 
                inquilino.eve_fecha_consentimiento, 
                inquilino.eve_calificativo, 
                inquilino.eve_descripcioncalificativo, 
                inquilino.eve_deudatotalinquilino, 
                inquilino.eve_deudatotalcontrato, 
                inquilino.eve_deudatotalplandepago, 
                inquilino.eve_deudatotalanticipo, 
                inquilino.eve_importe_total_pendiente_vencimiento_plan, 
                inquilino.eve_descripcionsociedad, 
                inquilino.name, 
                inquilino.eve_nombreapellidos,
                inquilino.eve_envio_carta_deuda,
                inquilino.eve_estado_anterior_deuda,
                inquilino.eve_estado_deuda,
                inquilino.eve_fecha_cita,
                inquilino.eve_fecha_cita_adicional,
                inquilino.eve_fecha_promesa_pago,
                inquilino.eve_fecha_ultima_cita,
                inquilino.eve_fecha_ultima_gestion,
                inquilino.eve_gestion_de_impagos,
                inquilino.eve_gestion_de_impagos_adicional,
                inquilino.eve_gestora_deuda,
                inquilino.eve_multiplessociedades,
                inquilino.eve_observacion_carta,
                inquilino.eve_observacion_gestion,
                inquilino.eve_observaciones_deuda,
                inquilino.modifiedon);
            Contact contact = new Contact();
            contact.Instance = "Testa";
            contact.ID = inquilino.name;
            contact.Email = inquilino.eve_email; //se actualiza
            contact.Title = null;
            contact.Firstname = inquilino.eve_nombre; //se actualiza
            contact.Lastname = inquilino.eve_apellidos; //se actualiza
            contact.Score = null;
            contact.Stage = "lead";
            contact.Language = null;
            contact.Owner = null;
            contact.Company = null;
            contact.Position = null;
            contact.Phone = inquilino.eve_telefono1; //se actualiza
            contact.PhoneLocal = null;
            contact.PhoneInternational = null;
            contact.Mobile = inquilino.eve_telefono2; //se actualiza
            contact.Fax = inquilino.eve_telefono3; //se actualiza
            contact.Website = null;
            contact.Address1 = inquilino.eve_tipovia; //se actualiza
            contact.Address2 = inquilino.eve_calle; //se actualiza
            contact.Country = null;
            contact.State = null;
            contact.City = inquilino.eve_poblacion; //se actualiza
            contact.Zip = null;
            contact.Facebook = null;
            contact.Twitter = null;
            contact.Skype = null;
            contact.GooglePlus = null;
            contact.Linkedin = null;
            contact.Instagram = null;
            contact.Comments = null;
            contact.ClientComments = null;
            contact.CustomData = customData.SerializarCustomDataContacto(); // se actualiza
            contact.Membership = "{}";
            contact.Blacklist = false;
            contact.ReferredDate = null;
            contact.ReferredByContactId = null;
            contact.ReferredAtCampaignId = null;
            contact.ReferredAtInteractionId = null;
            contact.LastTrackingId = null;
            contact.LastFingerprint = null;
            contact.HadDuplicateDetected = false;
            contact.FirstClickEventDate = null;
            contact.FirstClickEventId = null;
            contact.FirstClickEventCampaign = null;
            contact.LastClickEventDate = null;
            contact.LastClickEventId = null;
            contact.LastClickEventCampaign = null;
            contact.FirstVisitEventDate = null;
            contact.FirstVisitEventId = null;
            contact.FirstVisitEventCampaign = null;
            contact.LastVisitEventDate = null;
            contact.LastVisitEventId = null;
            contact.LastVisitEventCampaign = null;
            contact.FirstConversionEventDate = null;
            contact.FirstConversionEventId = null;
            contact.FirstConversionEventCampaign = null;
            contact.FirstConversionEventUrl = null;
            contact.FirstConversionEventContentType = null;
            contact.FirstConversionEventContentId = null;
            contact.FirstConversionEventVariantId = null;
            contact.FirstConversionEventPopupId = null;
            contact.FirstConversionEventFormId = null;
            contact.LastConversionEventDate = null;
            contact.LastConversionEventId = null;
            contact.LastConversionEventCampaign = null;
            contact.LastConversionEventUrl = null;
            contact.LastConversionEventContentType = null;
            contact.LastConversionEventContentId = null;
            contact.LastConversionEventVariantId = null;
            contact.LastConversionEventPopupId = null;
            contact.LastConversionEventFormId = null;
            contact.FirstCallEventDate = null;
            contact.FirstCallEventId = null;
            contact.FirstCallEventCampaign = null;
            contact.FirstCallEventCcCampaign = null;
            contact.FirstCallEventAgent = null;
            contact.FirstCallEventDisposition = null;
            contact.FirstCallEventResult = null;
            contact.FirstCallEventOutOfHour = null;
            contact.LastCallEventDate = null;
            contact.LastCallEventId = null;
            contact.LastCallEventCampaign = null;
            contact.LastCallEventCcCampaign = null;
            contact.LastCallEventAgent = null;
            contact.LastCallEventDisposition = null;
            contact.LastCallEventResult = null;
            contact.LastCallEventOutOfHour = null;
            contact.FirstManagedCallEventDate = null;
            contact.FirstManagedCallEventId = null;
            contact.FirstManagedCallEventCampaign = null;
            contact.FirstManagedCallEventCcCampaign = null;
            contact.FirstManagedCallEventAgent = null;
            contact.FirstManagedCallEventManagementResult = null;
            contact.LastManagedCallEventDate = null;
            contact.LastManagedCallEventId = null;
            contact.LastManagedCallEventCampaign = null;
            contact.LastManagedCallEventCcCampaign = null;
            contact.LastManagedCallEventAgent = null;
            contact.LastManagedCallEventManagementResult = null;
            contact.FirstGoalEventDate = null;
            contact.FirstGoalEventId = null;
            contact.FirstGoalEventCampaign = null;
            contact.FirstGoalEventCcCampaign = null;
            contact.FirstGoalEventAgent = null;
            contact.FirstGoalEventManagementResult = null;
            contact.LastGoalEventDate = null;
            contact.LastGoalEventId = null;
            contact.LastGoalEventCampaign = null;
            contact.LastGoalEventCcCampaign = null;
            contact.LastGoalEventAgent = null;
            contact.LastGoalEventManagementResult = null;
            contact.LastEventDate = null;
            contact.LastEventType = null;
            contact.LastEventId = null;
            contact.LastEventCampaign = null;
            contact.CreatedDate = null;
            contact.CreatedOutOfHour = null;
            contact.CreatedByCampaignId = null;
            contact.CreatedByCampaignCategory = null;
            contact.CreatedByCampaignClient = "Testa";
            contact.CreatedByUserId = null;
            contact.CreatedByEventType = null;
            contact.CreatedByEventId = null;
            contact.CreatedBySource = null;
            contact.CreatedBySourceId = null;
            contact.CreatedByClickSource = null;
            contact.CreatedByClickId = null;
            contact.CreatedByClickCampaign = null;
            contact.CreatedByClickAdGroup = null;
            contact.CreatedByClickKeyword = null;
            contact.CreatedByClickAdPosition = null;
            contact.Blocked = false;
            //log.Debug($"contact: {contact.Instance}, {contact.ID}, {contact.Firstname}, {contact.Lastname}, {contact.Phone}, {contact.PhoneLocal}, {contact.PhoneInternational}, {contact.Email}, {contact.Address1}, {contact.Address2}, {contact.City}, {contact.CustomData}");
            return contact;
        }
        private Contact UpdateContact(Inquilino inquilino)
        {
            CustomDataContacto customData = new CustomDataContacto(
                inquilino.accountid, 
                inquilino.eve_nif_nie, 
                inquilino.eve_numero, 
                inquilino.eve_portal, 
                inquilino.eve_escalera, 
                inquilino.eve_piso, 
                inquilino.eve_puerta, 
                inquilino.eve_codigopostal, 
                inquilino.eve_consentimiento_grupo, 
                inquilino.eve_consentimiento_terceras, 
                inquilino.eve_origen_consentimiento, 
                inquilino.eve_fecha_consentimiento, 
                inquilino.eve_calificativo, 
                inquilino.eve_descripcioncalificativo, 
                inquilino.eve_deudatotalinquilino, 
                inquilino.eve_deudatotalcontrato, 
                inquilino.eve_deudatotalplandepago, 
                inquilino.eve_deudatotalanticipo, 
                inquilino.eve_importe_total_pendiente_vencimiento_plan, 
                inquilino.eve_descripcionsociedad, 
                inquilino.name, 
                inquilino.eve_nombreapellidos,
                inquilino.eve_envio_carta_deuda,
                inquilino.eve_estado_anterior_deuda,
                inquilino.eve_estado_deuda,
                inquilino.eve_fecha_cita,
                inquilino.eve_fecha_cita_adicional,
                inquilino.eve_fecha_promesa_pago,
                inquilino.eve_fecha_ultima_cita,
                inquilino.eve_fecha_ultima_gestion,
                inquilino.eve_gestion_de_impagos,
                inquilino.eve_gestion_de_impagos_adicional,
                inquilino.eve_gestora_deuda,
                inquilino.eve_multiplessociedades,
                inquilino.eve_observacion_carta,
                inquilino.eve_observacion_gestion,
                inquilino.eve_observaciones_deuda,
                inquilino.modifiedon);
            Contact contact = new Contact();
            contact.Instance = "Testa";
            contact.ID = inquilino.name;
            contact.Email = inquilino.eve_email; //se actualiza
            contact.Title = null;
            contact.Firstname = inquilino.eve_nombre; //se actualiza
            contact.Lastname = inquilino.eve_apellidos; //se actualiza
            contact.Score = null;
            contact.Stage = null;
            contact.Language = null;
            contact.Owner = null;
            contact.Company = null;
            contact.Position = null;
            contact.Phone = inquilino.eve_telefono1; //se actualiza
            contact.PhoneLocal = null;
            contact.PhoneInternational = null;
            contact.Mobile = inquilino.eve_telefono2; //se actualiza
            contact.Fax = inquilino.eve_telefono3; //se actualiza
            contact.Website = null;
            contact.Address1 = inquilino.eve_tipovia; //se actualiza
            contact.Address2 = inquilino.eve_calle; //se actualiza
            contact.Country = null;
            contact.State = null;
            contact.City = inquilino.eve_poblacion; //se actualiza
            contact.Zip = null;
            contact.Facebook = null;
            contact.Twitter = null;
            contact.Skype = null;
            contact.GooglePlus = null;
            contact.Linkedin = null;
            contact.Instagram = null;
            contact.Comments = null;
            contact.ClientComments = null;
            contact.CustomData = customData.SerializarCustomDataContacto(); // se actualiza
            contact.Membership = null;
            contact.Blacklist = null;
            contact.ReferredDate = null;
            contact.ReferredByContactId = null;
            contact.ReferredAtCampaignId = null;
            contact.ReferredAtInteractionId = null;
            contact.LastTrackingId = null;
            contact.LastFingerprint = null;
            contact.HadDuplicateDetected = null;
            contact.FirstClickEventDate = null;
            contact.FirstClickEventId = null;
            contact.FirstClickEventCampaign = null;
            contact.LastClickEventDate = null;
            contact.LastClickEventId = null;
            contact.LastClickEventCampaign = null;
            contact.FirstVisitEventDate = null;
            contact.FirstVisitEventId = null;
            contact.FirstVisitEventCampaign = null;
            contact.LastVisitEventDate = null;
            contact.LastVisitEventId = null;
            contact.LastVisitEventCampaign = null;
            contact.FirstConversionEventDate = null;
            contact.FirstConversionEventId = null;
            contact.FirstConversionEventCampaign = null;
            contact.FirstConversionEventUrl = null;
            contact.FirstConversionEventContentType = null;
            contact.FirstConversionEventContentId = null;
            contact.FirstConversionEventVariantId = null;
            contact.FirstConversionEventPopupId = null;
            contact.FirstConversionEventFormId = null;
            contact.LastConversionEventDate = null;
            contact.LastConversionEventId = null;
            contact.LastConversionEventCampaign = null;
            contact.LastConversionEventUrl = null;
            contact.LastConversionEventContentType = null;
            contact.LastConversionEventContentId = null;
            contact.LastConversionEventVariantId = null;
            contact.LastConversionEventPopupId = null;
            contact.LastConversionEventFormId = null;
            contact.FirstCallEventDate = null;
            contact.FirstCallEventId = null;
            contact.FirstCallEventCampaign = null;
            contact.FirstCallEventCcCampaign = null;
            contact.FirstCallEventAgent = null;
            contact.FirstCallEventDisposition = null;
            contact.FirstCallEventResult = null;
            contact.FirstCallEventOutOfHour = null;
            contact.LastCallEventDate = null;
            contact.LastCallEventId = null;
            contact.LastCallEventCampaign = null;
            contact.LastCallEventCcCampaign = null;
            contact.LastCallEventAgent = null;
            contact.LastCallEventDisposition = null;
            contact.LastCallEventResult = null;
            contact.LastCallEventOutOfHour = null;
            contact.FirstManagedCallEventDate = null;
            contact.FirstManagedCallEventId = null;
            contact.FirstManagedCallEventCampaign = null;
            contact.FirstManagedCallEventCcCampaign = null;
            contact.FirstManagedCallEventAgent = null;
            contact.FirstManagedCallEventManagementResult = null;
            contact.LastManagedCallEventDate = null;
            contact.LastManagedCallEventId = null;
            contact.LastManagedCallEventCampaign = null;
            contact.LastManagedCallEventCcCampaign = null;
            contact.LastManagedCallEventAgent = null;
            contact.LastManagedCallEventManagementResult = null;
            contact.FirstGoalEventDate = null;
            contact.FirstGoalEventId = null;
            contact.FirstGoalEventCampaign = null;
            contact.FirstGoalEventCcCampaign = null;
            contact.FirstGoalEventAgent = null;
            contact.FirstGoalEventManagementResult = null;
            contact.LastGoalEventDate = null;
            contact.LastGoalEventId = null;
            contact.LastGoalEventCampaign = null;
            contact.LastGoalEventCcCampaign = null;
            contact.LastGoalEventAgent = null;
            contact.LastGoalEventManagementResult = null;
            contact.LastEventDate = null;
            contact.LastEventType = null;
            contact.LastEventId = null;
            contact.LastEventCampaign = null;
            contact.CreatedDate = null;
            contact.CreatedOutOfHour = null;
            contact.CreatedByCampaignId = null;
            contact.CreatedByCampaignCategory = null;
            contact.CreatedByCampaignClient = null;
            contact.CreatedByUserId = null;
            contact.CreatedByEventType = null;
            contact.CreatedByEventId = null;
            contact.CreatedBySource = null;
            contact.CreatedBySourceId = null;
            contact.CreatedByClickSource = null;
            contact.CreatedByClickId = null;
            contact.CreatedByClickCampaign = null;
            contact.CreatedByClickAdGroup = null;
            contact.CreatedByClickKeyword = null;
            contact.CreatedByClickAdPosition = null;
            contact.Blocked = null;
            //log.Debug($"contact: {contact.Instance}, {contact.ID}, {contact.Firstname}, {contact.Lastname}, {contact.Phone}, {contact.PhoneLocal}, {contact.PhoneInternational}, {contact.Email}, {contact.Address1}, {contact.Address2}, {contact.City}, {contact.CustomData}");
            return contact;
        }
        private CustomDataContacto UpdateCustomDataContact (Inquilino inquilino)
        {
            CustomDataContacto customData = new CustomDataContacto(
                inquilino.accountid,
                inquilino.eve_nif_nie,
                inquilino.eve_numero,
                inquilino.eve_portal,
                inquilino.eve_escalera,
                inquilino.eve_piso,
                inquilino.eve_puerta,
                inquilino.eve_codigopostal,
                inquilino.eve_consentimiento_grupo,
                inquilino.eve_consentimiento_terceras,
                inquilino.eve_origen_consentimiento,
                inquilino.eve_fecha_consentimiento,
                inquilino.eve_calificativo,
                inquilino.eve_descripcioncalificativo,
                inquilino.eve_deudatotalinquilino,
                inquilino.eve_deudatotalcontrato,
                inquilino.eve_deudatotalplandepago,
                inquilino.eve_deudatotalanticipo,
                inquilino.eve_importe_total_pendiente_vencimiento_plan,
                inquilino.eve_descripcionsociedad,
                inquilino.name,
                inquilino.eve_nombreapellidos,
                inquilino.eve_envio_carta_deuda,
                inquilino.eve_estado_anterior_deuda,
                inquilino.eve_estado_deuda,
                inquilino.eve_fecha_cita,
                inquilino.eve_fecha_cita_adicional,
                inquilino.eve_fecha_promesa_pago,
                inquilino.eve_fecha_ultima_cita,
                inquilino.eve_fecha_ultima_gestion,
                inquilino.eve_gestion_de_impagos,
                inquilino.eve_gestion_de_impagos_adicional,
                inquilino.eve_gestora_deuda,
                inquilino.eve_multiplessociedades,
                inquilino.eve_observacion_carta,
                inquilino.eve_observacion_gestion,
                inquilino.eve_observaciones_deuda,
                inquilino.modifiedon
                );
            return customData;
        }
        private void UpsertCampanya_comercial(IEnumerable<string> datos)
        {
            Campanya_Comercial cc = null;
            Dynamics_Campanya_Comercial d_cc = null;
            int total = 0;

            if (null != datos)
            {
                var lotes = Utils.Batch<string>(datos, 100);
                int num_lotes = lotes.Count<IEnumerable<string>>();
                logger.Info("UpsertCampanya_comercial. Número de lotes: " + num_lotes);
                int contador_lotes = 0;

                foreach (var lote in lotes)
                {
                    using (var db = new InconcertOnline2())
                    {
                        foreach (var item in lote)
                        {
                            cc = JsonConvert.DeserializeObject<Campanya_Comercial>(item, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                            d_cc = cc.Convertir2BD();
                            if (LlamadaSPSaveCampanyaComercial(db, d_cc))
                            {
                                total++;
                            }
                        }
                    }
                    contador_lotes++;
                    logger.Info($"Lote {contador_lotes} Registros procesados {total}");
                }
            }
            logger.Info($"CampanyasComerciales procesadas del CRM {total} de {datos.Count()}");
        }
        private bool LlamadaSPSaveCampanyaComercial(InconcertOnline2 db, Dynamics_Campanya_Comercial d_cc)
        {
            bool respuesta = false;
            try
            {
                db.Dynamics_SaveCanpanyaComercial(d_cc.OdataEtag, d_cc.eve_fecha_contacto, d_cc.eve_reasignacion, d_cc.C_eve_inquilino_principal_id_value, d_cc.createdon, d_cc.eve_name, 
                    d_cc.importsequencenumber, d_cc.statecode, d_cc.eve_estado_recuperacion_adicional, d_cc.eve_fecha_cambio_motivo_1, d_cc.eve_fecha_cambio_motivo_2, d_cc.eve_tipo_recuperacion,
                    d_cc.C_ownerid_value, d_cc.modifiedon, d_cc.C_eve_codigo_contrato_id_value, d_cc.eve_comentarios_motivo_finalizacion, d_cc.versionnumber, d_cc.eve_motivo_inquilino, 
                    d_cc.timezoneruleversionnumber, d_cc.eve_comentarios_estado_recuperacion, d_cc.C_modifiedby_value, d_cc.eve_tipo_envio_renovacion, d_cc.statuscode, d_cc.eve_estado_recuperacion,
                    d_cc.eve_fecha_envio_renovacion_historico, d_cc.eve_motivo_finalizacion_contrato_anterior_2, d_cc.eve_campanya_comercialid, d_cc.eve_motivo_finalizacion_contrato_anterior_1, 
                    d_cc.C_createdby_value, d_cc.C_owningbusinessunit_value, d_cc.eve_motivo_finalizacion_contrato_adicional, d_cc.C_owninguser_value, d_cc.eve_equipo_encargado_recuperacion, 
                    d_cc.eve_motivo_no_recuperacion_1, d_cc.eve_cierre_automatico_recuperacion, d_cc.eve_otros_motivos_i, d_cc.eve_email_recuperacion_inquilino_2, d_cc.eve_fecha_fin_contrato, 
                    d_cc.eve_cierre_expediente, d_cc.eve_fecha_finalizacion_recuperacion, d_cc.eve_otros_motivos_ii, d_cc.eve_inquilino_recuperacion_3, d_cc.overriddencreatedon, 
                    d_cc.eve_inquilino_recuperacion_1, d_cc.eve_fecha_cierre_automatico, d_cc.C_modifiedonbehalfby_value, d_cc.eve_email_recuperacion_inquilino_3, d_cc.eve_codigo_postal_destino,
                    d_cc.eve_poblacion_destino, d_cc.eve_email_recuperacion_inquilino_1, d_cc.eve_clasificacion_cambio_vivienda, d_cc.utcconversiontimezonecode, d_cc.eve_motivo_no_recuperacion_2, 
                    d_cc.C_createdonbehalfby_value, d_cc.eve_telefono_recuperacion_1, d_cc.eve_telefono_recuperacion_3, d_cc.eve_telefono_recuperacion_2, d_cc.eve_inquilino_recuperacion_2, 
                    d_cc.C_owningteam_value);
                respuesta = true;
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                logger.Error(e.InnerException);
                logger.Error(e.StackTrace);
            }
            return respuesta;
        }
        #endregion
        #region Lectura
        private void UpsertProducts(IEnumerable<string> datos)
        {
            int inserciones = 0;
            int actualizados = 0;
            int total = 0;
            Inmueble inmueble = null;
            Product product = null;
            if (null != datos)
            {
                var lotes = Utils.Batch<string>(datos, 100);
                int num_lotes = lotes.Count<IEnumerable<string>>();
                logger.Info("UpsertProducts. Número de lotes: " + num_lotes);
                int contador_lotes = 0;
                foreach (var lote in lotes)
                {
                    using (var db = new InconcertOnline2())
                    {
                        foreach (var item in lote)
                        {
                            inmueble = JsonConvert.DeserializeObject<Inmueble>(item, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                            Product productDB = db.Products.Find("Testa", inmueble.eve_inmuebleid);
                            if (null != productDB)
                            {
                                product = UpdateProduct(inmueble);
                                productDB.Description = product.Description;
                                productDB.Price = product.Price;
                                productDB.CustomData = product.CustomData;
                                productDB.LastModifiedDate = product.LastModifiedDate;
                                productDB.Category = product.Category;
                                productDB.Group = product.Group;
                                if (LlamadaSPSaveProduct(db, productDB))
                                {
                                    actualizados++;
                                }
                            }
                            else
                            {
                                product = CreateProduct(inmueble);
                                if (LlamadaSPSaveProduct(db, product))
                                {
                                    inserciones++;
                                }
                            }
                            total++;
                        }
                    }
                    contador_lotes++;
                    logger.Info($"Lote {contador_lotes} Registros procesados {total}");
                }
                
                logger.Info($"Inmuebles obtenidos del CRM {total}, insertados {inserciones}, actualizados {actualizados}");
            }
        }
        private Product CreateProduct(Inmueble inmueble)
        {
            CustomDataProducto customData = new CustomDataProducto(inmueble.eve_nombre,
                                                                    inmueble.eve_codigosociedad,
                                                                    inmueble.eve_codigopromocion,
                                                                    inmueble.eve_descripcionpromocion,
                                                                    inmueble.eve_estado,
                                                                    inmueble.eve_tipoinmueble,
                                                                    inmueble.eve_descripcionportal,
                                                                    inmueble.eve_tipovia,
                                                                    inmueble.eve_calle,
                                                                    inmueble.eve_numero,
                                                                    inmueble.eve_portal,
                                                                    inmueble.eve_escalera,
                                                                    inmueble.eve_piso,
                                                                    inmueble.eve_puerta,
                                                                    inmueble.eve_codigopostal,
                                                                    inmueble.eve_poblacion,
                                                                    inmueble.eve_codigo_reserva,
                                                                    inmueble.eve_bonificacion,
                                                                    inmueble.eve_meses_apli,
                                                                    inmueble.eve_mesesdegarantia,
                                                                    inmueble.eve_mesesfianza,
                                                                    inmueble.eve_alquiler,
                                                                    inmueble.eve_ibi_anual,
                                                                    inmueble.eve_comunidad,
                                                                    inmueble.eve_comunidad_anual,
                                                                    inmueble.eve_sujeto_a_permanencia,
                                                                    inmueble.eve_meses_permanencia,
                                                                    inmueble.eve_sujeto_a_impagos,
                                                                    inmueble.eve_hasta_fin_de_contrato,
                                                                    inmueble.eve_numero_meses_corriente_de_pago,
                                                                    inmueble.eve_numeroexpediente,
                                                                    inmueble.eve_tipo_capex_actual,
                                                                    inmueble.eve_metrosutiles,
                                                                    inmueble.eve_dormitorios,
                                                                    inmueble.eve_banyos,
                                                                    inmueble.eve_provincia,
                                                                    inmueble.eve_comunidadautonoma,
                                                                    inmueble.eve_fecha_validacion_acceso_fisico_vivienda,
                                                                    inmueble.eve_fecha_fin_obras_prevista,
                                                                    inmueble.eve_fecha_fin_obras_prevista_revisada,
                                                                    inmueble.eve_anejo,
                                                                    inmueble.eve_tipo_capex_actual_descripcion,
                                                                    inmueble.eve_tipo_capex_en_proceso_descripcion,
                                                                    inmueble.eve_minusvalido,
                                                                    inmueble.OdataEtag,
                                                                    inmueble.statecode.ToString(),
                                                                    inmueble.statuscode.ToString(),
                                                                    inmueble.eve_numeroregistro,
                                                                    inmueble.eve_fechamodificacionregistro,
                                                                    inmueble._eve_sociedadinmuebleid_value,
                                                                    inmueble.eve_pais,
                                                                    inmueble.createdon,
                                                                    inmueble.eve_comunidadlistames.ToString(),
                                                                    inmueble.eve_codigo_provincia,
                                                                    inmueble.eve_visualizacionportal.ToString(),
                                                                    inmueble._ownerid_value,
                                                                    inmueble.eve_cuotabloque.ToString(),
                                                                    inmueble.modifiedon,
                                                                    inmueble.eve_idsociedadprinex,
                                                                    inmueble.versionnumber.ToString(),
                                                                    inmueble.eve_idinmuebleprinex,
                                                                    inmueble.timezoneruleversionnumber.ToString(),
                                                                    inmueble._modifiedby_value,
                                                                    inmueble.eve_descripcion,
                                                                    inmueble.eve_inmuebleid,
                                                                    inmueble._eve_promocininmuebleid_value,
                                                                    inmueble.eve_metrosconstruidos,
                                                                    inmueble._createdby_value,
                                                                    inmueble._owningbusinessunit_value,
                                                                    inmueble._owninguser_value,
                                                                    inmueble.eve_comunidadlistaanual.ToString(),
                                                                    inmueble.eve_hace_esquina,
                                                                    inmueble.eve_suite_incorporado_api,
                                                                    inmueble.eve_subcategoria_tecnica,
                                                                    inmueble.eve_texto_10,
                                                                    inmueble.eve_marca_horno_microondas,
                                                                    inmueble.eve_comentarios_internos_entrega_llaves,
                                                                    inmueble.eve_congelador,
                                                                    inmueble.importsequencenumber,
                                                                    inmueble.eve_primera_revision_cambio_sumin,
                                                                    inmueble.eve_soleria_vivienda,
                                                                    inmueble.eve_sumatorio_enseres,
                                                                    inmueble.eve_fecha_aprobacion_reserva,
                                                                    inmueble.eve_categoria_tecnica,
                                                                    inmueble.eve_cotejo_bonificaciones,
                                                                    inmueble.eve_estado_validac_estado_obra_adicional,
                                                                    inmueble.eve_carencia_vivienda_ibi,
                                                                    inmueble.eve_amueblado_api,
                                                                    inmueble.eve_salida_humos,
                                                                    inmueble.eve_terraza_api,
                                                                    inmueble.eve_envio_comunicacion_a_centro_custodia,
                                                                    inmueble.eve_estado_gestion_revision_suministros,
                                                                    inmueble.eve_criticidad,
                                                                    inmueble.eve_estado_conservacion,
                                                                    inmueble.eve_color_lavavajillas,
                                                                    inmueble.eve_estado_adicional,
                                                                    inmueble.eve_importe_3,
                                                                    inmueble.eve_ubicacion,
                                                                    inmueble.eve_importe_2,
                                                                    inmueble.eve_importe_1,
                                                                    inmueble.eve_importe_5,
                                                                    inmueble.eve_importe_4,
                                                                    inmueble.eve_induccion,
                                                                    inmueble.eve_pre_creado_fecha_5,
                                                                    inmueble.eve_fecha_entrega_llaves_api,
                                                                    inmueble.eve_pre_creado_texto_3,
                                                                    inmueble.eve_tipo_adecuacion_actual,
                                                                    inmueble.eve_referenciacatastral,
                                                                    inmueble.eve_color_campana,
                                                                    inmueble.eve_codigo_contrato,
                                                                    inmueble.eve_pre_creado_fecha_1,
                                                                    inmueble.eve_banyos_suite,
                                                                    inmueble.eve_iniciar_creacion_peticion,
                                                                    inmueble.eve_color_vitroceramica,
                                                                    inmueble.eve_fecha_entrega_llaves_api_adicional,
                                                                    inmueble.eve_estado_validacion_suministros,
                                                                    inmueble.eve_datos_recogida_completados,
                                                                    inmueble.eve_periodicidadcomunidad,
                                                                    inmueble.eve_lavadora_integrada,
                                                                    inmueble.eve_color_microondas,
                                                                    inmueble.eve_amueblado,
                                                                    inmueble.eve_marca_lavadora,
                                                                    inmueble.eve_dias_desde_disponibilidad_primer_juego,
                                                                    inmueble.eve_codigo_contratista_adecuacion,
                                                                    inmueble.eve_dias_desde_disponibilidad_ultimo_juego,
                                                                    inmueble.eve_comentario_interno_1,
                                                                    inmueble.eve_aire_acondicionado_api,
                                                                    inmueble.eve_importe_final_obra,
                                                                    inmueble.eve_texto_13,
                                                                    inmueble.eve_comunicacion_a_centro_custodia,
                                                                    inmueble.eve_distribucion,
                                                                    inmueble.eve_jardin_privado_api,
                                                                    inmueble.eve_estado_comercializacion,
                                                                    inmueble.eve_fecha_valoracion,
                                                                    inmueble.eve_fecha_disp_primer_juego_llaves_ant,
                                                                    inmueble.eve_tipo_cocina,
                                                                    inmueble.eve_numerofincaregistral,
                                                                    inmueble._owningteam_value,
                                                                    inmueble.eve_presupuesto_valoracion_aceptado,
                                                                    inmueble.eve_microondas,
                                                                    inmueble._eve_reserva_asociada_value,
                                                                    inmueble.eve_empresa_calidad,
                                                                    inmueble.eve_carencia_vivienda_comunidad,
                                                                    inmueble.eve_comentarios_validacion_estado_obra,
                                                                    inmueble.eve_calefaccion,
                                                                    inmueble.eve_pre_creado_fecha_6,
                                                                    inmueble.eve_ciclo_capex_actual,
                                                                    inmueble._modifiedonbehalfby_value,
                                                                    inmueble.eve_tipologia_inmueble_txt,
                                                                    inmueble.eve_pre_creado_fecha_2,
                                                                    inmueble.eve_fecha_recepcion_api_llaves_ultimo,
                                                                    inmueble.eve_pre_creado_texto_2,
                                                                    inmueble.eve_peticion_titularidad_suministrothanterior,
                                                                    inmueble.eve_causa_desviacion,
                                                                    inmueble.eve_estado_general_suministros,
                                                                    inmueble.eve_pre_creado_texto_6,
                                                                    inmueble.eve_numero_expediente_vpo,
                                                                    inmueble.eve_fecha_validacion_suministros,
                                                                    inmueble.eve_fecha_inicio_obras_prevista,
                                                                    inmueble.eve_congelador_integrado,
                                                                    inmueble.eve_certificado_energetico,
                                                                    inmueble.eve_calefaccion_api,
                                                                    inmueble.eve_estado_validacion_suministros_adicional,
                                                                    inmueble.eve_texto_12,
                                                                    inmueble.eve_peticion_titularidad_suministroth,
                                                                    inmueble.eve_aire_acondicionado,
                                                                    inmueble.eve_puerta_seguridad,
                                                                    inmueble.eve_lavavajillas_integrado,
                                                                    inmueble.eve_estado_entrega_ultimo_juego_llaves,
                                                                    inmueble.eve_lavavajillas,
                                                                    inmueble.eve_estado_adecuacion_adicional,
                                                                    inmueble.eve_estado_validacion_anterior,
                                                                    inmueble.eve_estado_validacion_vivienda_api,
                                                                    inmueble.eve_operacion,
                                                                    inmueble.eve_cups_gas,
                                                                    inmueble.eve_marca_vitroceramica,
                                                                    inmueble.eve_frigorifico_con_congelador_integrado,
                                                                    inmueble.eve_sigla_tipo_via,
                                                                    inmueble.eve_estado_entrega_primer_juego_llaves,
                                                                    inmueble.eve_importe_oc,
                                                                    inmueble.eve_marca_lavavajillas,
                                                                    inmueble.eve_datosregistrales,
                                                                    inmueble.eve_pre_creado_fecha_7,
                                                                    inmueble.eve_pre_creado_texto_1,
                                                                    inmueble.eve_comentarios_acceso_vivienda,
                                                                    inmueble.eve_color_lavadora_secadora,
                                                                    inmueble.eve_estatus,
                                                                    inmueble.eve_contratista,
                                                                    inmueble.eve_fecha_validacion_vivienda,
                                                                    inmueble.eve_estado_validac_acceso_fisico_vivienda,
                                                                    inmueble.eve_equipamiento_cocina_api,
                                                                    inmueble.utcconversiontimezonecode,
                                                                    inmueble.eve_pre_creado_fecha_3,
                                                                    inmueble.eve_pre_creado_texto_5,
                                                                    inmueble.eve_pre_creado_numerico_3,
                                                                    inmueble.eve_pre_creado_numerico_2,
                                                                    inmueble.eve_pre_creado_numerico_1,
                                                                    inmueble.eve_tipo_cocina_api,
                                                                    inmueble.eve_cotejo_rentas_vpo,
                                                                    inmueble.eve_fecha_recogida_datos,
                                                                    inmueble.eve_pre_creado_numerico_4,
                                                                    inmueble.eve_cups_luz,
                                                                    inmueble.eve_texto_15,
                                                                    inmueble.eve_marca_horno,
                                                                    inmueble.eve_importe_bon,
                                                                    inmueble.eve_fees_ss,
                                                                    inmueble.eve_color_induccion,
                                                                    inmueble.eve_horno,
                                                                    inmueble.eve_vitroceramica,
                                                                    inmueble.eve_color_lavadora,
                                                                    inmueble.eve_lavadora_secadora,
                                                                    inmueble.eve_num_armarios_no_empotrados_api,
                                                                    inmueble.eve_texto_11,
                                                                    inmueble.eve_fecha_disponibilidad_primer_juego_llaves,
                                                                    inmueble.eve_orientacion,
                                                                    inmueble.eve_fecha_inicio_obras,
                                                                    inmueble.eve_renta,
                                                                    inmueble.eve_marca_microondas,
                                                                    inmueble.eve_balcon_api,
                                                                    inmueble.eve_contratista_adecuacion,
                                                                    inmueble.eve_comentario_interno_2,
                                                                    inmueble.eve_presupuesto_valoracion,
                                                                    inmueble.eve_m2_escaparate,
                                                                    inmueble.eve_anyo_ibi,
                                                                    inmueble.eve_capex_extraordinario,
                                                                    inmueble.eve_periodicidadibi,
                                                                    inmueble.eve_marca_lavadora_secadora,
                                                                    inmueble.eve_tecnico_responsable_capex,
                                                                    inmueble.overriddencreatedon,
                                                                    inmueble.eve_balcon,
                                                                    inmueble.eve_peticion_titularidad_suministro_inquilino,
                                                                    inmueble.eve_fees_revision_2,
                                                                    inmueble.eve_ciclo_capex_en_proceso,
                                                                    inmueble.eve_crear_peticion_tecnica,
                                                                    inmueble.eve_fecha_recepcion_api_llaves,
                                                                    inmueble.eve_localidadregistro,
                                                                    inmueble.eve_frigorifico_con_congelador,
                                                                    inmueble.eve_color_frigorifico,
                                                                    inmueble.eve_fecha_check_list,
                                                                    inmueble.eve_terraza,
                                                                    inmueble._eve_propietario_tecnico_id_value,
                                                                    inmueble.eve_pre_creado_fecha_4,
                                                                    inmueble.eve_texto_6,
                                                                    inmueble.eve_texto_7,
                                                                    inmueble.eve_texto_5,
                                                                    inmueble.eve_tipo,
                                                                    inmueble.eve_agua_caliente,
                                                                    inmueble.eve_texto_8,
                                                                    inmueble.eve_fecha_real_finalizacion_2,
                                                                    inmueble.eve_texto_9,
                                                                    inmueble.eve_fecha_3,
                                                                    inmueble.eve_fecha_2,
                                                                    inmueble.eve_fecha_5,
                                                                    inmueble.eve_fecha_4,
                                                                    inmueble.eve_electrodomesticos,
                                                                    inmueble.eve_revision_suministros_adicional,
                                                                    inmueble.eve_texto_14,
                                                                    inmueble.eve_comentarios_validacion_suministros,
                                                                    inmueble.eve_codigo_adecuacion_actual_texto,
                                                                    inmueble.eve_precio_maximo_vivienda_protegida,
                                                                    inmueble._createdonbehalfby_value,
                                                                    inmueble.eve_fecha_validacion_estado_obra,
                                                                    inmueble.eve_campana_extractora,
                                                                    inmueble.eve_fecha_fin_obras,
                                                                    inmueble.eve_color_horno_microondas,
                                                                    inmueble.eve_marca_campana,
                                                                    inmueble.eve_jardin_privado,
                                                                    inmueble.eve_horno_microondas,
                                                                    inmueble.eve_fecha_entrega_llaves_inquilino,
                                                                    inmueble.eve_descripcion_tecnica,
                                                                    inmueble._eve_codigo_adecuacion_actual_value,
                                                                    inmueble.eve_marca_congelador,
                                                                    inmueble.eve_color_horno,
                                                                    inmueble.eve_tecnico_responsable,
                                                                    inmueble.eve_ubicacion_banyos,
                                                                    inmueble.eve_tipo_capex_en_proceso,
                                                                    inmueble.eve_marca_induccion,
                                                                    inmueble.eve_soleria_vivienda_api,
                                                                    inmueble.eve_estado_adecuacion,
                                                                    inmueble.eve_lavadora,
                                                                    inmueble.eve_tipologia_inmueble,
                                                                    inmueble.eve_equipamiento_cocina,
                                                                    inmueble.eve_estado_validac_acceso_fisic_vivienda_adic,
                                                                    inmueble.eve_marca_frigorifico,
                                                                    inmueble.eve_estado_validacion_anterior_2,
                                                                    inmueble.eve_estado_validac_estado_obra,
                                                                    inmueble.eve_num_armarios_empotrados_api,
                                                                    inmueble.eve_color_congelador,
                                                                    inmueble.eve_lavadora_secadora_integrada,
                                                                    inmueble.eve_numero_aseos,
                                                                    inmueble.eve_pre_creado_texto_4,
                                                                    inmueble.eve_fees_revision,
                                                                    inmueble.eve_armarios_empotrados,
                                                                    inmueble.eve_clave_4_prinex,
                                                                    inmueble.eve_fecha_fin_obras_prevista_revisada_nuevo,
                                                                    inmueble.eve_id_crm,
                                                                    inmueble.eve_datos_registrales_renovacion,
                                                                    inmueble.eve_num_contrato_suministro_agua,
                                                                    inmueble.eve_promocion_prinex,
                                                                    inmueble._eve_peticion_tecnica_cambio_titularidad_agua_value,
                                                                    inmueble.eve_fecha_anulacion_parada_comercializacion,
                                                                    inmueble._eve_peticion_tecnica_alta_suministros_value,
                                                                    inmueble.eve_companya_suministros_agua,
                                                                    inmueble.eve_clave_2_prinex,
                                                                    inmueble.eve_fecha_guardado_precomercializacion,
                                                                    inmueble.eve_num_contrato_suministro_luz,
                                                                    inmueble.eve_fecha_general_comercializacion,
                                                                    inmueble._eve_usuario_guardado_precom_id_value,
                                                                    inmueble.eve_clave_1_prinex,
                                                                    inmueble.eve_tipo_de_inmueble_prinex,
                                                                    inmueble.eve_clave_3_prinex,
                                                                    inmueble.eve_companya_suministros_luz,
                                                                    inmueble.eve_fecha_fin_garantia_adecuacion,
                                                                    inmueble.eve_id_activo,
                                                                    inmueble._eve_peticion_tecnica_alta_agua_value,
                                                                    inmueble.eve_cups_luz_peticion_tecnica,
                                                                    inmueble.eve_categoria_inmueble,
                                                                    inmueble.eve_numero_reclamaciones_centro_custodio,
                                                                    inmueble.eve_parada_okupacion,
                                                                    inmueble.eve_parada_comercializacion_tecnica,
                                                                    inmueble.eve_companya_suministros_gas,
                                                                    inmueble.eve_num_contrato_suministro_gas,
                                                                    inmueble.eve_parada_estrategica_comercializacion,
                                                                    inmueble.eve_estado_precomercializacion,
                                                                    inmueble.eve_punto_de_suministro_agua_peticion_tecnica,
                                                                    inmueble.eve_bloqueo_validacion_llaves,
                                                                    inmueble.eve_referencia_catastral_renovacion,
                                                                    inmueble.eve_descripcion_motivo_paralizacion,
                                                                    inmueble.eve_incidencia_tras_revision_inicial,
                                                                    inmueble.eve_fecha_ultima_comunicacion_centro_custodia,
                                                                    inmueble.eve_fecha_anulacion_paralizacion_estrategica,
                                                                    inmueble.eve_fecha_incidencia_tras_revision_inicial,
                                                                    inmueble.eve_luz_nodisponible,
                                                                    inmueble.eve_fecha_parada_comercializacion,
                                                                    inmueble.eve_agua_nodisponible,
                                                                    inmueble.eve_gas_nodisponible,
                                                                    inmueble._eve_peticion_tecnica_cambio_titularidad_sums_value,
                                                                    inmueble.eve_fecha_paralizacion_estrategica,
                                                                    inmueble.eve_cups_gas_peticion_tecnica,
                                                                    inmueble.eve_fecha_contrato_agua,
                                                                    inmueble.eve_parada_comercializacion_estrategica
                                                                    );

            Product product = new Product
            {
                Instance = "Testa",
                ID = inmueble.eve_inmuebleid,
                Description = inmueble.eve_descripcion, // se actualiza
                Client = "Testa",
                RecurrenceType = 1,
                Currency = "EUR"
            };
            if (null != inmueble.eve_renta)
            {
                product.Price = decimal.Parse(inmueble.eve_renta); //se actualiza
            }
            else
            {
                product.Price = 0;
            }
            product.CostPerUnit = null;
            product.Category = inmueble._eve_promocininmuebleid_value;
            product.Group = inmueble.eve_tipoinmueble;
            product.Reference = null;
            product.CustomData = customData.SerializarCustomDataProducto(); // se actualiza
            product.CreatedDate = inmueble.createdon;
            product.CreatedByUserId = "Testa";
            product.LastModifiedDate = inmueble.modifiedon; // se actualiza
            product.LastModifiedByUserId = product.CreatedByUserId;
            product.Deleted = false;
            return product;
        }
        private Product UpdateProduct(Inmueble inmueble)
        {
            CustomDataProducto customData = new CustomDataProducto(inmueble.eve_nombre,
                                                                    inmueble.eve_codigosociedad,
                                                                    inmueble.eve_codigopromocion,
                                                                    inmueble.eve_descripcionpromocion,
                                                                    inmueble.eve_estado,
                                                                    inmueble.eve_tipoinmueble,
                                                                    inmueble.eve_descripcionportal,
                                                                    inmueble.eve_tipovia,
                                                                    inmueble.eve_calle,
                                                                    inmueble.eve_numero,
                                                                    inmueble.eve_portal,
                                                                    inmueble.eve_escalera,
                                                                    inmueble.eve_piso,
                                                                    inmueble.eve_puerta,
                                                                    inmueble.eve_codigopostal,
                                                                    inmueble.eve_poblacion,
                                                                    inmueble.eve_codigo_reserva,
                                                                    inmueble.eve_bonificacion,
                                                                    inmueble.eve_meses_apli,
                                                                    inmueble.eve_mesesdegarantia,
                                                                    inmueble.eve_mesesfianza,
                                                                    inmueble.eve_alquiler,
                                                                    inmueble.eve_ibi_anual,
                                                                    inmueble.eve_comunidad,
                                                                    inmueble.eve_comunidad_anual,
                                                                    inmueble.eve_sujeto_a_permanencia,
                                                                    inmueble.eve_meses_permanencia,
                                                                    inmueble.eve_sujeto_a_impagos,
                                                                    inmueble.eve_hasta_fin_de_contrato,
                                                                    inmueble.eve_numero_meses_corriente_de_pago,
                                                                    inmueble.eve_numeroexpediente,
                                                                    inmueble.eve_tipo_capex_actual,
                                                                    inmueble.eve_metrosutiles,
                                                                    inmueble.eve_dormitorios,
                                                                    inmueble.eve_banyos,
                                                                    inmueble.eve_provincia,
                                                                    inmueble.eve_comunidadautonoma,
                                                                    inmueble.eve_fecha_validacion_acceso_fisico_vivienda,
                                                                    inmueble.eve_fecha_fin_obras_prevista,
                                                                    inmueble.eve_fecha_fin_obras_prevista_revisada,
                                                                    inmueble.eve_anejo,
                                                                    inmueble.eve_tipo_capex_actual_descripcion,
                                                                    inmueble.eve_tipo_capex_en_proceso_descripcion,
                                                                    inmueble.eve_minusvalido,
                                                                    inmueble.OdataEtag,
                                                                    inmueble.statecode.ToString(),
                                                                    inmueble.statuscode.ToString(),
                                                                    inmueble.eve_numeroregistro,
                                                                    inmueble.eve_fechamodificacionregistro,
                                                                    inmueble._eve_sociedadinmuebleid_value,
                                                                    inmueble.eve_pais,
                                                                    inmueble.createdon,
                                                                    inmueble.eve_comunidadlistames.ToString(),
                                                                    inmueble.eve_codigo_provincia,
                                                                    inmueble.eve_visualizacionportal.ToString(),
                                                                    inmueble._ownerid_value,
                                                                    inmueble.eve_cuotabloque.ToString(),
                                                                    inmueble.modifiedon,
                                                                    inmueble.eve_idsociedadprinex,
                                                                    inmueble.versionnumber.ToString(),
                                                                    inmueble.eve_idinmuebleprinex,
                                                                    inmueble.timezoneruleversionnumber.ToString(),
                                                                    inmueble._modifiedby_value,
                                                                    inmueble.eve_descripcion,
                                                                    inmueble.eve_inmuebleid,
                                                                    inmueble._eve_promocininmuebleid_value,
                                                                    inmueble.eve_metrosconstruidos,
                                                                    inmueble._createdby_value,
                                                                    inmueble._owningbusinessunit_value,
                                                                    inmueble._owninguser_value,
                                                                    inmueble.eve_comunidadlistaanual.ToString(),
                                                                    inmueble.eve_hace_esquina,
                                                                    inmueble.eve_suite_incorporado_api,
                                                                    inmueble.eve_subcategoria_tecnica,
                                                                    inmueble.eve_texto_10,
                                                                    inmueble.eve_marca_horno_microondas,
                                                                    inmueble.eve_comentarios_internos_entrega_llaves,
                                                                    inmueble.eve_congelador,
                                                                    inmueble.importsequencenumber,
                                                                    inmueble.eve_primera_revision_cambio_sumin,
                                                                    inmueble.eve_soleria_vivienda,
                                                                    inmueble.eve_sumatorio_enseres,
                                                                    inmueble.eve_fecha_aprobacion_reserva,
                                                                    inmueble.eve_categoria_tecnica,
                                                                    inmueble.eve_cotejo_bonificaciones,
                                                                    inmueble.eve_estado_validac_estado_obra_adicional,
                                                                    inmueble.eve_carencia_vivienda_ibi,
                                                                    inmueble.eve_amueblado_api,
                                                                    inmueble.eve_salida_humos,
                                                                    inmueble.eve_terraza_api,
                                                                    inmueble.eve_envio_comunicacion_a_centro_custodia,
                                                                    inmueble.eve_estado_gestion_revision_suministros,
                                                                    inmueble.eve_criticidad,
                                                                    inmueble.eve_estado_conservacion,
                                                                    inmueble.eve_color_lavavajillas,
                                                                    inmueble.eve_estado_adicional,
                                                                    inmueble.eve_importe_3,
                                                                    inmueble.eve_ubicacion,
                                                                    inmueble.eve_importe_2,
                                                                    inmueble.eve_importe_1,
                                                                    inmueble.eve_importe_5,
                                                                    inmueble.eve_importe_4,
                                                                    inmueble.eve_induccion,
                                                                    inmueble.eve_pre_creado_fecha_5,
                                                                    inmueble.eve_fecha_entrega_llaves_api,
                                                                    inmueble.eve_pre_creado_texto_3,
                                                                    inmueble.eve_tipo_adecuacion_actual,
                                                                    inmueble.eve_referenciacatastral,
                                                                    inmueble.eve_color_campana,
                                                                    inmueble.eve_codigo_contrato,
                                                                    inmueble.eve_pre_creado_fecha_1,
                                                                    inmueble.eve_banyos_suite,
                                                                    inmueble.eve_iniciar_creacion_peticion,
                                                                    inmueble.eve_color_vitroceramica,
                                                                    inmueble.eve_fecha_entrega_llaves_api_adicional,
                                                                    inmueble.eve_estado_validacion_suministros,
                                                                    inmueble.eve_datos_recogida_completados,
                                                                    inmueble.eve_periodicidadcomunidad,
                                                                    inmueble.eve_lavadora_integrada,
                                                                    inmueble.eve_color_microondas,
                                                                    inmueble.eve_amueblado,
                                                                    inmueble.eve_marca_lavadora,
                                                                    inmueble.eve_dias_desde_disponibilidad_primer_juego,
                                                                    inmueble.eve_codigo_contratista_adecuacion,
                                                                    inmueble.eve_dias_desde_disponibilidad_ultimo_juego,
                                                                    inmueble.eve_comentario_interno_1,
                                                                    inmueble.eve_aire_acondicionado_api,
                                                                    inmueble.eve_importe_final_obra,
                                                                    inmueble.eve_texto_13,
                                                                    inmueble.eve_comunicacion_a_centro_custodia,
                                                                    inmueble.eve_distribucion,
                                                                    inmueble.eve_jardin_privado_api,
                                                                    inmueble.eve_estado_comercializacion,
                                                                    inmueble.eve_fecha_valoracion,
                                                                    inmueble.eve_fecha_disp_primer_juego_llaves_ant,
                                                                    inmueble.eve_tipo_cocina,
                                                                    inmueble.eve_numerofincaregistral,
                                                                    inmueble._owningteam_value,
                                                                    inmueble.eve_presupuesto_valoracion_aceptado,
                                                                    inmueble.eve_microondas,
                                                                    inmueble._eve_reserva_asociada_value,
                                                                    inmueble.eve_empresa_calidad,
                                                                    inmueble.eve_carencia_vivienda_comunidad,
                                                                    inmueble.eve_comentarios_validacion_estado_obra,
                                                                    inmueble.eve_calefaccion,
                                                                    inmueble.eve_pre_creado_fecha_6,
                                                                    inmueble.eve_ciclo_capex_actual,
                                                                    inmueble._modifiedonbehalfby_value,
                                                                    inmueble.eve_tipologia_inmueble_txt,
                                                                    inmueble.eve_pre_creado_fecha_2,
                                                                    inmueble.eve_fecha_recepcion_api_llaves_ultimo,
                                                                    inmueble.eve_pre_creado_texto_2,
                                                                    inmueble.eve_peticion_titularidad_suministrothanterior,
                                                                    inmueble.eve_causa_desviacion,
                                                                    inmueble.eve_estado_general_suministros,
                                                                    inmueble.eve_pre_creado_texto_6,
                                                                    inmueble.eve_numero_expediente_vpo,
                                                                    inmueble.eve_fecha_validacion_suministros,
                                                                    inmueble.eve_fecha_inicio_obras_prevista,
                                                                    inmueble.eve_congelador_integrado,
                                                                    inmueble.eve_certificado_energetico,
                                                                    inmueble.eve_calefaccion_api,
                                                                    inmueble.eve_estado_validacion_suministros_adicional,
                                                                    inmueble.eve_texto_12,
                                                                    inmueble.eve_peticion_titularidad_suministroth,
                                                                    inmueble.eve_aire_acondicionado,
                                                                    inmueble.eve_puerta_seguridad,
                                                                    inmueble.eve_lavavajillas_integrado,
                                                                    inmueble.eve_estado_entrega_ultimo_juego_llaves,
                                                                    inmueble.eve_lavavajillas,
                                                                    inmueble.eve_estado_adecuacion_adicional,
                                                                    inmueble.eve_estado_validacion_anterior,
                                                                    inmueble.eve_estado_validacion_vivienda_api,
                                                                    inmueble.eve_operacion,
                                                                    inmueble.eve_cups_gas,
                                                                    inmueble.eve_marca_vitroceramica,
                                                                    inmueble.eve_frigorifico_con_congelador_integrado,
                                                                    inmueble.eve_sigla_tipo_via,
                                                                    inmueble.eve_estado_entrega_primer_juego_llaves,
                                                                    inmueble.eve_importe_oc,
                                                                    inmueble.eve_marca_lavavajillas,
                                                                    inmueble.eve_datosregistrales,
                                                                    inmueble.eve_pre_creado_fecha_7,
                                                                    inmueble.eve_pre_creado_texto_1,
                                                                    inmueble.eve_comentarios_acceso_vivienda,
                                                                    inmueble.eve_color_lavadora_secadora,
                                                                    inmueble.eve_estatus,
                                                                    inmueble.eve_contratista,
                                                                    inmueble.eve_fecha_validacion_vivienda,
                                                                    inmueble.eve_estado_validac_acceso_fisico_vivienda,
                                                                    inmueble.eve_equipamiento_cocina_api,
                                                                    inmueble.utcconversiontimezonecode,
                                                                    inmueble.eve_pre_creado_fecha_3,
                                                                    inmueble.eve_pre_creado_texto_5,
                                                                    inmueble.eve_pre_creado_numerico_3,
                                                                    inmueble.eve_pre_creado_numerico_2,
                                                                    inmueble.eve_pre_creado_numerico_1,
                                                                    inmueble.eve_tipo_cocina_api,
                                                                    inmueble.eve_cotejo_rentas_vpo,
                                                                    inmueble.eve_fecha_recogida_datos,
                                                                    inmueble.eve_pre_creado_numerico_4,
                                                                    inmueble.eve_cups_luz,
                                                                    inmueble.eve_texto_15,
                                                                    inmueble.eve_marca_horno,
                                                                    inmueble.eve_importe_bon,
                                                                    inmueble.eve_fees_ss,
                                                                    inmueble.eve_color_induccion,
                                                                    inmueble.eve_horno,
                                                                    inmueble.eve_vitroceramica,
                                                                    inmueble.eve_color_lavadora,
                                                                    inmueble.eve_lavadora_secadora,
                                                                    inmueble.eve_num_armarios_no_empotrados_api,
                                                                    inmueble.eve_texto_11,
                                                                    inmueble.eve_fecha_disponibilidad_primer_juego_llaves,
                                                                    inmueble.eve_orientacion,
                                                                    inmueble.eve_fecha_inicio_obras,
                                                                    inmueble.eve_renta,
                                                                    inmueble.eve_marca_microondas,
                                                                    inmueble.eve_balcon_api,
                                                                    inmueble.eve_contratista_adecuacion,
                                                                    inmueble.eve_comentario_interno_2,
                                                                    inmueble.eve_presupuesto_valoracion,
                                                                    inmueble.eve_m2_escaparate,
                                                                    inmueble.eve_anyo_ibi,
                                                                    inmueble.eve_capex_extraordinario,
                                                                    inmueble.eve_periodicidadibi,
                                                                    inmueble.eve_marca_lavadora_secadora,
                                                                    inmueble.eve_tecnico_responsable_capex,
                                                                    inmueble.overriddencreatedon,
                                                                    inmueble.eve_balcon,
                                                                    inmueble.eve_peticion_titularidad_suministro_inquilino,
                                                                    inmueble.eve_fees_revision_2,
                                                                    inmueble.eve_ciclo_capex_en_proceso,
                                                                    inmueble.eve_crear_peticion_tecnica,
                                                                    inmueble.eve_fecha_recepcion_api_llaves,
                                                                    inmueble.eve_localidadregistro,
                                                                    inmueble.eve_frigorifico_con_congelador,
                                                                    inmueble.eve_color_frigorifico,
                                                                    inmueble.eve_fecha_check_list,
                                                                    inmueble.eve_terraza,
                                                                    inmueble._eve_propietario_tecnico_id_value,
                                                                    inmueble.eve_pre_creado_fecha_4,
                                                                    inmueble.eve_texto_6,
                                                                    inmueble.eve_texto_7,
                                                                    inmueble.eve_texto_5,
                                                                    inmueble.eve_tipo,
                                                                    inmueble.eve_agua_caliente,
                                                                    inmueble.eve_texto_8,
                                                                    inmueble.eve_fecha_real_finalizacion_2,
                                                                    inmueble.eve_texto_9,
                                                                    inmueble.eve_fecha_3,
                                                                    inmueble.eve_fecha_2,
                                                                    inmueble.eve_fecha_5,
                                                                    inmueble.eve_fecha_4,
                                                                    inmueble.eve_electrodomesticos,
                                                                    inmueble.eve_revision_suministros_adicional,
                                                                    inmueble.eve_texto_14,
                                                                    inmueble.eve_comentarios_validacion_suministros,
                                                                    inmueble.eve_codigo_adecuacion_actual_texto,
                                                                    inmueble.eve_precio_maximo_vivienda_protegida,
                                                                    inmueble._createdonbehalfby_value,
                                                                    inmueble.eve_fecha_validacion_estado_obra,
                                                                    inmueble.eve_campana_extractora,
                                                                    inmueble.eve_fecha_fin_obras,
                                                                    inmueble.eve_color_horno_microondas,
                                                                    inmueble.eve_marca_campana,
                                                                    inmueble.eve_jardin_privado,
                                                                    inmueble.eve_horno_microondas,
                                                                    inmueble.eve_fecha_entrega_llaves_inquilino,
                                                                    inmueble.eve_descripcion_tecnica,
                                                                    inmueble._eve_codigo_adecuacion_actual_value,
                                                                    inmueble.eve_marca_congelador,
                                                                    inmueble.eve_color_horno,
                                                                    inmueble.eve_tecnico_responsable,
                                                                    inmueble.eve_ubicacion_banyos,
                                                                    inmueble.eve_tipo_capex_en_proceso,
                                                                    inmueble.eve_marca_induccion,
                                                                    inmueble.eve_soleria_vivienda_api,
                                                                    inmueble.eve_estado_adecuacion,
                                                                    inmueble.eve_lavadora,
                                                                    inmueble.eve_tipologia_inmueble,
                                                                    inmueble.eve_equipamiento_cocina,
                                                                    inmueble.eve_estado_validac_acceso_fisic_vivienda_adic,
                                                                    inmueble.eve_marca_frigorifico,
                                                                    inmueble.eve_estado_validacion_anterior_2,
                                                                    inmueble.eve_estado_validac_estado_obra,
                                                                    inmueble.eve_num_armarios_empotrados_api,
                                                                    inmueble.eve_color_congelador,
                                                                    inmueble.eve_lavadora_secadora_integrada,
                                                                    inmueble.eve_numero_aseos,
                                                                    inmueble.eve_pre_creado_texto_4,
                                                                    inmueble.eve_fees_revision,
                                                                    inmueble.eve_armarios_empotrados,
                                                                    inmueble.eve_clave_4_prinex,
                                                                    inmueble.eve_fecha_fin_obras_prevista_revisada_nuevo,
                                                                    inmueble.eve_id_crm,
                                                                    inmueble.eve_datos_registrales_renovacion,
                                                                    inmueble.eve_num_contrato_suministro_agua,
                                                                    inmueble.eve_promocion_prinex,
                                                                    inmueble._eve_peticion_tecnica_cambio_titularidad_agua_value,
                                                                    inmueble.eve_fecha_anulacion_parada_comercializacion,
                                                                    inmueble._eve_peticion_tecnica_alta_suministros_value,
                                                                    inmueble.eve_companya_suministros_agua,
                                                                    inmueble.eve_clave_2_prinex,
                                                                    inmueble.eve_fecha_guardado_precomercializacion,
                                                                    inmueble.eve_num_contrato_suministro_luz,
                                                                    inmueble.eve_fecha_general_comercializacion,
                                                                    inmueble._eve_usuario_guardado_precom_id_value,
                                                                    inmueble.eve_clave_1_prinex,
                                                                    inmueble.eve_tipo_de_inmueble_prinex,
                                                                    inmueble.eve_clave_3_prinex,
                                                                    inmueble.eve_companya_suministros_luz,
                                                                    inmueble.eve_fecha_fin_garantia_adecuacion,
                                                                    inmueble.eve_id_activo,
                                                                    inmueble._eve_peticion_tecnica_alta_agua_value,
                                                                    inmueble.eve_cups_luz_peticion_tecnica,
                                                                    inmueble.eve_categoria_inmueble,
                                                                    inmueble.eve_numero_reclamaciones_centro_custodio,
                                                                    inmueble.eve_parada_okupacion,
                                                                    inmueble.eve_parada_comercializacion_tecnica,
                                                                    inmueble.eve_companya_suministros_gas,
                                                                    inmueble.eve_num_contrato_suministro_gas,
                                                                    inmueble.eve_parada_estrategica_comercializacion,
                                                                    inmueble.eve_estado_precomercializacion,
                                                                    inmueble.eve_punto_de_suministro_agua_peticion_tecnica,
                                                                    inmueble.eve_bloqueo_validacion_llaves,
                                                                    inmueble.eve_referencia_catastral_renovacion,
                                                                    inmueble.eve_descripcion_motivo_paralizacion,
                                                                    inmueble.eve_incidencia_tras_revision_inicial,
                                                                    inmueble.eve_fecha_ultima_comunicacion_centro_custodia,
                                                                    inmueble.eve_fecha_anulacion_paralizacion_estrategica,
                                                                    inmueble.eve_fecha_incidencia_tras_revision_inicial,
                                                                    inmueble.eve_luz_nodisponible,
                                                                    inmueble.eve_fecha_parada_comercializacion,
                                                                    inmueble.eve_agua_nodisponible,
                                                                    inmueble.eve_gas_nodisponible,
                                                                    inmueble._eve_peticion_tecnica_cambio_titularidad_sums_value,
                                                                    inmueble.eve_fecha_paralizacion_estrategica,
                                                                    inmueble.eve_cups_gas_peticion_tecnica,
                                                                    inmueble.eve_fecha_contrato_agua,
                                                                    inmueble.eve_parada_comercializacion_estrategica
                                                                    );
            Product product = new Product
            {
                Description = inmueble.eve_descripcion // se actualiza
            };
            if (null != inmueble.eve_renta)
            {
                product.Price = decimal.Parse(inmueble.eve_renta); //se actualiza
            }
            else
            {
                product.Price = 0;
            }
            product.CustomData = customData.SerializarCustomDataProducto(); // se actualiza
            product.LastModifiedDate = inmueble.modifiedon; // se actualiza
            product.Category = inmueble._eve_promocininmuebleid_value;
            product.Group = inmueble.eve_tipoinmueble;
            return product;
        }
        private bool LlamadaSPSaveProduct(InconcertOnline2 db, Product producto)
        {
            bool respuesta = false;
            try
            {
                db.Dynamics_SaveProduct("Testa", producto.ID, producto.Description, producto.Client, producto.Category, producto.Group, producto.Reference,
                    producto.Currency, producto.Price, producto.RecurrenceType, producto.CostPerUnit, producto.CustomData,
                    producto.CreatedDate, producto.CreatedByUserId, producto.LastModifiedDate, producto.LastModifiedByUserId);
                respuesta = true;
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                logger.Error(e.InnerException);
                logger.Error(e.StackTrace);
            }
            return respuesta;
        }
        private void UpsertReservas(IEnumerable<string> datos)
        {
            Reserva reserva = null;
            Dynamics_Reservas d_reserva = null;
            int total = 0;

            if (null != datos)
            {
                var lotes = Utils.Batch<string>(datos, 100);
                int num_lotes = lotes.Count<IEnumerable<string>>();
                logger.Info("UpsertReservas. Número de lotes: " + num_lotes);
                int contador_lotes = 0;

                foreach (var lote in lotes)
                {
                    using(var db = new InconcertOnline2())
                    {
                        foreach (var item in lote)
                        {
                            reserva = JsonConvert.DeserializeObject<Reserva>(item, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                            d_reserva = reserva.Convertir2BD();
                            if (LlamadaSPSaveReserva(db, d_reserva))
                            {
                                total++;
                            }
                        }
                    }
                    contador_lotes++;
                    logger.Info($"Lote {contador_lotes} Registros procesados {total}");
                }
            }
            logger.Info($"Reservas procesadas del CRM {total} de {datos.Count()}");
        }
        private bool LlamadaSPSaveReserva(InconcertOnline2 db, Dynamics_Reservas d_reserva)
        {
            bool respuesta = false;
            try
            {
                db.Dynamics_SaveReserva(d_reserva.OdataEtag, d_reserva.eve_importebonificaciontrastero1, d_reserva.eve_periodicidadibitrastero1, d_reserva.eve_garantiaadicionaltrastero1, 
                    d_reserva.eve_lleida_request_id, d_reserva.eve_fecha_fin, d_reserva.eve_dias_alta_suministro, d_reserva.eve_empresa, d_reserva.eve_fecha_alta_suministro, 
                    d_reserva.C_eve_pago_reserva_id_value, d_reserva.eve_ibitrastero1, d_reserva.eve_nombretitular, d_reserva.C_owningbusinessunit_value, d_reserva.eve_inquilinos_doc_subida, 
                    d_reserva.C_owninguser_value, d_reserva.C_eve_promocioninmueble4_value, d_reserva.statecode, d_reserva.eve_firmado, d_reserva.eve_fecha_revision_asnef, 
                    d_reserva.eve_fecha_inicio, d_reserva.C_ownerid_value, d_reserva.eve_alta_reserva_prinex, d_reserva.eve_alta_suministros, d_reserva.eve_fecha_primera_entrega_doc_inquilino, 
                    d_reserva.C_eve_titular_principal_value, d_reserva.eve_dias_primera_entrega_doc_inquilino, d_reserva.eve_fecha_firma, d_reserva.timezoneruleversionnumber, 
                    d_reserva.statuscode, d_reserva.createdon, d_reserva.versionnumber, d_reserva.eve_aprobado_por_tecnico, d_reserva.eve_envio_notificacion_api, 
                    d_reserva.eve_enlacerecuperacionreserva, d_reserva.eve_fecha_aprobacion_reserva, d_reserva.eve_name, d_reserva.modifiedon, d_reserva.eve_comunidadtrastero1, 
                    d_reserva.eve_campanya_empresas, d_reserva.eve_fecha_aprobada_api, d_reserva.eve_rentaanio3, d_reserva.eve_mesesfianzatrastero1, d_reserva.C_eve_cotitular1_value, 
                    d_reserva.C_eve_cotitular2_value, d_reserva.eve_mesesgarantiatrastero1, d_reserva.eve_estado_adicional, d_reserva.C_eve_trastero_no_anejo_1_id_value, d_reserva.eve_reservaid, 
                    d_reserva.C_modifiedby_value, d_reserva.eve_dias_gestion_reserva, d_reserva.eve_importedelareservatrastero1, d_reserva.C_createdby_value, 
                    d_reserva.eve_periodicidadcomunidadtrastero1, d_reserva.eve_gdpr, d_reserva.eve_dias_aprobada_tecnico, d_reserva.eve_rentatrastero1, d_reserva.eve_comunicacionfinalizada, 
                    d_reserva.eve_fecha_aprobada_tecnico, d_reserva.eve_dias_revision_asnef, d_reserva.eve_dias_aprobada_api, d_reserva.C_eve_contrato_id_value, d_reserva.eve_operacionprinexgaraje1,
                    d_reserva.eve_documentacion_recibida_titular_1, d_reserva.eve_comentarios_internos, d_reserva.eve_tipoinquilinoprincipal, d_reserva.eve_periodicidadcomunidadgaraje1,
                    d_reserva.eve_idprinextitularprincipal, d_reserva.eve_devolucion_reserva_extraordinaria, d_reserva.eve_estado_documentacion_titular_3, d_reserva.eve_carencia_comunidad_inmuebleprincipal, 
                    d_reserva.eve_marca_horno_microondas, d_reserva.eve_carencia_ibi_trastero2, d_reserva.importsequencenumber, d_reserva.eve_garantiaadicionalgaraje2, d_reserva.eve_reserva_contabilizada,
                    d_reserva.eve_enseres_frigorficoconcongelador, d_reserva.eve_sumatorio_enseres, d_reserva.eve_periodicidadibigaraje1, d_reserva.eve_meses_corriente_pago_inmueble_principal, 
                    d_reserva.eve_documentacion_visada, d_reserva.eve_enseres_campanaextractora, d_reserva.eve_enseres_congelador, d_reserva.eve_mesesgarantiagaraje2, d_reserva.eve_comentarios_internos_api, 
                    d_reserva.eve_comentario_interno_seguros, d_reserva.eve_mesesgarantiatrastero2, d_reserva.eve_meses_apli, d_reserva.C_eve_promocioninmueble2_value, d_reserva.eve_agrupar_documentacion_total, 
                    d_reserva.eve_black_friday, d_reserva.eve_enseres_lavadora, d_reserva.eve_comentario_bonificacion_recomendacion, d_reserva.eve_carencia_ibi_garaje2, 
                    d_reserva.eve_importedelareservatrastero2, d_reserva.eve_idprinexcotitular2, d_reserva.eve_comercial_interno_asignado, d_reserva.eve_importe_oc_trastero_1,
                    d_reserva.eve_importebonificacioninmuebleprincipal, d_reserva.eve_importe_oc_trastero_2, d_reserva.C_eve_garaje_no_anejo_1_id_value, d_reserva.eve_ibigaraje1, 
                    d_reserva.eve_importe_oc_inmueble_principal, d_reserva.eve_mesesaplicaciongaraje2, d_reserva.eve_scoring3, d_reserva.eve_color_lavavajillas, 
                    d_reserva.eve_fecha_modificacion_cambio_vivienda, d_reserva.eve_reserva_procedente_cambio_vivienda, d_reserva.eve_comunidadgaraje2, d_reserva.C_eve_inmueble_prinex1_value,
                    d_reserva.eve_comentariocomercializadora10, d_reserva.eve_enseres_hornomicroondas, d_reserva.eve_importebonificaciontrastero2, d_reserva.eve_cargaprinex2,
                    d_reserva.eve_bonificacion_recomendacion_propietario, d_reserva.eve_ivainmuebleprincipal, d_reserva.eve_sujeto_permanencia_garaje_1, d_reserva.eve_enviar_recordatorio_seguro_hogar,
                    d_reserva.eve_sujeto_permanencia_garaje_2, d_reserva.C_eve_garaje_no_anejo_2_id_value, d_reserva.eve_codigo_promocion, d_reserva.eve_ivagaraje1, d_reserva.eve_color_campana,
                    d_reserva.eve_meses_permanencia_inmueble_principal, d_reserva.eve_enseres_induccion, d_reserva.eve_hasta_fin_contrato_trastero_1, d_reserva.eve_hasta_fin_contrato_trastero_2,
                    d_reserva.eve_mesesaplicacioninmuebleprincipal, d_reserva.eve_operacionprinexgaraje2, d_reserva.eve_documentacion_pendiente_titular_1, d_reserva.eve_documentacion_pendiente_titular_3,
                    d_reserva.eve_documentacion_pendiente_titular_2, d_reserva.eve_sujeto_a_impagos_garaje_1, d_reserva.eve_sujeto_a_impagos_garaje_2, d_reserva.eve_color_vitroceramica,
                    d_reserva.eve_enviar_contrato_reserva_firma, d_reserva.eve_mesesfianzagaraje1, d_reserva.eve_periodicidadcomunidad, d_reserva.eve_fecha_comentariofidere9,
                    d_reserva.eve_fecha_comentariofidere8, d_reserva.eve_color_microondas, d_reserva.eve_fecha_comentariofidere1, d_reserva.eve_marca_lavadora, d_reserva.eve_fecha_comentariofidere3,
                    d_reserva.eve_mesesfianza, d_reserva.eve_fecha_comentariofidere2, d_reserva.eve_fecha_comentariofidere5, d_reserva.eve_fecha_comentariofidere4, d_reserva.eve_fecha_comentariofidere7,
                    d_reserva.eve_fecha_comentariofidere6, d_reserva.eve_comentariocomercializadora8, d_reserva.eve_comentariocomercializadora9, d_reserva.eve_fuerza_seguridad,
                    d_reserva.eve_agrupar_documentacion, d_reserva.eve_documentacion_visada_titular_3, d_reserva.eve_comentariocomercializadora1, d_reserva.eve_comentariocomercializadora2,
                    d_reserva.eve_comentariocomercializadora3, d_reserva.eve_comentariocomercializadora4, d_reserva.eve_comentariocomercializadora5, d_reserva.eve_importedelareservainmuebleprincipal,
                    d_reserva.eve_comentariocomercializadora6, d_reserva.eve_comentariocomercializadora7, d_reserva.eve_plazotrastero2, d_reserva.eve_mesesgarantiainmuebleprincipal,
                    d_reserva.eve_periodicidadibiinmuebleprincipal, d_reserva.C_eve_trastero_no_anejo_2_id_value, d_reserva.eve_comentario_cambio_vivienda, d_reserva.eve_estado_interno_adicional,
                    d_reserva.eve_comentarios_visado_contratos, d_reserva.eve_ibitrastero2, d_reserva.eve_scoringanio3, d_reserva.C_owningteam_value, d_reserva.eve_ibigaraje2,
                    d_reserva.eve_enseres_congeladorintegrado, d_reserva.eve_usuario_envio_activacion_titulares, d_reserva.eve_rentatrastero2, d_reserva.eve_campana_revisada,
                    d_reserva.eve_rentainmuebleprincipal, d_reserva.C_eve_conviviente3_value, d_reserva.eve_mesesgarantia, d_reserva.eve_minusvalido_inmueble_principal,
                    d_reserva.eve_estado_cambio_vivienda_anterior, d_reserva.eve_usuario_doc_completa, d_reserva.eve_comentariofidere10, d_reserva.eve_motivo_condiciones_ko, 
                    d_reserva.eve_plazogaraje1, d_reserva.eve_ivagaraje2, d_reserva.eve_tipoinquilinocotitular1, d_reserva.eve_tipoinquilinocotitular2, d_reserva.eve_donde_ha_conocido,
                    d_reserva.C_modifiedonbehalfby_value, d_reserva.eve_comentario_campana, d_reserva.eve_tipo_campana, d_reserva.eve_carencia_comunidad_garaje2, d_reserva.eve_fecha_comentariofidere10,
                    d_reserva.eve_ibiinmuebleprincipal, d_reserva.eve_importe_oc_garaje_2, d_reserva.eve_porcentajeivatxt, d_reserva.eve_codigo_recuperacion, d_reserva.eve_importe_oc_garaje_1, 
                    d_reserva.eve_hasta_fin_contrato_garaje_2, d_reserva.eve_hasta_fin_contrato_garaje_1, d_reserva.eve_nombre_y_apellidos_propietario, d_reserva.eve_enseres_vitrocermicainduccion, 
                    d_reserva.eve_documento_identidad_propietario, d_reserva.eve_periodicidadcomunidadtrastero2, d_reserva.eve_estado_interno, d_reserva.eve_comentarios_nota_localizacion, 
                    d_reserva.eve_alquiler, d_reserva.eve_contabilidad, d_reserva.eve_mesesaplicaciontrastero2, d_reserva.eve_observaciones_contratacion, d_reserva.eve_iban, d_reserva.eve_comunidad,
                    d_reserva.eve_dias_rechazo_documentacion, d_reserva.eve_documentacion_recibida_titular_3, d_reserva.eve_fecha_envio_manual_contrato, d_reserva.eve_descuento_400, 
                    d_reserva.eve_campanya_navidad, d_reserva.eve_enseres_lavadora_secadora_integrada, d_reserva.eve_fecha_ultima_activacion_cambio_titulares, d_reserva.eve_minusvalido_garaje_2, 
                    d_reserva.eve_enseres_lavavajillas, d_reserva.eve_fecha_fijada_firma, d_reserva.eve_operacionprinextrastero1, d_reserva.eve_operacion_contrato, d_reserva.eve_nota_simple_titular_2,
                    d_reserva.eve_idprinexcotitular1, d_reserva.eve_nota_simple_titular_3, d_reserva.eve_devolucion_reserva_realizada, d_reserva.eve_nota_simple_titular_1, d_reserva.eve_bic,
                    d_reserva.eve_hasta_fin_contrato_inmueble_principal, d_reserva.eve_bonificacion_extra_fin_mes, d_reserva.eve_sujeto_a_impagos_inmueble_principal, d_reserva.eve_plazogaraje2,
                    d_reserva.eve_mesesfianzatrastero2, d_reserva.eve_documentacion_pendiente_conv_3, d_reserva.eve_marca_lavavajillas, d_reserva.eve_enseres_lavadorasecadora, 
                    d_reserva.eve_descripcion_rechazo, d_reserva.eve_ivatrastero1, d_reserva.eve_importebonificaciongaraje2, d_reserva.C_eve_promocioninmueble3_value, d_reserva.eve_fecha_fin_visado,
                    d_reserva.eve_ivaprinextrastero1, d_reserva.eve_cuentacorrientetitularprincipal, d_reserva.C_eve_inquilinoreferenciaid_value, d_reserva.C_eve_conviviente1_value, 
                    d_reserva.eve_tres_meses_gratis, d_reserva.eve_color_lavadora_secadora, d_reserva.eve_mesesfianzainmuebleprincipal, d_reserva.eve_importedelareservagaraje2,
                    d_reserva.eve_asientoanulacion, d_reserva.eve_importedelareservagaraje1, d_reserva.eve_cargaprinex1, d_reserva.eve_estado_cambio_titulares, d_reserva.utcconversiontimezonecode, 
                    d_reserva.eve_documentacion_recibida_conv_2, d_reserva.eve_documentacion_recibida_conv_3, d_reserva.eve_numero_rechazos_documentacion, d_reserva.eve_documentacion_recibida_conv_1,
                    d_reserva.eve_enseres_horno, d_reserva.eve_carencia_comunidad_trastero1, d_reserva.eve_comunidadinmuebleprincipal, d_reserva.eve_marca_horno, d_reserva.eve_importe_bon, 
                    d_reserva.eve_estado_cambio_vivienda, d_reserva.eve_comentarios_financiero, d_reserva.eve_color_induccion, d_reserva.eve_fecha_comentariocomercializadora8, 
                    d_reserva.eve_fecha_comentariocomercializadora9, d_reserva.eve_color_lavadora, d_reserva.eve_fecha_comentariocomercializadora4, d_reserva.eve_asientocontabilizado, 
                    d_reserva.eve_fecha_comentariocomercializadora5, d_reserva.eve_fecha_comentariocomercializadora6, d_reserva.eve_fecha_comentariocomercializadora7, d_reserva.eve_comunidadtrastero2,
                    d_reserva.eve_fecha_comentariocomercializadora1, d_reserva.eve_fecha_comentariocomercializadora2, d_reserva.eve_fecha_comentariocomercializadora3,
                    d_reserva.eve_documentacion_visada_titular_1, d_reserva.eve_meses_permanencia_trastero_2, d_reserva.eve_requiere_datos_aval, d_reserva.eve_periodicidadcomunidadgaraje2, 
                    d_reserva.eve_renta, d_reserva.eve_ivapersonajuridicainmuebleprincipal, d_reserva.eve_marca_microondas, d_reserva.eve_garantiaadicionalinmuebleprincipal,
                    d_reserva.eve_operacionprinextrastero2, d_reserva.eve_estado_documentacion_titular_1, d_reserva.eve_sanitarios, d_reserva.eve_usuario_envio_firma, 
                    d_reserva.eve_enlace_cambio_titulares, d_reserva.eve_plazoinmuebleprincipal, d_reserva.eve_estado_documentacion_conv_2, d_reserva.eve_requiere_doc_aval, 
                    d_reserva.eve_estado_documentacion_conv_3, d_reserva.eve_estado_documentacion_conv_1, d_reserva.eve_minusvalido_garaje_1, d_reserva.eve_fecha_contratacion,
                    d_reserva.eve_periodicidadibi, d_reserva.eve_activacion_cambio_titulares, d_reserva.eve_marca_lavadora_secadora, d_reserva.C_eve_avalista_value, d_reserva.eve_ivatrastero2, 
                    d_reserva.eve_fecha_rechazo_documentacion, d_reserva.eve_asnef, d_reserva.eve_ivaprinextrastero2, d_reserva.overriddencreatedon, d_reserva.eve_minusvalido_trastero_2,
                    d_reserva.eve_nombre_apellidos, d_reserva.eve_carencia_ibi_garaje1, d_reserva.eve_estado_bonificacion_propietario, d_reserva.eve_color_frigorifico, 
                    d_reserva.eve_sujeto_permanencia_inmueble_principal, d_reserva.eve_importe_devolucion_parcial, d_reserva.eve_scoring1, d_reserva.eve_documentacion_pendiente_conv_2, 
                    d_reserva.eve_enseres_frigorficoconcongeladorintegrado, d_reserva.eve_ivaprinexgaraje1, d_reserva.eve_importebonificaciongaraje1, d_reserva.eve_meses_permanencia_garaje_2,
                    d_reserva.eve_enseres_microondas, d_reserva.eve_fecha_fijada_firma_anterior, d_reserva.eve_direccion_postal_titulares, d_reserva.eve_carencia_comunidad_trastero2, 
                    d_reserva.eve_apellidostitular, d_reserva.C_eve_promocioninmueble1_value, d_reserva.eve_ivaprinexinmuebleprincipal, d_reserva.eve_fecha_fijada_firma_adicional,
                    d_reserva.eve_bonificacion, d_reserva.eve_notificacion_asnef, d_reserva.eve_datos_contacto_propietario, d_reserva.eve_carencia_ibi_inmuebleprincipal, 
                    d_reserva.eve_periodicidadcomunidadinmuebleprincipal, d_reserva.eve_operacionprinexinmuebleprincipal, d_reserva.eve_mesesfianzagaraje2, d_reserva.eve_no_facturar_firma_contrato,
                    d_reserva.eve_documentacion_visada_titular_2, d_reserva.eve_rentagaraje2, d_reserva.eve_comentariofidere9, d_reserva.eve_revision_nota_titular_2, 
                    d_reserva.eve_revision_nota_titular_3, d_reserva.eve_comentariofidere8, d_reserva.C_createdonbehalfby_value, d_reserva.eve_revision_nota_titular_1,
                    d_reserva.eve_comentariofidere5, d_reserva.eve_comentariofidere4, d_reserva.eve_numero_meses_corriente_de_pago_garaje_2, d_reserva.eve_comentariofidere7, 
                    d_reserva.eve_numero_meses_corriente_de_pago_garaje_1, d_reserva.eve_comentariofidere6, d_reserva.eve_estado_documentacion_titular_2, d_reserva.eve_comentariofidere1,
                    d_reserva.eve_comentariofidere3, d_reserva.eve_comentariofidere2, d_reserva.eve_fecha_comentariocomercializadora10, d_reserva.eve_color_horno_microondas, 
                    d_reserva.eve_documentacion_recibida_titular_2, d_reserva.eve_marca_campana, d_reserva.eve_garantiaadicionalgaraje1, d_reserva.eve_dias_rechazo_documentacion_1, 
                    d_reserva.eve_importecotitular1, d_reserva.eve_marca_vitrocermica, d_reserva.eve_importecotitular2, d_reserva.eve_enseres_lavavajillasintegrado, d_reserva.C_eve_conviviente2_value,
                    d_reserva.eve_plazotrastero1, d_reserva.eve_minusvalido_trastero_1, d_reserva.eve_scoringanio1, d_reserva.eve_carencia_ibi_trastero1, d_reserva.eve_bonificacion_recomendacion,
                    d_reserva.eve_numero_meses_corriente_de_pago_trastero_1, d_reserva.eve_numero_meses_corriente_de_pago_trastero_2, d_reserva.eve_leads_verano, d_reserva.eve_periodicidadibitrastero2, 
                    d_reserva.eve_documentacion_pendiente_conv_1, d_reserva.eve_periodicidadibigaraje2, d_reserva.eve_marca_congelador, d_reserva.eve_ivaprinexgaraje2, d_reserva.eve_color_horno, 
                    d_reserva.eve_mesesgarantiagaraje1, d_reserva.eve_codigo_inquilino, d_reserva.eve_importetitularprincipal, d_reserva.eve_garantiaadicionaltrastero2, d_reserva.eve_marca_induccion,
                    d_reserva.eve_meses_permanencia_garaje_1, d_reserva.eve_enseres_lavadoraintegrada, d_reserva.eve_ivapersonajuridicatrastero1, d_reserva.eve_bonificacion_campana,
                    d_reserva.eve_sujeto_a_impagos_trastero_2, d_reserva.eve_sujeto_a_impagos_trastero_1, d_reserva.eve_ivapersonajuridicagaraje1, d_reserva.eve_ivapersonajuridicatrastero2,
                    d_reserva.eve_numero_expediente, d_reserva.eve_ivapersonajuridicagaraje2, d_reserva.eve_nota_localizacion_titular_3, d_reserva.eve_mesesaplicaciongaraje1, 
                    d_reserva.eve_nota_localizacion_titular_2, d_reserva.eve_nota_localizacion_titular_1, d_reserva.eve_condiciones_firma_contrato, d_reserva.eve_comunidadgaraje1,
                    d_reserva.eve_marca_frigorifico, d_reserva.eve_carencia_comunidad_garaje1, d_reserva.eve_titular_relevante_1, d_reserva.eve_fecha_envio_recordatorio_seguro, 
                    d_reserva.eve_color_congelador, d_reserva.eve_vivienda_contingencia_rotacion, d_reserva.eve_rentagaraje1, d_reserva.eve_promocion_200, d_reserva.C_eve_promocioninmueble5_value,
                    d_reserva.eve_motivo_cancelacion_reserva, d_reserva.eve_comentarios_documentacion_viv_protegidas, d_reserva.eve_sujeto_permanencia_trastero_2,
                    d_reserva.eve_sujeto_permanencia_trastero_1, d_reserva.eve_mesesaplicaciontrastero1, d_reserva.eve_cotitular_relevante_2, d_reserva.eve_meses_permanencia_trastero_1, 
                    d_reserva.eve_cotitular_relevante_1);
                respuesta = true;
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                logger.Error(e.InnerException);
                logger.Error(e.StackTrace);
            }
            return respuesta;
        }
        private void UpsertInmuebleAnejoComercial(IEnumerable<string> datos)
        {
            Inmueble_anejo_comercial iac = null;
            Dynamics_InmuebleAnejoComercial d_iac = null;
            int total = 0;

            if (null != datos)
            {
                var lotes = Utils.Batch<string>(datos, 100);
                int num_lotes = lotes.Count<IEnumerable<string>>();
                logger.Info("UpsertInmuebleAnejoComercial. Número de lotes: " + num_lotes);
                int contador_lotes = 0;

                foreach (var lote in lotes)
                {
                    using (var db = new InconcertOnline2())
                    {
                        foreach (var item in lote)
                        {
                            iac = JsonConvert.DeserializeObject<Inmueble_anejo_comercial>(item, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                            d_iac = iac.Convertir2BD();
                            if (LlamadaSPSaveInmuebleAnejoComercial(db, d_iac))
                            {
                                total++;
                            }
                        }
                    }
                    contador_lotes++;
                    logger.Info($"Lote {contador_lotes} Registros procesados {total}");
                }
            }
            logger.Info($"InmuebleAnejoComerciales procesados del CRM {total} de {datos.Count()}");
        }
        private bool LlamadaSPSaveInmuebleAnejoComercial(InconcertOnline2 db, Dynamics_InmuebleAnejoComercial d_iac)
        {
            bool respuesta = false;
            try
            {
                db.Dynamics_SaveInmuebleAnejoComercial(d_iac.odataetag, d_iac.C_owningbusinessunit_value, d_iac.eve_relacionvigente, 
                    d_iac.C_ownerid_value, d_iac.statuscode, d_iac.importsequencenumber, d_iac.eve_name, d_iac.C_eve_codigoinmuebleprincipalid_value,
                    d_iac.C_eve_codigoinmuebleanejocomercialid_value, d_iac.modifiedon, d_iac.C_owninguser_value, d_iac.C_modifiedby_value,
                    d_iac.versionnumber, d_iac.eve_inmuebleanejoscomercialid, d_iac.createdon, d_iac.statecode, d_iac.C_createdby_value,
                    d_iac.overriddencreatedon, d_iac.eve_idanejocomercial, d_iac.emailaddress, d_iac.C_modifiedonbehalfby_value, d_iac.eve_comentarios_anejos_comerciales,
                    d_iac.C_eve_codigopromocioninmuebleprincid_value, d_iac.utcconversiontimezonecode, d_iac.eve_idanejocomercial0, d_iac.C_createdonbehalfby_value,
                    d_iac.C_eve_codigopromocionanejocomercialid_value, d_iac.C_owningteam_value, d_iac.timezoneruleversionnumber);
                respuesta = true;
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                logger.Error(e.InnerException);
                logger.Error(e.StackTrace);
            }
            return respuesta;
        }
        private void UpsertParticipacionContratos(IEnumerable<string> datos)
        {
            Participacion_Contrato pc = null;
            Dynamics_ParticipacionesContratos d_pc = null;
            int total = 0;

            if (null != datos)
            {
                var lotes = Utils.Batch<string>(datos, 100);
                int num_lotes = lotes.Count<IEnumerable<string>>();
                logger.Info("UpsertParticipacionContratos. Número de lotes: " + num_lotes);
                int contador_lotes = 0;
                foreach (var lote in lotes)
                {
                    using (var db = new InconcertOnline2())
                    {
                        foreach (var item in lote)
                        {
                            pc = JsonConvert.DeserializeObject<Participacion_Contrato>(item, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                            d_pc = pc.Convertir2BD();
                            if (LlamadaSPSaveParticipacionContrato(db, d_pc))
                            {
                                total++;
                            }
                        }
                    }
                    contador_lotes++;
                    logger.Info($"Lote {contador_lotes} Registros procesados {total}");
                }
            }
            logger.Info($"ParticipacionContratos procesados del CRM {total} de {datos.Count()}");
        }
        private bool LlamadaSPSaveParticipacionContrato(InconcertOnline2 db, Dynamics_ParticipacionesContratos d_pc)
        {
            bool respuesta = false;
            try
            {
                db.Dynamics_SaveParticipacionContratos(d_pc.OdataEtag, d_pc.statecode, d_pc.C_eve_cuentaparticipacioncontratoid_value, d_pc.eve_apellidos,
                    d_pc.eve_fechamodificacionregistro, d_pc.eve_fechaaltaparticipante, d_pc.createdon, d_pc.eve_idcontratoprinex, d_pc.eve_porcentajeparticipaciontesta,
                    d_pc.eve_visualizacionportal, d_pc.C_ownerid_value, d_pc.eve_codigoinquilino, d_pc.modifiedon, d_pc.versionnumber, d_pc.timezoneruleversionnumber,
                    d_pc.eve_idinquilinoprinex, d_pc.C_modifiedby_value, d_pc.eve_telefono3, d_pc.eve_nombre, d_pc.eve_nombreinquilino, d_pc.eve_idparticipacioncontratoprinex,
                    d_pc.C_eve_contratoparticipacioncontrato_id_value, d_pc.eve_nif_nie, d_pc.C_createdby_value, d_pc.eve_participacioncontratoid, d_pc.C_owningbusinessunit_value,
                    d_pc.C_owninguser_value, d_pc.eve_codigocontrato, d_pc.eve_nombreapellidos, d_pc.overriddencreatedon, d_pc.importsequencenumber,
                    d_pc.eve_fechabajaparticipante, d_pc.C_eve_contratoparticipacioncontratoid_value, d_pc.C_modifiedonbehalfby_value, d_pc.eve_email, d_pc.utcconversiontimezonecode,
                    d_pc.eve_porcentajeparticipacion, d_pc.eve_telefono1, d_pc.C_createdonbehalfby_value, d_pc.eve_telefono2, d_pc.C_eve_contactoparticipacioncontratoid_value,
                    d_pc.C_owningteam_value);
                respuesta = true;
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                logger.Error(e.InnerException);
                logger.Error(e.StackTrace);
            }
            return respuesta;
        }
        private void UpsertRenovaciones(IEnumerable<string> datos)
        {
            Renovacion renovacion = null;
            Dynamics_Renovaciones d_renovacion = null;
            int total = 0;

            if (null != datos)
            {
                var lotes = Utils.Batch<string>(datos, 100);
                int num_lotes = lotes.Count<IEnumerable<string>>();
                logger.Info("UpsertRenovaciones. Número de lotes: " + num_lotes);
                int contador_lotes = 0;

                foreach (var lote in lotes)
                {
                    using (var db = new InconcertOnline2())
                    {
                        foreach (var item in lote)
                        {
                            renovacion = JsonConvert.DeserializeObject<Renovacion>(item, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                            d_renovacion = renovacion.Convertir2BD();
                            if (LlamadaSPSaveRenovacion(db, d_renovacion))
                            {
                                total++;
                            }
                        }
                    }
                    contador_lotes++;
                    logger.Info($"Lote {contador_lotes} Registros procesados {total}");
                }
            }
            logger.Info($"Renovaciones procesadas del CRM {total} de {datos.Count()}");
        }
        private bool LlamadaSPSaveRenovacion(InconcertOnline2 db, Dynamics_Renovaciones d_renovacion)
        {
            bool respuesta = false;
            try
            {
                db.Dynamics_SaveRenovacions(d_renovacion.OdataEtag, d_renovacion.eve_renta_neta_y1, d_renovacion.eve_inquilino_2, 
                    d_renovacion.eve_envio_burof_legal, d_renovacion.eve_nif_inquilino_2, d_renovacion.eve_nif_inquilino_3, d_renovacion.modifiedon, 
                    d_renovacion.C_eve_contratorenovacionid_value, d_renovacion.C_owninguser_value, d_renovacion.eve_cod_postal, d_renovacion.eve_tipo_envio_renovacion, 
                    d_renovacion.eve_ibi, d_renovacion.eve_recibidoburofaxopcioncompra, d_renovacion.eve_renovacionid, d_renovacion.eve_descripcionpromocion, 
                    d_renovacion.eve_activacionburofaxrecibido, d_renovacion.C_ownerid_value, d_renovacion.eve_nombre, d_renovacion.eve_poblacion, 
                    d_renovacion.C_eve_promocinrenovacinid_value, d_renovacion.eve_inquilino_1, d_renovacion.eve_nif_inquilino_4, d_renovacion.eve_fecha_envio_comunicacion, 
                    d_renovacion.eve_solicitarenvionotificaciones, d_renovacion.timezoneruleversionnumber, d_renovacion.eve_com_teo_12, d_renovacion.createdon, 
                    d_renovacion.versionnumber, d_renovacion.eve_tipo_comunicacion, d_renovacion.eve_nif_inquilino_1, d_renovacion.eve_periodo_carencia_ibi, 
                    d_renovacion.eve_telefono2, d_renovacion.eve_tipo_plantilla, d_renovacion.eve_fecha_fin_prorroga, d_renovacion.eve_solicitarenviorenovacionsinpeticion, 
                    d_renovacion.eve_periodo_carencia_comunidad, d_renovacion.C_eve_sociedadid_value, d_renovacion.eve_codigo_inmueble_principal, d_renovacion.eve_mes_facturacion_fidelizacion, 
                    d_renovacion.eve_fecha_fin_contrato, d_renovacion.C_modifiedby_value, d_renovacion.eve_dif_fianza, d_renovacion.eve_email1, d_renovacion.C_createdby_value, 
                    d_renovacion.eve_numeroparticipantes, d_renovacion.statuscode, d_renovacion.eve_fechafirma, d_renovacion.eve_envioemailrenovacionsinpeticion, 
                    d_renovacion.eve_fecha_inicio_contrato, d_renovacion.eve_inquilino_3, d_renovacion.eve_envioemailrenovacionopcioncompra, d_renovacion.eve_inquilino_4, 
                    d_renovacion.eve_ubicacion, d_renovacion.statecode, d_renovacion.C_owningbusinessunit_value, d_renovacion.eve_fianza, d_renovacion.eve_supuesto12, 
                    d_renovacion.eve_camponumerico3, d_renovacion.eve_apoderado_1, d_renovacion.eve_nombreapellidos_c2, d_renovacion.eve_apoderado_2, d_renovacion.eve_apoderado_3, 
                    d_renovacion.eve_apoderado_4, d_renovacion.eve_apoderado_5, d_renovacion.eve_renta_sin_iva_y2_gar1_12, d_renovacion.eve_garantia, d_renovacion.eve_ibi_men_y3, 
                    d_renovacion.eve_nif_c5, d_renovacion.eve_supuesto10, d_renovacion.eve_com_lista_gar2, d_renovacion.eve_total_impagados, d_renovacion.importsequencenumber, 
                    d_renovacion.C_eve_contratorenovacinid_value, d_renovacion.eve_renta_sin_iva_y1_gar3_12, d_renovacion.eve_camponumerico5, d_renovacion.utcconversiontimezonecode, 
                    d_renovacion.eve_parrafo_garantia, d_renovacion.overriddencreatedon, d_renovacion.eve_bonificacionmensual3, d_renovacion.eve_rentas_ant_prop_50por, 
                    d_renovacion.eve_camponumerico2, d_renovacion.eve_enlaceportalfidere, d_renovacion.eve_bonificaciontexto5, d_renovacion.eve_bonificacionmensual6, 
                    d_renovacion.eve_n_garaje_1, d_renovacion.eve_telefonofidere, d_renovacion.eve_nombreapellidos_c5, d_renovacion.eve_renta_sin_iva_y1_gar2_12, 
                    d_renovacion.eve_ibi_men_y2, d_renovacion.eve_dni_aval_1, d_renovacion.eve_com_lista_gar1, d_renovacion.eve_renta_neta_y5_12, d_renovacion.eve_telefono5, 
                    d_renovacion.eve_telefono4, d_renovacion.eve_renta_sin_iva_y3_12, d_renovacion.eve_renta_total_ga3, d_renovacion.eve_telefono1, d_renovacion.eve_renta_total_ga2, 
                    d_renovacion.eve_renta_total_ga1, d_renovacion.eve_telefono3, d_renovacion.eve_rta_fidere_50por, d_renovacion.eve_fianza_gar2, d_renovacion.eve_fianza_gar3, 
                    d_renovacion.processid, d_renovacion.eve_renta_sin_iva_y1_gar1_12, d_renovacion.eve_fianza_gar1, d_renovacion.eve_bonificacionmensual7, d_renovacion.eve_duraciondelcontrato, 
                    d_renovacion.eve_com_lista_gar3, d_renovacion.eve_ryc_prop_y4, d_renovacion.eve_nuevo_precio_oc, d_renovacion.eve_nombreapellidos_c4, d_renovacion.eve_fecha_inicio_renovacion, 
                    d_renovacion.eve_com_lista, d_renovacion.traversedpath, d_renovacion.eve_ryc_prop_y1, d_renovacion.eve_ryc_prop_y2, d_renovacion.eve_ryc_prop_y3, d_renovacion.eve_ibi_men_y1,
                    d_renovacion.eve_bonificaciontexto1, d_renovacion.eve_numeroconciertofianza, d_renovacion.eve_cif, d_renovacion.eve_bonificaciontexto4, d_renovacion.eve_bonificacionmensual1, 
                    d_renovacion.eve_anejo_4, d_renovacion.eve_anejo_5, d_renovacion.eve_anejo_2, d_renovacion.eve_m2_utiles_anejo_5, d_renovacion.eve_anejo_3, d_renovacion.eve_m2_utiles_anejo_4, 
                    d_renovacion.eve_anejo_1, d_renovacion.eve_m2_utiles_anejo_3, d_renovacion.eve_com_teo_gar2_12, d_renovacion.eve_m2_utiles_anejo_2, d_renovacion.eve_m2_utiles_anejo_1, 
                    d_renovacion.eve_campofecha1, d_renovacion.eve_campofecha3, d_renovacion.eve_campofecha2, d_renovacion.eve_campofecha5, d_renovacion.eve_campofecha4, d_renovacion.eve_dni_aval_2, 
                    d_renovacion.eve_bonificacionmensual4, d_renovacion.eve_referenciacatastral, d_renovacion.eve_rentas_ant_prop, d_renovacion.eve_fecharenovacion, d_renovacion.eve_com_teo_gar3_12, 
                    d_renovacion.eve_parentesco_c1, d_renovacion.eve_parentesco_c2, d_renovacion.eve_dif_fianza_gar1, d_renovacion.eve_bonificaciontexto2, d_renovacion.C_owningteam_value, 
                    d_renovacion.eve_parentesco_c3, d_renovacion.eve_dif_fianza_gar2, d_renovacion.eve_renta_sin_iva_y4, d_renovacion.eve_parentesco_c4, d_renovacion.eve_dif_fianza_gar3, 
                    d_renovacion.eve_numero_finca, d_renovacion.eve_parentesco_c5, d_renovacion.eve_estimacion_renta, d_renovacion.eve_parrafo_aval, d_renovacion.eve_renta_neta_y4_12, 
                    d_renovacion.eve_rentaycomunidad_y3, d_renovacion.eve_renta_neta_y5, d_renovacion.eve_renta_neta_y4, d_renovacion.eve_renta_sin_iva_y1, d_renovacion.C_modifiedonbehalfby_value, 
                    d_renovacion.eve_m2_utiles_ga_3, d_renovacion.eve_renta_neta_y3, d_renovacion.eve_renta_neta_y2, d_renovacion.eve_bonificacionmensual5, d_renovacion.eve_com_lista_gar2_12, 
                    d_renovacion.eve_fecha_fin_novacion, d_renovacion.eve_rentaycomunidad_y4, d_renovacion.eve_inquilino_5, d_renovacion.eve_m2_utiles_ga_4, d_renovacion.eve_com_teo_gar1_12, 
                    d_renovacion.eve_rentas_100por, d_renovacion.eve_rentaycomunidad_y1, d_renovacion.C_createdonbehalfby_value, d_renovacion.eve_bonificaciontexto3, d_renovacion.eve_m2_utiles_ga_1, 
                    d_renovacion.eve_renta_neta_y1_12, d_renovacion.eve_rentas_fidere, d_renovacion.eve_observacion_renovacion, d_renovacion.eve_rentaycomunidad_y2, d_renovacion.eve_fechanacimiento_c5, 
                    d_renovacion.eve_m2_utiles_ga_2, d_renovacion.eve_renta_sin_iva_y4_12, d_renovacion.eve_registro, d_renovacion.eve_renta_sin_iva_y4_gar3_12, d_renovacion.eve_m2_utiles_vi, 
                    d_renovacion.eve_promocion, d_renovacion.eve_email4, d_renovacion.eve_com_lista_12, d_renovacion.eve_precio_max_califi, d_renovacion.eve_nif_inquilino_5, 
                    d_renovacion.eve_renta_sin_iva_y4_gar2, d_renovacion.eve_renta_sin_iva_y4_gar3, d_renovacion.eve_renta_sin_iva_y4_gar1, d_renovacion.eve_dif_precio_max_50por, 
                    d_renovacion.eve_renta_sin_iva_y4_gar2_12, d_renovacion.eve_email2, d_renovacion.eve_m2_utiles_tr_2, d_renovacion.eve_renta_sin_iva_y3, d_renovacion.eve_fechanacimiento_c4, 
                    d_renovacion.eve_renta_sin_iva_y2_gar2, d_renovacion.eve_renta_sin_iva_y2_gar3, d_renovacion.eve_anioibi2, d_renovacion.eve_renta_sin_iva_y2_gar1, 
                    d_renovacion.eve_renta_neta_y3_12, d_renovacion.eve_nif_c3, d_renovacion.eve_email5, d_renovacion.eve_m2_utiles_tr_1, d_renovacion.eve_fechanacimiento_c1, 
                    d_renovacion.eve_gsqm, d_renovacion.eve_renta_sin_iva_y4_gar1_12, d_renovacion.eve_renta_sin_iva_y3_gar3_12, d_renovacion.eve_supuesto7, d_renovacion.eve_supuesto6, 
                    d_renovacion.eve_supuesto5, d_renovacion.eve_supuesto4, d_renovacion.eve_supuesto3, d_renovacion.eve_renta_sin_iva_y1_gar2, d_renovacion.eve_supuesto2, 
                    d_renovacion.eve_nif_apoderado_1, d_renovacion.eve_supuesto1, d_renovacion.eve_renta_sin_iva_y1_gar3, d_renovacion.eve_email3, d_renovacion.eve_nif_apoderado_2,
                    d_renovacion.eve_nif_apoderado_3, d_renovacion.eve_renta_sin_iva_y1_gar1, d_renovacion.eve_m2_utiles_tr_3, d_renovacion.eve_nif_apoderado_4, d_renovacion.eve_fechanacimiento_c3, 
                    d_renovacion.eve_renta_sin_iva_y2, d_renovacion.eve_nif_apoderado_5, d_renovacion.eve_supuesto9, d_renovacion.eve_aval_personal_2, d_renovacion.eve_anioibi1, 
                    d_renovacion.eve_supuesto8, d_renovacion.eve_idcontrato, d_renovacion.eve_aval_personal_1, d_renovacion.eve_operacion, d_renovacion.eve_mesdeactualizacion, 
                    d_renovacion.C_stageid_value, d_renovacion.eve_n_garaje_2, d_renovacion.eve_renta_sin_iva_y1_12, d_renovacion.eve_n_trastero_2, d_renovacion.eve_com_lista_gar3_12, 
                    d_renovacion.eve_renta_sin_iva_y3_gar2_12, d_renovacion.eve_fecha_fin_renovacion, d_renovacion.eve_emailcontactofidere, d_renovacion.eve_coef_par, 
                    d_renovacion.eve_renta_sin_iva_y2_12, d_renovacion.eve_nif_c2, d_renovacion.eve_n_garaje_4, d_renovacion.eve_campotexto5, d_renovacion.eve_renta_sin_iva_y2_gar3_12, 
                    d_renovacion.eve_fechanacimiento_c2, d_renovacion.eve_campotexto4, d_renovacion.eve_supuesto11, d_renovacion.eve_campotexto1, d_renovacion.eve_campotexto3, 
                    d_renovacion.eve_camponumerico4, d_renovacion.eve_campotexto2, d_renovacion.eve_renta_sin_iva_y3_gar1_12, d_renovacion.eve_bonificaciontotal, d_renovacion.eve_com_lista_gar1_12, 
                    d_renovacion.eve_nif_c1, d_renovacion.eve_n_garaje_3, d_renovacion.eve_nombreapellidos_c3, d_renovacion.eve_renta_neta_y2_12, d_renovacion.eve_n_trastero_3, 
                    d_renovacion.eve_camponumerico1, d_renovacion.eve_fechamodificacionregistro, d_renovacion.eve_nif_c4, d_renovacion.eve_rentas_desc_50por, d_renovacion.eve_renta_sin_iva_y3_gar2, 
                    d_renovacion.eve_renta_sin_iva_y2_gar2_12, d_renovacion.eve_renta_sin_iva_y3_gar3, d_renovacion.eve_renta_sin_iva_y3_gar1, d_renovacion.eve_anyos_novacion, 
                    d_renovacion.eve_bonificacionmensual2, d_renovacion.eve_nombreapellidos_c1, d_renovacion.eve_n_trastero_1);
                respuesta = true;
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                logger.Error(e.InnerException);
                logger.Error(e.StackTrace);
            }
            return respuesta;
        }
        private void UpsertPromociones(IEnumerable<string> datos)
        {
            Promocion promocion = null;
            Dynamics_Promociones d_promocion = null;
            List<Dynamics_Promociones> promocionesDynamics = new List<Dynamics_Promociones>();
            List<Dynamics_Promociones> promocionesLocalesParaBorrar = new List<Dynamics_Promociones>();
            List<Dynamics_Promociones> promocionesenBD = null;

            bool borrado = false;
            int total = 0;

            if (null != datos)
            {
                var lotes = Utils.Batch<string>(datos, 100);
                int num_lotes = lotes.Count<IEnumerable<string>>();
                logger.Info("UpsertPromociones. Número de lotes: " + num_lotes);
                int contador_lotes = 0;

                foreach (var lote in lotes)
                {
                    using (var db = new InconcertOnline2())
                    {
                        foreach (var item in lote)
                        {
                            promocion = JsonConvert.DeserializeObject<Promocion>(item, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                            d_promocion = promocion.Convertir2BD();
                            d_promocion.data_dynamics = item;
                            promocionesDynamics.Add(d_promocion);
                            if (LlamadaSPSavePromocion(db, d_promocion))
                            {
                                total++;
                            }
                        }
                        promocionesenBD = db.Dynamics_Promociones.ToList();
                    }
                    contador_lotes++;
                    logger.Info($"Lote {contador_lotes} Registros procesados {total}");
                }

                promocionesLocalesParaBorrar = CompararPromocionesLocalesDynamics(promocionesDynamics, promocionesenBD);
                if (promocionesLocalesParaBorrar.Count > 0)
                {
                    borrado = BorrarPromociones(promocionesLocalesParaBorrar);
                }
            }
            logger.Info($"Promociones procesadas del CRM {total} de {datos.Count()}");
            if (borrado)
            {
                logger.Info($"Promociones locales que se han borrado: {promocionesLocalesParaBorrar.Count()}");
            }

            //TODO las promociones se insertan en la tabla Category
            bool control = SyncTablaCategoriesConPromociones(promocionesDynamics);
        }
        
        private bool BorrarPromociones(List<Dynamics_Promociones> promocionesLocalesParaBorrar)
        {
            logger.Info("UpsertPromociones. BorrarPromociones: " + promocionesLocalesParaBorrar.Count);
            bool resultado = false;
            int total = promocionesLocalesParaBorrar.Count;
            int contador = 0;

            using (var db = new InconcertOnline2())
            {
                foreach (var item in promocionesLocalesParaBorrar)
                {
                    try
                    {
                        var p = db.Dynamics_Promociones.ToList().Find(x => x.eve_promocionid == item.eve_promocionid);
                        if (null != p)
                        {
                            db.Dynamics_Promociones.Remove(p);
                            contador++;
                        }
                        
                    }
                    catch (Exception e)
                    {
                        logger.Error($"Excepcion al borrar las promociones sobrantes: {e.Message}");
                    }
                }
                db.SaveChanges();
            }
            if (total == contador)
            {
                resultado = true;
            }
            return resultado;

        }

        private List<Dynamics_Promociones> CompararPromocionesLocalesDynamics(List<Dynamics_Promociones> promocionesDynamics, List<Dynamics_Promociones> promocionesBD)
        {
            List<Dynamics_Promociones> aborrar = new List<Dynamics_Promociones>();
            logger.Info("CompararPromocionesLocalesDynamics: Dynamics = " + promocionesDynamics.Count + " VS BBDD = " + promocionesBD.Count);
            foreach (var item in promocionesBD)
            {
                var promo = promocionesDynamics.Find(x => x.eve_promocionid == item.eve_promocionid);
                if (null == promo)
                {
                    aborrar.Add(item);
                }
            }
            return aborrar;
        }

        private bool SyncTablaCategoriesConPromociones(List<Dynamics_Promociones> promociones)
        {
            logger.Info("SyncTablaCategoriesConPromociones");
            bool resultado = false;
            DateTime ahora = DateTime.UtcNow;
            if (null != promociones)
            {
                List<Dynamics_Promociones> promocionesProcesadas = new List<Dynamics_Promociones>();
                int numPromociones = promociones.Count;
                //Buscar en BD las promociones ya introducidas en la tabla de Category
                using (InconcertOnline2 db = new InconcertOnline2())
                {
                    //Comparar con las introducidas en BD en la tabla Dynamics_promociones
                    //Si ya existen no hacer nada, si no existe añadir, y si sobran borrarlas.
                    foreach (Dynamics_Promociones promocion in promociones)
                    {
                        // Buscar si la promoción existe en la tabla Category
                        Category cat = db.Categories.Find("Testa", "productCategory", promocion.eve_promocionid);
                        if (null != cat)
                        {
                            if (!cat.Description.Equals(promocion.eve_nombre))
                            {
                                cat.Description = promocion.eve_nombre;
                                cat.LastModifiedDate = ahora;
                                cat.LastModifiedByUserId = "admin";
                                cat.TimeStamp = ahora;
                            }
                            //Añadir a la lista de promociones procesadas
                            promocionesProcesadas.Add(promocion);
                        }
                        else
                        {

                            //Insertar en la tabla Category
                            Category catNew = new Category();
                            catNew.Instance = "Testa";
                            catNew.Group = "productCategory";
                            catNew.ID = promocion.eve_promocionid;
                            catNew.Description = promocion.eve_nombre;
                            catNew.ReadOnly = false;
                            catNew.CreatedDate = ahora;
                            catNew.CreatedByUserId = "admin";
                            catNew.LastModifiedDate = ahora;
                            catNew.LastModifiedByUserId = "admin";
                            catNew.Deleted = false;
                            catNew.TimeStamp = ahora;

                            db.Categories.Add(catNew);

                            //Añadir a la lista de promociones procesadas
                            promocionesProcesadas.Add(promocion);
                        }
                    }
                    int cambios = db.SaveChanges();
                    if (cambios > 0)
                    {
                        logger.Debug("Añadir Categorias, número de cambios en la tabla Category " + cambios);
                        resultado = true;
                    }

                    //Comprobar las entradas en Category que tienen que ver con las promociones y cotejar, si sobran borrar... no deberían faltar
                    List<Category> categoriasenBD = db.Categories.Where(b => b.Instance == "Testa" && b.Group == "productCategory").ToList();
                    ahora = DateTime.UtcNow;
                    foreach (Category categoria in categoriasenBD)
                    {
                        Dynamics_Promociones promocion = promociones.Find(x => x.eve_promocionid == categoria.ID);
                        if (null == promocion)
                        {
                            categoria.Deleted = true;
                            categoria.LastModifiedByUserId = "admin";
                            categoria.LastModifiedDate = ahora;
                            categoria.TimeStamp = ahora;
                        }
                    }
                    cambios = db.SaveChanges();
                    if (cambios > 0)
                    {
                        logger.Debug("Eliminar categorias si no hay promoción " + cambios);
                        resultado = true;
                    }
                }

            }
            return resultado;
        }

        private bool LlamadaSPSavePromocion(InconcertOnline2 db, Dynamics_Promociones d_promocion)
        {
            bool respuesta = false;
            try
            {
                db.Dynamics_SavePromocions(d_promocion.OdataEtag, d_promocion.eve_provincia, d_promocion.eve_carencia_garaje_comunidad, d_promocion.eve_protocolo_suministros, 
                    d_promocion.eve_direccion, d_promocion.eve_titularidad_ccpp, d_promocion.eve_calendario_citas_personalizado, d_promocion.eve_restringir_envio_notificaciones_impagados, 
                    d_promocion.eve_ivatrastero, d_promocion.eve_restringir_acceso_portal, d_promocion.eve_ms_flow, d_promocion.eve_promocionid, 
                    d_promocion.eve_activar_restriccion_visualizacion_portal, d_promocion.eve_carencia_vivienda_comunidad, d_promocion.eve_carencia_trastero_comunidad, 
                    d_promocion.eve_estado_promocion, d_promocion.C_ownerid_value, d_promocion.createdon, d_promocion.eve_nombre, d_promocion.eve_poblacion, 
                    d_promocion.eve_habilitarrenovacion_opcioncompra, d_promocion.eve_equipo_deuda, d_promocion.eve_ivaviviendapersonajuridica, d_promocion.eve_equipo_comercial, 
                    d_promocion.eve_reserva_vi, d_promocion.eve_activar_restriccion_acceso_portal, d_promocion.statuscode, d_promocion.versionnumber, d_promocion.emailaddress, 
                    d_promocion.eve_carencia_garaje_ibi, d_promocion.eve_descripcionpromocion, d_promocion.eve_ivavivienda, d_promocion.eve_visualizacion_portal, 
                    d_promocion.eve_ivagarajepersonajuridica, d_promocion.eve_ivagaraje, d_promocion.eve_sociedad_origen, d_promocion.modifiedon, d_promocion.eve_equipo_comercializadora, 
                    d_promocion.eve_asset_manager, d_promocion.C_eve_sociedadid_value, d_promocion.eve_nombre_comercializadora, d_promocion.C_modifiedby_value, 
                    d_promocion.eve_reserva_ga_tr, d_promocion.C_createdby_value, d_promocion.eve_calendario_citas_personalizado_renovacion, d_promocion.C_owninguser_value,
                    d_promocion.eve_plazovivienda, d_promocion.eve_actualizacionpropietario, d_promocion.eve_no_anejos_disponibles, d_promocion.eve__carencia_vivienda_ibi, 
                    d_promocion.eve_nombre_comercializadora_reservas, d_promocion.eve_carencia_trastero_ibi, d_promocion.statecode, d_promocion.C_owningbusinessunit_value, 
                    d_promocion.eve_ivatrasteropersonajuridica, d_promocion.eve_tipologia_viviendas, d_promocion.eve_plazogaraje, d_promocion.eve_agua_fria, 
                    d_promocion.eve_plantillaborradorviviendanovacion, d_promocion.eve_email_gestor_acompanamiento_suministroiii, d_promocion.eve_email_llaves_3, 
                    d_promocion.C_eve_responsablefirmaid_value, d_promocion.eve_delegacion, d_promocion.importsequencenumber, d_promocion.eve_telefono_comercializadora_reservas, 
                    d_promocion.eve_parking_bicicleta, d_promocion.utcconversiontimezonecode, d_promocion.overriddencreatedon, d_promocion.eve_email_comercializadora_reservasiii,
                    d_promocion.eve_mensajerestriccionfactura, d_promocion.eve_email_validacion_seguros_ii, d_promocion.eve_padel, d_promocion.eve_electricidad, 
                    d_promocion.eve_email_centro_custodio_3, d_promocion.eve_email_comercializadora_reservas, d_promocion.eve_email_centro_custodio_2, 
                    d_promocion.eve_email_centro_custodio_1, d_promocion.eve_sala_social, d_promocion.eve_email_llaves_2, d_promocion.eve_email_validacion_contrato_ii, 
                    d_promocion.eve_ascensor, d_promocion.eve_mailbox_lockerbox, d_promocion.eve_email_llaves_1, d_promocion.eve_direccion_postal_api, 
                    d_promocion.eve_email_gestor_energetico_ii, d_promocion.eve_portfolio, d_promocion.eve_ivalocalpersonajuridica, d_promocion.eve_plantillarenovacionfirma,
                    d_promocion.eve_vigilancia_24h, d_promocion.eve_plantillarenovacionviviendanovacion, d_promocion.eve_email_gestor_energetico_i, d_promocion.eve_gimnasio, 
                    d_promocion.timezoneruleversionnumber, d_promocion.eve_email_validacion_seguros_i, d_promocion.C_owningteam_value, d_promocion.eve_agua_caliente,
                    d_promocion.eve_portero_fisico, d_promocion.eve_carencia_local_ibi, d_promocion.C_modifiedonbehalfby_value, d_promocion.eve_plantillaborradorgaraje, 
                    d_promocion.eve_nombre_gestor_energetico, d_promocion.eve_jardin, d_promocion.eve_ivalocal, d_promocion.eve_email_gestor_acompanamiento_suministros_i, 
                    d_promocion.eve_email_validacion_contrato_iii, d_promocion.eve_zona_infantil, d_promocion.eve_plantillarenovacion, d_promocion.C_createdonbehalfby_value, 
                    d_promocion.eve_telefono_comercializadora, d_promocion.eve_carencia_local_comunidad, d_promocion.eve_restringir_envio_notificaciones, 
                    d_promocion.eve_agua_caliente_placas_solares, d_promocion.eve_acceso_fibra_optica, d_promocion.eve_reserva_lo, d_promocion.eve_acepta_mascotas, 
                    d_promocion.eve_equipo_financiero, d_promocion.eve_zona_juegos, d_promocion.eve_certificado_energetico, d_promocion.eve_recarga_vehiculos_electricos, 
                    d_promocion.eve_concepto_supuesto, d_promocion.eve_climatizacion, d_promocion.eve_plantillarenovaciongarajenovacion, d_promocion.eve_calefaccion, 
                    d_promocion.eve_plazolocal, d_promocion.eve_email_validacion_contrato_iv, d_promocion.eve_email_validacion_contrato_i, d_promocion.eve_fecharestriccionfactura,
                    d_promocion.eve_equipo_tecnico, d_promocion.eve_nombre_gestor_acompanamiento_suministros, d_promocion.eve_energia_solar, d_promocion.eve_email_gestor_energetico_iii, 
                    d_promocion.eve_plazotrastero, d_promocion.eve_email_gestor_acompanamiento_suministrosii, d_promocion.eve_piscina, d_promocion.eve_zona_deportiva,
                    d_promocion.eve_gestor_energetico, d_promocion.eve_reforma_zzcc, d_promocion.eve_email_comercializadora_reservasii, d_promocion.eve_plantillaborradorgarajenovacion, 
                    d_promocion.eve_nombre_centro_custodio, d_promocion.eve_email_comercializadora_reservasiv, d_promocion.eve_plantillarenovaciongaraje, d_promocion.eve_fecha_hora_bloqueo, 
                    d_promocion.eve_supuesto, d_promocion.eve_perimetro_inconcert, d_promocion.eve_caracteristicas_comerciales, d_promocion.eve_observaciones_comerciales, d_promocion.data_dynamics, d_promocion.eve_promocion_fecha_renovacion);
                respuesta = true;
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                logger.Error(e.InnerException);
                logger.Error(e.StackTrace);
            }
            return respuesta;
        }
        private void UpsertSociedades(IEnumerable<string> datos)
        {
            Sociedad sociedad = null;
            Dynamics_Sociedades d_sociedad = null;
            int total = 0;

            if (null != datos)
            {
                var lotes = Utils.Batch<string>(datos, 100);
                int num_lotes = lotes.Count<IEnumerable<string>>();
                logger.Info("UpsertSociedades. Número de lotes: " + num_lotes);
                int contador_lotes = 0;
                foreach (var lote in lotes)
                {
                    using (var db = new InconcertOnline2())
                    {
                        foreach (var item in lote)
                        {
                            sociedad = JsonConvert.DeserializeObject<Sociedad>(item, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                            d_sociedad = sociedad.Convertir2BD();
                            if (LlamadaSPSaveSociedad(db, d_sociedad))
                            {
                                total++;
                            }
                        }
                    }
                    contador_lotes++;
                    logger.Info($"Lote {contador_lotes} Registros procesados {total}");
                }
            }
            logger.Info($"Sociedades procesadas del CRM {total} de {datos.Count()}");
        }
        private bool LlamadaSPSaveSociedad(InconcertOnline2 db, Dynamics_Sociedades sociedad)
        {
            bool respuesta = false;
            try
            {
                db.Dynamics_SaveSociedad(sociedad.OdataEtag, sociedad.eve_sociedadid, sociedad.eve_descripcionsociedad, 
                    sociedad.eve_puerta, sociedad.eve_visualizacion_portal_anterior, "" /*sociedad.C_owningbusinessunit_value*/, sociedad.eve_cif,
                    sociedad.eve_codigopostal, sociedad.eve_fechamodificacionregistro, sociedad.eve_idsociedadprinex, sociedad.createdon, 
                    sociedad.statecode, sociedad.eve_provincia, sociedad.eve_siglas, sociedad.C_ownerid_value, sociedad.eve_telefono, 
                    sociedad.modifiedon, sociedad.eve_numero, sociedad.eve_escalera, sociedad.versionnumber, sociedad.eve_piso, sociedad.eve_poblacion, 
                    sociedad.timezoneruleversionnumber, sociedad.eve_registromercantil, sociedad.statuscode, sociedad.C_modifiedby_value,
                    sociedad.eve_nombreviapublica, sociedad.eve_nombre, sociedad.C_createdby_value, sociedad.eve_ms_flow, sociedad.C_owninguser_value,
                    sociedad.eve_activar_restriccion_visualizacion_portal, sociedad.eve_visualizacion_portal, sociedad.C_eve_remitenteemailpagos_value,
                    sociedad.eve_logo_sociedad, sociedad.C_eve_remitenteemailgenerico_value, sociedad.eve_avisos_legales, sociedad.eve_notaria,
                    sociedad.eve_enlace_portal_inquilino, sociedad.eve_telefono_call_center, sociedad.eve_email_renovaciones, sociedad.eve_fecha_hora_bloqueo,
                    sociedad.eve_url_logo, sociedad.eve_email_otros, sociedad.eve_restringir_envio_notificaciones_impagados, sociedad.C_eve_remitenterenovaciones_value,
                    sociedad.C_modifiedonbehalfby_value, sociedad.eve_firma_responsable, sociedad.eve_nomodificarfechademodificacin, sociedad.importsequencenumber,
                    sociedad.utcconversiontimezonecode, sociedad.C_createdonbehalfby_value, sociedad.eve_email_pagos_renovaciones, sociedad.C_owningteam_value,
                    sociedad.eve_direccion, sociedad.eve_sello, sociedad.eve_urlfirmaincrementorenta, sociedad.C_eve_configuraciontpvid_value,
                    sociedad.overriddencreatedon, sociedad.eve_responsableincrementorenta, sociedad.eve_nomodificarsumadecomprobacindefila,
                    sociedad.eve_fax, sociedad.eve_fecha_actualizacion_cobros, sociedad.eve_nomodificaridsociedadcrm, sociedad.C_eve_configuraciontpv_reserva_value);
                respuesta = true;
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                logger.Error(e.InnerException);
                logger.Error(e.StackTrace);
            }
            return respuesta;
        }
        private void UpsertContratos(IEnumerable<string> datos)
        {
            Contrato contrato = null;
            Dynamics_Contratos d_contrato = null;
            int total = 0;
            if (null != datos)
            {
                var lotes = Utils.Batch<string>(datos, 100);
                int num_lotes = lotes.Count<IEnumerable<string>>();
                logger.Info("UpsertContratos. Número de lotes: " + num_lotes);
                int contador_lotes = 0;
                foreach (var lote in lotes)
                {
                    using (var db = new InconcertOnline2())
                    {
                        foreach (var item in lote)
                        {
                            contrato = JsonConvert.DeserializeObject<Contrato>(item, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                            d_contrato = contrato.Convertir2BD();
                            if (LlamadaSPSaveContrato(db, d_contrato))
                            {
                                total++;
                            }
                        }
                    }
                    contador_lotes++;
                    logger.Info($"Lote {contador_lotes} Registros procesados {total}");
                }
            }
            logger.Info($"Contratos procesados del CRM {total} de {datos.Count()}");
        }
        private bool LlamadaSPSaveContrato(InconcertOnline2 db, Dynamics_Contratos contrato)
        {
            bool respuesta = false;
            try
            {
                db.Dynamics_SaveContrato(contrato.odataetag, contrato.eve_estadocontrato, contrato.eve_renovarrentasipcnegativo, 
                    contrato.eve_idsociedadprinex, contrato.eve_fecha_inicio_primer_contrato, contrato.eve_importe_seguro_impago, 
                    contrato.eve_tipopromocion, contrato.eve_sumatorioporcentajeparticipacion, contrato.eve_cuentabancaria, 
                    contrato.eve_fechafirmamandato, contrato.statuscode, contrato.eve_rentamensual, contrato.eve_fechaefectoentregallaves, 
                    contrato.eve_fechainicio, contrato.eve_tipoadquisicion, contrato.eve_cuentabancariaingreso, contrato.modifiedon, 
                    contrato.eve_numeroconciertofianzas, contrato.C_owninguser_value, contrato.eve_sumatorioporcentajeparticipacion_date, 
                    contrato.eve_visualizacionportal, contrato.eve_tipofinalizacion, contrato.eve_operacion, 
                    contrato.eve_sumatorioporcentajeparticipacion_state, contrato.eve_descripcionpromocion, contrato.eve_mandato, 
                    contrato.eve_fechafin, contrato.C_ownerid_value, contrato.eve_contratoopcioncompra, contrato.eve_prorrogasacumuladas, 
                    contrato.C_eve_sociedadcontrato_id_value, contrato.eve_fechamodificacionregistro, contrato.eve_renovaciontacita, 
                    contrato.eve_actualizarfianza, contrato.C_eve_inquilinocontratoid_value, contrato.eve_fechaliquidacioncontrato, 
                    contrato.eve_descripcionsociedad, contrato.eve_fechaentregallaves, contrato.eve_importegarantiaadicional, 
                    contrato.eve_liquidaciongestionfueraportal, contrato.timezoneruleversionnumber, contrato.eve_importetotaldeuda, 
                    contrato.eve_porcentajeanyo3, contrato.createdon, contrato.eve_cuentabancariaportal, contrato.eve_numero_recibo_seguro_impago, 
                    contrato.eve_bloqueado_promocion_sociedad, contrato.eve_repercusionibi, contrato.eve_titularcuentabancaria, 
                    contrato.eve_tiporevision, contrato.eve_eve_importe_garantia_importe, contrato.eve_enviar_recordatorio_seguro_hogar, 
                    contrato.versionnumber, contrato.eve_name, contrato.C_eve_promocincontrato_id_value, contrato.eve_mandatofirmado, 
                    contrato.eve_fechaadquisicion, contrato.eve_organismofianzas, contrato.eve_importefianza, contrato.eve_tipodeposito, 
                    contrato.eve_duracionprorrogas, contrato.C_modifiedby_value, contrato.C_createdby_value, contrato.eve_contratoid, 
                    contrato.eve_indicevigente, contrato.eve_numeromaximoprorrogas, contrato.eve_causafinalizacion, contrato.eve_tipo_poliza_seguro_impago, 
                    contrato.eve_renovacionautomatica, contrato.eve_contratoretenido, contrato.statecode, contrato.C_owningbusinessunit_value, 
                    contrato.eve_porcentajeanyo1, contrato.eve_deudatotal, contrato.eve_importeproximarenta, contrato.eve_porcentajeanyo2, 
                    contrato.eve_idcontratoprinex, contrato.eve_diavencimientorecibos, contrato.eve_recibospendientesvencimiento, 
                    contrato.eve_codigosociedad, contrato.eve_codigopromocion, contrato.eve_mandatovigor, contrato.eve_importe_rentas_impagadas, 
                    contrato.eve_inquilino_recuperacion_3, contrato.eve_descripcion_seguro_impago, contrato.importsequencenumber, 
                    contrato.utcconversiontimezonecode, contrato.eve_numero_referencia_aval_bancario, contrato.overriddencreatedon, 
                    contrato.eve_fecharepercusionhasta, contrato.eve_importe_deuda_bonificacion_nuevo, contrato.eve_motivo_finalizacion_contrato, 
                    contrato.eve_comentarios_motivo_finalizacion, contrato.eve_motivoretencion, contrato.eve_observacionescontrato, 
                    contrato.eve_importe_deuda_bonificacion, contrato.eve_inquilino_recuperacion_1, contrato.eve_fechabajagas, 
                    contrato.C_eve_codigo_primer_contrato_id_value, contrato.eve_renovacionvivienda, contrato.eve_fecha_caducidad_aval_bancario, 
                    contrato.eve_comentarios_estado_recuperacion, contrato.eve_fecha_inicio_vigencia_aval_bancario, contrato.eve_inquilino_recuperacion_2, 
                    contrato.eve_observacionesavalista, contrato.eve_fecha_registro_deuda, contrato.eve_otros_motivos_ii, contrato.eve_fecha_contacto, 
                    contrato.processid, contrato.eve_referencia_seguro_impago, contrato.eve_motivoresolucion, contrato.traversedpath, 
                    contrato.eve_origen_finalizacion_contrato, contrato.eve_estado_recuperacion, contrato.eve_codigoetapadevolucionfianza_anterior, 
                    contrato.eve_fechaproximarevisionrentaportal, contrato.C_owningteam_value, contrato.eve_fecha_comunicacion_bonificacion, 
                    contrato.eve_fecha_inicio_seguro_impago, contrato.eve_id_inmueble_principal_prinex, contrato.eve_tipogarantiaadicional, 
                    contrato.C_modifiedonbehalfby_value, contrato.eve_descripcionactualetapadevolucionfianza, contrato.eve_proveedor_seguro_impago, 
                    contrato.eve_fechaproximarevisionrenta, contrato.eve_operacioncontratoduplicado, contrato.C_createdonbehalfby_value, 
                    contrato.eve_cierre_expediente, contrato.eve_fecha_fin_seguro_impago, contrato.eve_envio_comunicacion_bonificacion, 
                    contrato.eve_tipo_envio_renovacion, contrato.eve_fecha_finalizacion_recuperacion, contrato.eve_fecha_envio_recordatorio_seguro, 
                    contrato.eve_codigoetapadevolucionfianza, contrato.eve_clasificacion_cambio_vivienda, contrato.eve_fechabajaagua, 
                    contrato.eve_email_recuperacion_inquilino_3, contrato.eve_eve_entidad_aval_bancario, contrato.eve_motivo_finalizacion_contrato_adicional, 
                    contrato.eve_fecharepercusiondesde, contrato.eve_codigo_inmueble_principal, contrato.eve_motivo_no_recuperacion_1, 
                    contrato.eve_tipodocumentoavalista, contrato.eve_tipo_comunicacion_bonificacion, contrato.eve_fecha_cambio_motivo_1, 
                    contrato.eve_fecha_cambio_motivo_2, contrato.eve_otros_motivos_i, contrato.eve_inquilinoprincipal, contrato.eve_email_recuperacion_inquilino_2, 
                    contrato.eve_fecha_minima_resolucion, contrato.eve_apellidosavalista, contrato.eve_telefono_recuperacion_1, contrato.eve_telefono_recuperacion_3, 
                    contrato.eve_telefono_recuperacion_2, contrato.eve_fecha_envio_comunicacion, contrato.eve_estado_recuperacion_adicional, 
                    contrato.eve_fecha_liquidacion_bonificacion, contrato.eve_comentario_aval_bancario, contrato.eve_inmuebleprincipaloperacion, 
                    contrato.C_stageid_value, contrato.eve_comentario_interno_seguros, contrato.eve_equipo_encargado_recuperacion, contrato.eve_rentamensualportal, 
                    contrato.eve_email_recuperacion_inquilino_1, contrato.eve_documentoavalista, contrato.eve_motivo_no_recuperacion_2, contrato.eve_fechabajaluz, 
                    contrato.eve_reasignacion, contrato.eve_observaciones_intern_bonificacion, contrato.eve_formadecobro, contrato.eve_nombreavalista, 
                    contrato.C_eve_codigo_contrato_renovado_id_value, contrato.eve_descripcionetapadevolucionfianza, contrato.eve_motivo_finalizacion_contrato_anterior_2, 
                    contrato.eve_motivo_finalizacion_contrato_anterior_1, contrato.C_eve_inquilinoprincipalliquidacion_value, contrato.eve_fechaenviomail, 
                    contrato.eve_fecha_registro_garantia);
                respuesta = true;
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                logger.Error(e.InnerException);
                logger.Error(e.StackTrace);
            }
            return respuesta;
        }
        private void UpsertInquilino_Reserva(IEnumerable<string> datos)
        {
            Inquilinos_Reserva ir = null;
            Dynamics_Inquilinos_Reservas d_ir = null;
            int total = 0;
            if (null != datos)
            {
                var lotes = Utils.Batch<string>(datos, 100);
                int num_lotes = lotes.Count<IEnumerable<string>>();
                logger.Info("UpsertInquilino_Reserva. Número de lotes: " + num_lotes);
                int contador_lotes = 0;
                foreach (var lote in lotes)
                {
                    using (var db = new InconcertOnline2())
                    {
                        foreach (var item in lote)
                        {
                            ir = JsonConvert.DeserializeObject<Inquilinos_Reserva>(item, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                            d_ir = ir.Convertir2BD();
                            if (LlamadaSPSaveInquilinoReserva(db, d_ir))
                            {
                                total++;
                            }
                        }
                    }
                    contador_lotes++;
                    logger.Info($"Lote {contador_lotes} Registros procesados {total}");
                }
            }
            logger.Info($"Inquilino_reservas procesadas del CRM {total} de {datos.Count()}");
        }
        private bool LlamadaSPSaveInquilinoReserva(InconcertOnline2 db, Dynamics_Inquilinos_Reservas d_ir)
        {
            bool respuesta = false;
            try
            {
                db.Dynamics_SaveInquilinosReservas(d_ir.OdataEtag, d_ir.eve_estado_civil, d_ir.statecode, d_ir.eve_nacionalidad, d_ir.eve_asnef,
                    d_ir.eve_documento_identidad, d_ir.eve_coeficiente_parcialidad, d_ir.createdon, d_ir.eve_name, d_ir.eve_consentimiento_reserva_terceras,
                    d_ir.eve_consentimiento_reserva_grupo, d_ir.eve_movil, d_ir.C_ownerid_value, d_ir.eve_situacion_laboral, d_ir.eve_inquilinos_reservaid,
                    d_ir.modifiedon, d_ir.emailaddress, d_ir.versionnumber, d_ir.eve_tipo_documento_identidad, d_ir.eve_telefono_movil_sin_prefijo,
                    d_ir.timezoneruleversionnumber, d_ir.eve_fecha_nacimiento, d_ir.C_modifiedby_value, d_ir.statuscode, d_ir.eve_nombreinquilino,
                    d_ir.eve_posee_otro_inmueble, d_ir.C_createdby_value, d_ir.eve_gdpr, d_ir.C_owningbusinessunit_value, d_ir.eve_extranjero, d_ir.eve_telefono,
                    d_ir.C_owninguser_value, d_ir.eve_apellidosinquilino, d_ir.eve_sexo, d_ir.eve_horas_jornada_laboral, d_ir.eve_tipo_jornada_laboral,
                    d_ir.eve_tipo_contrato_laboral, d_ir.overriddencreatedon, d_ir.importsequencenumber, d_ir.C_modifiedonbehalfby_value, d_ir.eve_direccion,
                    d_ir.eve_cuentabancariaingreso, d_ir.eve_poblacion, d_ir.eve_bic, d_ir.eve_horas_trabajo_semanales, d_ir.utcconversiontimezonecode,
                    d_ir.C_createdonbehalfby_value, d_ir.eve_codigo_postal, d_ir.C_owningteam_value);
                respuesta = true;
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                logger.Error(e.InnerException);
                logger.Error(e.StackTrace);
            }
            return respuesta;
        }
        #endregion
        private IEnumerable<string> ObtenerDatosEntidad(string entidad)
        {
            logger.Info($"ObtenerDatosEntidad: {entidad}");
            string token = GetToken(_url, _apiVersion).Result;
            IEnumerable<string> resultado = null;
            string filtro = string.Empty;
            if (null != token)
            {
                using (InconcertOnline2 db = new InconcertOnline2())
                {
                    var filtros = db.Dynamics_Filters_Configuration;
                    if(null != filtros)
                    {
                        foreach(var item in filtros)
                        {
                            if (item.Entidad.Equals(entidad))
                            {
                                filtro = item.Filtro;
                            }
                        }
                    }
                }
                resultado = ConsultarAPI(_url, _apiVersion, entidad, token, filtro);
                int valores = 0;

                if (null != resultado)
                {
                    valores = resultado.Count<string>();
                }
                logger.Info($"Registros obtenidos de {entidad}: {valores}");
            }
            
            return resultado;
        }
        private IEnumerable<string> ObtenerDatosContactos()
        {
            logger.Info($"ObtenerDatosEntidad contactos");
            List<string> names = new List<string> { "FI-028838", "FI-039819", "FI-043783", "FI-029887", "FI-041102", "FI-040666", "FI-041640", "FI-045281", "FI-018187", "FI-036588", "FI-028864", "FI-026657", "FI-023668", "FI-032647", "FI-046579", "FI-023066", "FI-042883", "FI-043309", "FI-035874", "FI-026258", "FI-040513", "FI-022812", "FI-041490", "FI-017970", "FI-018121", "FI-041822", "FI-040317", "FI-040260", "FI-050036", "FI-040953", "FI-029944", "FI-031367", "FI-046024", "FI-010031", "FI-013045", "FI-020091", "FI-017000", "FI-016717", "FI-038735", "FI-023973", "FI-022480", "FI-033700", "FI-020253", "FI-022862", "FI-028530", "FI-025946", "FI-039909", "FI-040439", "FI-043575", "FI-024117", "FI-033135", "FI-036449", "FI-022300", "FI-030096", "FI-046149", "FI-027089", "FI-004926", "FI-030642", "FI-023436", "FI-021393", "FI-000647", "FI-023911", "FI-024716", "FI-038133", "FI-021082", "FI-039388", "FI-032867", "FI-029878", "FI-039281", "FI-026804", "FI-032720", "FI-030647", "FI-028296", "FI-046181", "FI-025480", "FI-018086", "FI-042767", "FI-041440", "FI-028237", "FI-042450", "FI-028597", "FI-033266", "FI-034931", "FI-041420", "FI-011329", "FI-022774", "FI-023102", "FI-024810", "FI-044926", "FI-040348", "FI-036579", "FI-047181", "FI-042735", "FI-040624", "FI-000497", "FI-039438", "FI-015640", "FI-039118", "FI-043398", "FI-040487", "FI-041643", "FI-040558", "FI-022515", "FI-028194", "FI-027473", "FI-040238", "FI-040038", "FI-035377", "FI-036945", "FI-016060", "FI-036382", "FI-040448", "FI-041732", "FI-021995", "FI-014251", "FI-044218", "FI-028344", "FI-036387", "FI-022201", "FI-021558", "FI-041222", "FI-031392", "FI-030637", "FI-043350", "FI-022399", "FI-025433", "FI-029318", "FI-024293", "FI-040571", "FI-029685", "FI-030530", "FI-044878", "FI-041585", "FI-012598", "FI-026817", "FI-042208", "FI-024376", "FI-017767", "FI-022158", "FI-021999", "FI-043385", "FI-023077", "FI-026206", "FI-031490", "FI-028498", "FI-039983", "FI-018273", "FI-040113", "FI-040373", "FI-012354", "FI-041257", "FI-014969", "FI-041750", "FI-038527", "FI-017088", "FI-040410", "FI-041065", "FI-025227", "FI-040319", "FI-038599", "FI-038120", "FI-028596", "FI-018374", "FI-039806", "FI-020937", "FI-007969", "FI-011236", "FI-009510", "FI-026838", "FI-026039", "FI-028531", "FI-024046", "FI-016744", "FI-016101", "FI-035248", "FI-036687", "FI-027638", "FI-010288", "FI-022136", "FI-042802", "FI-018630", "FI-048202", "FI-027014", "FI-034003", "FI-040940", "FI-012840", "FI-016908", "FI-035940", "FI-026391", "FI-036971", "FI-028519", "FI-041965", "FI-021073", "FI-024377", "FI-025927", "FI-008106", "FI-023993", "FI-014385", "FI-028111", "FI-040755", "FI-044577", "FI-004931", "FI-024084", "FI-029797", "FI-029160", "FI-014471", "FI-029980", "FI-040199", "FI-038615", "FI-035048", "FI-040502", "FI-024014", "FI-040362", "FI-036825", "FI-031574", "FI-045003", "FI-042401", "FI-038414", "FI-024595", "FI-022739", "FI-029815", "FI-026118", "FI-024605", "FI-014699", "FI-042025", "FI-044869", "FI-023717", "FI-017988", "FI-027042", "FI-035742", "FI-040150", "FI-042456", "FI-043096", "FI-026644", "FI-000936", "FI-017391", "FI-040904", "FI-043279", "FI-024410", "FI-032426", "FI-022533", "FI-029357", "FI-025147", "FI-029661", "FI-024364", "FI-020824", "FI-029515", "FI-029233", "FI-031455", "FI-015470", "FI-032804", "FI-038727", "FI-029916", "FI-041913", "FI-044023", "FI-028717", "FI-030375", "FI-016579", "FI-030792", "FI-026015", "FI-041538", "FI-022324", "FI-043744", "FI-030348", "FI-041944", "FI-044833", "FI-036557", "FI-016957", "FI-040769", "FI-027931", "FI-040707", "FI-004883", "FI-040676", "FI-041477", "FI-033150", "FI-036727", "FI-044734", "FI-021203", "FI-039257", "FI-027168", "FI-015652", "FI-024976", "FI-040488", "FI-030422", "FI-040434", "FI-027330", "FI-031086", "FI-036154", "FI-041114", "FI-023724", "FI-030147", "FI-004329", "FI-023826", "FI-029057", "FI-038326", "FI-024895", "FI-026120", "FI-013250", "FI-026691", "FI-039366", "FI-032751", "FI-012825", "FI-030927", "FI-028990", "FI-031502", "FI-026410", "FI-031751", "FI-021744", "FI-034155", "FI-015371", "FI-024278", "FI-024830", "FI-045349", "FI-020452", "FI-032693", "FI-030937", "FI-025214", "FI-041676", "FI-032453", "FI-024993", "FI-021862", "FI-027880", "FI-034455", "FI-045405", "FI-025249", "FI-032826", "FI-040608", "FI-033923", "FI-040887", "FI-035956", "FI-028869", "FI-023223", "FI-040475", "FI-021597", "FI-034847", "FI-023977", "FI-025595", "FI-043695", "FI-024445", "FI-043112", "FI-029940", "FI-021832", "FI-030569", "FI-029838", "FI-021859", "FI-016335", "FI-022787", "FI-022788", "FI-022789", "FI-003785", "FI-004252", "FI-009135", "FI-008096", "FI-002832", "FI-009036", "FI-009141", "FI-040207", "FI-040208", "FI-021717", "FI-022799", "FI-022800", "FI-018128", "FI-041055", "FI-045287", "FI-045288", "FI-036992", "FI-045174", "FI-045175", "FI-023540", "FI-036144", "FI-045129", "FI-045130", "FI-021860", "FI-042376", "FI-042378", "FI-037073", "FI-022523", "FI-022524", "FI-018577", "FI-018578", "FI-001173", "FI-022137", "FI-022416", "FI-020034", "FI-022418", "FI-043019", "FI-041019", "FI-041025", "FI-041026", "FI-042821", "FI-020036", "FI-004180", "FI-043775", "FI-045964", "FI-023067", "FI-044754", "FI-044755", "FI-044190", "FI-044191", "FI-040906", "FI-040907", "FI-043776", "FI-036868", "FI-036873", "FI-012455", "FI-041361", "FI-044059", "FI-004178", "FI-009051", "FI-044061", "FI-017118", "FI-039972", "FI-040949", "FI-040952", "FI-043341", "FI-042284", "FI-042285", "FI-010558", "FI-010559", "FI-016916", "FI-043633", "FI-043634", "FI-041263", "FI-041264", "FI-041265", "FI-039874", "FI-039875", "FI-037666", "FI-017009", "FI-017010", "FI-004898", "FI-004956", "FI-017457", "FI-004847", "FI-018641", "FI-018642", "FI-006692", "FI-007348", "FI-040959", "FI-039338", "FI-006985", "FI-038902", "FI-004772", "FI-038958", "FI-039892", "FI-039893", "FI-039894", "FI-004987", "FI-007758", "FI-004808", "FI-017215", "FI-013267", "FI-013329", "FI-016734", "FI-016735", "FI-016224", "FI-012981", "FI-043271", "FI-043272", "FI-010150", "FI-012262", "FI-012263", "FI-023063", "FI-023064", "FI-036773", "FI-007158", "FI-041145", "FI-041431", "FI-041434", "FI-018007", "FI-018785", "FI-018786", "FI-017876", "FI-018117", "FI-017875", "FI-041751", "FI-041843", "FI-036155", "FI-036156", "FI-015028", "FI-017198", "FI-017196", "FI-017197", "FI-017164", "FI-011868", "FI-022092", "FI-022093", "FI-022185", "FI-022186", "FI-022187", "FI-044217", "FI-042177", "FI-038238", "FI-040908", "FI-040909", "FI-011885", "FI-018348", "FI-018766", "FI-013241", "FI-018199", "FI-013277", "FI-018174", "FI-018175", "FI-016519", "FI-013262", "FI-022777", "FI-022779", "FI-020698", "FI-038090", "FI-038091", "FI-040416", "FI-040417", "FI-040418", "FI-021061", "FI-020098", "FI-020893", "FI-020894", "FI-023046", "FI-020006", "FI-019936", "FI-042934", "FI-042935", "FI-021062", "FI-019833", "FI-023216", "FI-023215", "FI-019935", "FI-020005", "FI-019857", "FI-019961", "FI-019656", "FI-007586", "FI-011103", "FI-006247", "FI-040174", "FI-045715", "FI-045714", "FI-007048", "FI-008169", "FI-017301", "FI-008168", "FI-011260", "FI-006442", "FI-039198", "FI-006524", "FI-011454", "FI-007956", "FI-045937", "FI-046186", "FI-006271", "FI-006591", "FI-021772", "FI-021773", "FI-015316", "FI-015318", "FI-042179", "FI-040172", "FI-040173", "FI-012830", "FI-022270", "FI-039989", "FI-022979", "FI-022981", "FI-012831", "FI-040521", "FI-013079", "FI-013080", "FI-045929", "FI-045930", "FI-012964", "FI-011488", "FI-015299", "FI-011516", "FI-011529", "FI-018300", "FI-021333", "FI-012692", "FI-018519", "FI-021334", "FI-013130", "FI-021513", "FI-040145", "FI-040147", "FI-018088", "FI-018110", "FI-038834", "FI-017366", "FI-036802", "FI-014261", "FI-018479", "FI-043697", "FI-043700", "FI-043701", "FI-017375", "FI-016123", "FI-021555", "FI-021556", "FI-009780", "FI-017361", "FI-017362", "FI-011148", "FI-020056", "FI-006843", "FI-006899", "FI-044168", "FI-021748", "FI-043674", "FI-010564", "FI-016551", "FI-016552", "FI-022949", "FI-012239", "FI-004348", "FI-022679", "FI-022680", "FI-000375", "FI-001477", "FI-004347", "FI-020058", "FI-006942", "FI-006990", "FI-012971", "FI-042567", "FI-042568", "FI-042998", "FI-041579", "FI-017685", "FI-017686", "FI-005085", "FI-041918", "FI-041921", "FI-023195", "FI-023196", "FI-023197", "FI-041430", "FI-013103", "FI-013104", "FI-007130", "FI-007798", "FI-007741", "FI-022130", "FI-012972", "FI-018568", "FI-018569", "FI-020983", "FI-020986", "FI-036388", "FI-020834", "FI-020835", "FI-020833", "FI-036386", "FI-017820", "FI-017821", "FI-016404", "FI-016405", "FI-000780", "FI-021235", "FI-021236", "FI-022950", "FI-022951", "FI-023097", "FI-023098", "FI-023095", "FI-017746", "FI-017747", "FI-021098", "FI-021099", "FI-043981", "FI-041351", "FI-041352", "FI-020100", "FI-044988", "FI-044989", "FI-016840", "FI-016842", "FI-000677", "FI-017378", "FI-017379", "FI-004388", "FI-015769", "FI-015770", "FI-015771", "FI-015772", "FI-045966", "FI-045992", "FI-007046", "FI-006970", "FI-009861", "FI-018564", "FI-018565", "FI-018327", "FI-018328", "FI-018329", "FI-018295", "FI-020924", "FI-020923", "FI-018294", "FI-000357", "FI-010196", "FI-010197", "FI-016679", "FI-020101", "FI-014644", "FI-004325", "FI-000433", "FI-000191", "FI-021807", "FI-021808", "FI-000446", "FI-039348", "FI-023068", "FI-023070", "FI-018768", "FI-000149", "FI-001386", "FI-044406", "FI-044407", "FI-007077", "FI-041408", "FI-041412", "FI-008237", "FI-010739", "FI-012837", "FI-039482", "FI-008901", "FI-010306", "FI-010307", "FI-048372", "FI-022748", "FI-009912", "FI-009913", "FI-036453", "FI-018526", "FI-022960", "FI-046759", "FI-043433", "FI-045192", "FI-039653", "FI-034476", "FI-032722", "FI-032677", "FI-036567", "FI-036633", "FI-030006", "FI-034946", "FI-045190", "FI-030472", "FI-034499", "FI-042077", "FI-042079", "FI-038437", "FI-036113", "FI-032865", "FI-024690", "FI-036946", "FI-024713", "FI-025319", "FI-025968", "FI-030417", "FI-033347", "FI-026638", "FI-035995", "FI-031096", "FI-032802", "FI-035838", "FI-032113", "FI-035941", "FI-036170", "FI-030251", "FI-033025", "FI-032410", "FI-028270", "FI-033134", "FI-036869", "FI-036870", "FI-036871", "FI-036872", "FI-031141", "FI-031952", "FI-034746", "FI-029158", "FI-032518", "FI-045379", "FI-045380", "FI-032248", "FI-023633", "FI-026543", "FI-031236", "FI-035498", "FI-035617", "FI-030460", "FI-040913", "FI-040914", "FI-044454", "FI-039248", "FI-029970", "FI-023691", "FI-023779", "FI-027069", "FI-026256", "FI-030712", "FI-031988", "FI-032380", "FI-032679", "FI-046004", "FI-031109", "FI-035466", "FI-029786", "FI-031118", "FI-027530", "FI-034130", "FI-032146", "FI-034957", "FI-038911", "FI-038912", "FI-034330", "FI-027876", "FI-023024", "FI-031161", "FI-035676", "FI-026060", "FI-027729", "FI-034358", "FI-033900", "FI-035636", "FI-032156", "FI-025969", "FI-036550", "FI-045472", "FI-034722", "FI-029517", "FI-031528", "FI-032276", "FI-031209", "FI-029717", "FI-035919", "FI-031207", "FI-042638", "FI-042641", "FI-042642", "FI-024268", "FI-033063", "FI-025967", "FI-041621", "FI-041622", "FI-045398", "FI-045546", "FI-027212", "FI-031684", "FI-041623", "FI-032474", "FI-024927", "FI-035662", "FI-042173", "FI-027997", "FI-027122", "FI-032463", "FI-041575", "FI-041576", "FI-041603", "FI-024451", "FI-027013", "FI-034384", "FI-030142", "FI-034369", "FI-028292", "FI-025867", "FI-032115", "FI-027753", "FI-032027", "FI-041371", "FI-035786", "FI-029339", "FI-030475", "FI-034807", "FI-028323", "FI-029589", "FI-033537", "FI-035988", "FI-035891", "FI-035140", "FI-030108", "FI-032344", "FI-028897", "FI-032336", "FI-023859", "FI-032445", "FI-031047", "FI-041349", "FI-041350", "FI-027840", "FI-032433", "FI-032535", "FI-027422", "FI-036922", "FI-043480", "FI-041492", "FI-041494", "FI-043685", "FI-028872", "FI-028646", "FI-041604", "FI-042888", "FI-034876", "FI-027323", "FI-029646", "FI-027401", "FI-034017", "FI-028191", "FI-024797", "FI-030537", "FI-032939", "FI-027348", "FI-040361", "FI-023832", "FI-035732", "FI-028818", "FI-033476", "FI-044631", "FI-044721", "FI-039091", "FI-039093", "FI-046049", "FI-039090", "FI-035731", "FI-032551", "FI-038783", "FI-035150", "FI-030236", "FI-025274", "FI-025224", "FI-030075", "FI-040530", "FI-034001", "FI-027181", "FI-028236", "FI-001277", "FI-029829", "FI-033156", "FI-034958", "FI-038689", "FI-038690", "FI-033845", "FI-035459", "FI-025930", "FI-024793", "FI-027177", "FI-042104", "FI-028813", "FI-038169", "FI-035480", "FI-032303", "FI-034749", "FI-024196", "FI-032189", "FI-027289", "FI-034451", "FI-035264", "FI-032444", "FI-035979", "FI-033061", "FI-049004", "FI-044636", "FI-036994", "FI-036995", "FI-035050", "FI-031266", "FI-033785", "FI-032781", "FI-025964", "FI-024536", "FI-035069", "FI-033677", "FI-034594", "FI-026076", "FI-029529", "FI-025064", "FI-032727", "FI-039318", "FI-039319", "FI-028743", "FI-043728", "FI-028247", "FI-040954", "FI-033668", "FI-032759", "FI-035763", "FI-032090", "FI-031127", "FI-027898", "FI-029271", "FI-038173", "FI-033217", "FI-038784", "FI-025175", "FI-042105", "FI-042303", "FI-042304", "FI-041261", "FI-035492", "FI-032229", "FI-027592", "FI-030002", "FI-033069", "FI-030493", "FI-023964", "FI-031449", "FI-035571", "FI-025646", "FI-027712", "FI-035613", "FI-028643", "FI-031418", "FI-033950", "FI-028071", "FI-034298", "FI-035704", "FI-025520", "FI-042560", "FI-032394", "FI-035871", "FI-043898", "FI-036078", "FI-023697", "FI-047628", "FI-047680", "FI-023920", "FI-029725", "FI-027561", "FI-025405", "FI-035533", "FI-028149", "FI-035581", "FI-026610", "FI-031720", "FI-026768", "FI-027312", "FI-032567", "FI-036242", "FI-026627", "FI-024934", "FI-028021", "FI-032617", "FI-034712", "FI-026172", "FI-034936", "FI-024197", "FI-032140", "FI-025295", "FI-044110", "FI-044121", "FI-042833", "FI-027855", "FI-025003", "FI-036200", "FI-026965", "FI-034339", "FI-039111", "FI-027727", "FI-035615", "FI-032613", "FI-038349", "FI-032555", "FI-036729", "FI-025121", "FI-033442", "FI-044470", "FI-029989", "FI-030329", "FI-033429", "FI-029405", "FI-025099", "FI-031257", "FI-029375", "FI-030544", "FI-035286", "FI-036129", "FI-036210", "FI-025440", "FI-026562", "FI-038956", "FI-038957", "FI-027879", "FI-032117", "FI-027980", "FI-030234", "FI-046336", "FI-046337", "FI-024852", "FI-032048", "FI-031645", "FI-033084", "FI-028063", "FI-028821", "FI-024612", "FI-038619", "FI-027966", "FI-027785", "FI-029734", "FI-027553", "FI-034212", "FI-029843", "FI-025437", "FI-033524", "FI-028073", "FI-032270", "FI-031604", "FI-025436", "FI-033527", "FI-029088", "FI-031634", "FI-025025", "FI-033404", "FI-046943", "FI-046944", "FI-032021", "FI-027875", "FI-027885", "FI-031914", "FI-027236", "FI-035965", "FI-040736", "FI-044137", "FI-029573", "FI-033129", "FI-025664", "FI-023998", "FI-030238", "FI-043649", "FI-032361", "FI-031724", "FI-023183", "FI-017210", "FI-017211", "FI-025411", "FI-031097", "FI-030723", "FI-031550", "FI-031172", "FI-027844", "FI-025354", "FI-042051", "FI-042053", "FI-032420", "FI-024361", "FI-033704", "FI-026128", "FI-046610", "FI-046611", "FI-038817", "FI-028824", "FI-026175", "FI-032182", "FI-028337", "FI-035809", "FI-031961", "FI-035687", "FI-024274", "FI-032829", "FI-029149", "FI-023969", "FI-031064", "FI-026574", "FI-042172", "FI-046658", "FI-029527", "FI-035779", "FI-026374", "FI-023808", "FI-032524", "FI-034279", "FI-025948", "FI-038162", "FI-038163", "FI-025637", "FI-033503", "FI-031697", "FI-025641", "FI-024659", "FI-045102", "FI-031108", "FI-025454", "FI-033385", "FI-031017", "FI-034779", "FI-034787", "FI-028001", "FI-046132", "FI-046133", "FI-046134", "FI-029966", "FI-038605", "FI-038609", "FI-038610", "FI-032532", "FI-032654", "FI-042596", "FI-042598", "FI-010812", "FI-024304", "FI-030644", "FI-032233", "FI-024896", "FI-024701", "FI-034593", "FI-028685", "FI-032217", "FI-045482", "FI-036115", "FI-030333", "FI-040839", "FI-040840", "FI-033109", "FI-042854", "FI-043011", "FI-043012", "FI-032874", "FI-032886", "FI-034094", "FI-032623", "FI-031586", "FI-038245", "FI-025762", "FI-025712", "FI-030944", "FI-025564", "FI-026928", "FI-025546", "FI-026927", "FI-041181", "FI-025913", "FI-030943", "FI-033602", "FI-026888", "FI-025717", "FI-031522", "FI-031072", "FI-027645", "FI-027695", "FI-033914", "FI-009858", "FI-000087", "FI-000020", "FI-000028", "FI-015719", "FI-016503", "FI-016504", "FI-001368", "FI-017898", "FI-020829", "FI-020830", "FI-021030", "FI-021401", "FI-021402", "FI-001366", "FI-010897", "FI-010898", "FI-022138", "FI-022139", "FI-011232", "FI-011797", "FI-023180", "FI-023181", "FI-038584", "FI-038586", "FI-041846", "FI-042536", "FI-042537", "FI-049446", "FI-049447", "FI-000200", "FI-015906", "FI-004169", "FI-016140", "FI-016141", "FI-016142", "FI-016199", "FI-016200", "FI-000189", "FI-005326", "FI-017207", "FI-017208", "FI-007637", "FI-007682", "FI-007336", "FI-017491", "FI-017492", "FI-017805", "FI-017806", "FI-018350", "FI-018351", "FI-018352", "FI-018487", "FI-018488", "FI-041972", "FI-009408", "FI-018782", "FI-009049", "FI-000171", "FI-001397", "FI-000105", "FI-000117", "FI-012389", "FI-001381", "FI-022822", "FI-017444", "FI-023203", "FI-000177", "FI-001399", "FI-014683", "FI-038394", "FI-038396", "FI-041552", "FI-041553", "FI-043595", "FI-043597", "FI-000237", "FI-015724", "FI-016639", "FI-016640", "FI-016641", "FI-017439", "FI-018098", "FI-018106", "FI-017667", "FI-021376", "FI-021377", "FI-022264", "FI-022946", "FI-022947", "FI-036441", "FI-036443", "FI-036454", "FI-039327", "FI-039328", "FI-012501", "FI-041790", "FI-038302", "FI-001842", "FI-001458", "FI-000301", "FI-001453", "FI-020791", "FI-020792", "FI-021264", "FI-021265", "FI-021266", "FI-021833", "FI-021834", "FI-022589", "FI-022806", "FI-023088", "FI-023089", "FI-041890", "FI-041891", "FI-030924", "FI-017074", "FI-017075", "FI-020070", "FI-020071", "FI-000359", "FI-001469", "FI-042321", "FI-042323", "FI-042387", "FI-043761", "FI-044018", "FI-000415", "FI-000371", "FI-000367", "FI-000447", "FI-020851", "FI-037045", "FI-045913", "FI-045914", "FI-046738", "FI-046741", "FI-000518", "FI-014656", "FI-006857", "FI-005296", "FI-000534", "FI-001497", "FI-018534", "FI-018535", "FI-018536", "FI-036366", "FI-036367", "FI-038983", "FI-038985", "FI-000661", "FI-000654", "FI-000651", "FI-011077", "FI-037964", "FI-005371", "FI-006881", "FI-018554", "FI-018555", "FI-021048", "FI-021051", "FI-022261", "FI-000662", "FI-000752", "FI-010101", "FI-003762", "FI-004199", "FI-004200", "FI-000772", "FI-001546", "FI-017965", "FI-017966", "FI-018705", "FI-018706", "FI-020807", "FI-020808", "FI-022124", "FI-022125", "FI-039196", "FI-039197", "FI-020828", "FI-042402", "FI-042403", "FI-042404", "FI-048848", "FI-013160", "FI-015117", "FI-015118", "FI-015883", "FI-015885", "FI-015887", "FI-016455", "FI-016578", "FI-016739", "FI-016740", "FI-017788", "FI-017790", "FI-018166", "FI-018167", "FI-018168", "FI-001266", "FI-020563", "FI-020564", "FI-020866", "FI-020868", "FI-020869", "FI-010116", "FI-010238", "FI-040602", "FI-040603", "FI-009922", "FI-009923", "FI-011138", "FI-015283", "FI-015285", "FI-007101", "FI-018275", "FI-008049", "FI-021554", "FI-021586", "FI-022677", "FI-022821", "FI-022823", "FI-023161", "FI-023163", "FI-023164", "FI-023209", "FI-023210", "FI-039940", "FI-039944", "FI-041504", "FI-041505", "FI-039961", "FI-039962", "FI-001908", "FI-000560", "FI-009130", "FI-006815", "FI-012792", "FI-021945", "FI-039775", "FI-020094", "FI-020095", "FI-022665", "FI-022666", "FI-022667", "FI-022837", "FI-001084", "FI-016325", "FI-021562", "FI-021566", "FI-040553", "FI-045283", "FI-045284", "FI-001101", "FI-015405", "FI-013159", "FI-022103", "FI-039117", "FI-040649", "FI-001309", "FI-001627", "FI-001305", "FI-001183", "FI-001609", "FI-001132", "FI-009989", "FI-012490", "FI-007841", "FI-009039", "FI-012961", "FI-001218", "FI-001616", "FI-016865", "FI-011251", "FI-011252", "FI-003675", "FI-017238", "FI-017881", "FI-017882", "FI-018427", "FI-018429", "FI-020871", "FI-020872", "FI-036978", "FI-036979", "FI-038061", "FI-038062", "FI-044306", "FI-007859", "FI-008156", "FI-017406", "FI-001667", "FI-007450", "FI-016412", "FI-016413", "FI-007334", "FI-001663", "FI-017310", "FI-017438", "FI-021450", "FI-022308", "FI-022309", "FI-022310", "FI-022764", "FI-022765", "FI-022965", "FI-022966", "FI-040863", "FI-004351", "FI-015446", "FI-018191", "FI-018194", "FI-009064", "FI-009519", "FI-009628", "FI-002962", "FI-002980", "FI-022798", "FI-040024", "FI-040025", "FI-011189", "FI-011190", "FI-002834", "FI-002821", "FI-002817", "FI-008093", "FI-021314", "FI-021315", "FI-004167", "FI-004168", "FI-008933", "FI-008934", "FI-018236", "FI-015362", "FI-012392", "FI-022153", "FI-044271", "FI-044559", "FI-007148", "FI-016272", "FI-012525", "FI-012945", "FI-012712", "FI-007239", "FI-015468", "FI-016985", "FI-012605", "FI-012606", "FI-007249", "FI-007269", "FI-007182", "FI-043806", "FI-004924", "FI-004890", "FI-004840", "FI-007948", "FI-009409", "FI-009460", "FI-010235", "FI-015740", "FI-015741", "FI-015863", "FI-009803", "FI-038222", "FI-038223", "FI-046572", "FI-046573", "FI-014661", "FI-007874", "FI-023012", "FI-005232", "FI-002764", "FI-006793", "FI-017711", "FI-013023", "FI-017204", "FI-040793", "FI-006572", "FI-012963", "FI-017311", "FI-017312", "FI-018489", "FI-018490", "FI-007641", "FI-042229", "FI-042232", "FI-042389", "FI-042390", "FI-042504", "FI-011177", "FI-009659", "FI-012164", "FI-006292", "FI-017749", "FI-017750", "FI-018405", "FI-018407", "FI-018549", "FI-018550", "FI-021449", "FI-006239", "FI-003514", "FI-007369", "FI-009310", "FI-022737", "FI-022855", "FI-023082", "FI-023083", "FI-006232", "FI-016812", "FI-007052", "FI-007368", "FI-008788", "FI-008725", "FI-008841", "FI-008716", "FI-008604", "FI-015858", "FI-015931", "FI-015932", "FI-020049", "FI-020050", "FI-023151", "FI-039949", "FI-041692", "FI-042088", "FI-038397", "FI-047284", "FI-047285", "FI-049049", "FI-049050", "FI-011361", "FI-011433", "FI-012244", "FI-011536", "FI-015410", "FI-017199", "FI-021970", "FI-021972", "FI-023516", "FI-023517", "FI-011491", "FI-014639", "FI-038602", "FI-038663", "FI-014668", "FI-040631", "FI-040633", "FI-042369", "FI-042370", "FI-011758", "FI-013411", "FI-011580", "FI-017093", "FI-017094", "FI-017351", "FI-018520", "FI-018707", "FI-018755", "FI-018794", "FI-018795", "FI-018852", "FI-018853", "FI-018854", "FI-021619", "FI-021620", "FI-021849", "FI-022379", "FI-022380", "FI-012632", "FI-023512", "FI-023513", "FI-012689", "FI-011748", "FI-011638", "FI-038945", "FI-038947", "FI-045859", "FI-046724", "FI-036161", "FI-011852", "FI-039178", "FI-043300", "FI-043769", "FI-043934", "FI-011907", "FI-011955", "FI-040044", "FI-040045", "FI-047840", "FI-047842", "FI-048081", "FI-014212", "FI-014333", "FI-018297", "FI-021161", "FI-021874", "FI-041320", "FI-045547", "FI-046786", "FI-046787", "FI-047462", "FI-047463", "FI-014944", "FI-016613", "FI-022545", "FI-014970", "FI-015045", "FI-015679", "FI-020846", "FI-013320", "FI-013334", "FI-021392", "FI-021394", "FI-022094", "FI-022096", "FI-022903", "FI-022904", "FI-018491", "FI-018492", "FI-042395", "FI-042396", "FI-018059", "FI-018041", "FI-021289", "FI-021290", "FI-021809", "FI-021810", "FI-021811", "FI-022118", "FI-022119", "FI-040781", "FI-040782", "FI-019664", "FI-019678", "FI-019701", "FI-019980", "FI-019716", "FI-019749", "FI-019963", "FI-019966", "FI-019968", "FI-019970", "FI-019971", "FI-019922", "FI-019926", "FI-019938", "FI-020007", "FI-019913", "FI-019915", "FI-020001", "FI-021444", "FI-021445", "FI-019781", "FI-019988", "FI-019761", "FI-021925", "FI-021927", "FI-019882", "FI-019894", "FI-019845", "FI-019858", "FI-020711", "FI-020760", "FI-020717", "FI-020123", "FI-021497", "FI-021498", "FI-021499", "FI-021512", "FI-021515", "FI-022668", "FI-021034", "FI-021035", "FI-021036", "FI-022313", "FI-022225", "FI-020203", "FI-036383", "FI-038979", "FI-039478", "FI-041577", "FI-041578", "FI-045917", "FI-038499", "FI-038500", "FI-032831", "FI-036023", "FI-036265", "FI-047075", "FI-047077", "FI-047244", "FI-032066", "FI-023645", "FI-038684", "FI-038686", "FI-042158", "FI-042159", "FI-042160", "FI-042292", "FI-013212", "FI-032206", "FI-026546", "FI-035543", "FI-035568", "FI-032798", "FI-036102", "FI-029054", "FI-032052", "FI-032560", "FI-035887", "FI-028478", "FI-036217", "FI-038138", "FI-038139", "FI-029909", "FI-031814", "FI-026377", "FI-031779", "FI-030052", "FI-030502", "FI-024234", "FI-031691", "FI-030916", "FI-036625", "FI-029710", "FI-039459", "FI-042093", "FI-042095", "FI-024735", "FI-036182", "FI-010340", "FI-010341", "FI-007803", "FI-035208", "FI-028830", "FI-034934", "FI-009991", "FI-046450", "FI-028248", "FI-023960", "FI-024664", "FI-035719", "FI-030506", "FI-034880", "FI-029623", "FI-034935", "FI-029800", "FI-025181", "FI-021626", "FI-036059", "FI-040000", "FI-040007", "FI-043859", "FI-043860", "FI-031848", "FI-030610", "FI-043558", "FI-024188", "FI-033118", "FI-034079", "FI-036197", "FI-036342", "FI-029199", "FI-029766", "FI-031877", "FI-024128", "FI-035920", "FI-031851", "FI-023677", "FI-028277", "FI-028397", "FI-032078", "FI-029578", "FI-026958", "FI-029706", "FI-023562", "FI-031925", "FI-025117", "FI-034855", "FI-031782", "FI-024990", "FI-036188", "FI-036167", "FI-029656", "FI-028831", "FI-034646", "FI-000354", "FI-036245", "FI-036343", "FI-042797", "FI-042798", "FI-040579", "FI-040580", "FI-040581", "FI-024824", "FI-033323", "FI-043421", "FI-043599", "FI-029466", "FI-031391", "FI-026035", "FI-028177", "FI-035522", "FI-041429", "FI-043826", "FI-032109", "FI-035710", "FI-024870", "FI-028771", "FI-024425", "FI-039087", "FI-039088", "FI-039089", "FI-036315", "FI-029777", "FI-032620", "FI-028059", "FI-031863", "FI-032912", "FI-024680", "FI-032034", "FI-035727", "FI-042282", "FI-042283", "FI-043071", "FI-032706", "FI-029643", "FI-032017", "FI-034684", "FI-028245", "FI-024919", "FI-035693", "FI-032035", "FI-028473", "FI-033477", "FI-030512", "FI-033354", "FI-035772", "FI-023821", "FI-032314", "FI-031991", "FI-031279", "FI-035849", "FI-038428", "FI-038875", "FI-041849", "FI-046005", "FI-032640", "FI-029925", "FI-031718", "FI-036548", "FI-030767", "FI-024471", "FI-034793", "FI-032281", "FI-035803", "FI-031713", "FI-029883", "FI-035033", "FI-024631", "FI-043422", "FI-044311", "FI-031256", "FI-035550", "FI-032736", "FI-036046", "FI-043755", "FI-031263", "FI-035833", "FI-025924", "FI-031694", "FI-031704", "FI-031454", "FI-028757", "FI-035114", "FI-030841", "FI-030874", "FI-035753", "FI-025028", "FI-035383", "FI-036533", "FI-039337", "FI-031958", "FI-028879", "FI-027384", "FI-043490", "FI-027404", "FI-033305", "FI-030060", "FI-036029", "FI-037999", "FI-045591", "FI-046994", "FI-046995", "FI-047955", "FI-038561", "FI-038562", "FI-032822", "FI-027733", "FI-028046", "FI-027345", "FI-027318", "FI-044561", "FI-044562", "FI-027916", "FI-027286", "FI-036108", "FI-028093", "FI-034197", "FI-027549", "FI-027589", "FI-027720", "FI-024333", "FI-024404", "FI-030618", "FI-024400", "FI-024399", "FI-029852", "FI-032277", "FI-035254", "FI-032167", "FI-035756", "FI-032476", "FI-028856", "FI-034748", "FI-028797", "FI-032764", "FI-033670", "FI-032461", "FI-035847", "FI-024805", "FI-024329", "FI-029802", "FI-033539", "FI-032672", "FI-036048", "FI-030548", "FI-034531", "FI-029157", "FI-034747", "FI-035226", "FI-032741", "FI-036051", "FI-030584", "FI-025201", "FI-032974", "FI-032165", "FI-028782", "FI-024049", "FI-023770", "FI-029380", "FI-029761", "FI-030311", "FI-032366", "FI-030527", "FI-034661", "FI-031942", "FI-036311", "FI-024905", "FI-038642", "FI-038837", "FI-038838", "FI-039546", "FI-039547", "FI-042139", "FI-042140", "FI-042152", "FI-042153", "FI-043429", "FI-048307", "FI-024277", "FI-026520", "FI-033058", "FI-033843", "FI-030044", "FI-032808", "FI-036047", "FI-032639", "FI-026529", "FI-033852", "FI-039216", "FI-039217", "FI-025357", "FI-027973", "FI-025450", "FI-030897", "FI-025417", "FI-030771", "FI-025447", "FI-030698", "FI-025441", "FI-025259", "FI-024939", "FI-032731", "FI-025313", "FI-036560", "FI-038884", "FI-024292", "FI-036913", "FI-024942", "FI-028615", "FI-035038", "FI-027131", "FI-033928", "FI-028840", "FI-027562", "FI-033969", "FI-027125", "FI-027111", "FI-037954", "FI-037959", "FI-040687", "FI-031349", "FI-027629", "FI-028136", "FI-027240", "FI-028587", "FI-037035", "FI-028213", "FI-026082", "FI-038519", "FI-038532", "FI-038618", "FI-038638", "FI-041241", "FI-043790", "FI-043791", "FI-033440", "FI-029143", "FI-025096", "FI-028666", "FI-036760", "FI-041592", "FI-028726", "FI-044420", "FI-026205", "FI-033922", "FI-027815", "FI-026003", "FI-033726", "FI-032225", "FI-026045", "FI-034344", "FI-028653", "FI-036001", "FI-028449", "FI-026701", "FI-026575", "FI-026567", "FI-028642", "FI-034496", "FI-032166", "FI-026718", "FI-028113", "FI-038489", "FI-038492", "FI-027001", "FI-027807", "FI-031177", "FI-026455", "FI-031144", "FI-035468", "FI-026783", "FI-025957", "FI-032355", "FI-025961", "FI-031163", "FI-039726", "FI-039762", "FI-031300", "FI-033828", "FI-026776", "FI-033572", "FI-032323", "FI-031178", "FI-031099", "FI-031200", "FI-031227", "FI-031187", "FI-035485", "FI-036298", "FI-028637", "FI-036033", "FI-031027", "FI-038849", "FI-031769", "FI-024601", "FI-035702", "FI-042794", "FI-043339", "FI-043340", "FI-026892", "FI-023594", "FI-024739", "FI-033411", "FI-031693", "FI-031885", "FI-026931", "FI-036184", "FI-039022", "FI-039027", "FI-032801", "FI-035997", "FI-036572", "FI-036661", "FI-039812", "FI-039813", "FI-039858", "FI-031765", "FI-041630", "FI-042142", "FI-029424", "FI-028975", "FI-032609", "FI-035930", "FI-036521", "FI-036970", "FI-037037", "FI-032661", "FI-035911", "FI-029767", "FI-035679", "FI-042512", "FI-042513", "FI-023555", "FI-032884", "FI-029043", "FI-032688", "FI-035247", "FI-029571", "FI-024446", "FI-032960", "FI-036594", "FI-036654", "FI-036587", "FI-036658", "FI-036252", "FI-030387", "FI-040464", "FI-040720", "FI-044011", "FI-033756", "FI-033772", "FI-036193", "FI-032390", "FI-033780", "FI-026362", "FI-032703", "FI-036028", "FI-029190", "FI-035861", "FI-026964", "FI-026960", "FI-035528", "FI-031336", "FI-033794", "FI-035538", "FI-031361", "FI-028035", "FI-035119", "FI-036843", "FI-036844", "FI-025110", "FI-025138", "FI-033446", "FI-025137", "FI-033447", "FI-046816", "FI-023802", "FI-034938", "FI-027398", "FI-031340", "FI-047204", "FI-027521", "FI-034184", "FI-027387", "FI-027426", "FI-033542", "FI-036336", "FI-027275", "FI-032664", "FI-035751", "FI-028164", "FI-036611", "FI-032374", "FI-027438", "FI-034192", "FI-027884", "FI-034356", "FI-031569", "FI-035237", "FI-025425", "FI-029876", "FI-034204", "FI-029877", "FI-028607", "FI-027169", "FI-036509", "FI-036845", "FI-036846", "FI-047962", "FI-047963", "FI-027201", "FI-034046", "FI-027794", "FI-034375", "FI-026954", "FI-033947", "FI-029962", "FI-035487", "FI-035496", "FI-041054", "FI-029260", "FI-028700", "FI-033724", "FI-035218", "FI-032402", "FI-035813", "FI-033944", "FI-031042", "FI-031167", "FI-032537", "FI-032403", "FI-037030", "FI-028369", "FI-030328", "FI-032079", "FI-043157", "FI-043203", "FI-035736", "FI-032689", "FI-035475", "FI-036322", "FI-036357", "FI-030252", "FI-038293", "FI-038295", "FI-038296", "FI-031990", "FI-035271", "FI-024050", "FI-035846", "FI-040244", "FI-040245", "FI-042127", "FI-042128", "FI-043210", "FI-045510", "FI-029236", "FI-031733", "FI-032135", "FI-034927", "FI-025208", "FI-030430", "FI-035253", "FI-031856", "FI-032411", "FI-032265", "FI-038156", "FI-038157", "FI-029403", "FI-031935", "FI-032252", "FI-035759", "FI-029837", "FI-029905", "FI-040910", "FI-040911", "FI-041987", "FI-041989", "FI-041990", "FI-042789", "FI-030322", "FI-030469", "FI-030258", "FI-029719", "FI-035259", "FI-032369", "FI-030297", "FI-032663", "FI-029844", "FI-030323", "FI-023571", "FI-023805", "FI-023897", "FI-030559", "FI-036333", "FI-036360", "FI-027559", "FI-032547", "FI-030458", "FI-027551", "FI-031764", "FI-027554", "FI-027339", "FI-034101", "FI-027731", "FI-031762", "FI-031554", "FI-032141", "FI-031549", "FI-031763", "FI-043833", "FI-043835", "FI-031917", "FI-026924", "FI-025534", "FI-039537", "FI-039538", "FI-025748", "FI-025735", "FI-030945", "FI-025778", "FI-030972", "FI-030985", "FI-030977", "FI-026919", "FI-025674", "FI-025818", "FI-025811", "FI-033603", "FI-039534" };
            //List<string> names = new List<string> { "FI-028838", "FI-039819", "FI-043783" };

            List<string> datos = new List<string>();
            string accessToken = GetToken(_url, _apiVersion).Result;
            foreach (var name in names)
            {
                IEnumerable<string> resultado = ConsultarAPIFiltrandoContactos(_url, _apiVersion, accessToken, name);
                try
                {
                    if (null != resultado)
                    {
                        foreach (var item in resultado)
                        {
                            datos.Add(item);
                            logger.Info(datos.Count);
                        }
                    }
                }
                catch (Exception e)
                {
                    logger.Error(e.Message);
                    logger.Error(e.InnerException);
                    logger.Error(e.StackTrace);
                }
            }
            logger.Info($"Registros obtenidos de contactos: {datos.Count}");
            return datos;
        }
        private IEnumerable<string> ObtenerDatosContratos()
        {
            logger.Info($"ObtenerDatosEntidad contratos");
            List<string> names = new List<string> { "FI-6120-0002", "FI-1201-0538", "FI-6055-0043", "FI-6078-0039", "FI-1901-0985", "FI-1104-0173", "FI-1901-0997", "FI-6106-0089", "FI-2002-0543", "FI-2002-0544", "FI-6134-0043", "FI-6102-0006", "FI-6101-0026", "FI-6021-0007", "FI-6193-0062", "FI-1102-0232", "FI-1105-0962", "FI-1102-0226", "FI-6039-0112", "FI-6180-0046", "FI-6109-0109", "FI-6011-0134", "FI-1105-0953", "FI-3003-0084", "FI-1010-0282", "FI-1901-0787", "FI-6034-0094", "FI-1103-0200", "FI-6005-0092", "FI-6193-0251", "FI-6107-0055", "FI-6080-0051", "FI-6048-0063", "FI-1501-0623", "FI-1202-0382", "FI-1502-0535", "FI-1701-0409", "FI-1502-0581", "FI-2002-0477", "FI-6115-0123", "FI-6129-0015", "FI-2102-0298", "FI-6093-0056", "FI-3004-0134", "FI-3004-0106", "FI-6047-0125", "FI-6142-0005", "FI-6115-0130", "FI-1201-0554", "FI-1901-1047", "FI-6015-0177", "FI-6052-0040", "FI-1012-0475", "FI-1105-0925", "FI-6107-0020", "FI-2301-1237", "FI-6107-0016", "FI-1701-0041", "FI-6075-0186", "FI-6193-0180", "FI-6193-0147", "FI-6047-0202", "FI-1101-0351", "FI-6133-0009", "FI-6174-0009", "FI-1501-0543", "FI-2001-0734", "FI-1501-0575", "FI-6088-0042", "FI-6091-0074", "FI-6054-0059", "FI-6093-0026", "FI-6010-0003", "FI-6041-0073", "FI-6180-0010", "FI-6193-0225", "FI-6075-0095", "FI-1502-0520", "FI-6075-0276", "FI-6034-0088", "FI-6038-0036", "FI-1201-0576", "FI-6029-0148", "FI-6034-0020", "FI-6032-0002", "FI-6117-0158", "FI-1501-0474", "FI-1007-0279", "FI-1105-0964", "FI-6004-0191", "FI-6017-0071", "FI-6133-0152", "FI-6151-0014", "FI-6067-0093", "FI-6075-0275", "FI-6106-0074", "FI-1007-0040", "FI-1105-0995", "FI-2501-0063", "FI-6071-0384", "FI-1701-0524", "FI-6147-0123", "FI-6013-0019", "FI-2301-1068", "FI-3002-0002", "FI-6088-0024", "FI-6064-0217", "FI-1105-1004", "FI-6131-0057", "FI-6107-0039", "FI-6159-0014", "FI-1204-0227", "FI-2301-0921", "FI-6133-0153", "FI-6038-0075", "FI-2102-0287", "FI-2301-0734", "FI-1202-0536", "FI-6048-0130", "FI-1202-0488", "FI-1102-0201", "FI-1804-0166", "FI-6175-0035", "FI-6046-0020", "FI-6071-0153", "FI-6097-0005", "FI-1901-0872", "FI-6075-0173", "FI-6034-0003", "FI-6073-0001", "FI-6120-0202", "FI-6071-0016", "FI-6013-0010", "FI-6073-0052", "FI-6174-0078", "FI-1701-0321", "FI-6133-0057", "FI-6002-0121", "FI-6035-0004", "FI-1802-0626", "FI-1201-0506", "FI-2702-0122", "FI-1901-1043", "FI-1204-0328", "FI-6164-0049", "FI-6134-0013", "FI-6075-0195", "FI-6016-0143", "FI-1502-0536", "FI-2301-1006", "FI-2301-1005", "FI-2301-1009", "FI-1501-0437", "FI-3005-0168", "FI-2401-0263", "FI-6145-0022", "FI-6048-0108", "FI-1801-0512", "FI-1901-0972", "FI-1901-0983", "FI-6016-0011", "FI-1011-0464", "FI-6019-0064", "FI-1804-0204", "FI-6025-0004", "FI-1301-0125", "FI-6040-0040", "FI-2101-0194", "FI-2101-0197", "FI-1012-0367", "FI-1202-0354", "FI-1006-0280", "FI-6121-0022", "FI-6004-0202", "FI-6006-0034", "FI-6019-0021", "FI-1901-0792", "FI-2102-0199", "FI-6088-0012", "FI-6158-0017", "FI-6097-0017", "FI-1301-0132", "FI-1101-0477", "FI-6006-0142", "FI-1105-1027", "FI-6141-0009", "FI-6115-0025", "FI-6167-0006", "FI-6050-0161", "FI-1804-0194", "FI-2301-0436", "FI-2301-0437", "FI-6176-0014", "FI-6140-0010", "FI-6130-0023", "FI-6006-0013", "FI-2702-0176", "FI-1802-0653", "FI-6050-0004", "FI-6058-0037", "FI-1301-0120", "FI-6172-0021", "FI-2301-0832", "FI-6050-0010", "FI-6120-0206", "FI-6138-0197", "FI-1701-0214", "FI-6015-0122", "FI-6015-0173", "FI-6039-0025", "FI-6071-0039", "FI-2301-0795", "FI-6141-0004", "FI-6072-0081", "FI-6075-0249", "FI-6129-0003", "FI-2301-1018", "FI-6001-0109", "FI-3003-0071", "FI-6046-0106", "FI-6017-0036", "FI-6017-0024", "FI-6005-0121", "FI-6091-0136", "FI-8190-0155", "FI-6021-0052", "FI-1901-0893", "FI-6171-0036", "FI-6094-0007", "FI-6156-0003", "FI-2001-0766", "FI-2001-0404", "FI-6093-0179", "FI-6139-0040", "FI-6181-0058", "FI-1901-0784", "FI-6106-0016", "FI-6091-0148", "FI-2002-0742", "FI-1204-0383", "FI-6116-0192", "FI-6101-0052", "FI-1012-0409", "FI-1202-0415", "FI-6036-0071", "FI-1702-0600", "FI-6039-0064", "FI-6168-0054", "FI-2901-0105", "FI-6047-0140", "FI-6143-0005", "FI-6043-0019", "FI-6004-0214", "FI-1304-0218", "FI-6178-0030", "FI-6071-0260", "FI-6058-0088", "FI-1011-0277", "FI-6116-0063", "FI-2002-0730", "FI-7126-0010", "FI-6193-0198", "FI-6164-0047", "FI-6091-0042", "FI-6145-0015", "FI-6038-0072", "FI-6048-0008", "FI-6165-0013", "FI-1804-0223", "FI-1105-0928", "FI-1004-0234", "FI-6176-0015", "FI-1202-0527", "FI-6053-0021", "FI-6143-0025", "FI-1501-0460", "FI-1105-1010", "FI-6164-0013", "FI-1105-1009", "FI-1701-0084", "FI-2301-1031", "FI-2301-1027", "FI-6011-0138", "FI-6016-0084", "FI-6105-0061", "FI-2401-0338", "FI-1103-0197", "FI-6035-0061", "FI-6082-0010", "FI-2501-0104", "FI-2501-0096", "FI-6075-0189", "FI-1103-0204", "FI-6133-0063", "FI-1103-0202", "FI-6153-0009", "FI-6058-0046", "FI-2401-0295", "FI-6071-0409", "FI-6117-0015", "FI-6092-0015", "FI-6012-0014", "FI-6046-0074", "FI-6039-0044", "FI-1502-0642", "FI-6053-0011", "FI-6054-0012", "FI-6003-0062", "FI-6118-0009", "FI-6138-0149", "FI-6115-0039", "FI-1702-0475", "FI-6011-0010", "FI-6030-0072", "FI-6059-0006", "FI-6095-0011", "FI-6088-0017", "FI-2901-0092", "FI-6064-0064", "FI-2401-0331", "FI-6021-0015", "FI-6061-0008", "FI-1702-0648", "FI-6005-0119", "FI-6021-0022", "FI-8191-0076", "FI-6071-0252", "FI-6004-0311", "FI-6071-0056", "FI-6062-0021", "FI-2701-0093", "FI-2701-0141", "FI-6069-0040", "FI-6038-0037", "FI-1501-0612", "FI-6016-0097", "FI-6120-0118", "FI-6030-0125", "FI-6093-0044", "FI-6140-0037", "FI-6035-0011", "FI-6044-0049", "FI-2301-0911", "FI-6003-0197", "FI-3003-0027", "FI-6051-0006", "FI-6039-0012", "FI-6001-0025", "FI-6043-0027", "FI-6001-0064", "FI-6069-0168", "FI-6080-0039", "FI-1101-0472", "FI-6071-0054", "FI-6129-0029", "FI-1001-0205", "FI-1001-0256", "FI-1001-0275", "FI-1001-0276", "FI-1001-0297", "FI-1001-0298", "FI-1001-0302", "FI-1001-0316", "FI-1001-0330", "FI-1001-0336", "FI-1001-0355", "FI-1001-0362", "FI-1001-0367", "FI-1001-0369", "FI-1001-0384", "FI-1001-0390", "FI-1001-0392", "FI-1001-0398", "FI-1001-0400", "FI-1001-0408", "FI-1001-0414", "FI-1001-0415", "FI-1001-0429", "FI-1001-0441", "FI-1001-0442", "FI-1001-0445", "FI-1001-0451", "FI-1001-0471", "FI-1001-0483", "FI-1002-0195", "FI-1002-0262", "FI-1002-0266", "FI-1002-0268", "FI-1002-0270", "FI-1002-0275", "FI-1002-0278", "FI-1002-0288", "FI-1002-0291", "FI-1002-0293", "FI-1002-0299", "FI-1002-0301", "FI-1002-0304", "FI-1002-0306", "FI-1002-0313", "FI-1002-0323", "FI-1002-0330", "FI-1002-0332", "FI-1002-0335", "FI-1002-0336", "FI-1002-0340", "FI-1002-0341", "FI-1002-0348", "FI-1002-0357", "FI-1002-0370", "FI-1002-0380", "FI-1002-0382", "FI-1002-0385", "FI-1002-0386", "FI-1002-0389", "FI-1002-0391", "FI-1002-0392", "FI-1002-0393", "FI-1002-0397", "FI-1002-0402", "FI-1002-0404", "FI-1002-0414", "FI-1002-0423", "FI-1003-0097", "FI-1003-0112", "FI-1003-0152", "FI-1003-0162", "FI-1003-0169", "FI-1003-0174", "FI-1003-0184", "FI-1003-0188", "FI-1003-0192", "FI-1003-0200", "FI-1003-0221", "FI-1003-0227", "FI-1003-0231", "FI-1003-0232", "FI-1003-0235", "FI-1003-0236", "FI-1003-0245", "FI-1003-0247", "FI-1003-0251", "FI-1003-0253", "FI-1003-0265", "FI-1003-0275", "FI-1004-0059", "FI-1004-0104", "FI-1004-0135", "FI-1004-0159", "FI-1004-0169", "FI-1004-0176", "FI-1004-0183", "FI-1004-0188", "FI-1004-0190", "FI-1004-0192", "FI-1004-0219", "FI-1004-0221", "FI-1004-0222", "FI-1004-0225", "FI-1004-0247", "FI-1005-0007", "FI-1005-0155", "FI-1005-0161", "FI-1005-0166", "FI-1005-0175", "FI-1005-0186", "FI-1005-0202", "FI-1005-0218", "FI-1005-0221", "FI-1005-0222", "FI-1005-0232", "FI-1006-0130", "FI-1006-0178", "FI-1006-0187", "FI-1006-0189", "FI-1006-0206", "FI-1006-0245", "FI-1006-0283", "FI-1006-0291", "FI-1006-0307", "FI-1006-0316", "FI-1006-0320", "FI-1006-0389", "FI-1006-0402", "FI-1006-0409", "FI-1006-0423", "FI-1007-0180", "FI-1007-0184", "FI-1007-0198", "FI-1007-0215", "FI-1007-0218", "FI-1007-0223", "FI-1007-0234", "FI-1007-0238", "FI-1007-0261", "FI-1007-0269", "FI-1007-0291", "FI-1007-0292", "FI-1007-0311", "FI-1007-0315", "FI-1007-0326", "FI-1008-0002", "FI-1008-0011", "FI-1008-0013", "FI-1008-0031", "FI-1008-0051", "FI-1009-0075", "FI-1009-0077", "FI-1009-0111", "FI-1009-0114", "FI-1009-0115", "FI-1009-0116", "FI-1009-0117", "FI-1009-0119", "FI-1009-0126", "FI-1009-0129", "FI-1009-0133", "FI-1009-0138", "FI-1009-0140", "FI-1009-0150", "FI-1009-0151", "FI-1009-0163", "FI-1009-0166", "FI-1010-0032", "FI-1010-0232", "FI-1010-0233", "FI-1010-0239", "FI-1010-0247", "FI-1010-0249", "FI-1010-0251", "FI-1010-0258", "FI-1010-0265", "FI-1010-0267", "FI-1010-0274", "FI-1010-0280", "FI-1010-0308", "FI-1010-0317", "FI-1010-0336", "FI-1010-0362", "FI-1010-0374", "FI-1010-0379", "FI-1010-0393", "FI-1010-0410", "FI-1010-0412", "FI-1010-0416", "FI-1010-0422", "FI-1011-0224", "FI-1011-0258", "FI-1011-0266", "FI-1011-0283", "FI-1011-0285", "FI-1011-0286", "FI-1011-0300", "FI-1011-0308", "FI-1011-0313", "FI-1011-0321", "FI-1011-0362", "FI-1011-0376", "FI-1011-0382", "FI-1011-0394", "FI-1011-0395", "FI-1011-0399", "FI-1011-0400", "FI-1011-0402", "FI-1011-0438", "FI-1011-0449", "FI-1011-0466", "FI-1011-0481", "FI-1011-0486", "FI-1011-0488", "FI-1012-0234", "FI-1012-0252", "FI-1012-0291", "FI-1012-0342", "FI-1012-0353", "FI-1012-0379", "FI-1012-0381", "FI-1012-0386", "FI-1012-0390", "FI-1012-0423", "FI-1012-0432", "FI-1012-0433", "FI-1012-0447", "FI-1012-0453", "FI-1012-0465", "FI-1012-0468", "FI-1012-0498", "FI-1012-0501", "FI-1012-0503", "FI-1012-0517", "FI-1012-0522", "FI-1012-0544", "FI-1101-0121", "FI-1101-0277", "FI-1101-0311", "FI-1101-0314", "FI-1101-0364", "FI-1101-0458", "FI-1101-0474", "FI-1101-0490", "FI-1101-0502", "FI-1101-0509", "FI-1101-0518", "FI-1101-0520", "FI-1101-0545", "FI-1101-0573", "FI-1102-0182", "FI-1102-0210", "FI-1102-0211", "FI-1103-0110", "FI-1103-0155", "FI-1103-0172", "FI-1103-0195", "FI-1103-0206", "FI-1103-0216", "FI-1103-0218", "FI-1103-0223", "FI-1104-0117", "FI-1104-0119", "FI-1104-0136", "FI-1104-0163", "FI-1104-0171", "FI-1104-0172", "FI-1104-0176", "FI-1104-0189", "FI-1105-0039", "FI-1105-0043", "FI-1105-0165", "FI-1105-0216", "FI-1105-0447", "FI-1105-0504", "FI-1105-0587", "FI-1105-0608", "FI-1105-0655", "FI-1105-0660", "FI-1105-0694", "FI-1105-0695", "FI-1105-0708", "FI-1105-0724", "FI-1105-0749", "FI-1105-0750", "FI-1105-0751", "FI-1105-0764", "FI-1105-0801", "FI-1105-0830", "FI-1105-0836", "FI-1105-0875", "FI-1105-0931", "FI-1105-0975", "FI-1105-0976", "FI-1105-1013", "FI-1105-1032", "FI-1105-1040", "FI-1105-1041", "FI-1201-0415", "FI-1201-0427", "FI-1201-0439", "FI-1201-0463", "FI-1201-0465", "FI-1201-0485", "FI-1201-0490", "FI-1201-0498", "FI-1201-0501", "FI-1201-0509", "FI-1201-0515", "FI-1201-0525", "FI-1201-0561", "FI-1201-0566", "FI-1201-0590", "FI-1202-0112", "FI-1202-0300", "FI-1202-0346", "FI-1202-0381", "FI-1202-0387", "FI-1202-0401", "FI-1202-0487", "FI-1204-0232", "FI-1204-0274", "FI-1204-0311", "FI-1204-0354", "FI-1204-0358", "FI-1204-0365", "FI-1204-0380", "FI-1204-0399", "FI-1204-0403", "FI-1301-0027", "FI-1301-0029", "FI-1301-0040", "FI-1301-0044", "FI-1301-0087", "FI-1301-0098", "FI-1301-0119", "FI-1301-0134", "FI-1301-0137", "FI-1301-0139", "FI-1302-0052", "FI-1302-0074", "FI-1302-0093", "FI-1303-0129", "FI-1303-0142", "FI-1303-0153", "FI-1303-0156", "FI-1303-0183", "FI-1303-0202", "FI-1304-0166", "FI-1304-0179", "FI-1304-0233", "FI-1304-0258", "FI-1501-0016", "FI-1501-0386", "FI-1501-0449", "FI-1501-0484", "FI-1501-0520", "FI-1501-0526", "FI-1501-0535", "FI-1501-0542", "FI-1501-0590", "FI-1502-0334", "FI-1502-0398", "FI-1502-0447", "FI-1502-0477", "FI-1502-0484", "FI-1502-0511", "FI-1502-0534", "FI-1502-0538", "FI-1502-0542", "FI-1502-0548", "FI-1502-0599", "FI-1502-0617", "FI-1502-0680", "FI-1701-0011", "FI-1701-0043", "FI-1701-0069", "FI-1701-0077", "FI-1701-0120", "FI-1701-0127", "FI-1701-0159", "FI-1701-0195", "FI-1701-0244", "FI-1701-0252", "FI-1701-0265", "FI-1701-0279", "FI-1701-0346", "FI-1701-0348", "FI-1701-0375", "FI-1701-0399", "FI-1701-0403", "FI-1701-0432", "FI-1701-0441", "FI-1701-0460", "FI-1701-0466", "FI-1701-0499", "FI-1701-0543", "FI-1702-0336", "FI-1702-0414", "FI-1702-0442", "FI-1702-0447", "FI-1702-0470", "FI-1702-0472", "FI-1702-0480", "FI-1702-0489", "FI-1702-0491", "FI-1702-0492", "FI-1702-0502", "FI-1702-0506", "FI-1702-0511", "FI-1702-0559", "FI-1801-0165", "FI-1801-0353", "FI-1801-0367", "FI-1801-0467", "FI-1801-0492", "FI-1801-0523", "FI-1801-0534", "FI-1801-0535", "FI-1801-0563", "FI-1801-0573", "FI-1801-0629", "FI-1801-0640", "FI-1801-0646", "FI-1801-0728", "FI-1801-0731", "FI-1802-0504", "FI-1802-0513", "FI-1802-0517", "FI-1802-0535", "FI-1802-0542", "FI-1802-0564", "FI-1802-0613", "FI-1802-0624", "FI-1802-0625", "FI-1802-0640", "FI-1802-0644", "FI-1802-0661", "FI-1802-0667", "FI-1802-0669", "FI-1802-0670", "FI-1802-0678", "FI-1802-0710", "FI-1802-0762", "FI-1802-0785", "FI-1802-0839", "FI-1802-0860", "FI-1802-0870", "FI-1802-0901", "FI-1803-0143", "FI-1803-0174", "FI-1803-0175", "FI-1803-0180", "FI-1804-0110", "FI-1804-0155", "FI-1804-0173", "FI-1804-0183", "FI-1901-0046", "FI-1901-0109", "FI-1901-0118", "FI-1901-0230", "FI-1901-0638", "FI-1901-0641", "FI-1901-0716", "FI-1901-0818", "FI-1901-0905", "FI-1901-0961", "FI-1901-0963", "FI-1901-0978", "FI-1901-0990", "FI-1901-1007", "FI-1901-1010", "FI-1901-1041", "FI-1901-1126", "FI-1902-0171", "FI-1902-0213", "FI-1902-0243", "FI-1902-0257", "FI-1902-0265", "FI-2001-0187", "FI-2001-0299", "FI-2001-0307", "FI-2001-0350", "FI-2001-0368", "FI-2001-0449", "FI-2001-0450", "FI-2001-0465", "FI-2001-0517", "FI-2001-0518", "FI-2001-0628", "FI-2001-0629", "FI-2001-0643", "FI-2001-0644", "FI-2001-0651", "FI-2001-0689", "FI-2001-0693", "FI-2001-0706", "FI-2001-0709", "FI-2001-0713", "FI-2001-0721", "FI-2001-0738", "FI-2001-0754", "FI-2001-0755", "FI-2001-0764", "FI-2001-0771", "FI-2001-0779", "FI-2001-0792", "FI-2001-0818", "FI-2001-0833", "FI-2002-0004", "FI-2002-0423", "FI-2002-0424", "FI-2002-0443", "FI-2002-0444", "FI-2002-0448", "FI-2002-0497", "FI-2002-0499", "FI-2002-0507", "FI-2002-0534", "FI-2002-0535", "FI-2002-0547", "FI-2002-0553", "FI-2002-0554", "FI-2002-0560", "FI-2002-0561", "FI-2002-0562", "FI-2002-0563", "FI-2002-0565", "FI-2002-0566", "FI-2002-0573", "FI-2002-0574", "FI-2002-0594", "FI-2002-0595", "FI-2002-0600", "FI-2002-0601", "FI-2002-0612", "FI-2002-0613", "FI-2002-0614", "FI-2002-0615", "FI-2002-0629", "FI-2002-0630", "FI-2002-0645", "FI-2002-0682", "FI-2002-0687", "FI-2002-0689", "FI-2002-0693", "FI-2002-0700", "FI-2002-0727", "FI-2002-0736", "FI-2002-0741", "FI-2002-0744", "FI-2002-0750", "FI-2002-0772", "FI-2002-0810", "FI-2002-0818", "FI-2002-0848", "FI-2101-0184", "FI-2101-0237", "FI-2101-0239", "FI-2101-0240", "FI-2101-0241", "FI-2101-0242", "FI-2101-0254", "FI-2101-0271", "FI-2101-0279", "FI-2101-0292", "FI-2101-0293", "FI-2101-0300", "FI-2101-0303", "FI-2101-0304", "FI-2102-0150", "FI-2102-0157", "FI-2102-0249", "FI-2102-0251", "FI-2102-0291", "FI-2102-0294", "FI-2102-0309", "FI-2102-0345", "FI-2301-0019", "FI-2301-0270", "FI-2301-0271", "FI-2301-0529", "FI-2301-0667", "FI-2301-0668", "FI-2301-0812", "FI-2301-0813", "FI-2301-0844", "FI-2301-0893", "FI-2301-0986", "FI-2301-1058", "FI-2301-1059", "FI-2301-1213", "FI-2301-1224", "FI-2301-1257", "FI-2301-1278", "FI-2301-1279", "FI-2301-1280", "FI-2401-0111", "FI-2401-0176", "FI-2401-0210", "FI-2401-0221", "FI-2401-0227", "FI-2401-0237", "FI-2401-0247", "FI-2401-0251", "FI-2401-0264", "FI-2401-0289", "FI-2401-0291", "FI-2401-0302", "FI-2501-0045", "FI-2501-0125", "FI-2601-0009", "FI-2601-0013", "FI-2601-0019", "FI-2601-0032", "FI-2601-0033", "FI-2601-0034", "FI-2601-0035", "FI-2601-0041", "FI-2601-0045", "FI-2601-0053", "FI-2601-0064", "FI-2601-0065", "FI-2601-0079", "FI-2601-0084", "FI-2601-0085", "FI-2601-0087", "FI-2601-0114", "FI-2701-0019", "FI-2701-0023", "FI-2701-0039", "FI-2701-0054", "FI-2701-0065", "FI-2701-0073", "FI-2701-0091", "FI-2701-0098", "FI-2701-0104", "FI-2701-0113", "FI-2702-0013", "FI-2702-0021", "FI-2702-0035", "FI-2702-0058", "FI-2702-0073", "FI-2702-0107", "FI-2702-0140", "FI-2703-0015", "FI-2703-0017", "FI-2703-0020", "FI-2703-0022", "FI-2703-0024", "FI-2703-0025", "FI-2703-0033", "FI-2703-0037", "FI-2703-0046", "FI-2703-0047", "FI-2703-0049", "FI-2703-0058", "FI-2703-0060", "FI-2703-0064", "FI-2703-0075", "FI-2703-0077", "FI-2704-0017", "FI-2704-0073", "FI-2704-0089", "FI-2704-0105", "FI-2705-0021", "FI-2705-0033", "FI-2705-0045", "FI-2705-0057", "FI-2705-0058", "FI-2705-0121", "FI-2901-0005", "FI-2901-0056", "FI-2901-0080", "FI-2901-0110", "FI-3001-0005", "FI-3002-0018", "FI-3002-0021", "FI-3002-0022", "FI-3002-0035", "FI-3002-0104", "FI-3002-0131", "FI-3003-0020", "FI-3003-0043", "FI-3004-0040", "FI-3004-0074", "FI-3004-0108", "FI-3004-0122", "FI-3004-0151", "FI-3004-0157", "FI-3004-0192", "FI-3004-0232", "FI-3004-0242", "FI-3005-0142", "FI-6001-0008", "FI-6001-0031", "FI-6001-0057", "FI-6001-0104", "FI-6001-0112", "FI-6001-0128", "FI-6001-0155", "FI-6001-0165", "FI-6001-0166", "FI-6002-0046", "FI-6002-0054", "FI-6002-0099", "FI-6002-0120", "FI-6002-0123", "FI-6002-0124", "FI-6003-0004", "FI-6003-0010", "FI-6003-0026", "FI-6003-0031", "FI-6003-0050", "FI-6003-0056", "FI-6003-0078", "FI-6003-0085", "FI-6003-0088", "FI-6003-0089", "FI-6003-0104", "FI-6003-0106", "FI-6003-0136", "FI-6003-0139", "FI-6003-0146", "FI-6003-0155", "FI-6003-0163", "FI-6003-0193", "FI-6003-0228", "FI-6004-0006", "FI-6004-0023", "FI-6004-0026", "FI-6004-0050", "FI-6004-0052", "FI-6004-0112", "FI-6004-0126", "FI-6004-0195", "FI-6004-0244", "FI-6004-0263", "FI-6004-0294", "FI-6004-0321", "FI-6004-0364", "FI-6004-0384", "FI-6005-0072", "FI-6005-0139", "FI-6006-0042", "FI-6006-0046", "FI-6006-0055", "FI-6006-0058", "FI-6006-0084", "FI-6006-0125", "FI-6006-0156", "FI-6007-0018", "FI-6007-0045", "FI-6007-0094", "FI-6007-0108", "FI-6010-0005", "FI-6010-0008", "FI-6010-0015", "FI-6011-0029", "FI-6011-0040", "FI-6011-0053", "FI-6011-0056", "FI-6011-0064", "FI-6011-0127", "FI-6011-0146", "FI-6011-0156", "FI-6012-0020", "FI-6012-0024", "FI-6012-0065", "FI-6012-0085", "FI-6012-0088", "FI-6012-0093", "FI-6012-0102", "FI-6013-0004", "FI-6013-0007", "FI-6013-0022", "FI-6013-0023", "FI-6015-0019", "FI-6015-0040", "FI-6015-0053", "FI-6015-0056", "FI-6015-0109", "FI-6015-0155", "FI-6016-0023", "FI-6016-0109", "FI-6016-0150", "FI-6017-0041", "FI-6019-0030", "FI-6020-0007", "FI-6020-0018", "FI-6020-0019", "FI-6020-0026", "FI-6021-0060", "FI-6022-0004", "FI-6024-0001", "FI-6027-0004", "FI-6027-0019", "FI-6027-0053", "FI-6028-0003", "FI-6028-0006", "FI-6028-0035", "FI-6028-0054", "FI-6028-0055", "FI-6028-0056", "FI-6029-0008", "FI-6029-0015", "FI-6029-0029", "FI-6029-0035", "FI-6029-0059", "FI-6029-0062", "FI-6029-0074", "FI-6029-0099", "FI-6029-0151", "FI-6029-0164", "FI-6029-0178", "FI-6029-0197", "FI-6029-0221", "FI-6029-0223", "FI-6030-0022", "FI-6030-0026", "FI-6030-0070", "FI-6030-0075", "FI-6030-0078", "FI-6030-0092", "FI-6030-0093", "FI-6030-0104", "FI-6030-0126", "FI-6030-0132", "FI-6031-0055", "FI-6031-0066", "FI-6033-0026", "FI-6033-0056", "FI-6033-0080", "FI-6033-0090", "FI-6034-0072", "FI-6035-0021", "FI-6035-0059", "FI-6036-0008", "FI-6036-0045", "FI-6036-0046", "FI-6036-0073", "FI-6037-0010", "FI-6037-0036", "FI-6037-0053", "FI-6039-0054", "FI-6039-0084", "FI-6040-0003", "FI-6040-0026", "FI-6040-0028", "FI-6040-0065", "FI-6041-0009", "FI-6041-0013", "FI-6041-0020", "FI-6041-0025", "FI-6041-0103", "FI-6041-0108", "FI-6043-0020", "FI-6043-0044", "FI-6044-0017", "FI-6044-0052", "FI-6046-0003", "FI-6046-0006", "FI-6046-0022", "FI-6046-0038", "FI-6046-0048", "FI-6046-0063", "FI-6046-0066", "FI-6046-0072", "FI-6046-0135", "FI-6046-0142", "FI-6046-0155", "FI-6047-0028", "FI-6047-0045", "FI-6047-0051", "FI-6047-0057", "FI-6047-0063", "FI-6047-0065", "FI-6047-0075", "FI-6047-0082", "FI-6047-0085", "FI-6047-0093", "FI-6047-0099", "FI-6047-0121", "FI-6047-0127", "FI-6047-0144", "FI-6047-0147", "FI-6047-0183", "FI-6047-0188", "FI-6048-0038", "FI-6048-0102", "FI-6048-0107", "FI-6048-0109", "FI-6048-0111", "FI-6048-0123", "FI-6048-0147", "FI-6050-0043", "FI-6050-0046", "FI-6050-0109", "FI-6050-0110", "FI-6050-0121", "FI-6050-0138", "FI-6050-0163", "FI-6050-0165", "FI-6050-0180", "FI-6052-0016", "FI-6052-0026", "FI-6052-0048", "FI-6054-0018", "FI-6054-0035", "FI-6054-0037", "FI-6054-0040", "FI-6054-0072", "FI-6054-0073", "FI-6055-0023", "FI-6056-0018", "FI-6056-0028", "FI-6057-0003", "FI-6058-0025", "FI-6058-0060", "FI-6058-0083", "FI-6058-0085", "FI-6058-0098", "FI-6058-0111", "FI-6059-0028", "FI-6060-0011", "FI-6060-0026", "FI-6060-0034", "FI-6060-0041", "FI-6060-0076", "FI-6060-0077", "FI-6060-0089", "FI-6060-0092", "FI-6061-0018", "FI-6062-0009", "FI-6062-0029", "FI-6063-0042", "FI-6064-0005", "FI-6064-0012", "FI-6064-0019", "FI-6064-0068", "FI-6064-0083", "FI-6064-0096", "FI-6064-0116", "FI-6064-0157", "FI-6064-0172", "FI-6064-0193", "FI-6064-0238", "FI-6064-0242", "FI-6064-0246", "FI-6064-0248", "FI-6064-0249", "FI-6064-0253", "FI-6065-0032", "FI-6067-0003", "FI-6067-0009", "FI-6067-0010", "FI-6067-0032", "FI-6067-0041", "FI-6067-0042", "FI-6067-0076", "FI-6067-0095", "FI-6068-0025", "FI-6068-0038", "FI-6068-0039", "FI-6069-0008", "FI-6069-0013", "FI-6069-0028", "FI-6069-0035", "FI-6069-0048", "FI-6069-0052", "FI-6069-0055", "FI-6069-0056", "FI-6069-0059", "FI-6069-0060", "FI-6069-0078", "FI-6069-0080", "FI-6069-0086", "FI-6069-0108", "FI-6069-0120", "FI-6069-0151", "FI-6070-0003", "FI-6070-0004", "FI-6070-0026", "FI-6070-0027", "FI-6070-0033", "FI-6070-0042", "FI-6070-0129", "FI-6071-0013", "FI-6071-0018", "FI-6071-0033", "FI-6071-0037", "FI-6071-0038", "FI-6071-0043", "FI-6071-0045", "FI-6071-0064", "FI-6071-0065", "FI-6071-0068", "FI-6071-0087", "FI-6071-0098", "FI-6071-0105", "FI-6071-0106", "FI-6071-0119", "FI-6071-0122", "FI-6071-0124", "FI-6071-0132", "FI-6071-0133", "FI-6071-0137", "FI-6071-0154", "FI-6071-0168", "FI-6071-0169", "FI-6071-0170", "FI-6071-0172", "FI-6071-0175", "FI-6071-0183", "FI-6071-0184", "FI-6071-0187", "FI-6071-0188", "FI-6071-0195", "FI-6071-0203", "FI-6071-0218", "FI-6071-0231", "FI-6071-0242", "FI-6071-0243", "FI-6071-0256", "FI-6071-0258", "FI-6071-0270", "FI-6071-0271", "FI-6071-0277", "FI-6071-0280", "FI-6071-0285", "FI-6071-0367", "FI-6071-0373", "FI-6071-0374", "FI-6071-0379", "FI-6071-0383", "FI-6071-0391", "FI-6071-0413", "FI-6071-0428", "FI-6071-0430", "FI-6071-0431", "FI-6071-0433", "FI-6071-0442", "FI-6071-0452", "FI-6071-0462", "FI-6071-0505", "FI-6071-0517", "FI-6072-0007", "FI-6072-0008", "FI-6072-0013", "FI-6072-0034", "FI-6072-0035", "FI-6072-0046", "FI-6072-0077", "FI-6074-0021", "FI-6074-0048", "FI-6074-0052", "FI-6074-0053", "FI-6074-0073", "FI-6075-0009", "FI-6075-0016", "FI-6075-0028", "FI-6075-0038", "FI-6075-0039", "FI-6075-0044", "FI-6075-0058", "FI-6075-0074", "FI-6075-0077", "FI-6075-0098", "FI-6075-0129", "FI-6075-0130", "FI-6075-0132", "FI-6075-0138", "FI-6075-0141", "FI-6075-0158", "FI-6075-0164", "FI-6075-0167", "FI-6075-0171", "FI-6075-0192", "FI-6075-0206", "FI-6075-0236", "FI-6075-0250", "FI-6075-0252", "FI-6075-0258", "FI-6076-0007", "FI-6077-0004", "FI-6077-0005", "FI-6077-0019", "FI-6077-0030", "FI-6077-0039", "FI-6077-0044", "FI-6078-0070", "FI-6079-0002", "FI-6080-0040", "FI-6080-0047", "FI-6080-0056", "FI-6081-0017", "FI-6082-0005", "FI-6082-0020", "FI-6083-0007", "FI-6083-0019", "FI-6085-0004", "FI-6086-0010", "FI-6087-0020", "FI-6088-0073", "FI-6089-0009", "FI-6089-0018", "FI-6089-0024", "FI-6089-0050", "FI-6089-0055", "FI-6089-0064", "FI-6089-0075", "FI-6090-0001", "FI-6090-0020", "FI-6090-0024", "FI-6090-0035", "FI-6090-0046", "FI-6091-0020", "FI-6091-0049", "FI-6091-0055", "FI-6091-0067", "FI-6091-0093", "FI-6091-0122", "FI-6091-0124", "FI-6091-0131", "FI-6091-0149", "FI-6092-0009", "FI-6092-0030", "FI-6092-0046", "FI-6092-0057", "FI-6092-0073", "FI-6092-0086", "FI-6093-0048", "FI-6093-0086", "FI-6093-0105", "FI-6093-0196", "FI-6095-0003", "FI-6095-0004", "FI-6095-0043", "FI-6098-0021", "FI-6099-0003", "FI-6099-0006", "FI-6099-0007", "FI-6101-0028", "FI-6101-0038", "FI-6102-0012", "FI-6102-0041", "FI-6102-0056", "FI-6102-0063", "FI-6102-0078", "FI-6102-0111", "FI-6102-0138", "FI-6102-0140", "FI-6105-0020", "FI-6105-0023", "FI-6105-0024", "FI-6105-0077", "FI-6106-0046", "FI-6106-0053", "FI-6107-0001", "FI-6107-0051", "FI-6108-0012", "FI-6108-0014", "FI-6108-0038", "FI-6108-0046", "FI-6108-0054", "FI-6108-0055", "FI-6108-0058", "FI-6108-0085", "FI-6108-0087", "FI-6109-0025", "FI-6109-0030", "FI-6109-0036", "FI-6109-0043", "FI-6109-0047", "FI-6109-0049", "FI-6109-0056", "FI-6109-0065", "FI-6109-0072", "FI-6109-0099", "FI-6109-0108", "FI-6109-0126", "FI-6110-0001", "FI-6110-0007", "FI-6110-0013", "FI-6110-0027", "FI-6110-0040", "FI-6110-0045", "FI-6110-0049", "FI-6110-0050", "FI-6110-0052", "FI-6110-0053", "FI-6110-0054", "FI-6110-0056", "FI-6110-0058", "FI-6110-0061", "FI-6110-0064", "FI-6110-0066", "FI-6110-0076", "FI-6111-0003", "FI-6111-0004", "FI-6111-0011", "FI-6111-0014", "FI-6111-0017", "FI-6111-0029", "FI-6111-0031", "FI-6111-0036", "FI-6112-0005", "FI-6112-0025", "FI-6113-0003", "FI-6113-0006", "FI-6113-0010", "FI-6113-0016", "FI-6113-0040", "FI-6113-0042", "FI-6113-0045", "FI-6113-0054", "FI-6115-0012", "FI-6115-0014", "FI-6115-0054", "FI-6115-0139", "FI-6116-0032", "FI-6116-0046", "FI-6116-0083", "FI-6116-0108", "FI-6116-0221", "FI-6117-0010", "FI-6117-0024", "FI-6117-0029", "FI-6117-0061", "FI-6117-0068", "FI-6117-0095", "FI-6117-0102", "FI-6117-0122", "FI-6117-0150", "FI-6118-0059", "FI-6118-0074", "FI-6118-0094", "FI-6118-0095", "FI-6120-0007", "FI-6120-0039", "FI-6120-0082", "FI-6120-0158", "FI-6120-0230", "FI-6120-0250", "FI-6121-0051", "FI-6121-0125", "FI-6121-0126", "FI-6121-0133", "FI-6124-0011", "FI-6129-0016", "FI-6130-0010", "FI-6130-0021", "FI-6130-0025", "FI-6130-0026", "FI-6131-0029", "FI-6131-0031", "FI-6131-0032", "FI-6131-0033", "FI-6131-0048", "FI-6131-0067", "FI-6133-0005", "FI-6133-0011", "FI-6133-0015", "FI-6133-0025", "FI-6133-0030", "FI-6133-0033", "FI-6133-0039", "FI-6133-0047", "FI-6133-0051", "FI-6133-0069", "FI-6133-0072", "FI-6133-0073", "FI-6133-0074", "FI-6133-0098", "FI-6133-0101", "FI-6133-0113", "FI-6133-0139", "FI-6133-0157", "FI-6133-0162", "FI-6133-0163", "FI-6133-0165", "FI-6133-0174", "FI-6133-0196", "FI-6134-0001", "FI-6134-0009", "FI-6134-0026", "FI-6134-0027", "FI-6134-0041", "FI-6135-0011", "FI-6135-0013", "FI-6136-0002", "FI-6138-0010", "FI-6138-0024", "FI-6138-0027", "FI-6138-0029", "FI-6138-0036", "FI-6138-0046", "FI-6138-0049", "FI-6138-0053", "FI-6138-0083", "FI-6138-0097", "FI-6138-0134", "FI-6140-0016", "FI-6140-0030", "FI-6142-0006", "FI-6143-0007", "FI-6143-0018", "FI-6143-0023", "FI-6143-0036", "FI-6143-0043", "FI-6144-0004", "FI-6144-0027", "FI-6146-0017", "FI-6147-0048", "FI-6147-0069", "FI-6147-0073", "FI-6147-0085", "FI-6147-0091", "FI-6147-0094", "FI-6147-0117", "FI-6147-0148", "FI-6148-0006", "FI-6148-0017", "FI-6148-0027", "FI-6148-0035", "FI-6148-0040", "FI-6149-0005", "FI-6149-0008", "FI-6149-0009", "FI-6150-0005", "FI-6150-0007", "FI-6151-0006", "FI-6151-0017", "FI-6152-0001", "FI-6153-0003", "FI-6153-0010", "FI-6153-0012", "FI-6153-0023", "FI-6153-0036", "FI-6153-0045", "FI-6153-0066", "FI-6154-0001", "FI-6154-0002", "FI-6154-0006", "FI-6154-0018", "FI-6157-0008", "FI-6157-0025", "FI-6157-0029", "FI-6157-0064", "FI-6157-0073", "FI-6157-0094", "FI-6157-0103", "FI-6157-0112", "FI-6157-0115", "FI-6157-0118", "FI-6157-0171", "FI-6158-0007", "FI-6159-0009", "FI-6159-0019", "FI-6163-0030", "FI-6163-0049", "FI-6163-0050", "FI-6164-0006", "FI-6164-0044", "FI-6165-0014", "FI-6166-0003", "FI-6168-0005", "FI-6168-0007", "FI-6168-0030", "FI-6168-0031", "FI-6168-0034", "FI-6168-0049", "FI-6168-0057", "FI-6168-0058", "FI-6168-0060", "FI-6168-0062", "FI-6168-0066", "FI-6168-0067", "FI-6168-0070", "FI-6168-0073", "FI-6170-0018", "FI-6170-0031", "FI-6170-0035", "FI-6170-0050", "FI-6170-0054", "FI-6170-0056", "FI-6170-0061", "FI-6170-0065", "FI-6170-0074", "FI-6170-0078", "FI-6171-0020", "FI-6171-0029", "FI-6171-0037", "FI-6171-0054", "FI-6171-0057", "FI-6172-0023", "FI-6172-0027", "FI-6173-0012", "FI-6173-0018", "FI-6174-0030", "FI-6174-0034", "FI-6174-0047", "FI-6174-0072", "FI-6174-0081", "FI-6174-0085", "FI-6174-0095", "FI-6174-0097", "FI-6175-0008", "FI-6175-0014", "FI-6175-0025", "FI-6176-0039", "FI-6176-0055", "FI-6177-0006", "FI-6177-0008", "FI-6177-0020", "FI-6177-0031", "FI-6177-0033", "FI-6177-0034", "FI-6177-0035", "FI-6177-0048", "FI-6177-0054", "FI-6177-0059", "FI-6178-0008", "FI-6178-0013", "FI-6178-0018", "FI-6178-0022", "FI-6178-0023", "FI-6178-0026", "FI-6178-0031", "FI-6178-0032", "FI-6178-0035", "FI-6178-0037", "FI-6179-0022", "FI-6179-0030", "FI-6179-0031", "FI-6179-0036", "FI-6180-0013", "FI-6180-0015", "FI-6180-0024", "FI-6180-0031", "FI-6180-0033", "FI-6180-0041", "FI-6180-0047", "FI-6180-0072", "FI-6180-0076", "FI-6180-0078", "FI-6180-0086", "FI-6180-0101", "FI-6181-0005", "FI-6181-0021", "FI-6181-0030", "FI-6181-0031", "FI-6181-0032", "FI-6181-0033", "FI-6181-0040", "FI-6181-0045", "FI-6181-0057", "FI-6181-0071", "FI-6181-0072", "FI-6181-0083", "FI-6181-0085", "FI-6182-0002", "FI-6182-0008", "FI-6192-0013", "FI-6192-0019", "FI-6192-0032", "FI-6192-0035", "FI-6192-0052", "FI-6192-0053", "FI-6192-0055", "FI-6192-0062", "FI-6192-0065", "FI-6192-0067", "FI-6192-0076", "FI-6192-0079", "FI-6192-0101", "FI-6192-0108", "FI-6193-0015", "FI-6193-0027", "FI-6193-0051", "FI-6193-0055", "FI-6193-0056", "FI-6193-0057", "FI-6193-0067", "FI-6193-0068", "FI-6193-0071", "FI-6193-0075", "FI-6193-0082", "FI-6193-0084", "FI-6193-0097", "FI-6193-0110", "FI-6193-0112", "FI-6193-0134", "FI-6193-0189", "FI-6193-0196", "FI-6193-0213", "FI-7122-0001", "FI-8114-0031", "FI-8114-0034", "FI-8114-0070", "FI-8114-0072", "FI-8114-0129", "FI-8114-0155", "FI-8114-0158", "FI-8114-0159", "FI-8114-0186", "FI-8114-0189", "FI-8190-0013", "FI-8190-0016", "FI-8190-0032", "FI-8190-0058", "FI-8190-0068", "FI-8190-0077", "FI-8190-0088", "FI-8190-0089", "FI-8190-0090", "FI-8190-0099", "FI-8190-0108", "FI-8190-0115", "FI-8190-0145", "FI-8191-0003", "FI-8191-0006", "FI-8191-0022", "FI-8191-0027", "FI-8191-0034", "FI-8191-0052", "FI-8191-0060", "FI-8191-0101", "FI-8191-0163", "FI-8191-0186" };

            List<string> datos = new List<string>();
            //IEnumerable<string> dato = null;
            string accessToken = GetToken(_url, _apiVersion).Result;
            foreach (var name in names)
            {
                IEnumerable<string> resultado = ConsultarAPIFiltrandoContratos(_url, _apiVersion, accessToken, name);
                try
                {
                    if (null != resultado)
                    {
                        foreach (var item in resultado)
                        {
                            datos.Add(item);
                            logger.Info(datos.Count);
                        }
                    }
                }
                catch (Exception e)
                {
                    logger.Error(e.Message);
                    logger.Error(e.InnerException);
                    logger.Error(e.StackTrace);
                }
            }
            logger.Info($"Registros obtenidos de contratos: {datos.Count}");
            return datos;
        }
        private IEnumerable<string> ConsultarAPI(string url, string apiVersion, string entidad, string token, string filtro)
        {
            string webApiUrl = $"{url}/api/data/v{apiVersion}/";
            string consulta = $"{entidad}";
            string query = string.Empty;

            if (!filtro.Equals(string.Empty))
            {
                consulta += $"?{filtro}";
            }
            //string consulta = $"{entidad}?$top=100"; //ojo con esto para PRO
            //string consulta = $"{entidad}?$select={campos} ";
            List<string> lista = null;
            var authHeader = new AuthenticationHeaderValue("Bearer", token);
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(webApiUrl);
                client.DefaultRequestHeaders.Authorization = authHeader;
                string next_consulta = string.Empty;
                lista = new List<string>();
                do
                {
                    HttpResponseMessage retrieveResponse1 = client.GetAsync(consulta, HttpCompletionOption.ResponseHeadersRead).Result;

                    if (retrieveResponse1.IsSuccessStatusCode)
                    {
                        JObject body = JObject.Parse(retrieveResponse1.Content.ReadAsStringAsync().Result);
                        try
                        {
                            foreach (JObject fila in body["value"])
                            {
                                lista.Add(fila.ToString());
                            }
                            if (null != body["@odata.nextLink"])
                            {
                                query = new Uri(body["@odata.nextLink"].ToString()).Query;
                                consulta = $"{entidad}{query}";
                            }
                            else
                            {
                                query = string.Empty;
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine($"ERROR en ConsultarAPI: {ex.Message}");
                            return null;
                        }
                    }
                    logger.Info($"Registros recuperados {lista.Count}");
                } while (query != string.Empty);
               
                return lista.ToArray();
            }
        }

        //TODO Función para aplicar filtros que me traigo de una tabla Dynamics_Filters_Configuration

        //TODO Función para traer la configuración de los filtros


        //TODO inmuebles solo traer de las promociones que se han insertado
        //Una vez las promociones insertadas hacer un select distinct al eve_promocionid
        //y solo traer los inmuebles de estas promociones

        private IEnumerable<string> ConsultarAPIFiltrandoContactos(string url, string apiVersion, string name, string token)
        {
            string webApiUrl = $"{url}/api/data/v{apiVersion}/";
            string entidad = "accounts";
            string consulta = $"{entidad}?$filter=name eq '{name}'";
            //string consulta = $"{entidad}?$top=70000"; //ojo con esto para PRO
            //string consulta = $"{entidad}?$select={campos} ";
            List<string> lista = null;

            var authHeader = new AuthenticationHeaderValue("Bearer", token);
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(webApiUrl);
                client.DefaultRequestHeaders.Authorization = authHeader;

                Dictionary<string, string> valores = new Dictionary<string, string>();
                HttpResponseMessage retrieveResponse1 = client.GetAsync(consulta, HttpCompletionOption.ResponseHeadersRead).Result;
                lista = new List<string>();
                if (retrieveResponse1.IsSuccessStatusCode)
                {
                    JObject body = JObject.Parse(retrieveResponse1.Content.ReadAsStringAsync().Result);
                    try
                    {
                        foreach (JObject fila in body["value"])
                        {
                            lista.Add(fila.ToString());
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"ERROR en ConsultarAPI: {ex.Message}");
                        return null;
                    }
                }
                return lista.ToArray();
            }
        }
        private IEnumerable<string> ConsultarAPIFiltrandoContratos(string url, string apiVersion, string token, string name)
        {
            string webApiUrl = $"{url}/api/data/v{apiVersion}/";
            string entidad = "eve_contratos";
            string consulta = $"{entidad}?$filter=eve_name eq '{name}'";
            //string consulta = $"{entidad}?$top=70000"; //ojo con esto para PRO
            //string consulta = $"{entidad}?$select={campos} ";
            List<string> lista = null;

            var authHeader = new AuthenticationHeaderValue("Bearer", token);
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(webApiUrl);
                client.DefaultRequestHeaders.Authorization = authHeader;

                Dictionary<string, string> valores = new Dictionary<string, string>();
                HttpResponseMessage retrieveResponse1 = client.GetAsync(consulta, HttpCompletionOption.ResponseHeadersRead).Result;
                lista = new List<string>();
                if (retrieveResponse1.IsSuccessStatusCode)
                {
                    JObject body = JObject.Parse(retrieveResponse1.Content.ReadAsStringAsync().Result);
                    try
                    {
                        foreach (JObject fila in body["value"])
                        {
                            lista.Add(fila.ToString());
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"ERROR en ConsultarAPI: {ex.Message}");
                        return null;
                    }
                }
                return lista.ToArray();
            }
        }
        private async Task<string> GetToken(string url, string apiVersion)
        {
            string webApiUrl = $"{url}/api/data/v{apiVersion}/";
            logger.Info("GetToken: " + webApiUrl);
            string resultado = "No token";
            try
            {
                var userCredential = new UserCredential(_userName, _password);
                var authParameters = await AuthenticationParameters.CreateFromResourceUrlAsync(new Uri(webApiUrl));
                var authContext = new AuthenticationContext(authParameters.Authority, false);
                var authResult = authContext.AcquireToken(url, _clientId, userCredential);
                resultado = authResult.AccessToken;
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                logger.Error(e.InnerException);
                logger.Error(e.StackTrace);
            }
            logger.Info($"Token: {resultado}");
            return resultado;
        }
        private void ActualizarContactoEnMAS(string id)
        {
            string url = "https://mas-testa.inconcertcc.com/public/cache/process_contact_updated/";
            using (var cliente = new HttpClient())
            {
                try
                {
                    var respuesta = cliente.GetAsync($"{url}{id}").Result;
                    logger.Info($"ActualizarContactoEnMAS: {respuesta.StatusCode} {url}{id}");
                }
                catch (Exception e)
                {
                    logger.Info(e.Message);
                    logger.Info(e.InnerException);
                    logger.Info(e.StackTrace);
                }
            }
        }
    }
}
