﻿using Newtonsoft.Json;
using System;

namespace ClienteAPITesta
{
    public class CustomDataProducto
    {
        public string eve_nombre { get; set; }
        public string eve_codigosociedad { get; set; }
        public string eve_codigopromocion { get; set; }
        public string eve_descripcionpromocion { get; set; }
        public string eve_estado { get; set; }
        public string eve_tipoinmueble { get; set; }
        public string eve_descripcionportal { get; set; }
        public string eve_tipovia { get; set; }
        public string eve_calle { get; set; }
        public string eve_numero { get; set; }
        public string eve_portal { get; set; }
        public string eve_escalera { get; set; }
        public string eve_piso { get; set; }
        public string eve_puerta { get; set; }
        public string eve_codigopostal { get; set; }
        public string eve_poblacion { get; set; }
        public string eve_codigo_reserva { get; set; }
        public string eve_bonificacion { get; set; }
        public string eve_meses_apli { get; set; }
        public string eve_mesesdegarantia { get; set; }
        public string eve_mesesfianza { get; set; }
        public string eve_alquiler { get; set; }
        public string eve_ibi_anual { get; set; }
        public string eve_comunidad { get; set; }
        public string eve_comunidad_anual { get; set; }
        public string eve_sujeto_a_permanencia { get; set; }
        public string eve_meses_permanencia { get; set; }
        public string eve_sujeto_a_impagos { get; set; }
        public string eve_hasta_fin_de_contrato { get; set; }
        public string eve_numero_meses_corriente_de_pago { get; set; }
        public string eve_numeroexpediente { get; set; }
        public string eve_tipo_capex_actual { get; set; }
        public string eve_metrosutiles { get; set; }
        public string eve_dormitorios { get; set; }
        public string eve_banyos { get; set; }
        public string eve_provincia { get; set; }
        public string eve_comunidadautonoma { get; set; }
        public Nullable<DateTime> eve_fecha_validacion_acceso_fisico_vivienda { get; set; }
        public Nullable<DateTime> eve_fecha_fin_obras_prevista { get; set; }
        public Nullable<DateTime> eve_fecha_fin_obras_prevista_revisada { get; set; }
        public string eve_anejo { get; set; }
        public string eve_tipo_capex_actual_descripcion { get; set; }
        public string eve_tipo_capex_en_proceso_descripcion { get; set; }
        public string eve_minusvalido { get; set; }
        public string OdataEtag { get; set; }
        public string statecode { get; set; }
        public string statuscode { get; set; }
        public string eve_numeroregistro { get; set; }
        public Nullable<DateTime> eve_fechamodificacionregistro { get; set; }
        public string _eve_sociedadinmuebleid_value { get; set; }
        public string eve_pais { get; set; }
        public Nullable<DateTime> createdon { get; set; }
        public string eve_comunidadlistames { get; set; }
        public string eve_codigo_provincia { get; set; }
        public string eve_visualizacionportal { get; set; }
        public string _ownerid_value { get; set; }
        public string eve_cuotabloque { get; set; }
        public Nullable<DateTime> modifiedon { get; set; }
        public string eve_idsociedadprinex { get; set; }
        public string versionnumber { get; set; }
        public string eve_idinmuebleprinex { get; set; }
        public string timezoneruleversionnumber { get; set; }
        public string _modifiedby_value { get; set; }
        public string eve_descripcion { get; set; }
        public string eve_inmuebleid { get; set; }
        public string _eve_promocininmuebleid_value { get; set; }
        public string eve_metrosconstruidos { get; set; }
        public string _createdby_value { get; set; }
        public string _owningbusinessunit_value { get; set; }
        public string _owninguser_value { get; set; }
        public string eve_comunidadlistaanual { get; set; }
        public string eve_hace_esquina { get; set; }
        public string eve_suite_incorporado_api { get; set; }
        public string eve_subcategoria_tecnica { get; set; }
        public string eve_texto_10 { get; set; }
        public string eve_marca_horno_microondas { get; set; }
        public string eve_comentarios_internos_entrega_llaves { get; set; }
        public string eve_congelador { get; set; }
        public string importsequencenumber { get; set; }
        public string eve_primera_revision_cambio_sumin { get; set; }
        public string eve_soleria_vivienda { get; set; }
        public string eve_sumatorio_enseres { get; set; }
        public string eve_fecha_aprobacion_reserva { get; set; }
        public string eve_categoria_tecnica { get; set; }
        public string eve_cotejo_bonificaciones { get; set; }
        public string eve_estado_validac_estado_obra_adicional { get; set; }
        public string eve_carencia_vivienda_ibi { get; set; }
        public string eve_amueblado_api { get; set; }
        public string eve_salida_humos { get; set; }
        public string eve_terraza_api { get; set; }
        public string eve_envio_comunicacion_a_centro_custodia { get; set; }
        public string eve_estado_gestion_revision_suministros { get; set; }
        public string eve_criticidad { get; set; }
        public string eve_estado_conservacion { get; set; }
        public string eve_color_lavavajillas { get; set; }
        public string eve_estado_adicional { get; set; }
        public string eve_importe_3 { get; set; }
        public string eve_ubicacion { get; set; }
        public string eve_importe_2 { get; set; }
        public string eve_importe_1 { get; set; }
        public string eve_importe_5 { get; set; }
        public string eve_importe_4 { get; set; }
        public string eve_induccion { get; set; }
        public string eve_pre_creado_fecha_5 { get; set; }
        public string eve_fecha_entrega_llaves_api { get; set; }
        public string eve_pre_creado_texto_3 { get; set; }
        public string eve_tipo_adecuacion_actual { get; set; }
        public string eve_referenciacatastral { get; set; }
        public string eve_color_campana { get; set; }
        public string eve_codigo_contrato { get; set; }
        public string eve_pre_creado_fecha_1 { get; set; }
        public string eve_banyos_suite { get; set; }
        public string eve_iniciar_creacion_peticion { get; set; }
        public string eve_color_vitroceramica { get; set; }
        public string eve_fecha_entrega_llaves_api_adicional { get; set; }
        public string eve_estado_validacion_suministros { get; set; }
        public string eve_datos_recogida_completados { get; set; }
        public string eve_periodicidadcomunidad { get; set; }
        public string eve_lavadora_integrada { get; set; }
        public string eve_color_microondas { get; set; }
        public string eve_amueblado { get; set; }
        public string eve_marca_lavadora { get; set; }
        public string eve_dias_desde_disponibilidad_primer_juego { get; set; }
        public string eve_codigo_contratista_adecuacion { get; set; }
        public string eve_dias_desde_disponibilidad_ultimo_juego { get; set; }
        public string eve_comentario_interno_1 { get; set; }
        public string eve_aire_acondicionado_api { get; set; }
        public string eve_importe_final_obra { get; set; }
        public string eve_texto_13 { get; set; }
        public string eve_comunicacion_a_centro_custodia { get; set; }
        public string eve_distribucion { get; set; }
        public string eve_jardin_privado_api { get; set; }
        public string eve_estado_comercializacion { get; set; }
        public string eve_fecha_valoracion { get; set; }
        public string eve_fecha_disp_primer_juego_llaves_ant { get; set; }
        public string eve_tipo_cocina { get; set; }
        public string eve_numerofincaregistral { get; set; }
        public string _owningteam_value { get; set; }
        public string eve_presupuesto_valoracion_aceptado { get; set; }
        public string eve_microondas { get; set; }
        public string _eve_reserva_asociada_value { get; set; }
        public string eve_empresa_calidad { get; set; }
        public string eve_carencia_vivienda_comunidad { get; set; }
        public string eve_comentarios_validacion_estado_obra { get; set; }
        public string eve_calefaccion { get; set; }
        public string eve_pre_creado_fecha_6 { get; set; }
        public string eve_ciclo_capex_actual { get; set; }
        public string _modifiedonbehalfby_value { get; set; }
        public string eve_tipologia_inmueble_txt { get; set; }
        public string eve_pre_creado_fecha_2 { get; set; }
        public string eve_fecha_recepcion_api_llaves_ultimo { get; set; }
        public string eve_pre_creado_texto_2 { get; set; }
        public string eve_peticion_titularidad_suministrothanterior { get; set; }
        public string eve_causa_desviacion { get; set; }
        public string eve_estado_general_suministros { get; set; }
        public string eve_pre_creado_texto_6 { get; set; }
        public string eve_numero_expediente_vpo { get; set; }
        public string eve_fecha_validacion_suministros { get; set; }
        public string eve_fecha_inicio_obras_prevista { get; set; }
        public string eve_congelador_integrado { get; set; }
        public string eve_certificado_energetico { get; set; }
        public string eve_calefaccion_api { get; set; }
        public string eve_estado_validacion_suministros_adicional { get; set; }
        public string eve_texto_12 { get; set; }
        public string eve_peticion_titularidad_suministroth { get; set; }
        public string eve_aire_acondicionado { get; set; }
        public string eve_puerta_seguridad { get; set; }
        public string eve_lavavajillas_integrado { get; set; }
        public string eve_estado_entrega_ultimo_juego_llaves { get; set; }
        public string eve_lavavajillas { get; set; }
        public string eve_estado_adecuacion_adicional { get; set; }
        public string eve_estado_validacion_anterior { get; set; }
        public string eve_estado_validacion_vivienda_api { get; set; }
        public string eve_operacion { get; set; }
        public string eve_cups_gas { get; set; }
        public string eve_marca_vitroceramica { get; set; }
        public string eve_frigorifico_con_congelador_integrado { get; set; }
        public string eve_sigla_tipo_via { get; set; }
        public string eve_estado_entrega_primer_juego_llaves { get; set; }
        public string eve_importe_oc { get; set; }
        public string eve_marca_lavavajillas { get; set; }
        public string eve_datosregistrales { get; set; }
        public string eve_pre_creado_fecha_7 { get; set; }
        public string eve_pre_creado_texto_1 { get; set; }
        public string eve_comentarios_acceso_vivienda { get; set; }
        public string eve_color_lavadora_secadora { get; set; }
        public string eve_estatus { get; set; }
        public string eve_contratista { get; set; }
        public string eve_fecha_validacion_vivienda { get; set; }
        public string eve_estado_validac_acceso_fisico_vivienda { get; set; }
        public string eve_equipamiento_cocina_api { get; set; }
        public string utcconversiontimezonecode { get; set; }
        public string eve_pre_creado_fecha_3 { get; set; }
        public string eve_pre_creado_texto_5 { get; set; }
        public string eve_pre_creado_numerico_3 { get; set; }
        public string eve_pre_creado_numerico_2 { get; set; }
        public string eve_pre_creado_numerico_1 { get; set; }
        public string eve_tipo_cocina_api { get; set; }
        public string eve_cotejo_rentas_vpo { get; set; }
        public string eve_fecha_recogida_datos { get; set; }
        public string eve_pre_creado_numerico_4 { get; set; }
        public string eve_cups_luz { get; set; }
        public string eve_texto_15 { get; set; }
        public string eve_marca_horno { get; set; }
        public string eve_importe_bon { get; set; }
        public string eve_fees_ss { get; set; }
        public string eve_color_induccion { get; set; }
        public string eve_horno { get; set; }
        public string eve_vitroceramica { get; set; }
        public string eve_color_lavadora { get; set; }
        public string eve_lavadora_secadora { get; set; }
        public string eve_num_armarios_no_empotrados_api { get; set; }
        public string eve_texto_11 { get; set; }
        public string eve_fecha_disponibilidad_primer_juego_llaves { get; set; }
        public string eve_orientacion { get; set; }
        public string eve_fecha_inicio_obras { get; set; }
        public string eve_renta { get; set; }
        public string eve_marca_microondas { get; set; }
        public string eve_balcon_api { get; set; }
        public string eve_contratista_adecuacion { get; set; }
        public string eve_comentario_interno_2 { get; set; }
        public string eve_presupuesto_valoracion { get; set; }
        public string eve_m2_escaparate { get; set; }
        public string eve_anyo_ibi { get; set; }
        public string eve_capex_extraordinario { get; set; }
        public string eve_periodicidadibi { get; set; }
        public string eve_marca_lavadora_secadora { get; set; }
        public string eve_tecnico_responsable_capex { get; set; }
        public string overriddencreatedon { get; set; }
        public string eve_balcon { get; set; }
        public string eve_peticion_titularidad_suministro_inquilino { get; set; }
        public string eve_fees_revision_2 { get; set; }
        public string eve_ciclo_capex_en_proceso { get; set; }
        public string eve_crear_peticion_tecnica { get; set; }
        public string eve_fecha_recepcion_api_llaves { get; set; }
        public string eve_localidadregistro { get; set; }
        public string eve_frigorifico_con_congelador { get; set; }
        public string eve_color_frigorifico { get; set; }
        public string eve_fecha_check_list { get; set; }
        public string eve_terraza { get; set; }
        public string _eve_propietario_tecnico_id_value { get; set; }
        public string eve_pre_creado_fecha_4 { get; set; }
        public string eve_texto_6 { get; set; }
        public string eve_texto_7 { get; set; }
        public string eve_texto_5 { get; set; }
        public string eve_tipo { get; set; }
        public string eve_agua_caliente { get; set; }
        public string eve_texto_8 { get; set; }
        public string eve_fecha_real_finalizacion_2 { get; set; }
        public string eve_texto_9 { get; set; }
        public string eve_fecha_3 { get; set; }
        public string eve_fecha_2 { get; set; }
        public string eve_fecha_5 { get; set; }
        public string eve_fecha_4 { get; set; }
        public string eve_electrodomesticos { get; set; }
        public string eve_revision_suministros_adicional { get; set; }
        public string eve_texto_14 { get; set; }
        public string eve_comentarios_validacion_suministros { get; set; }
        public string eve_codigo_adecuacion_actual_texto { get; set; }
        public string eve_precio_maximo_vivienda_protegida { get; set; }
        public string _createdonbehalfby_value { get; set; }
        public string eve_fecha_validacion_estado_obra { get; set; }
        public string eve_campana_extractora { get; set; }
        public string eve_fecha_fin_obras { get; set; }
        public string eve_color_horno_microondas { get; set; }
        public string eve_marca_campana { get; set; }
        public string eve_jardin_privado { get; set; }
        public string eve_horno_microondas { get; set; }
        public string eve_fecha_entrega_llaves_inquilino { get; set; }
        public string eve_descripcion_tecnica { get; set; }
        public string _eve_codigo_adecuacion_actual_value { get; set; }
        public string eve_marca_congelador { get; set; }
        public string eve_color_horno { get; set; }
        public string eve_tecnico_responsable { get; set; }
        public string eve_ubicacion_banyos { get; set; }
        public string eve_tipo_capex_en_proceso { get; set; }
        public string eve_marca_induccion { get; set; }
        public string eve_soleria_vivienda_api { get; set; }
        public string eve_estado_adecuacion { get; set; }
        public string eve_lavadora { get; set; }
        public string eve_tipologia_inmueble { get; set; }
        public string eve_equipamiento_cocina { get; set; }
        public string eve_estado_validac_acceso_fisic_vivienda_adic { get; set; }
        public string eve_marca_frigorifico { get; set; }
        public string eve_estado_validacion_anterior_2 { get; set; }
        public string eve_estado_validac_estado_obra { get; set; }
        public string eve_num_armarios_empotrados_api { get; set; }
        public string eve_color_congelador { get; set; }
        public string eve_lavadora_secadora_integrada { get; set; }
        public string eve_numero_aseos { get; set; }
        public string eve_pre_creado_texto_4 { get; set; }
        public string eve_fees_revision { get; set; }
        public string eve_armarios_empotrados { get; set; }
        public string eve_clave_4_prinex { get; set; }
        public Nullable<DateTime> eve_fecha_fin_obras_prevista_revisada_nuevo { get; set; }
        public string eve_id_crm { get; set; }
        public string eve_datos_registrales_renovacion { get; set; }
        public string eve_num_contrato_suministro_agua { get; set; }
        public string eve_promocion_prinex { get; set; }
        public string _eve_peticion_tecnica_cambio_titularidad_agua_value { get; set; }
        public Nullable<DateTime> eve_fecha_anulacion_parada_comercializacion { get; set; }
        public string _eve_peticion_tecnica_alta_suministros_value { get; set; }
        public string eve_companya_suministros_agua { get; set; }
        public string eve_clave_2_prinex { get; set; }
        public string eve_fecha_guardado_precomercializacion { get; set; }
        public string eve_num_contrato_suministro_luz { get; set; }
        public Nullable<DateTime> eve_fecha_general_comercializacion { get; set; }
        public string _eve_usuario_guardado_precom_id_value { get; set; }
        public string eve_clave_1_prinex { get; set; }
        public string eve_tipo_de_inmueble_prinex { get; set; }
        public string eve_clave_3_prinex { get; set; }
        public string eve_companya_suministros_luz { get; set; }
        public Nullable<DateTime> eve_fecha_fin_garantia_adecuacion { get; set; }
        public string eve_id_activo { get; set; }
        public string _eve_peticion_tecnica_alta_agua_value { get; set; }
        public string eve_cups_luz_peticion_tecnica { get; set; }
        public string eve_categoria_inmueble { get; set; }
        public string eve_numero_reclamaciones_centro_custodio { get; set; }
        public string eve_parada_okupacion { get; set; }
        public string eve_parada_comercializacion_tecnica { get; set; }
        public string eve_companya_suministros_gas { get; set; }
        public string eve_num_contrato_suministro_gas { get; set; }
        public string eve_parada_estrategica_comercializacion { get; set; }
        public string eve_estado_precomercializacion { get; set; }
        public string eve_punto_de_suministro_agua_peticion_tecnica { get; set; }
        public string eve_bloqueo_validacion_llaves { get; set; }
        public string eve_referencia_catastral_renovacion { get; set; }
        public string eve_descripcion_motivo_paralizacion { get; set; }
        public string eve_incidencia_tras_revision_inicial { get; set; }
        public Nullable<DateTime> eve_fecha_ultima_comunicacion_centro_custodia { get; set; }
        public Nullable<DateTime> eve_fecha_anulacion_paralizacion_estrategica { get; set; }
        public Nullable<DateTime> eve_fecha_incidencia_tras_revision_inicial { get; set; }
        public string eve_luz_nodisponible { get; set; }
        public Nullable<DateTime> eve_fecha_parada_comercializacion { get; set; }
        public string eve_agua_nodisponible { get; set; }
        public string eve_gas_nodisponible { get; set; }
        public string _eve_peticion_tecnica_cambio_titularidad_sums_value { get; set; }
        public Nullable<DateTime> eve_fecha_paralizacion_estrategica { get; set; }
        public string eve_cups_gas_peticion_tecnica { get; set; }
        public Nullable<DateTime> eve_fecha_contrato_agua { get; set; }
        public string eve_parada_comercializacion_estrategica { get; set; }

        public CustomDataProducto(string eve_nombre, string eve_codigosociedad, string eve_codigopromocion, string eve_descripcionpromocion, string eve_estado, string eve_tipoinmueble, string eve_descripcionportal, string eve_tipovia, string eve_calle, string eve_numero, string eve_portal, string eve_escalera, string eve_piso, string eve_puerta, string eve_codigopostal, string eve_poblacion, string eve_codigo_reserva, string eve_bonificacion, string eve_meses_apli, string eve_mesesdegarantia, string eve_mesesfianza, string eve_alquiler, string eve_ibi_anual, string eve_comunidad, string eve_comunidad_anual, string eve_sujeto_a_permanencia, string eve_meses_permanencia, string eve_sujeto_a_impagos, string eve_hasta_fin_de_contrato, string eve_numero_meses_corriente_de_pago, string eve_numeroexpediente, string eve_tipo_capex_actual, string eve_metrosutiles, string eve_dormitorios, string eve_banyos, string eve_provincia, string eve_comunidadautonoma, DateTime? eve_fecha_validacion_acceso_fisico_vivienda, DateTime? eve_fecha_fin_obras_prevista, DateTime? eve_fecha_fin_obras_prevista_revisada, string eve_anejo, string eve_tipo_capex_actual_descripcion, string eve_tipo_capex_en_proceso_descripcion, string eve_minusvalido, string odataEtag, string statecode, string statuscode, string eve_numeroregistro, DateTime? eve_fechamodificacionregistro, string eve_sociedadinmuebleid_value, string eve_pais, DateTime? createdon, string eve_comunidadlistames, string eve_codigo_provincia, string eve_visualizacionportal, string ownerid_value, string eve_cuotabloque, DateTime? modifiedon, string eve_idsociedadprinex, string versionnumber, string eve_idinmuebleprinex, string timezoneruleversionnumber, string modifiedby_value, string eve_descripcion, string eve_inmuebleid, string eve_promocininmuebleid_value, string eve_metrosconstruidos, string createdby_value, string owningbusinessunit_value, string owninguser_value, string eve_comunidadlistaanual, string eve_hace_esquina, string eve_suite_incorporado_api, string eve_subcategoria_tecnica, string eve_texto_10, string eve_marca_horno_microondas, string eve_comentarios_internos_entrega_llaves, string eve_congelador, string importsequencenumber, string eve_primera_revision_cambio_sumin, string eve_soleria_vivienda, string eve_sumatorio_enseres, string eve_fecha_aprobacion_reserva, string eve_categoria_tecnica, string eve_cotejo_bonificaciones, string eve_estado_validac_estado_obra_adicional, string eve_carencia_vivienda_ibi, string eve_amueblado_api, string eve_salida_humos, string eve_terraza_api, string eve_envio_comunicacion_a_centro_custodia, string eve_estado_gestion_revision_suministros, string eve_criticidad, string eve_estado_conservacion, string eve_color_lavavajillas, string eve_estado_adicional, string eve_importe_3, string eve_ubicacion, string eve_importe_2, string eve_importe_1, string eve_importe_5, string eve_importe_4, string eve_induccion, string eve_pre_creado_fecha_5, string eve_fecha_entrega_llaves_api, string eve_pre_creado_texto_3, string eve_tipo_adecuacion_actual, string eve_referenciacatastral, string eve_color_campana, string eve_codigo_contrato, string eve_pre_creado_fecha_1, string eve_banyos_suite, string eve_iniciar_creacion_peticion, string eve_color_vitroceramica, string eve_fecha_entrega_llaves_api_adicional, string eve_estado_validacion_suministros, string eve_datos_recogida_completados, string eve_periodicidadcomunidad, string eve_lavadora_integrada, string eve_color_microondas, string eve_amueblado, string eve_marca_lavadora, string eve_dias_desde_disponibilidad_primer_juego, string eve_codigo_contratista_adecuacion, string eve_dias_desde_disponibilidad_ultimo_juego, string eve_comentario_interno_1, string eve_aire_acondicionado_api, string eve_importe_final_obra, string eve_texto_13, string eve_comunicacion_a_centro_custodia, string eve_distribucion, string eve_jardin_privado_api, string eve_estado_comercializacion, string eve_fecha_valoracion, string eve_fecha_disp_primer_juego_llaves_ant, string eve_tipo_cocina, string eve_numerofincaregistral, string owningteam_value, string eve_presupuesto_valoracion_aceptado, string eve_microondas, string eve_reserva_asociada_value, string eve_empresa_calidad, string eve_carencia_vivienda_comunidad, string eve_comentarios_validacion_estado_obra, string eve_calefaccion, string eve_pre_creado_fecha_6, string eve_ciclo_capex_actual, string modifiedonbehalfby_value, string eve_tipologia_inmueble_txt, string eve_pre_creado_fecha_2, string eve_fecha_recepcion_api_llaves_ultimo, string eve_pre_creado_texto_2, string eve_peticion_titularidad_suministrothanterior, string eve_causa_desviacion, string eve_estado_general_suministros, string eve_pre_creado_texto_6, string eve_numero_expediente_vpo, string eve_fecha_validacion_suministros, string eve_fecha_inicio_obras_prevista, string eve_congelador_integrado, string eve_certificado_energetico, string eve_calefaccion_api, string eve_estado_validacion_suministros_adicional, string eve_texto_12, string eve_peticion_titularidad_suministroth, string eve_aire_acondicionado, string eve_puerta_seguridad, string eve_lavavajillas_integrado, string eve_estado_entrega_ultimo_juego_llaves, string eve_lavavajillas, string eve_estado_adecuacion_adicional, string eve_estado_validacion_anterior, string eve_estado_validacion_vivienda_api, string eve_operacion, string eve_cups_gas, string eve_marca_vitroceramica, string eve_frigorifico_con_congelador_integrado, string eve_sigla_tipo_via, string eve_estado_entrega_primer_juego_llaves, string eve_importe_oc, string eve_marca_lavavajillas, string eve_datosregistrales, string eve_pre_creado_fecha_7, string eve_pre_creado_texto_1, string eve_comentarios_acceso_vivienda, string eve_color_lavadora_secadora, string eve_estatus, string eve_contratista, string eve_fecha_validacion_vivienda, string eve_estado_validac_acceso_fisico_vivienda, string eve_equipamiento_cocina_api, string utcconversiontimezonecode, string eve_pre_creado_fecha_3, string eve_pre_creado_texto_5, string eve_pre_creado_numerico_3, string eve_pre_creado_numerico_2, string eve_pre_creado_numerico_1, string eve_tipo_cocina_api, string eve_cotejo_rentas_vpo, string eve_fecha_recogida_datos, string eve_pre_creado_numerico_4, string eve_cups_luz, string eve_texto_15, string eve_marca_horno, string eve_importe_bon, string eve_fees_ss, string eve_color_induccion, string eve_horno, string eve_vitroceramica, string eve_color_lavadora, string eve_lavadora_secadora, string eve_num_armarios_no_empotrados_api, string eve_texto_11, string eve_fecha_disponibilidad_primer_juego_llaves, string eve_orientacion, string eve_fecha_inicio_obras, string eve_renta, string eve_marca_microondas, string eve_balcon_api, string eve_contratista_adecuacion, string eve_comentario_interno_2, string eve_presupuesto_valoracion, string eve_m2_escaparate, string eve_anyo_ibi, string eve_capex_extraordinario, string eve_periodicidadibi, string eve_marca_lavadora_secadora, string eve_tecnico_responsable_capex, string overriddencreatedon, string eve_balcon, string eve_peticion_titularidad_suministro_inquilino, string eve_fees_revision_2, string eve_ciclo_capex_en_proceso, string eve_crear_peticion_tecnica, string eve_fecha_recepcion_api_llaves, string eve_localidadregistro, string eve_frigorifico_con_congelador, string eve_color_frigorifico, string eve_fecha_check_list, string eve_terraza, string eve_propietario_tecnico_id_value, string eve_pre_creado_fecha_4, string eve_texto_6, string eve_texto_7, string eve_texto_5, string eve_tipo, string eve_agua_caliente, string eve_texto_8, string eve_fecha_real_finalizacion_2, string eve_texto_9, string eve_fecha_3, string eve_fecha_2, string eve_fecha_5, string eve_fecha_4, string eve_electrodomesticos, string eve_revision_suministros_adicional, string eve_texto_14, string eve_comentarios_validacion_suministros, string eve_codigo_adecuacion_actual_texto, string eve_precio_maximo_vivienda_protegida, string createdonbehalfby_value, string eve_fecha_validacion_estado_obra, string eve_campana_extractora, string eve_fecha_fin_obras, string eve_color_horno_microondas, string eve_marca_campana, string eve_jardin_privado, string eve_horno_microondas, string eve_fecha_entrega_llaves_inquilino, string eve_descripcion_tecnica, string eve_codigo_adecuacion_actual_value, string eve_marca_congelador, string eve_color_horno, string eve_tecnico_responsable, string eve_ubicacion_banyos, string eve_tipo_capex_en_proceso, string eve_marca_induccion, string eve_soleria_vivienda_api, string eve_estado_adecuacion, string eve_lavadora, string eve_tipologia_inmueble, string eve_equipamiento_cocina, string eve_estado_validac_acceso_fisic_vivienda_adic, string eve_marca_frigorifico, string eve_estado_validacion_anterior_2, string eve_estado_validac_estado_obra, string eve_num_armarios_empotrados_api, string eve_color_congelador, string eve_lavadora_secadora_integrada, string eve_numero_aseos, string eve_pre_creado_texto_4, string eve_fees_revision, string eve_armarios_empotrados, string eve_clave_4_prinex, DateTime? eve_fecha_fin_obras_prevista_revisada_nuevo, string eve_id_crm, string eve_datos_registrales_renovacion, string eve_num_contrato_suministro_agua, string eve_promocion_prinex, string eve_peticion_tecnica_cambio_titularidad_agua_value, DateTime? eve_fecha_anulacion_parada_comercializacion, string eve_peticion_tecnica_alta_suministros_value, string eve_companya_suministros_agua, string eve_clave_2_prinex, string eve_fecha_guardado_precomercializacion, string eve_num_contrato_suministro_luz, DateTime? eve_fecha_general_comercializacion, string eve_usuario_guardado_precom_id_value, string eve_clave_1_prinex, string eve_tipo_de_inmueble_prinex, string eve_clave_3_prinex, string eve_companya_suministros_luz, DateTime? eve_fecha_fin_garantia_adecuacion, string eve_id_activo, string eve_peticion_tecnica_alta_agua_value, string eve_cups_luz_peticion_tecnica, string eve_categoria_inmueble, string eve_numero_reclamaciones_centro_custodio, string eve_parada_okupacion, string eve_parada_comercializacion_tecnica, string eve_companya_suministros_gas, string eve_num_contrato_suministro_gas, string eve_parada_estrategica_comercializacion, string eve_estado_precomercializacion, string eve_punto_de_suministro_agua_peticion_tecnica, string eve_bloqueo_validacion_llaves, string eve_referencia_catastral_renovacion, string eve_descripcion_motivo_paralizacion, string eve_incidencia_tras_revision_inicial, DateTime? eve_fecha_ultima_comunicacion_centro_custodia, DateTime? eve_fecha_anulacion_paralizacion_estrategica, DateTime? eve_fecha_incidencia_tras_revision_inicial, string eve_luz_nodisponible, DateTime? eve_fecha_parada_comercializacion, string eve_agua_nodisponible, string eve_gas_nodisponible, string eve_peticion_tecnica_cambio_titularidad_sums_value, DateTime? eve_fecha_paralizacion_estrategica, string eve_cups_gas_peticion_tecnica, DateTime? eve_fecha_contrato_agua, string eve_parada_comercializacion_estrategica)
        {
            this.eve_nombre = eve_nombre;
            this.eve_codigosociedad = eve_codigosociedad;
            this.eve_codigopromocion = eve_codigopromocion;
            this.eve_descripcionpromocion = eve_descripcionpromocion;
            this.eve_estado = eve_estado;
            this.eve_tipoinmueble = eve_tipoinmueble;
            this.eve_descripcionportal = eve_descripcionportal;
            this.eve_tipovia = eve_tipovia;
            this.eve_calle = eve_calle;
            this.eve_numero = eve_numero;
            this.eve_portal = eve_portal;
            this.eve_escalera = eve_escalera;
            this.eve_piso = eve_piso;
            this.eve_puerta = eve_puerta;
            this.eve_codigopostal = eve_codigopostal;
            this.eve_poblacion = eve_poblacion;
            this.eve_codigo_reserva = eve_codigo_reserva;
            this.eve_bonificacion = eve_bonificacion;
            this.eve_meses_apli = eve_meses_apli;
            this.eve_mesesdegarantia = eve_mesesdegarantia;
            this.eve_mesesfianza = eve_mesesfianza;
            this.eve_alquiler = eve_alquiler;
            this.eve_ibi_anual = eve_ibi_anual;
            this.eve_comunidad = eve_comunidad;
            this.eve_comunidad_anual = eve_comunidad_anual;
            this.eve_sujeto_a_permanencia = eve_sujeto_a_permanencia;
            this.eve_meses_permanencia = eve_meses_permanencia;
            this.eve_sujeto_a_impagos = eve_sujeto_a_impagos;
            this.eve_hasta_fin_de_contrato = eve_hasta_fin_de_contrato;
            this.eve_numero_meses_corriente_de_pago = eve_numero_meses_corriente_de_pago;
            this.eve_numeroexpediente = eve_numeroexpediente;
            this.eve_tipo_capex_actual = eve_tipo_capex_actual;
            this.eve_metrosutiles = eve_metrosutiles;
            this.eve_dormitorios = eve_dormitorios;
            this.eve_banyos = eve_banyos;
            this.eve_provincia = eve_provincia;
            this.eve_comunidadautonoma = eve_comunidadautonoma;
            this.eve_fecha_validacion_acceso_fisico_vivienda = eve_fecha_validacion_acceso_fisico_vivienda;
            this.eve_fecha_fin_obras_prevista = eve_fecha_fin_obras_prevista;
            this.eve_fecha_fin_obras_prevista_revisada = eve_fecha_fin_obras_prevista_revisada;
            this.eve_anejo = eve_anejo;
            this.eve_tipo_capex_actual_descripcion = eve_tipo_capex_actual_descripcion;
            this.eve_tipo_capex_en_proceso_descripcion = eve_tipo_capex_en_proceso_descripcion;
            this.eve_minusvalido = eve_minusvalido;
            OdataEtag = odataEtag;
            this.statecode = statecode;
            this.statuscode = statuscode;
            this.eve_numeroregistro = eve_numeroregistro;
            this.eve_fechamodificacionregistro = eve_fechamodificacionregistro;
            _eve_sociedadinmuebleid_value = eve_sociedadinmuebleid_value;
            this.eve_pais = eve_pais;
            this.createdon = createdon;
            this.eve_comunidadlistames = eve_comunidadlistames;
            this.eve_codigo_provincia = eve_codigo_provincia;
            this.eve_visualizacionportal = eve_visualizacionportal;
            _ownerid_value = ownerid_value;
            this.eve_cuotabloque = eve_cuotabloque;
            this.modifiedon = modifiedon;
            this.eve_idsociedadprinex = eve_idsociedadprinex;
            this.versionnumber = versionnumber;
            this.eve_idinmuebleprinex = eve_idinmuebleprinex;
            this.timezoneruleversionnumber = timezoneruleversionnumber;
            _modifiedby_value = modifiedby_value;
            this.eve_descripcion = eve_descripcion;
            this.eve_inmuebleid = eve_inmuebleid;
            _eve_promocininmuebleid_value = eve_promocininmuebleid_value;
            this.eve_metrosconstruidos = eve_metrosconstruidos;
            _createdby_value = createdby_value;
            _owningbusinessunit_value = owningbusinessunit_value;
            _owninguser_value = owninguser_value;
            this.eve_comunidadlistaanual = eve_comunidadlistaanual;
            this.eve_hace_esquina = eve_hace_esquina;
            this.eve_suite_incorporado_api = eve_suite_incorporado_api;
            this.eve_subcategoria_tecnica = eve_subcategoria_tecnica;
            this.eve_texto_10 = eve_texto_10;
            this.eve_marca_horno_microondas = eve_marca_horno_microondas;
            this.eve_comentarios_internos_entrega_llaves = eve_comentarios_internos_entrega_llaves;
            this.eve_congelador = eve_congelador;
            this.importsequencenumber = importsequencenumber;
            this.eve_primera_revision_cambio_sumin = eve_primera_revision_cambio_sumin;
            this.eve_soleria_vivienda = eve_soleria_vivienda;
            this.eve_sumatorio_enseres = eve_sumatorio_enseres;
            this.eve_fecha_aprobacion_reserva = eve_fecha_aprobacion_reserva;
            this.eve_categoria_tecnica = eve_categoria_tecnica;
            this.eve_cotejo_bonificaciones = eve_cotejo_bonificaciones;
            this.eve_estado_validac_estado_obra_adicional = eve_estado_validac_estado_obra_adicional;
            this.eve_carencia_vivienda_ibi = eve_carencia_vivienda_ibi;
            this.eve_amueblado_api = eve_amueblado_api;
            this.eve_salida_humos = eve_salida_humos;
            this.eve_terraza_api = eve_terraza_api;
            this.eve_envio_comunicacion_a_centro_custodia = eve_envio_comunicacion_a_centro_custodia;
            this.eve_estado_gestion_revision_suministros = eve_estado_gestion_revision_suministros;
            this.eve_criticidad = eve_criticidad;
            this.eve_estado_conservacion = eve_estado_conservacion;
            this.eve_color_lavavajillas = eve_color_lavavajillas;
            this.eve_estado_adicional = eve_estado_adicional;
            this.eve_importe_3 = eve_importe_3;
            this.eve_ubicacion = eve_ubicacion;
            this.eve_importe_2 = eve_importe_2;
            this.eve_importe_1 = eve_importe_1;
            this.eve_importe_5 = eve_importe_5;
            this.eve_importe_4 = eve_importe_4;
            this.eve_induccion = eve_induccion;
            this.eve_pre_creado_fecha_5 = eve_pre_creado_fecha_5;
            this.eve_fecha_entrega_llaves_api = eve_fecha_entrega_llaves_api;
            this.eve_pre_creado_texto_3 = eve_pre_creado_texto_3;
            this.eve_tipo_adecuacion_actual = eve_tipo_adecuacion_actual;
            this.eve_referenciacatastral = eve_referenciacatastral;
            this.eve_color_campana = eve_color_campana;
            this.eve_codigo_contrato = eve_codigo_contrato;
            this.eve_pre_creado_fecha_1 = eve_pre_creado_fecha_1;
            this.eve_banyos_suite = eve_banyos_suite;
            this.eve_iniciar_creacion_peticion = eve_iniciar_creacion_peticion;
            this.eve_color_vitroceramica = eve_color_vitroceramica;
            this.eve_fecha_entrega_llaves_api_adicional = eve_fecha_entrega_llaves_api_adicional;
            this.eve_estado_validacion_suministros = eve_estado_validacion_suministros;
            this.eve_datos_recogida_completados = eve_datos_recogida_completados;
            this.eve_periodicidadcomunidad = eve_periodicidadcomunidad;
            this.eve_lavadora_integrada = eve_lavadora_integrada;
            this.eve_color_microondas = eve_color_microondas;
            this.eve_amueblado = eve_amueblado;
            this.eve_marca_lavadora = eve_marca_lavadora;
            this.eve_dias_desde_disponibilidad_primer_juego = eve_dias_desde_disponibilidad_primer_juego;
            this.eve_codigo_contratista_adecuacion = eve_codigo_contratista_adecuacion;
            this.eve_dias_desde_disponibilidad_ultimo_juego = eve_dias_desde_disponibilidad_ultimo_juego;
            this.eve_comentario_interno_1 = eve_comentario_interno_1;
            this.eve_aire_acondicionado_api = eve_aire_acondicionado_api;
            this.eve_importe_final_obra = eve_importe_final_obra;
            this.eve_texto_13 = eve_texto_13;
            this.eve_comunicacion_a_centro_custodia = eve_comunicacion_a_centro_custodia;
            this.eve_distribucion = eve_distribucion;
            this.eve_jardin_privado_api = eve_jardin_privado_api;
            this.eve_estado_comercializacion = eve_estado_comercializacion;
            this.eve_fecha_valoracion = eve_fecha_valoracion;
            this.eve_fecha_disp_primer_juego_llaves_ant = eve_fecha_disp_primer_juego_llaves_ant;
            this.eve_tipo_cocina = eve_tipo_cocina;
            this.eve_numerofincaregistral = eve_numerofincaregistral;
            _owningteam_value = owningteam_value;
            this.eve_presupuesto_valoracion_aceptado = eve_presupuesto_valoracion_aceptado;
            this.eve_microondas = eve_microondas;
            _eve_reserva_asociada_value = eve_reserva_asociada_value;
            this.eve_empresa_calidad = eve_empresa_calidad;
            this.eve_carencia_vivienda_comunidad = eve_carencia_vivienda_comunidad;
            this.eve_comentarios_validacion_estado_obra = eve_comentarios_validacion_estado_obra;
            this.eve_calefaccion = eve_calefaccion;
            this.eve_pre_creado_fecha_6 = eve_pre_creado_fecha_6;
            this.eve_ciclo_capex_actual = eve_ciclo_capex_actual;
            _modifiedonbehalfby_value = modifiedonbehalfby_value;
            this.eve_tipologia_inmueble_txt = eve_tipologia_inmueble_txt;
            this.eve_pre_creado_fecha_2 = eve_pre_creado_fecha_2;
            this.eve_fecha_recepcion_api_llaves_ultimo = eve_fecha_recepcion_api_llaves_ultimo;
            this.eve_pre_creado_texto_2 = eve_pre_creado_texto_2;
            this.eve_peticion_titularidad_suministrothanterior = eve_peticion_titularidad_suministrothanterior;
            this.eve_causa_desviacion = eve_causa_desviacion;
            this.eve_estado_general_suministros = eve_estado_general_suministros;
            this.eve_pre_creado_texto_6 = eve_pre_creado_texto_6;
            this.eve_numero_expediente_vpo = eve_numero_expediente_vpo;
            this.eve_fecha_validacion_suministros = eve_fecha_validacion_suministros;
            this.eve_fecha_inicio_obras_prevista = eve_fecha_inicio_obras_prevista;
            this.eve_congelador_integrado = eve_congelador_integrado;
            this.eve_certificado_energetico = eve_certificado_energetico;
            this.eve_calefaccion_api = eve_calefaccion_api;
            this.eve_estado_validacion_suministros_adicional = eve_estado_validacion_suministros_adicional;
            this.eve_texto_12 = eve_texto_12;
            this.eve_peticion_titularidad_suministroth = eve_peticion_titularidad_suministroth;
            this.eve_aire_acondicionado = eve_aire_acondicionado;
            this.eve_puerta_seguridad = eve_puerta_seguridad;
            this.eve_lavavajillas_integrado = eve_lavavajillas_integrado;
            this.eve_estado_entrega_ultimo_juego_llaves = eve_estado_entrega_ultimo_juego_llaves;
            this.eve_lavavajillas = eve_lavavajillas;
            this.eve_estado_adecuacion_adicional = eve_estado_adecuacion_adicional;
            this.eve_estado_validacion_anterior = eve_estado_validacion_anterior;
            this.eve_estado_validacion_vivienda_api = eve_estado_validacion_vivienda_api;
            this.eve_operacion = eve_operacion;
            this.eve_cups_gas = eve_cups_gas;
            this.eve_marca_vitroceramica = eve_marca_vitroceramica;
            this.eve_frigorifico_con_congelador_integrado = eve_frigorifico_con_congelador_integrado;
            this.eve_sigla_tipo_via = eve_sigla_tipo_via;
            this.eve_estado_entrega_primer_juego_llaves = eve_estado_entrega_primer_juego_llaves;
            this.eve_importe_oc = eve_importe_oc;
            this.eve_marca_lavavajillas = eve_marca_lavavajillas;
            this.eve_datosregistrales = eve_datosregistrales;
            this.eve_pre_creado_fecha_7 = eve_pre_creado_fecha_7;
            this.eve_pre_creado_texto_1 = eve_pre_creado_texto_1;
            this.eve_comentarios_acceso_vivienda = eve_comentarios_acceso_vivienda;
            this.eve_color_lavadora_secadora = eve_color_lavadora_secadora;
            this.eve_estatus = eve_estatus;
            this.eve_contratista = eve_contratista;
            this.eve_fecha_validacion_vivienda = eve_fecha_validacion_vivienda;
            this.eve_estado_validac_acceso_fisico_vivienda = eve_estado_validac_acceso_fisico_vivienda;
            this.eve_equipamiento_cocina_api = eve_equipamiento_cocina_api;
            this.utcconversiontimezonecode = utcconversiontimezonecode;
            this.eve_pre_creado_fecha_3 = eve_pre_creado_fecha_3;
            this.eve_pre_creado_texto_5 = eve_pre_creado_texto_5;
            this.eve_pre_creado_numerico_3 = eve_pre_creado_numerico_3;
            this.eve_pre_creado_numerico_2 = eve_pre_creado_numerico_2;
            this.eve_pre_creado_numerico_1 = eve_pre_creado_numerico_1;
            this.eve_tipo_cocina_api = eve_tipo_cocina_api;
            this.eve_cotejo_rentas_vpo = eve_cotejo_rentas_vpo;
            this.eve_fecha_recogida_datos = eve_fecha_recogida_datos;
            this.eve_pre_creado_numerico_4 = eve_pre_creado_numerico_4;
            this.eve_cups_luz = eve_cups_luz;
            this.eve_texto_15 = eve_texto_15;
            this.eve_marca_horno = eve_marca_horno;
            this.eve_importe_bon = eve_importe_bon;
            this.eve_fees_ss = eve_fees_ss;
            this.eve_color_induccion = eve_color_induccion;
            this.eve_horno = eve_horno;
            this.eve_vitroceramica = eve_vitroceramica;
            this.eve_color_lavadora = eve_color_lavadora;
            this.eve_lavadora_secadora = eve_lavadora_secadora;
            this.eve_num_armarios_no_empotrados_api = eve_num_armarios_no_empotrados_api;
            this.eve_texto_11 = eve_texto_11;
            this.eve_fecha_disponibilidad_primer_juego_llaves = eve_fecha_disponibilidad_primer_juego_llaves;
            this.eve_orientacion = eve_orientacion;
            this.eve_fecha_inicio_obras = eve_fecha_inicio_obras;
            this.eve_renta = eve_renta;
            this.eve_marca_microondas = eve_marca_microondas;
            this.eve_balcon_api = eve_balcon_api;
            this.eve_contratista_adecuacion = eve_contratista_adecuacion;
            this.eve_comentario_interno_2 = eve_comentario_interno_2;
            this.eve_presupuesto_valoracion = eve_presupuesto_valoracion;
            this.eve_m2_escaparate = eve_m2_escaparate;
            this.eve_anyo_ibi = eve_anyo_ibi;
            this.eve_capex_extraordinario = eve_capex_extraordinario;
            this.eve_periodicidadibi = eve_periodicidadibi;
            this.eve_marca_lavadora_secadora = eve_marca_lavadora_secadora;
            this.eve_tecnico_responsable_capex = eve_tecnico_responsable_capex;
            this.overriddencreatedon = overriddencreatedon;
            this.eve_balcon = eve_balcon;
            this.eve_peticion_titularidad_suministro_inquilino = eve_peticion_titularidad_suministro_inquilino;
            this.eve_fees_revision_2 = eve_fees_revision_2;
            this.eve_ciclo_capex_en_proceso = eve_ciclo_capex_en_proceso;
            this.eve_crear_peticion_tecnica = eve_crear_peticion_tecnica;
            this.eve_fecha_recepcion_api_llaves = eve_fecha_recepcion_api_llaves;
            this.eve_localidadregistro = eve_localidadregistro;
            this.eve_frigorifico_con_congelador = eve_frigorifico_con_congelador;
            this.eve_color_frigorifico = eve_color_frigorifico;
            this.eve_fecha_check_list = eve_fecha_check_list;
            this.eve_terraza = eve_terraza;
            _eve_propietario_tecnico_id_value = eve_propietario_tecnico_id_value;
            this.eve_pre_creado_fecha_4 = eve_pre_creado_fecha_4;
            this.eve_texto_6 = eve_texto_6;
            this.eve_texto_7 = eve_texto_7;
            this.eve_texto_5 = eve_texto_5;
            this.eve_tipo = eve_tipo;
            this.eve_agua_caliente = eve_agua_caliente;
            this.eve_texto_8 = eve_texto_8;
            this.eve_fecha_real_finalizacion_2 = eve_fecha_real_finalizacion_2;
            this.eve_texto_9 = eve_texto_9;
            this.eve_fecha_3 = eve_fecha_3;
            this.eve_fecha_2 = eve_fecha_2;
            this.eve_fecha_5 = eve_fecha_5;
            this.eve_fecha_4 = eve_fecha_4;
            this.eve_electrodomesticos = eve_electrodomesticos;
            this.eve_revision_suministros_adicional = eve_revision_suministros_adicional;
            this.eve_texto_14 = eve_texto_14;
            this.eve_comentarios_validacion_suministros = eve_comentarios_validacion_suministros;
            this.eve_codigo_adecuacion_actual_texto = eve_codigo_adecuacion_actual_texto;
            this.eve_precio_maximo_vivienda_protegida = eve_precio_maximo_vivienda_protegida;
            _createdonbehalfby_value = createdonbehalfby_value;
            this.eve_fecha_validacion_estado_obra = eve_fecha_validacion_estado_obra;
            this.eve_campana_extractora = eve_campana_extractora;
            this.eve_fecha_fin_obras = eve_fecha_fin_obras;
            this.eve_color_horno_microondas = eve_color_horno_microondas;
            this.eve_marca_campana = eve_marca_campana;
            this.eve_jardin_privado = eve_jardin_privado;
            this.eve_horno_microondas = eve_horno_microondas;
            this.eve_fecha_entrega_llaves_inquilino = eve_fecha_entrega_llaves_inquilino;
            this.eve_descripcion_tecnica = eve_descripcion_tecnica;
            _eve_codigo_adecuacion_actual_value = eve_codigo_adecuacion_actual_value;
            this.eve_marca_congelador = eve_marca_congelador;
            this.eve_color_horno = eve_color_horno;
            this.eve_tecnico_responsable = eve_tecnico_responsable;
            this.eve_ubicacion_banyos = eve_ubicacion_banyos;
            this.eve_tipo_capex_en_proceso = eve_tipo_capex_en_proceso;
            this.eve_marca_induccion = eve_marca_induccion;
            this.eve_soleria_vivienda_api = eve_soleria_vivienda_api;
            this.eve_estado_adecuacion = eve_estado_adecuacion;
            this.eve_lavadora = eve_lavadora;
            this.eve_tipologia_inmueble = eve_tipologia_inmueble;
            this.eve_equipamiento_cocina = eve_equipamiento_cocina;
            this.eve_estado_validac_acceso_fisic_vivienda_adic = eve_estado_validac_acceso_fisic_vivienda_adic;
            this.eve_marca_frigorifico = eve_marca_frigorifico;
            this.eve_estado_validacion_anterior_2 = eve_estado_validacion_anterior_2;
            this.eve_estado_validac_estado_obra = eve_estado_validac_estado_obra;
            this.eve_num_armarios_empotrados_api = eve_num_armarios_empotrados_api;
            this.eve_color_congelador = eve_color_congelador;
            this.eve_lavadora_secadora_integrada = eve_lavadora_secadora_integrada;
            this.eve_numero_aseos = eve_numero_aseos;
            this.eve_pre_creado_texto_4 = eve_pre_creado_texto_4;
            this.eve_fees_revision = eve_fees_revision;
            this.eve_armarios_empotrados = eve_armarios_empotrados;
            this.eve_clave_4_prinex = eve_clave_4_prinex;
            this.eve_fecha_fin_obras_prevista_revisada_nuevo = eve_fecha_fin_obras_prevista_revisada_nuevo;
            this.eve_id_crm = eve_id_crm;
            this.eve_datos_registrales_renovacion = eve_datos_registrales_renovacion;
            this.eve_num_contrato_suministro_agua = eve_num_contrato_suministro_agua;
            this.eve_promocion_prinex = eve_promocion_prinex;
            _eve_peticion_tecnica_cambio_titularidad_agua_value = eve_peticion_tecnica_cambio_titularidad_agua_value;
            this.eve_fecha_anulacion_parada_comercializacion = eve_fecha_anulacion_parada_comercializacion;
            _eve_peticion_tecnica_alta_suministros_value = eve_peticion_tecnica_alta_suministros_value;
            this.eve_companya_suministros_agua = eve_companya_suministros_agua;
            this.eve_clave_2_prinex = eve_clave_2_prinex;
            this.eve_fecha_guardado_precomercializacion = eve_fecha_guardado_precomercializacion;
            this.eve_num_contrato_suministro_luz = eve_num_contrato_suministro_luz;
            this.eve_fecha_general_comercializacion = eve_fecha_general_comercializacion;
            _eve_usuario_guardado_precom_id_value = eve_usuario_guardado_precom_id_value;
            this.eve_clave_1_prinex = eve_clave_1_prinex;
            this.eve_tipo_de_inmueble_prinex = eve_tipo_de_inmueble_prinex;
            this.eve_clave_3_prinex = eve_clave_3_prinex;
            this.eve_companya_suministros_luz = eve_companya_suministros_luz;
            this.eve_fecha_fin_garantia_adecuacion = eve_fecha_fin_garantia_adecuacion;
            this.eve_id_activo = eve_id_activo;
            _eve_peticion_tecnica_alta_agua_value = eve_peticion_tecnica_alta_agua_value;
            this.eve_cups_luz_peticion_tecnica = eve_cups_luz_peticion_tecnica;
            this.eve_categoria_inmueble = eve_categoria_inmueble;
            this.eve_numero_reclamaciones_centro_custodio = eve_numero_reclamaciones_centro_custodio;
            this.eve_parada_okupacion = eve_parada_okupacion;
            this.eve_parada_comercializacion_tecnica = eve_parada_comercializacion_tecnica;
            this.eve_companya_suministros_gas = eve_companya_suministros_gas;
            this.eve_num_contrato_suministro_gas = eve_num_contrato_suministro_gas;
            this.eve_parada_estrategica_comercializacion = eve_parada_estrategica_comercializacion;
            this.eve_estado_precomercializacion = eve_estado_precomercializacion;
            this.eve_punto_de_suministro_agua_peticion_tecnica = eve_punto_de_suministro_agua_peticion_tecnica;
            this.eve_bloqueo_validacion_llaves = eve_bloqueo_validacion_llaves;
            this.eve_referencia_catastral_renovacion = eve_referencia_catastral_renovacion;
            this.eve_descripcion_motivo_paralizacion = eve_descripcion_motivo_paralizacion;
            this.eve_incidencia_tras_revision_inicial = eve_incidencia_tras_revision_inicial;
            this.eve_fecha_ultima_comunicacion_centro_custodia = eve_fecha_ultima_comunicacion_centro_custodia;
            this.eve_fecha_anulacion_paralizacion_estrategica = eve_fecha_anulacion_paralizacion_estrategica;
            this.eve_fecha_incidencia_tras_revision_inicial = eve_fecha_incidencia_tras_revision_inicial;
            this.eve_luz_nodisponible = eve_luz_nodisponible;
            this.eve_fecha_parada_comercializacion = eve_fecha_parada_comercializacion;
            this.eve_agua_nodisponible = eve_agua_nodisponible;
            this.eve_gas_nodisponible = eve_gas_nodisponible;
            _eve_peticion_tecnica_cambio_titularidad_sums_value = eve_peticion_tecnica_cambio_titularidad_sums_value;
            this.eve_fecha_paralizacion_estrategica = eve_fecha_paralizacion_estrategica;
            this.eve_cups_gas_peticion_tecnica = eve_cups_gas_peticion_tecnica;
            this.eve_fecha_contrato_agua = eve_fecha_contrato_agua;
            this.eve_parada_comercializacion_estrategica = eve_parada_comercializacion_estrategica;

        }

        public string SerializarCustomDataProducto()
        {
            return JsonConvert.SerializeObject(this);
        }

    }
    public class CustomDataContacto
    {
        public string externalid { get; set; }
        public string eve_nif_nie { get; set; }
        public string eve_numero { get; set; }
        public string eve_portal { get; set; }
        public string eve_escalera { get; set; }
        public string eve_piso { get; set; }
        public string eve_puerta { get; set; }
        public string eve_codigoPostal { get; set; }
        public bool eve_consentimiento_grupo { get; set; }
        public bool eve_consentimiento_terceras { get; set; }
        public int eve_origen_consentimiento { get; set; }
        public DateTime eve_fecha_consentimiento { get; set; }
        public string eve_calificativo { get; set; }
        public string eve_descripcioncalificativo { get; set; }
        public double eve_deudatotalinquilino { get; set; }
        public double eve_deudatotalcontrato { get; set; }
        public double eve_deudatotalplandepago { get; set; }
        public double eve_deudatotalanticipo { get; set; }
        public double eve_importe_total_pendiente_vencimiento_plan { get; set; }
        public string eve_descripcionsociedad { get; set; }
        public string name { get; set; }
        public string eve_nombreapellidos { get; set; }
        public string eve_envio_carta_deuda { get; set; }
        public string eve_estado_anterior_deuda { get; set; }
        public string eve_estado_deuda { get; set; }
        public Nullable<DateTime> eve_fecha_cita { get; set; }
        public Nullable<DateTime> eve_fecha_cita_adicional { get; set; }
        public Nullable<DateTime> eve_fecha_promesa_pago { get; set; }
        public DateTime modifiedon { get; set; }
        public string eve_fecha_ultima_cita { get; set; }
        public string eve_fecha_ultima_gestion { get; set; }
        public string eve_gestion_de_impagos { get; set; }
        public string eve_gestion_de_impagos_adicional { get; set; }
        public string eve_gestora_deuda { get; set; }
        public bool eve_multiplessociedades { get; set; }
        public string eve_observacion_carta { get; set; }
        public string eve_observacion_gestion { get; set; }
        public string eve_observaciones_deuda { get; set; }

        public CustomDataContacto() { }

        public CustomDataContacto(string externalid, string eve_nif_nie, string eve_numero, string eve_portal, string eve_escalera, string eve_piso, string eve_puerta, string eve_codigoPostal, bool eve_consentimiento_grupo, bool eve_consentimiento_terceras, int eve_origen_consentimiento, DateTime eve_fecha_consentimiento, string eve_calificativo, string eve_descripcioncalificativo, double eve_deudatotalinquilino, double eve_deudatotalcontrato, double eve_deudatotalplandepago, double eve_deudatotalanticipo, double eve_importe_total_pendiente_vencimiento_plan, string eve_descripcionsociedad, string name, string eve_nombreapellidos, string eve_envio_carta_deuda, string eve_estado_anterior_deuda, string eve_estado_deuda, DateTime eve_fecha_cita, DateTime eve_fecha_cita_adicional, DateTime eve_fecha_promesa_pago, string eve_fecha_ultima_cita, string eve_fecha_ultima_gestion, string eve_gestion_de_impagos, string eve_gestion_de_impagos_adicional, string eve_gestora_deuda, bool eve_multiplessociedades, string eve_observacion_carta, string eve_observacion_gestion, string eve_observaciones_deuda, DateTime modifiedon)
        {
            this.externalid = externalid;
            this.eve_nif_nie = eve_nif_nie;
            this.eve_numero = eve_numero;
            this.eve_portal = eve_portal;
            this.eve_escalera = eve_escalera;
            this.eve_piso = eve_piso;
            this.eve_puerta = eve_puerta;
            this.eve_codigoPostal = eve_codigoPostal;
            this.eve_consentimiento_grupo = eve_consentimiento_grupo;
            this.eve_consentimiento_terceras = eve_consentimiento_terceras;
            this.eve_origen_consentimiento = eve_origen_consentimiento;
            this.eve_fecha_consentimiento = eve_fecha_consentimiento;
            this.eve_calificativo = eve_calificativo;
            this.eve_descripcioncalificativo = eve_descripcioncalificativo;
            this.eve_deudatotalinquilino = eve_deudatotalinquilino;
            this.eve_deudatotalcontrato = eve_deudatotalcontrato;
            this.eve_deudatotalplandepago = eve_deudatotalplandepago;
            this.eve_deudatotalanticipo = eve_deudatotalanticipo;
            this.eve_importe_total_pendiente_vencimiento_plan = eve_importe_total_pendiente_vencimiento_plan;
            this.eve_descripcionsociedad = eve_descripcionsociedad;
            this.name = name;
            this.eve_nombreapellidos = eve_nombreapellidos;
            this.eve_envio_carta_deuda = eve_envio_carta_deuda;
            this.eve_estado_anterior_deuda = eve_estado_anterior_deuda;
            this.eve_estado_deuda = eve_estado_deuda;
            this.eve_fecha_cita = eve_fecha_cita;
            this.eve_fecha_cita_adicional = eve_fecha_cita_adicional;
            this.eve_fecha_promesa_pago = eve_fecha_promesa_pago;
            this.eve_fecha_ultima_cita = eve_fecha_ultima_cita;
            this.eve_fecha_ultima_gestion = eve_fecha_ultima_gestion;
            this.eve_gestion_de_impagos = eve_gestion_de_impagos;
            this.eve_gestion_de_impagos_adicional = eve_gestion_de_impagos_adicional;
            this.eve_gestora_deuda = eve_gestora_deuda;
            this.eve_multiplessociedades = eve_multiplessociedades;
            this.eve_observacion_carta = eve_observacion_carta;
            this.eve_observacion_gestion = eve_observacion_gestion;
            this.eve_observaciones_deuda = eve_observaciones_deuda;
            this.modifiedon = modifiedon;
        }

        public string SerializarCustomDataContacto()
        {
            //return JsonConvert.SerializeObject(this, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            return JsonConvert.SerializeObject(this);
        }
    }
}

