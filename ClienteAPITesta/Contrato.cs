﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using ModeloDatos;

namespace ClienteAPITesta
{
    public class Contrato
    {

        [JsonProperty("@odata.etag")]
        public string odataEtag { get; set; }
        public string eve_estadocontrato { get; set; }
        public string eve_renovarrentasipcnegativo { get; set; }
        public string eve_idsociedadprinex { get; set; }
        public string eve_fecha_inicio_primer_contrato { get; set; }
        public string eve_importe_seguro_impago { get; set; }
        public string eve_tipopromocion { get; set; }
        public string eve_sumatorioporcentajeparticipacion { get; set; }
        public string eve_cuentabancaria { get; set; }
        public string eve_fechafirmamandato { get; set; }
        public string statuscode { get; set; }
        public string eve_rentamensual { get; set; }
        public string eve_fechaefectoentregallaves { get; set; }
        public string eve_fechainicio { get; set; }
        public string eve_tipoadquisicion { get; set; }
        public string eve_cuentabancariaingreso { get; set; }
        public string modifiedon { get; set; }
        public string eve_numeroconciertofianzas { get; set; }
        public string _owninguser_value { get; set; }
        public string eve_sumatorioporcentajeparticipacion_date { get; set; }
        public string eve_visualizacionportal { get; set; }
        public string eve_tipofinalizacion { get; set; }
        public string eve_operacion { get; set; }
        public string eve_sumatorioporcentajeparticipacion_state { get; set; }
        public string eve_descripcionpromocion { get; set; }
        public string eve_mandato { get; set; }
        public string eve_fechafin { get; set; }
        public string _ownerid_value { get; set; }
        public string eve_contratoopcioncompra { get; set; }
        public string eve_prorrogasacumuladas { get; set; }
        public string _eve_sociedadcontrato_id_value { get; set; }
        public string eve_fechamodificacionregistro { get; set; }
        public string eve_renovaciontacita { get; set; }
        public string eve_actualizarfianza { get; set; }
        public string _eve_inquilinocontratoid_value { get; set; }
        public string eve_fechaliquidacioncontrato { get; set; }
        public string eve_descripcionsociedad { get; set; }
        public string eve_fechaentregallaves { get; set; }
        public string eve_importegarantiaadicional { get; set; }
        public string eve_liquidaciongestionfueraportal { get; set; }
        public string timezoneruleversionnumber { get; set; }
        public string eve_importetotaldeuda { get; set; }
        public string eve_porcentajeanyo3 { get; set; }
        public string createdon { get; set; }
        public string eve_cuentabancariaportal { get; set; }
        public string eve_numero_recibo_seguro_impago { get; set; }
        public string eve_bloqueado_promocion_sociedad { get; set; }
        public string eve_repercusionibi { get; set; }
        public string eve_titularcuentabancaria { get; set; }
        public string eve_tiporevision { get; set; }
        public string eve_eve_importe_garantia_importe { get; set; }
        public string eve_enviar_recordatorio_seguro_hogar { get; set; }
        public string versionnumber { get; set; }
        public string eve_name { get; set; }
        public string _eve_promocincontrato_id_value { get; set; }
        public string eve_mandatofirmado { get; set; }
        public string eve_fechaadquisicion { get; set; }
        public string eve_organismofianzas { get; set; }
        public string eve_importefianza { get; set; }
        public string eve_tipodeposito { get; set; }
        public string eve_duracionprorrogas { get; set; }
        public string _modifiedby_value { get; set; }
        public string _createdby_value { get; set; }
        public string eve_contratoid { get; set; }
        public string eve_indicevigente { get; set; }
        public string eve_numeromaximoprorrogas { get; set; }
        public string eve_causafinalizacion { get; set; }
        public string eve_tipo_poliza_seguro_impago { get; set; }
        public string eve_renovacionautomatica { get; set; }
        public string eve_contratoretenido { get; set; }
        public string statecode { get; set; }
        public string _owningbusinessunit_value { get; set; }
        public string eve_porcentajeanyo1 { get; set; }
        public string eve_deudatotal { get; set; }
        public string eve_importeproximarenta { get; set; }
        public string eve_porcentajeanyo2 { get; set; }
        public string eve_idcontratoprinex { get; set; }
        public string eve_diavencimientorecibos { get; set; }
        public string eve_recibospendientesvencimiento { get; set; }
        public string eve_codigosociedad { get; set; }
        public string eve_codigopromocion { get; set; }
        public string eve_mandatovigor { get; set; }
        public string eve_importe_rentas_impagadas { get; set; }
        public string eve_inquilino_recuperacion_3 { get; set; }
        public string eve_descripcion_seguro_impago { get; set; }
        public string importsequencenumber { get; set; }
        public string utcconversiontimezonecode { get; set; }
        public string eve_numero_referencia_aval_bancario { get; set; }
        public string overriddencreatedon { get; set; }
        public string eve_fecharepercusionhasta { get; set; }
        public string eve_importe_deuda_bonificacion_nuevo { get; set; }
        public string eve_motivo_finalizacion_contrato { get; set; }
        public string eve_comentarios_motivo_finalizacion { get; set; }
        public string eve_motivoretencion { get; set; }
        public string eve_observacionescontrato { get; set; }
        public string eve_importe_deuda_bonificacion { get; set; }
        public string eve_inquilino_recuperacion_1 { get; set; }
        public string eve_fechabajagas { get; set; }
        public string _eve_codigo_primer_contrato_id_value { get; set; }
        public string eve_renovacionvivienda { get; set; }
        public string eve_fecha_caducidad_aval_bancario { get; set; }
        public string eve_comentarios_estado_recuperacion { get; set; }
        public string eve_fecha_inicio_vigencia_aval_bancario { get; set; }
        public string eve_inquilino_recuperacion_2 { get; set; }
        public string eve_observacionesavalista { get; set; }
        public string eve_fecha_registro_deuda { get; set; }
        public string eve_otros_motivos_ii { get; set; }
        public string eve_fecha_contacto { get; set; }
        public string processid { get; set; }
        public string eve_referencia_seguro_impago { get; set; }
        public string eve_motivoresolucion { get; set; }
        public string traversedpath { get; set; }
        public string eve_origen_finalizacion_contrato { get; set; }
        public string eve_estado_recuperacion { get; set; }
        public string eve_codigoetapadevolucionfianza_anterior { get; set; }
        public string eve_fechaproximarevisionrentaportal { get; set; }
        public string _owningteam_value { get; set; }
        public string eve_fecha_comunicacion_bonificacion { get; set; }
        public string eve_fecha_inicio_seguro_impago { get; set; }
        public string eve_id_inmueble_principal_prinex { get; set; }
        public string eve_tipogarantiaadicional { get; set; }
        public string _modifiedonbehalfby_value { get; set; }
        public string eve_descripcionactualetapadevolucionfianza { get; set; }
        public string eve_proveedor_seguro_impago { get; set; }
        public string eve_fechaproximarevisionrenta { get; set; }
        public string eve_operacioncontratoduplicado { get; set; }
        public string _createdonbehalfby_value { get; set; }
        public string eve_cierre_expediente { get; set; }
        public string eve_fecha_fin_seguro_impago { get; set; }
        public string eve_envio_comunicacion_bonificacion { get; set; }
        public string eve_tipo_envio_renovacion { get; set; }
        public string eve_fecha_finalizacion_recuperacion { get; set; }
        public string eve_fecha_envio_recordatorio_seguro { get; set; }
        public string eve_codigoetapadevolucionfianza { get; set; }
        public string eve_clasificacion_cambio_vivienda { get; set; }
        public string eve_fechabajaagua { get; set; }
        public string eve_email_recuperacion_inquilino_3 { get; set; }
        public string eve_eve_entidad_aval_bancario { get; set; }
        public string eve_motivo_finalizacion_contrato_adicional { get; set; }
        public string eve_fecharepercusiondesde { get; set; }
        public string eve_codigo_inmueble_principal { get; set; }
        public string eve_motivo_no_recuperacion_1 { get; set; }
        public string eve_tipodocumentoavalista { get; set; }
        public string eve_tipo_comunicacion_bonificacion { get; set; }
        public string eve_fecha_cambio_motivo_1 { get; set; }
        public string eve_fecha_cambio_motivo_2 { get; set; }
        public string eve_otros_motivos_i { get; set; }
        public string eve_inquilinoprincipal { get; set; }
        public string eve_email_recuperacion_inquilino_2 { get; set; }
        public string eve_fecha_minima_resolucion { get; set; }
        public string eve_apellidosavalista { get; set; }
        public string eve_telefono_recuperacion_1 { get; set; }
        public string eve_telefono_recuperacion_3 { get; set; }
        public string eve_telefono_recuperacion_2 { get; set; }
        public string eve_fecha_envio_comunicacion { get; set; }
        public string eve_estado_recuperacion_adicional { get; set; }
        public string eve_fecha_liquidacion_bonificacion { get; set; }
        public string eve_comentario_aval_bancario { get; set; }
        public string eve_inmuebleprincipaloperacion { get; set; }
        public string _stageid_value { get; set; }
        public string eve_comentario_interno_seguros { get; set; }
        public string eve_equipo_encargado_recuperacion { get; set; }
        public string eve_rentamensualportal { get; set; }
        public string eve_email_recuperacion_inquilino_1 { get; set; }
        public string eve_documentoavalista { get; set; }
        public string eve_motivo_no_recuperacion_2 { get; set; }
        public string eve_fechabajaluz { get; set; }
        public string eve_reasignacion { get; set; }
        public string eve_observaciones_intern_bonificacion { get; set; }
        public string eve_formadecobro { get; set; }
        public string eve_nombreavalista { get; set; }
        public string _eve_codigo_contrato_renovado_id_value { get; set; }
        public string eve_descripcionetapadevolucionfianza { get; set; }
        public string eve_motivo_finalizacion_contrato_anterior_2 { get; set; }
        public string eve_motivo_finalizacion_contrato_anterior_1 { get; set; }
        public string _eve_inquilinoprincipalliquidacion_value { get; set; }
        public string eve_fechaenviomail { get; set; }
        public string eve_fecha_registro_garantia { get; set; }
        public Contrato(){}
        public Contrato(string odataEtag, string eve_estadocontrato, string eve_renovarrentasipcnegativo, string eve_idsociedadprinex, string eve_fecha_inicio_primer_contrato, string eve_importe_seguro_impago, string eve_tipopromocion, string eve_sumatorioporcentajeparticipacion, string eve_cuentabancaria, string eve_fechafirmamandato, string statuscode, string eve_rentamensual, string eve_fechaefectoentregallaves, string eve_fechainicio, string eve_tipoadquisicion, string eve_cuentabancariaingreso, string modifiedon, string eve_numeroconciertofianzas, string _owninguser_value, string eve_sumatorioporcentajeparticipacion_date, string eve_visualizacionportal, string eve_tipofinalizacion, string eve_operacion, string eve_sumatorioporcentajeparticipacion_state, string eve_descripcionpromocion, string eve_mandato, string eve_fechafin, string _ownerid_value, string eve_contratoopcioncompra, string eve_prorrogasacumuladas, string _eve_sociedadcontrato_id_value, string eve_fechamodificacionregistro, string eve_renovaciontacita, string eve_actualizarfianza, string _eve_inquilinocontratoid_value, string eve_fechaliquidacioncontrato, string eve_descripcionsociedad, string eve_fechaentregallaves, string eve_importegarantiaadicional, string eve_liquidaciongestionfueraportal, string timezoneruleversionnumber, string eve_importetotaldeuda, string eve_porcentajeanyo3, string createdon, string eve_cuentabancariaportal, string eve_numero_recibo_seguro_impago, string eve_bloqueado_promocion_sociedad, string eve_repercusionibi, string eve_titularcuentabancaria, string eve_tiporevision, string eve_eve_importe_garantia_importe, string eve_enviar_recordatorio_seguro_hogar, string versionnumber, string eve_name, string _eve_promocincontrato_id_value, string eve_mandatofirmado, string eve_fechaadquisicion, string eve_organismofianzas, string eve_importefianza, string eve_tipodeposito, string eve_duracionprorrogas, string _modifiedby_value, string _createdby_value, string eve_contratoid, string eve_indicevigente, string eve_numeromaximoprorrogas, string eve_causafinalizacion, string eve_tipo_poliza_seguro_impago, string eve_renovacionautomatica, string eve_contratoretenido, string statecode, string _owningbusinessunit_value, string eve_porcentajeanyo1, string eve_deudatotal, string eve_importeproximarenta, string eve_porcentajeanyo2, string eve_idcontratoprinex, string eve_diavencimientorecibos, string eve_recibospendientesvencimiento, string eve_codigosociedad, string eve_codigopromocion, string eve_mandatovigor, string eve_importe_rentas_impagadas, string eve_inquilino_recuperacion_3, string eve_descripcion_seguro_impago, string importsequencenumber, string utcconversiontimezonecode, string eve_numero_referencia_aval_bancario, string overriddencreatedon, string eve_fecharepercusionhasta, string eve_importe_deuda_bonificacion_nuevo, string eve_motivo_finalizacion_contrato, string eve_comentarios_motivo_finalizacion, string eve_motivoretencion, string eve_observacionescontrato, string eve_importe_deuda_bonificacion, string eve_inquilino_recuperacion_1, string eve_fechabajagas, string _eve_codigo_primer_contrato_id_value, string eve_renovacionvivienda, string eve_fecha_caducidad_aval_bancario, string eve_comentarios_estado_recuperacion, string eve_fecha_inicio_vigencia_aval_bancario, string eve_inquilino_recuperacion_2, string eve_observacionesavalista, string eve_fecha_registro_deuda, string eve_otros_motivos_ii, string eve_fecha_contacto, string processid, string eve_referencia_seguro_impago, string eve_motivoresolucion, string traversedpath, string eve_origen_finalizacion_contrato, string eve_estado_recuperacion, string eve_codigoetapadevolucionfianza_anterior, string eve_fechaproximarevisionrentaportal, string _owningteam_value, string eve_fecha_comunicacion_bonificacion, string eve_fecha_inicio_seguro_impago, string eve_id_inmueble_principal_prinex, string eve_tipogarantiaadicional, string _modifiedonbehalfby_value, string eve_descripcionactualetapadevolucionfianza, string eve_proveedor_seguro_impago, string eve_fechaproximarevisionrenta, string eve_operacioncontratoduplicado, string _createdonbehalfby_value, string eve_cierre_expediente, string eve_fecha_fin_seguro_impago, string eve_envio_comunicacion_bonificacion, string eve_tipo_envio_renovacion, string eve_fecha_finalizacion_recuperacion, string eve_fecha_envio_recordatorio_seguro, string eve_codigoetapadevolucionfianza, string eve_clasificacion_cambio_vivienda, string eve_fechabajaagua, string eve_email_recuperacion_inquilino_3, string eve_eve_entidad_aval_bancario, string eve_motivo_finalizacion_contrato_adicional, string eve_fecharepercusiondesde, string eve_codigo_inmueble_principal, string eve_motivo_no_recuperacion_1, string eve_tipodocumentoavalista, string eve_tipo_comunicacion_bonificacion, string eve_fecha_cambio_motivo_1, string eve_fecha_cambio_motivo_2, string eve_otros_motivos_i, string eve_inquilinoprincipal, string eve_email_recuperacion_inquilino_2, string eve_fecha_minima_resolucion, string eve_apellidosavalista, string eve_telefono_recuperacion_1, string eve_telefono_recuperacion_3, string eve_telefono_recuperacion_2, string eve_fecha_envio_comunicacion, string eve_estado_recuperacion_adicional, string eve_fecha_liquidacion_bonificacion, string eve_comentario_aval_bancario, string eve_inmuebleprincipaloperacion, string _stageid_value, string eve_comentario_interno_seguros, string eve_equipo_encargado_recuperacion, string eve_rentamensualportal, string eve_email_recuperacion_inquilino_1, string eve_documentoavalista, string eve_motivo_no_recuperacion_2, string eve_fechabajaluz, string eve_reasignacion, string eve_observaciones_intern_bonificacion, string eve_formadecobro, string eve_nombreavalista, string _eve_codigo_contrato_renovado_id_value, string eve_descripcionetapadevolucionfianza, string eve_motivo_finalizacion_contrato_anterior_2, string eve_motivo_finalizacion_contrato_anterior_1, string _eve_inquilinoprincipalliquidacion_value, string eve_fechaenviomail, string eve_fecha_registro_garantia)
        {
            this.odataEtag = odataEtag;
            this.eve_estadocontrato = eve_estadocontrato;
            this.eve_renovarrentasipcnegativo = eve_renovarrentasipcnegativo;
            this.eve_idsociedadprinex = eve_idsociedadprinex;
            this.eve_fecha_inicio_primer_contrato = eve_fecha_inicio_primer_contrato;
            this.eve_importe_seguro_impago = eve_importe_seguro_impago;
            this.eve_tipopromocion = eve_tipopromocion;
            this.eve_sumatorioporcentajeparticipacion = eve_sumatorioporcentajeparticipacion;
            this.eve_cuentabancaria = eve_cuentabancaria;
            this.eve_fechafirmamandato = eve_fechafirmamandato;
            this.statuscode = statuscode;
            this.eve_rentamensual = eve_rentamensual;
            this.eve_fechaefectoentregallaves = eve_fechaefectoentregallaves;
            this.eve_fechainicio = eve_fechainicio;
            this.eve_tipoadquisicion = eve_tipoadquisicion;
            this.eve_cuentabancariaingreso = eve_cuentabancariaingreso;
            this.modifiedon = modifiedon;
            this.eve_numeroconciertofianzas = eve_numeroconciertofianzas;
            this._owninguser_value = _owninguser_value;
            this.eve_sumatorioporcentajeparticipacion_date = eve_sumatorioporcentajeparticipacion_date;
            this.eve_visualizacionportal = eve_visualizacionportal;
            this.eve_tipofinalizacion = eve_tipofinalizacion;
            this.eve_operacion = eve_operacion;
            this.eve_sumatorioporcentajeparticipacion_state = eve_sumatorioporcentajeparticipacion_state;
            this.eve_descripcionpromocion = eve_descripcionpromocion;
            this.eve_mandato = eve_mandato;
            this.eve_fechafin = eve_fechafin;
            this._ownerid_value = _ownerid_value;
            this.eve_contratoopcioncompra = eve_contratoopcioncompra;
            this.eve_prorrogasacumuladas = eve_prorrogasacumuladas;
            this._eve_sociedadcontrato_id_value = _eve_sociedadcontrato_id_value;
            this.eve_fechamodificacionregistro = eve_fechamodificacionregistro;
            this.eve_renovaciontacita = eve_renovaciontacita;
            this.eve_actualizarfianza = eve_actualizarfianza;
            this._eve_inquilinocontratoid_value = _eve_inquilinocontratoid_value;
            this.eve_fechaliquidacioncontrato = eve_fechaliquidacioncontrato;
            this.eve_descripcionsociedad = eve_descripcionsociedad;
            this.eve_fechaentregallaves = eve_fechaentregallaves;
            this.eve_importegarantiaadicional = eve_importegarantiaadicional;
            this.eve_liquidaciongestionfueraportal = eve_liquidaciongestionfueraportal;
            this.timezoneruleversionnumber = timezoneruleversionnumber;
            this.eve_importetotaldeuda = eve_importetotaldeuda;
            this.eve_porcentajeanyo3 = eve_porcentajeanyo3;
            this.createdon = createdon;
            this.eve_cuentabancariaportal = eve_cuentabancariaportal;
            this.eve_numero_recibo_seguro_impago = eve_numero_recibo_seguro_impago;
            this.eve_bloqueado_promocion_sociedad = eve_bloqueado_promocion_sociedad;
            this.eve_repercusionibi = eve_repercusionibi;
            this.eve_titularcuentabancaria = eve_titularcuentabancaria;
            this.eve_tiporevision = eve_tiporevision;
            this.eve_eve_importe_garantia_importe = eve_eve_importe_garantia_importe;
            this.eve_enviar_recordatorio_seguro_hogar = eve_enviar_recordatorio_seguro_hogar;
            this.versionnumber = versionnumber;
            this.eve_name = eve_name;
            this._eve_promocincontrato_id_value = _eve_promocincontrato_id_value;
            this.eve_mandatofirmado = eve_mandatofirmado;
            this.eve_fechaadquisicion = eve_fechaadquisicion;
            this.eve_organismofianzas = eve_organismofianzas;
            this.eve_importefianza = eve_importefianza;
            this.eve_tipodeposito = eve_tipodeposito;
            this.eve_duracionprorrogas = eve_duracionprorrogas;
            this._modifiedby_value = _modifiedby_value;
            this._createdby_value = _createdby_value;
            this.eve_contratoid = eve_contratoid;
            this.eve_indicevigente = eve_indicevigente;
            this.eve_numeromaximoprorrogas = eve_numeromaximoprorrogas;
            this.eve_causafinalizacion = eve_causafinalizacion;
            this.eve_tipo_poliza_seguro_impago = eve_tipo_poliza_seguro_impago;
            this.eve_renovacionautomatica = eve_renovacionautomatica;
            this.eve_contratoretenido = eve_contratoretenido;
            this.statecode = statecode;
            this._owningbusinessunit_value = _owningbusinessunit_value;
            this.eve_porcentajeanyo1 = eve_porcentajeanyo1;
            this.eve_deudatotal = eve_deudatotal;
            this.eve_importeproximarenta = eve_importeproximarenta;
            this.eve_porcentajeanyo2 = eve_porcentajeanyo2;
            this.eve_idcontratoprinex = eve_idcontratoprinex;
            this.eve_diavencimientorecibos = eve_diavencimientorecibos;
            this.eve_recibospendientesvencimiento = eve_recibospendientesvencimiento;
            this.eve_codigosociedad = eve_codigosociedad;
            this.eve_codigopromocion = eve_codigopromocion;
            this.eve_mandatovigor = eve_mandatovigor;
            this.eve_importe_rentas_impagadas = eve_importe_rentas_impagadas;
            this.eve_inquilino_recuperacion_3 = eve_inquilino_recuperacion_3;
            this.eve_descripcion_seguro_impago = eve_descripcion_seguro_impago;
            this.importsequencenumber = importsequencenumber;
            this.utcconversiontimezonecode = utcconversiontimezonecode;
            this.eve_numero_referencia_aval_bancario = eve_numero_referencia_aval_bancario;
            this.overriddencreatedon = overriddencreatedon;
            this.eve_fecharepercusionhasta = eve_fecharepercusionhasta;
            this.eve_importe_deuda_bonificacion_nuevo = eve_importe_deuda_bonificacion_nuevo;
            this.eve_motivo_finalizacion_contrato = eve_motivo_finalizacion_contrato;
            this.eve_comentarios_motivo_finalizacion = eve_comentarios_motivo_finalizacion;
            this.eve_motivoretencion = eve_motivoretencion;
            this.eve_observacionescontrato = eve_observacionescontrato;
            this.eve_importe_deuda_bonificacion = eve_importe_deuda_bonificacion;
            this.eve_inquilino_recuperacion_1 = eve_inquilino_recuperacion_1;
            this.eve_fechabajagas = eve_fechabajagas;
            this._eve_codigo_primer_contrato_id_value = _eve_codigo_primer_contrato_id_value;
            this.eve_renovacionvivienda = eve_renovacionvivienda;
            this.eve_fecha_caducidad_aval_bancario = eve_fecha_caducidad_aval_bancario;
            this.eve_comentarios_estado_recuperacion = eve_comentarios_estado_recuperacion;
            this.eve_fecha_inicio_vigencia_aval_bancario = eve_fecha_inicio_vigencia_aval_bancario;
            this.eve_inquilino_recuperacion_2 = eve_inquilino_recuperacion_2;
            this.eve_observacionesavalista = eve_observacionesavalista;
            this.eve_fecha_registro_deuda = eve_fecha_registro_deuda;
            this.eve_otros_motivos_ii = eve_otros_motivos_ii;
            this.eve_fecha_contacto = eve_fecha_contacto;
            this.processid = processid;
            this.eve_referencia_seguro_impago = eve_referencia_seguro_impago;
            this.eve_motivoresolucion = eve_motivoresolucion;
            this.traversedpath = traversedpath;
            this.eve_origen_finalizacion_contrato = eve_origen_finalizacion_contrato;
            this.eve_estado_recuperacion = eve_estado_recuperacion;
            this.eve_codigoetapadevolucionfianza_anterior = eve_codigoetapadevolucionfianza_anterior;
            this.eve_fechaproximarevisionrentaportal = eve_fechaproximarevisionrentaportal;
            this._owningteam_value = _owningteam_value;
            this.eve_fecha_comunicacion_bonificacion = eve_fecha_comunicacion_bonificacion;
            this.eve_fecha_inicio_seguro_impago = eve_fecha_inicio_seguro_impago;
            this.eve_id_inmueble_principal_prinex = eve_id_inmueble_principal_prinex;
            this.eve_tipogarantiaadicional = eve_tipogarantiaadicional;
            this._modifiedonbehalfby_value = _modifiedonbehalfby_value;
            this.eve_descripcionactualetapadevolucionfianza = eve_descripcionactualetapadevolucionfianza;
            this.eve_proveedor_seguro_impago = eve_proveedor_seguro_impago;
            this.eve_fechaproximarevisionrenta = eve_fechaproximarevisionrenta;
            this.eve_operacioncontratoduplicado = eve_operacioncontratoduplicado;
            this._createdonbehalfby_value = _createdonbehalfby_value;
            this.eve_cierre_expediente = eve_cierre_expediente;
            this.eve_fecha_fin_seguro_impago = eve_fecha_fin_seguro_impago;
            this.eve_envio_comunicacion_bonificacion = eve_envio_comunicacion_bonificacion;
            this.eve_tipo_envio_renovacion = eve_tipo_envio_renovacion;
            this.eve_fecha_finalizacion_recuperacion = eve_fecha_finalizacion_recuperacion;
            this.eve_fecha_envio_recordatorio_seguro = eve_fecha_envio_recordatorio_seguro;
            this.eve_codigoetapadevolucionfianza = eve_codigoetapadevolucionfianza;
            this.eve_clasificacion_cambio_vivienda = eve_clasificacion_cambio_vivienda;
            this.eve_fechabajaagua = eve_fechabajaagua;
            this.eve_email_recuperacion_inquilino_3 = eve_email_recuperacion_inquilino_3;
            this.eve_eve_entidad_aval_bancario = eve_eve_entidad_aval_bancario;
            this.eve_motivo_finalizacion_contrato_adicional = eve_motivo_finalizacion_contrato_adicional;
            this.eve_fecharepercusiondesde = eve_fecharepercusiondesde;
            this.eve_codigo_inmueble_principal = eve_codigo_inmueble_principal;
            this.eve_motivo_no_recuperacion_1 = eve_motivo_no_recuperacion_1;
            this.eve_tipodocumentoavalista = eve_tipodocumentoavalista;
            this.eve_tipo_comunicacion_bonificacion = eve_tipo_comunicacion_bonificacion;
            this.eve_fecha_cambio_motivo_1 = eve_fecha_cambio_motivo_1;
            this.eve_fecha_cambio_motivo_2 = eve_fecha_cambio_motivo_2;
            this.eve_otros_motivos_i = eve_otros_motivos_i;
            this.eve_inquilinoprincipal = eve_inquilinoprincipal;
            this.eve_email_recuperacion_inquilino_2 = eve_email_recuperacion_inquilino_2;
            this.eve_fecha_minima_resolucion = eve_fecha_minima_resolucion;
            this.eve_apellidosavalista = eve_apellidosavalista;
            this.eve_telefono_recuperacion_1 = eve_telefono_recuperacion_1;
            this.eve_telefono_recuperacion_3 = eve_telefono_recuperacion_3;
            this.eve_telefono_recuperacion_2 = eve_telefono_recuperacion_2;
            this.eve_fecha_envio_comunicacion = eve_fecha_envio_comunicacion;
            this.eve_estado_recuperacion_adicional = eve_estado_recuperacion_adicional;
            this.eve_fecha_liquidacion_bonificacion = eve_fecha_liquidacion_bonificacion;
            this.eve_comentario_aval_bancario = eve_comentario_aval_bancario;
            this.eve_inmuebleprincipaloperacion = eve_inmuebleprincipaloperacion;
            this._stageid_value = _stageid_value;
            this.eve_comentario_interno_seguros = eve_comentario_interno_seguros;
            this.eve_equipo_encargado_recuperacion = eve_equipo_encargado_recuperacion;
            this.eve_rentamensualportal = eve_rentamensualportal;
            this.eve_email_recuperacion_inquilino_1 = eve_email_recuperacion_inquilino_1;
            this.eve_documentoavalista = eve_documentoavalista;
            this.eve_motivo_no_recuperacion_2 = eve_motivo_no_recuperacion_2;
            this.eve_fechabajaluz = eve_fechabajaluz;
            this.eve_reasignacion = eve_reasignacion;
            this.eve_observaciones_intern_bonificacion = eve_observaciones_intern_bonificacion;
            this.eve_formadecobro = eve_formadecobro;
            this.eve_nombreavalista = eve_nombreavalista;
            this._eve_codigo_contrato_renovado_id_value = _eve_codigo_contrato_renovado_id_value;
            this.eve_descripcionetapadevolucionfianza = eve_descripcionetapadevolucionfianza;
            this.eve_motivo_finalizacion_contrato_anterior_2 = eve_motivo_finalizacion_contrato_anterior_2;
            this.eve_motivo_finalizacion_contrato_anterior_1 = eve_motivo_finalizacion_contrato_anterior_1;
            this._eve_inquilinoprincipalliquidacion_value = _eve_inquilinoprincipalliquidacion_value;
            this.eve_fechaenviomail = eve_fechaenviomail;
            this.eve_fecha_registro_garantia = eve_fecha_registro_garantia;
        }

        public Dynamics_Contratos Convertir2BD()
        {
            Dynamics_Contratos dynamics_Contrato = new Dynamics_Contratos();
            dynamics_Contrato.odataetag = odataEtag;
            dynamics_Contrato.eve_estadocontrato = eve_estadocontrato;
            dynamics_Contrato.eve_renovarrentasipcnegativo = eve_renovarrentasipcnegativo;
            dynamics_Contrato.eve_idsociedadprinex = eve_idsociedadprinex;
            dynamics_Contrato.eve_fecha_inicio_primer_contrato = eve_fecha_inicio_primer_contrato;
            dynamics_Contrato.eve_importe_seguro_impago = eve_importe_seguro_impago;
            dynamics_Contrato.eve_tipopromocion = eve_tipopromocion;
            dynamics_Contrato.eve_sumatorioporcentajeparticipacion = eve_sumatorioporcentajeparticipacion;
            dynamics_Contrato.eve_cuentabancaria = eve_cuentabancaria;
            dynamics_Contrato.eve_fechafirmamandato = eve_fechafirmamandato;
            dynamics_Contrato.statuscode = statuscode;
            dynamics_Contrato.eve_rentamensual = eve_rentamensual;
            dynamics_Contrato.eve_fechaefectoentregallaves = eve_fechaefectoentregallaves;
            dynamics_Contrato.eve_fechainicio = eve_fechainicio;
            dynamics_Contrato.eve_tipoadquisicion = eve_tipoadquisicion;
            dynamics_Contrato.eve_cuentabancariaingreso = eve_cuentabancariaingreso;
            dynamics_Contrato.modifiedon = modifiedon;
            dynamics_Contrato.eve_numeroconciertofianzas = eve_numeroconciertofianzas;
            dynamics_Contrato.C_owninguser_value = _owninguser_value;
            dynamics_Contrato.eve_sumatorioporcentajeparticipacion_date = eve_sumatorioporcentajeparticipacion_date;
            dynamics_Contrato.eve_visualizacionportal = eve_visualizacionportal;
            dynamics_Contrato.eve_tipofinalizacion = eve_tipofinalizacion;
            dynamics_Contrato.eve_operacion = eve_operacion;
            dynamics_Contrato.eve_sumatorioporcentajeparticipacion_state = eve_sumatorioporcentajeparticipacion_state;
            dynamics_Contrato.eve_descripcionpromocion = eve_descripcionpromocion;
            dynamics_Contrato.eve_mandato = eve_mandato;
            dynamics_Contrato.eve_fechafin = eve_fechafin;
            dynamics_Contrato.C_ownerid_value = _ownerid_value;
            dynamics_Contrato.eve_contratoopcioncompra = eve_contratoopcioncompra;
            dynamics_Contrato.eve_prorrogasacumuladas = eve_prorrogasacumuladas;
            dynamics_Contrato.C_eve_sociedadcontrato_id_value = _eve_sociedadcontrato_id_value;
            dynamics_Contrato.eve_fechamodificacionregistro = eve_fechamodificacionregistro;
            dynamics_Contrato.eve_renovaciontacita = eve_renovaciontacita;
            dynamics_Contrato.eve_actualizarfianza = eve_actualizarfianza;
            dynamics_Contrato.C_eve_inquilinocontratoid_value = _eve_inquilinocontratoid_value;
            dynamics_Contrato.eve_fechaliquidacioncontrato = eve_fechaliquidacioncontrato;
            dynamics_Contrato.eve_descripcionsociedad = eve_descripcionsociedad;
            dynamics_Contrato.eve_fechaentregallaves = eve_fechaentregallaves;
            dynamics_Contrato.eve_importegarantiaadicional = eve_importegarantiaadicional;
            dynamics_Contrato.eve_liquidaciongestionfueraportal = eve_liquidaciongestionfueraportal;
            dynamics_Contrato.timezoneruleversionnumber = timezoneruleversionnumber;
            dynamics_Contrato.eve_importetotaldeuda = eve_importetotaldeuda;
            dynamics_Contrato.eve_porcentajeanyo3 = eve_porcentajeanyo3;
            dynamics_Contrato.createdon = createdon;
            dynamics_Contrato.eve_cuentabancariaportal = eve_cuentabancariaportal;
            dynamics_Contrato.eve_numero_recibo_seguro_impago = eve_numero_recibo_seguro_impago;
            dynamics_Contrato.eve_bloqueado_promocion_sociedad = eve_bloqueado_promocion_sociedad;
            dynamics_Contrato.eve_repercusionibi = eve_repercusionibi;
            dynamics_Contrato.eve_titularcuentabancaria = eve_titularcuentabancaria;
            dynamics_Contrato.eve_tiporevision = eve_tiporevision;
            dynamics_Contrato.eve_eve_importe_garantia_importe = eve_eve_importe_garantia_importe;
            dynamics_Contrato.eve_enviar_recordatorio_seguro_hogar = eve_enviar_recordatorio_seguro_hogar;
            dynamics_Contrato.versionnumber = versionnumber;
            dynamics_Contrato.eve_name = eve_name;
            dynamics_Contrato.C_eve_promocincontrato_id_value = _eve_promocincontrato_id_value;
            dynamics_Contrato.eve_mandatofirmado = eve_mandatofirmado;
            dynamics_Contrato.eve_fechaadquisicion = eve_fechaadquisicion;
            dynamics_Contrato.eve_organismofianzas = eve_organismofianzas;
            dynamics_Contrato.eve_importefianza = eve_importefianza;
            dynamics_Contrato.eve_tipodeposito = eve_tipodeposito;
            dynamics_Contrato.eve_duracionprorrogas = eve_duracionprorrogas;
            dynamics_Contrato.C_modifiedby_value = _modifiedby_value;
            dynamics_Contrato.C_createdby_value = _createdby_value;
            dynamics_Contrato.eve_contratoid = eve_contratoid;
            dynamics_Contrato.eve_indicevigente = eve_indicevigente;
            dynamics_Contrato.eve_numeromaximoprorrogas = eve_numeromaximoprorrogas;
            dynamics_Contrato.eve_causafinalizacion = eve_causafinalizacion;
            dynamics_Contrato.eve_tipo_poliza_seguro_impago = eve_tipo_poliza_seguro_impago;
            dynamics_Contrato.eve_renovacionautomatica = eve_renovacionautomatica;
            dynamics_Contrato.eve_contratoretenido = eve_contratoretenido;
            dynamics_Contrato.statecode = statecode;
            dynamics_Contrato.C_owningbusinessunit_value = _owningbusinessunit_value;
            dynamics_Contrato.eve_porcentajeanyo1 = eve_porcentajeanyo1;
            dynamics_Contrato.eve_deudatotal = eve_deudatotal;
            dynamics_Contrato.eve_importeproximarenta = eve_importeproximarenta;
            dynamics_Contrato.eve_porcentajeanyo2 = eve_porcentajeanyo2;
            dynamics_Contrato.eve_idcontratoprinex = eve_idcontratoprinex;
            dynamics_Contrato.eve_diavencimientorecibos = eve_diavencimientorecibos;
            dynamics_Contrato.eve_recibospendientesvencimiento = eve_recibospendientesvencimiento;
            dynamics_Contrato.eve_codigosociedad = eve_codigosociedad;
            dynamics_Contrato.eve_codigopromocion = eve_codigopromocion;
            dynamics_Contrato.eve_mandatovigor = eve_mandatovigor;
            dynamics_Contrato.eve_importe_rentas_impagadas = eve_importe_rentas_impagadas;
            dynamics_Contrato.eve_inquilino_recuperacion_3 = eve_inquilino_recuperacion_3;
            dynamics_Contrato.eve_descripcion_seguro_impago = eve_descripcion_seguro_impago;
            dynamics_Contrato.importsequencenumber = importsequencenumber;
            dynamics_Contrato.utcconversiontimezonecode = utcconversiontimezonecode;
            dynamics_Contrato.eve_numero_referencia_aval_bancario = eve_numero_referencia_aval_bancario;
            dynamics_Contrato.overriddencreatedon = overriddencreatedon;
            dynamics_Contrato.eve_fecharepercusionhasta = eve_fecharepercusionhasta;
            dynamics_Contrato.eve_importe_deuda_bonificacion_nuevo = eve_importe_deuda_bonificacion_nuevo;
            dynamics_Contrato.eve_motivo_finalizacion_contrato = eve_motivo_finalizacion_contrato;
            dynamics_Contrato.eve_comentarios_motivo_finalizacion = eve_comentarios_motivo_finalizacion;
            dynamics_Contrato.eve_motivoretencion = eve_motivoretencion;
            dynamics_Contrato.eve_observacionescontrato = eve_observacionescontrato;
            dynamics_Contrato.eve_importe_deuda_bonificacion = eve_importe_deuda_bonificacion;
            dynamics_Contrato.eve_inquilino_recuperacion_1 = eve_inquilino_recuperacion_1;
            dynamics_Contrato.eve_fechabajagas = eve_fechabajagas;
            dynamics_Contrato.C_eve_codigo_primer_contrato_id_value = _eve_codigo_primer_contrato_id_value;
            dynamics_Contrato.eve_renovacionvivienda = eve_renovacionvivienda;
            dynamics_Contrato.eve_fecha_caducidad_aval_bancario = eve_fecha_caducidad_aval_bancario;
            dynamics_Contrato.eve_comentarios_estado_recuperacion = eve_comentarios_estado_recuperacion;
            dynamics_Contrato.eve_fecha_inicio_vigencia_aval_bancario = eve_fecha_inicio_vigencia_aval_bancario;
            dynamics_Contrato.eve_inquilino_recuperacion_2 = eve_inquilino_recuperacion_2;
            dynamics_Contrato.eve_observacionesavalista = eve_observacionesavalista;
            dynamics_Contrato.eve_fecha_registro_deuda = eve_fecha_registro_deuda;
            dynamics_Contrato.eve_otros_motivos_ii = eve_otros_motivos_ii;
            dynamics_Contrato.eve_fecha_contacto = eve_fecha_contacto;
            dynamics_Contrato.processid = processid;
            dynamics_Contrato.eve_referencia_seguro_impago = eve_referencia_seguro_impago;
            dynamics_Contrato.eve_motivoresolucion = eve_motivoresolucion;
            dynamics_Contrato.traversedpath = traversedpath;
            dynamics_Contrato.eve_origen_finalizacion_contrato = eve_origen_finalizacion_contrato;
            dynamics_Contrato.eve_estado_recuperacion = eve_estado_recuperacion;
            dynamics_Contrato.eve_codigoetapadevolucionfianza_anterior = eve_codigoetapadevolucionfianza_anterior;
            dynamics_Contrato.eve_fechaproximarevisionrentaportal = eve_fechaproximarevisionrentaportal;
            dynamics_Contrato.C_owningteam_value = _owningteam_value;
            dynamics_Contrato.eve_fecha_comunicacion_bonificacion = eve_fecha_comunicacion_bonificacion;
            dynamics_Contrato.eve_fecha_inicio_seguro_impago = eve_fecha_inicio_seguro_impago;
            dynamics_Contrato.eve_id_inmueble_principal_prinex = eve_id_inmueble_principal_prinex;
            dynamics_Contrato.eve_tipogarantiaadicional = eve_tipogarantiaadicional;
            dynamics_Contrato.C_modifiedonbehalfby_value = _modifiedonbehalfby_value;
            dynamics_Contrato.eve_descripcionactualetapadevolucionfianza = eve_descripcionactualetapadevolucionfianza;
            dynamics_Contrato.eve_proveedor_seguro_impago = eve_proveedor_seguro_impago;
            dynamics_Contrato.eve_fechaproximarevisionrenta = eve_fechaproximarevisionrenta;
            dynamics_Contrato.eve_operacioncontratoduplicado = eve_operacioncontratoduplicado;
            dynamics_Contrato.C_createdonbehalfby_value = _createdonbehalfby_value;
            dynamics_Contrato.eve_cierre_expediente = eve_cierre_expediente;
            dynamics_Contrato.eve_fecha_fin_seguro_impago = eve_fecha_fin_seguro_impago;
            dynamics_Contrato.eve_envio_comunicacion_bonificacion = eve_envio_comunicacion_bonificacion;
            dynamics_Contrato.eve_tipo_envio_renovacion = eve_tipo_envio_renovacion;
            dynamics_Contrato.eve_fecha_finalizacion_recuperacion = eve_fecha_finalizacion_recuperacion;
            dynamics_Contrato.eve_fecha_envio_recordatorio_seguro = eve_fecha_envio_recordatorio_seguro;
            dynamics_Contrato.eve_codigoetapadevolucionfianza = eve_codigoetapadevolucionfianza;
            dynamics_Contrato.eve_clasificacion_cambio_vivienda = eve_clasificacion_cambio_vivienda;
            dynamics_Contrato.eve_fechabajaagua = eve_fechabajaagua;
            dynamics_Contrato.eve_email_recuperacion_inquilino_3 = eve_email_recuperacion_inquilino_3;
            dynamics_Contrato.eve_eve_entidad_aval_bancario = eve_eve_entidad_aval_bancario;
            dynamics_Contrato.eve_motivo_finalizacion_contrato_adicional = eve_motivo_finalizacion_contrato_adicional;
            dynamics_Contrato.eve_fecharepercusiondesde = eve_fecharepercusiondesde;
            dynamics_Contrato.eve_codigo_inmueble_principal = eve_codigo_inmueble_principal;
            dynamics_Contrato.eve_motivo_no_recuperacion_1 = eve_motivo_no_recuperacion_1;
            dynamics_Contrato.eve_tipodocumentoavalista = eve_tipodocumentoavalista;
            dynamics_Contrato.eve_tipo_comunicacion_bonificacion = eve_tipo_comunicacion_bonificacion;
            dynamics_Contrato.eve_fecha_cambio_motivo_1 = eve_fecha_cambio_motivo_1;
            dynamics_Contrato.eve_fecha_cambio_motivo_2 = eve_fecha_cambio_motivo_2;
            dynamics_Contrato.eve_otros_motivos_i = eve_otros_motivos_i;
            dynamics_Contrato.eve_inquilinoprincipal = eve_inquilinoprincipal;
            dynamics_Contrato.eve_email_recuperacion_inquilino_2 = eve_email_recuperacion_inquilino_2;
            dynamics_Contrato.eve_fecha_minima_resolucion = eve_fecha_minima_resolucion;
            dynamics_Contrato.eve_apellidosavalista = eve_apellidosavalista;
            dynamics_Contrato.eve_telefono_recuperacion_1 = eve_telefono_recuperacion_1;
            dynamics_Contrato.eve_telefono_recuperacion_3 = eve_telefono_recuperacion_3;
            dynamics_Contrato.eve_telefono_recuperacion_2 = eve_telefono_recuperacion_2;
            dynamics_Contrato.eve_fecha_envio_comunicacion = eve_fecha_envio_comunicacion;
            dynamics_Contrato.eve_estado_recuperacion_adicional = eve_estado_recuperacion_adicional;
            dynamics_Contrato.eve_fecha_liquidacion_bonificacion = eve_fecha_liquidacion_bonificacion;
            dynamics_Contrato.eve_comentario_aval_bancario = eve_comentario_aval_bancario;
            dynamics_Contrato.eve_inmuebleprincipaloperacion = eve_inmuebleprincipaloperacion;
            dynamics_Contrato.C_stageid_value = _stageid_value;
            dynamics_Contrato.eve_comentario_interno_seguros = eve_comentario_interno_seguros;
            dynamics_Contrato.eve_equipo_encargado_recuperacion = eve_equipo_encargado_recuperacion;
            dynamics_Contrato.eve_rentamensualportal = eve_rentamensualportal;
            dynamics_Contrato.eve_email_recuperacion_inquilino_1 = eve_email_recuperacion_inquilino_1;
            dynamics_Contrato.eve_documentoavalista = eve_documentoavalista;
            dynamics_Contrato.eve_motivo_no_recuperacion_2 = eve_motivo_no_recuperacion_2;
            dynamics_Contrato.eve_fechabajaluz = eve_fechabajaluz;
            dynamics_Contrato.eve_reasignacion = eve_reasignacion;
            dynamics_Contrato.eve_observaciones_intern_bonificacion = eve_observaciones_intern_bonificacion;
            dynamics_Contrato.eve_formadecobro = eve_formadecobro;
            dynamics_Contrato.eve_nombreavalista = eve_nombreavalista;
            dynamics_Contrato.C_eve_codigo_contrato_renovado_id_value = _eve_codigo_contrato_renovado_id_value;
            dynamics_Contrato.eve_descripcionetapadevolucionfianza = eve_descripcionetapadevolucionfianza;
            dynamics_Contrato.eve_motivo_finalizacion_contrato_anterior_2 = eve_motivo_finalizacion_contrato_anterior_2;
            dynamics_Contrato.eve_motivo_finalizacion_contrato_anterior_1 = eve_motivo_finalizacion_contrato_anterior_1;
            dynamics_Contrato.C_eve_inquilinoprincipalliquidacion_value = _eve_inquilinoprincipalliquidacion_value;
            dynamics_Contrato.eve_fechaenviomail = eve_fechaenviomail;
            dynamics_Contrato.eve_fecha_registro_garantia = eve_fecha_registro_garantia;

            return dynamics_Contrato;
        }
    }
}

