﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ModeloDatos;

namespace ClienteAPITesta
{
    public class Sociedad
    {
        public Sociedad()
        {
        }

        [JsonProperty("@odata.etag")]
        public string OdataEtag { get; set; }
        public string eve_sociedadid { get; set; }
        public string eve_descripcionsociedad { get; set; }
        public string eve_puerta { get; set; }
        public string eve_visualizacion_portal_anterior { get; set; }
        public string _owningbusinessunit_value { get; set; }
        public string eve_cif { get; set; }
        public string eve_codigopostal { get; set; }
        public Nullable<System.DateTime> eve_fechamodificacionregistro { get; set; }
        public string eve_idsociedadprinex { get; set; }
        public Nullable<System.DateTime> createdon { get; set; }
        public string statecode { get; set; }
        public string eve_provincia { get; set; }
        public string eve_siglas { get; set; }
        public string _ownerid_value { get; set; }
        public string eve_telefono { get; set; }
        public Nullable<System.DateTime> modifiedon { get; set; }
        public string eve_numero { get; set; }
        public string eve_escalera { get; set; }
        public string versionnumber { get; set; }
        public string eve_piso { get; set; }
        public string eve_poblacion { get; set; }
        public string timezoneruleversionnumber { get; set; }
        public string eve_registromercantil { get; set; }
        public string statuscode { get; set; }
        public string _modifiedby_value { get; set; }
        public string eve_nombreviapublica { get; set; }
        public string eve_nombre { get; set; }
        public string _createdby_value { get; set; }
        public string eve_ms_flow { get; set; }
        public string _owninguser_value { get; set; }
        public string eve_activar_restriccion_visualizacion_portal { get; set; }
        public string eve_visualizacion_portal { get; set; }
        public string _eve_remitenteemailpagos_value { get; set; }
        public string eve_logo_sociedad { get; set; }
        public string _eve_remitenteemailgenerico_value { get; set; }
        public string eve_avisos_legales { get; set; }
        public string eve_notaria { get; set; }
        public string eve_enlace_portal_inquilino { get; set; }
        public string eve_telefono_call_center { get; set; }
        public string eve_email_renovaciones { get; set; }
        public Nullable<System.DateTime> eve_fecha_hora_bloqueo { get; set; }
        public string eve_url_logo { get; set; }
        public string eve_email_otros { get; set; }
        public string eve_restringir_envio_notificaciones_impagados { get; set; }
        public string _eve_remitenterenovaciones_value { get; set; }
        public string _modifiedonbehalfby_value { get; set; }
        public string eve_firma_responsable { get; set; }
        public Nullable<System.DateTime> eve_nomodificarfechademodificacin { get; set; }
        public string importsequencenumber { get; set; }
        public string utcconversiontimezonecode { get; set; }
        public string _createdonbehalfby_value { get; set; }
        public string eve_email_pagos_renovaciones { get; set; }
        public string _owningteam_value { get; set; }
        public string eve_direccion { get; set; }
        public string eve_sello { get; set; }
        public string eve_urlfirmaincrementorenta { get; set; }
        public string _eve_configuraciontpvid_value { get; set; }
        public string overriddencreatedon { get; set; }
        public string eve_responsableincrementorenta { get; set; }
        public string eve_nomodificarsumadecomprobacindefila { get; set; }
        public string eve_fax { get; set; }
        public Nullable<System.DateTime> eve_fecha_actualizacion_cobros { get; set; }
        public string eve_nomodificaridsociedadcrm { get; set; }
        public string _eve_configuraciontpv_reserva_value { get; set; }
        public Dynamics_Sociedades Convertir2BD() 
        {
            Dynamics_Sociedades dynamics_Sociedades = new Dynamics_Sociedades();
            dynamics_Sociedades.OdataEtag = OdataEtag;
            dynamics_Sociedades.eve_sociedadid = eve_sociedadid;
            dynamics_Sociedades.eve_descripcionsociedad = eve_descripcionsociedad;
            dynamics_Sociedades.eve_puerta = eve_puerta;
            dynamics_Sociedades.eve_visualizacion_portal_anterior = eve_visualizacion_portal_anterior;
            dynamics_Sociedades.C_owningbusinessunit_value = _owningbusinessunit_value;
            dynamics_Sociedades.eve_cif = eve_cif;
            dynamics_Sociedades.eve_codigopostal = eve_codigopostal;
            dynamics_Sociedades.eve_fechamodificacionregistro = eve_fechamodificacionregistro;
            dynamics_Sociedades.eve_idsociedadprinex = eve_idsociedadprinex;
            dynamics_Sociedades.createdon = createdon;
            dynamics_Sociedades.statecode = statecode;
            dynamics_Sociedades.eve_provincia = eve_provincia;
            dynamics_Sociedades.eve_siglas = eve_siglas;
            dynamics_Sociedades.C_ownerid_value = _ownerid_value;
            dynamics_Sociedades.eve_telefono = eve_telefono;
            dynamics_Sociedades.modifiedon = modifiedon;
            dynamics_Sociedades.eve_numero = eve_numero;
            dynamics_Sociedades.eve_escalera = eve_escalera;
            dynamics_Sociedades.versionnumber = versionnumber;
            dynamics_Sociedades.eve_piso = eve_piso;
            dynamics_Sociedades.eve_poblacion = eve_poblacion;
            dynamics_Sociedades.timezoneruleversionnumber = timezoneruleversionnumber;
            dynamics_Sociedades.eve_registromercantil = eve_registromercantil;
            dynamics_Sociedades.statuscode = statuscode;
            dynamics_Sociedades.C_modifiedby_value = _modifiedby_value;
            dynamics_Sociedades.eve_nombreviapublica = eve_nombreviapublica;
            dynamics_Sociedades.eve_nombre = eve_nombre;
            dynamics_Sociedades.C_createdby_value = _createdby_value;
            dynamics_Sociedades.eve_ms_flow = eve_ms_flow;
            dynamics_Sociedades.C_owninguser_value = _owninguser_value;
            dynamics_Sociedades.eve_activar_restriccion_visualizacion_portal = eve_activar_restriccion_visualizacion_portal;
            dynamics_Sociedades.eve_visualizacion_portal = eve_visualizacion_portal;
            dynamics_Sociedades.C_eve_remitenteemailpagos_value = _eve_remitenteemailpagos_value;
            dynamics_Sociedades.eve_logo_sociedad = eve_logo_sociedad;
            dynamics_Sociedades.C_eve_remitenteemailgenerico_value = _eve_remitenteemailgenerico_value;
            dynamics_Sociedades.eve_avisos_legales = eve_avisos_legales;
            dynamics_Sociedades.eve_notaria = eve_notaria;
            dynamics_Sociedades.eve_enlace_portal_inquilino = eve_enlace_portal_inquilino;
            dynamics_Sociedades.eve_telefono_call_center = eve_telefono_call_center;
            dynamics_Sociedades.eve_email_renovaciones = eve_email_renovaciones;
            dynamics_Sociedades.eve_fecha_hora_bloqueo = eve_fecha_hora_bloqueo;
            dynamics_Sociedades.eve_url_logo = eve_url_logo;
            dynamics_Sociedades.eve_email_otros = eve_email_otros;
            dynamics_Sociedades.eve_restringir_envio_notificaciones_impagados = eve_restringir_envio_notificaciones_impagados;
            dynamics_Sociedades.C_eve_remitenterenovaciones_value = _eve_remitenterenovaciones_value;
            dynamics_Sociedades.C_modifiedonbehalfby_value = _modifiedonbehalfby_value;
            dynamics_Sociedades.eve_firma_responsable = eve_firma_responsable;
            dynamics_Sociedades.eve_nomodificarfechademodificacin = eve_nomodificarfechademodificacin;
            dynamics_Sociedades.importsequencenumber = importsequencenumber;
            dynamics_Sociedades.utcconversiontimezonecode = utcconversiontimezonecode;
            dynamics_Sociedades.C_createdonbehalfby_value = _createdonbehalfby_value;
            dynamics_Sociedades.eve_email_pagos_renovaciones = eve_email_pagos_renovaciones;
            dynamics_Sociedades.C_owningteam_value = _owningteam_value;
            dynamics_Sociedades.eve_direccion = eve_direccion;
            dynamics_Sociedades.eve_sello = eve_sello;
            dynamics_Sociedades.eve_urlfirmaincrementorenta = eve_urlfirmaincrementorenta;
            dynamics_Sociedades.C_eve_configuraciontpvid_value = _eve_configuraciontpvid_value;
            dynamics_Sociedades.overriddencreatedon = overriddencreatedon;
            dynamics_Sociedades.eve_responsableincrementorenta = eve_responsableincrementorenta;
            dynamics_Sociedades.eve_nomodificarsumadecomprobacindefila = eve_nomodificarsumadecomprobacindefila;
            dynamics_Sociedades.eve_fax = eve_fax;
            dynamics_Sociedades.eve_fecha_actualizacion_cobros = eve_fecha_actualizacion_cobros;
            dynamics_Sociedades.eve_nomodificaridsociedadcrm = eve_nomodificaridsociedadcrm;
            dynamics_Sociedades.C_eve_configuraciontpv_reserva_value = _eve_configuraciontpv_reserva_value;
            return dynamics_Sociedades;
        }

    }

}
