﻿using System;
using Newtonsoft.Json;
using ModeloDatos;

namespace ClienteAPITesta
{
    public class Participacion_Contrato
    {
        [JsonProperty("@odata.etag")]
        public string OdataEtag { get; set; }
        public string statecode { get; set; }
        public string _eve_cuentaparticipacioncontratoid_value { get; set; }
        public string eve_apellidos { get; set; }
        public string eve_fechamodificacionregistro { get; set; }
        public string eve_fechaaltaparticipante { get; set; }
        public string createdon { get; set; }
        public string eve_idcontratoprinex { get; set; }
        public string eve_porcentajeparticipaciontesta { get; set; }
        public string eve_visualizacionportal { get; set; }
        public string _ownerid_value { get; set; }
        public string eve_codigoinquilino { get; set; }
        public string modifiedon { get; set; }
        public string versionnumber { get; set; }
        public string timezoneruleversionnumber { get; set; }
        public string eve_idinquilinoprinex { get; set; }
        public string _modifiedby_value { get; set; }
        public string eve_telefono3 { get; set; }
        public string eve_nombre { get; set; }
        public string eve_nombreinquilino { get; set; }
        public string eve_idparticipacioncontratoprinex { get; set; }
        public string _eve_contratoparticipacioncontrato_id_value { get; set; }
        public string eve_nif_nie { get; set; }
        public string _createdby_value { get; set; }
        public string eve_participacioncontratoid { get; set; }
        public string _owningbusinessunit_value { get; set; }
        public string _owninguser_value { get; set; }
        public string eve_codigocontrato { get; set; }
        public string eve_nombreapellidos { get; set; }
        public string overriddencreatedon { get; set; }
        public string importsequencenumber { get; set; }
        public string eve_fechabajaparticipante { get; set; }
        public string _eve_contratoparticipacioncontratoid_value { get; set; }
        public string _modifiedonbehalfby_value { get; set; }
        public string eve_email { get; set; }
        public string utcconversiontimezonecode { get; set; }
        public string eve_porcentajeparticipacion { get; set; }
        public string eve_telefono1 { get; set; }
        public string _createdonbehalfby_value { get; set; }
        public string eve_telefono2 { get; set; }
        public string _eve_contactoparticipacioncontratoid_value { get; set; }
        public string _owningteam_value { get; set; }
        public Participacion_Contrato() { }

        public Dynamics_ParticipacionesContratos Convertir2BD()
        {
            Dynamics_ParticipacionesContratos dynamics_ParticipacionesContratos = new Dynamics_ParticipacionesContratos();
            dynamics_ParticipacionesContratos.OdataEtag = OdataEtag;
            dynamics_ParticipacionesContratos.statecode = statecode;
            dynamics_ParticipacionesContratos.C_eve_cuentaparticipacioncontratoid_value = _eve_cuentaparticipacioncontratoid_value;
            dynamics_ParticipacionesContratos.eve_apellidos = eve_apellidos;
            dynamics_ParticipacionesContratos.eve_fechamodificacionregistro = eve_fechamodificacionregistro;
            dynamics_ParticipacionesContratos.eve_fechaaltaparticipante = eve_fechaaltaparticipante;
            dynamics_ParticipacionesContratos.createdon = createdon;
            dynamics_ParticipacionesContratos.eve_idcontratoprinex = eve_idcontratoprinex;
            dynamics_ParticipacionesContratos.eve_porcentajeparticipaciontesta = eve_porcentajeparticipaciontesta;
            dynamics_ParticipacionesContratos.eve_visualizacionportal = eve_visualizacionportal;
            dynamics_ParticipacionesContratos.C_ownerid_value = _ownerid_value;
            dynamics_ParticipacionesContratos.eve_codigoinquilino = eve_codigoinquilino;
            dynamics_ParticipacionesContratos.modifiedon = modifiedon;
            dynamics_ParticipacionesContratos.versionnumber = versionnumber;
            dynamics_ParticipacionesContratos.timezoneruleversionnumber = timezoneruleversionnumber;
            dynamics_ParticipacionesContratos.eve_idinquilinoprinex = eve_idinquilinoprinex;
            dynamics_ParticipacionesContratos.C_modifiedby_value = _modifiedby_value;
            dynamics_ParticipacionesContratos.eve_telefono3 = eve_telefono3;
            dynamics_ParticipacionesContratos.eve_nombre = eve_nombre;
            dynamics_ParticipacionesContratos.eve_nombreinquilino = eve_nombreinquilino;
            dynamics_ParticipacionesContratos.eve_idparticipacioncontratoprinex = eve_idparticipacioncontratoprinex;
            dynamics_ParticipacionesContratos.C_eve_contratoparticipacioncontrato_id_value = _eve_contratoparticipacioncontrato_id_value;
            dynamics_ParticipacionesContratos.eve_nif_nie = eve_nif_nie;
            dynamics_ParticipacionesContratos.C_createdby_value = _createdby_value;
            dynamics_ParticipacionesContratos.eve_participacioncontratoid = eve_participacioncontratoid;
            dynamics_ParticipacionesContratos.C_owningbusinessunit_value = _owningbusinessunit_value;
            dynamics_ParticipacionesContratos.C_owninguser_value = _owninguser_value;
            dynamics_ParticipacionesContratos.eve_codigocontrato = eve_codigocontrato;
            dynamics_ParticipacionesContratos.eve_nombreapellidos = eve_nombreapellidos;
            dynamics_ParticipacionesContratos.overriddencreatedon = overriddencreatedon;
            dynamics_ParticipacionesContratos.importsequencenumber = importsequencenumber;
            dynamics_ParticipacionesContratos.eve_fechabajaparticipante = eve_fechabajaparticipante;
            dynamics_ParticipacionesContratos.C_eve_contratoparticipacioncontratoid_value = _eve_contratoparticipacioncontratoid_value;
            dynamics_ParticipacionesContratos.C_modifiedonbehalfby_value = _modifiedonbehalfby_value;
            dynamics_ParticipacionesContratos.eve_email = eve_email;
            dynamics_ParticipacionesContratos.utcconversiontimezonecode = utcconversiontimezonecode;
            dynamics_ParticipacionesContratos.eve_porcentajeparticipacion = eve_porcentajeparticipacion;
            dynamics_ParticipacionesContratos.eve_telefono1 = eve_telefono1;
            dynamics_ParticipacionesContratos.C_createdonbehalfby_value = _createdonbehalfby_value;
            dynamics_ParticipacionesContratos.eve_telefono2 = eve_telefono2;
            dynamics_ParticipacionesContratos.C_eve_contactoparticipacioncontratoid_value = _eve_contactoparticipacioncontratoid_value;
            dynamics_ParticipacionesContratos.C_owningteam_value = _owningteam_value;
            return dynamics_ParticipacionesContratos;
        }
    }
}
