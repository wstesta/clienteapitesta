﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ModeloDatos;

namespace ClienteAPITesta
{
    public class Reserva
    {
        [JsonProperty("@odata.etag")]
        public string OdataEtag { get; set; }
        public string eve_importebonificaciontrastero1 { get; set; }
        public string eve_periodicidadibitrastero1 { get; set; }
        public string eve_garantiaadicionaltrastero1 { get; set; }
        public string eve_lleida_request_id { get; set; }
        public string eve_fecha_fin { get; set; }
        public string eve_dias_alta_suministro { get; set; }
        public string eve_empresa { get; set; }
        public string eve_fecha_alta_suministro { get; set; }
        public string _eve_pago_reserva_id_value { get; set; }
        public string eve_ibitrastero1 { get; set; }
        public string eve_nombretitular { get; set; }
        public string _owningbusinessunit_value { get; set; }
        public string eve_inquilinos_doc_subida { get; set; }
        public string _owninguser_value { get; set; }
        public string _eve_promocioninmueble4_value { get; set; }
        public string statecode { get; set; }
        public string eve_firmado { get; set; }
        public string eve_fecha_revision_asnef { get; set; }
        public string eve_fecha_inicio { get; set; }
        public string _ownerid_value { get; set; }
        public string eve_alta_reserva_prinex { get; set; }
        public string eve_alta_suministros { get; set; }
        public string eve_fecha_primera_entrega_doc_inquilino { get; set; }
        public string _eve_titular_principal_value { get; set; }
        public string eve_dias_primera_entrega_doc_inquilino { get; set; }
        public string eve_fecha_firma { get; set; }
        public string timezoneruleversionnumber { get; set; }
        public string statuscode { get; set; }
        public string createdon { get; set; }
        public string versionnumber { get; set; }
        public string eve_aprobado_por_tecnico { get; set; }
        public string eve_envio_notificacion_api { get; set; }
        public string eve_enlacerecuperacionreserva { get; set; }
        public string eve_fecha_aprobacion_reserva { get; set; }
        public string eve_name { get; set; }
        public string modifiedon { get; set; }
        public string eve_comunidadtrastero1 { get; set; }
        public string eve_campanya_empresas { get; set; }
        public string eve_fecha_aprobada_api { get; set; }
        public string eve_rentaanio3 { get; set; }
        public string eve_mesesfianzatrastero1 { get; set; }
        public string _eve_cotitular1_value { get; set; }
        public string _eve_cotitular2_value { get; set; }
        public string eve_mesesgarantiatrastero1 { get; set; }
        public string eve_estado_adicional { get; set; }
        public string _eve_trastero_no_anejo_1_id_value { get; set; }
        public string eve_reservaid { get; set; }
        public string _modifiedby_value { get; set; }
        public string eve_dias_gestion_reserva { get; set; }
        public string eve_importedelareservatrastero1 { get; set; }
        public string _createdby_value { get; set; }
        public string eve_periodicidadcomunidadtrastero1 { get; set; }
        public string eve_gdpr { get; set; }
        public string eve_dias_aprobada_tecnico { get; set; }
        public string eve_rentatrastero1 { get; set; }
        public string eve_comunicacionfinalizada { get; set; }
        public string eve_fecha_aprobada_tecnico { get; set; }
        public string eve_dias_revision_asnef { get; set; }
        public string eve_dias_aprobada_api { get; set; }
        public string _eve_contrato_id_value { get; set; }
        public string eve_operacionprinexgaraje1 { get; set; }
        public string eve_documentacion_recibida_titular_1 { get; set; }
        public string eve_comentarios_internos { get; set; }
        public string eve_tipoinquilinoprincipal { get; set; }
        public string eve_periodicidadcomunidadgaraje1 { get; set; }
        public string eve_idprinextitularprincipal { get; set; }
        public string eve_devolucion_reserva_extraordinaria { get; set; }
        public string eve_estado_documentacion_titular_3 { get; set; }
        public string eve_carencia_comunidad_inmuebleprincipal { get; set; }
        public string eve_marca_horno_microondas { get; set; }
        public string eve_carencia_ibi_trastero2 { get; set; }
        public string importsequencenumber { get; set; }
        public string eve_garantiaadicionalgaraje2 { get; set; }
        public string eve_reserva_contabilizada { get; set; }
        public string eve_enseres_frigorficoconcongelador { get; set; }
        public string eve_sumatorio_enseres { get; set; }
        public string eve_periodicidadibigaraje1 { get; set; }
        public string eve_meses_corriente_pago_inmueble_principal { get; set; }
        public string eve_documentacion_visada { get; set; }
        public string eve_enseres_campanaextractora { get; set; }
        public string eve_enseres_congelador { get; set; }
        public string eve_mesesgarantiagaraje2 { get; set; }
        public string eve_comentarios_internos_api { get; set; }
        public string eve_comentario_interno_seguros { get; set; }
        public string eve_mesesgarantiatrastero2 { get; set; }
        public string eve_meses_apli { get; set; }
        public string _eve_promocioninmueble2_value { get; set; }
        public string eve_agrupar_documentacion_total { get; set; }
        public string eve_black_friday { get; set; }
        public string eve_enseres_lavadora { get; set; }
        public string eve_comentario_bonificacion_recomendacion { get; set; }
        public string eve_carencia_ibi_garaje2 { get; set; }
        public string eve_importedelareservatrastero2 { get; set; }
        public string eve_idprinexcotitular2 { get; set; }
        public string eve_comercial_interno_asignado { get; set; }
        public string eve_importe_oc_trastero_1 { get; set; }
        public string eve_importebonificacioninmuebleprincipal { get; set; }
        public string eve_importe_oc_trastero_2 { get; set; }
        public string _eve_garaje_no_anejo_1_id_value { get; set; }
        public string eve_ibigaraje1 { get; set; }
        public string eve_importe_oc_inmueble_principal { get; set; }
        public string eve_mesesaplicaciongaraje2 { get; set; }
        public string eve_scoring3 { get; set; }
        public string eve_color_lavavajillas { get; set; }
        public string eve_fecha_modificacion_cambio_vivienda { get; set; }
        public string eve_reserva_procedente_cambio_vivienda { get; set; }
        public string eve_comunidadgaraje2 { get; set; }
        public string _eve_inmueble_prinex1_value { get; set; }
        public string eve_comentariocomercializadora10 { get; set; }
        public string eve_enseres_hornomicroondas { get; set; }
        public string eve_importebonificaciontrastero2 { get; set; }
        public string eve_cargaprinex2 { get; set; }
        public string eve_bonificacion_recomendacion_propietario { get; set; }
        public string eve_ivainmuebleprincipal { get; set; }
        public string eve_sujeto_permanencia_garaje_1 { get; set; }
        public string eve_enviar_recordatorio_seguro_hogar { get; set; }
        public string eve_sujeto_permanencia_garaje_2 { get; set; }
        public string _eve_garaje_no_anejo_2_id_value { get; set; }
        public string eve_codigo_promocion { get; set; }
        public string eve_ivagaraje1 { get; set; }
        public string eve_color_campana { get; set; }
        public string eve_meses_permanencia_inmueble_principal { get; set; }
        public string eve_enseres_induccion { get; set; }
        public string eve_hasta_fin_contrato_trastero_1 { get; set; }
        public string eve_hasta_fin_contrato_trastero_2 { get; set; }
        public string eve_mesesaplicacioninmuebleprincipal { get; set; }
        public string eve_operacionprinexgaraje2 { get; set; }
        public string eve_documentacion_pendiente_titular_1 { get; set; }
        public string eve_documentacion_pendiente_titular_3 { get; set; }
        public string eve_documentacion_pendiente_titular_2 { get; set; }
        public string eve_sujeto_a_impagos_garaje_1 { get; set; }
        public string eve_sujeto_a_impagos_garaje_2 { get; set; }
        public string eve_color_vitroceramica { get; set; }
        public string eve_enviar_contrato_reserva_firma { get; set; }
        public string eve_mesesfianzagaraje1 { get; set; }
        public string eve_periodicidadcomunidad { get; set; }
        public string eve_fecha_comentariofidere9 { get; set; }
        public string eve_fecha_comentariofidere8 { get; set; }
        public string eve_color_microondas { get; set; }
        public string eve_fecha_comentariofidere1 { get; set; }
        public string eve_marca_lavadora { get; set; }
        public string eve_fecha_comentariofidere3 { get; set; }
        public string eve_mesesfianza { get; set; }
        public string eve_fecha_comentariofidere2 { get; set; }
        public string eve_fecha_comentariofidere5 { get; set; }
        public string eve_fecha_comentariofidere4 { get; set; }
        public string eve_fecha_comentariofidere7 { get; set; }
        public string eve_fecha_comentariofidere6 { get; set; }
        public string eve_comentariocomercializadora8 { get; set; }
        public string eve_comentariocomercializadora9 { get; set; }
        public string eve_fuerza_seguridad { get; set; }
        public string eve_agrupar_documentacion { get; set; }
        public string eve_documentacion_visada_titular_3 { get; set; }
        public string eve_comentariocomercializadora1 { get; set; }
        public string eve_comentariocomercializadora2 { get; set; }
        public string eve_comentariocomercializadora3 { get; set; }
        public string eve_comentariocomercializadora4 { get; set; }
        public string eve_comentariocomercializadora5 { get; set; }
        public string eve_importedelareservainmuebleprincipal { get; set; }
        public string eve_comentariocomercializadora6 { get; set; }
        public string eve_comentariocomercializadora7 { get; set; }
        public string eve_plazotrastero2 { get; set; }
        public string eve_mesesgarantiainmuebleprincipal { get; set; }
        public string eve_periodicidadibiinmuebleprincipal { get; set; }
        public string _eve_trastero_no_anejo_2_id_value { get; set; }
        public string eve_comentario_cambio_vivienda { get; set; }
        public string eve_estado_interno_adicional { get; set; }
        public string eve_comentarios_visado_contratos { get; set; }
        public string eve_ibitrastero2 { get; set; }
        public string eve_scoringanio3 { get; set; }
        public string _owningteam_value { get; set; }
        public string eve_ibigaraje2 { get; set; }
        public string eve_enseres_congeladorintegrado { get; set; }
        public string eve_usuario_envio_activacion_titulares { get; set; }
        public string eve_rentatrastero2 { get; set; }
        public string eve_campana_revisada { get; set; }
        public string eve_rentainmuebleprincipal { get; set; }
        public string _eve_conviviente3_value { get; set; }
        public string eve_mesesgarantia { get; set; }
        public string eve_minusvalido_inmueble_principal { get; set; }
        public string eve_estado_cambio_vivienda_anterior { get; set; }
        public string eve_usuario_doc_completa { get; set; }
        public string eve_comentariofidere10 { get; set; }
        public string eve_motivo_condiciones_ko { get; set; }
        public string eve_plazogaraje1 { get; set; }
        public string eve_ivagaraje2 { get; set; }
        public string eve_tipoinquilinocotitular1 { get; set; }
        public string eve_tipoinquilinocotitular2 { get; set; }
        public string eve_donde_ha_conocido { get; set; }
        public string _modifiedonbehalfby_value { get; set; }
        public string eve_comentario_campana { get; set; }
        public string eve_tipo_campana { get; set; }
        public string eve_carencia_comunidad_garaje2 { get; set; }
        public string eve_fecha_comentariofidere10 { get; set; }
        public string eve_ibiinmuebleprincipal { get; set; }
        public string eve_importe_oc_garaje_2 { get; set; }
        public string eve_porcentajeivatxt { get; set; }
        public string eve_codigo_recuperacion { get; set; }
        public string eve_importe_oc_garaje_1 { get; set; }
        public string eve_hasta_fin_contrato_garaje_2 { get; set; }
        public string eve_hasta_fin_contrato_garaje_1 { get; set; }
        public string eve_nombre_y_apellidos_propietario { get; set; }
        public string eve_enseres_vitrocermicainduccion { get; set; }
        public string eve_documento_identidad_propietario { get; set; }
        public string eve_periodicidadcomunidadtrastero2 { get; set; }
        public string eve_estado_interno { get; set; }
        public string eve_comentarios_nota_localizacion { get; set; }
        public string eve_alquiler { get; set; }
        public string eve_contabilidad { get; set; }
        public string eve_mesesaplicaciontrastero2 { get; set; }
        public string eve_observaciones_contratacion { get; set; }
        public string eve_iban { get; set; }
        public string eve_comunidad { get; set; }
        public string eve_dias_rechazo_documentacion { get; set; }
        public string eve_documentacion_recibida_titular_3 { get; set; }
        public string eve_fecha_envio_manual_contrato { get; set; }
        public string eve_descuento_400 { get; set; }
        public string eve_campanya_navidad { get; set; }
        public string eve_enseres_lavadora_secadora_integrada { get; set; }
        public string eve_fecha_ultima_activacion_cambio_titulares { get; set; }
        public string eve_minusvalido_garaje_2 { get; set; }
        public string eve_enseres_lavavajillas { get; set; }
        public string eve_fecha_fijada_firma { get; set; }
        public string eve_operacionprinextrastero1 { get; set; }
        public string eve_operacion_contrato { get; set; }
        public string eve_nota_simple_titular_2 { get; set; }
        public string eve_idprinexcotitular1 { get; set; }
        public string eve_nota_simple_titular_3 { get; set; }
        public string eve_devolucion_reserva_realizada { get; set; }
        public string eve_nota_simple_titular_1 { get; set; }
        public string eve_bic { get; set; }
        public string eve_hasta_fin_contrato_inmueble_principal { get; set; }
        public string eve_bonificacion_extra_fin_mes { get; set; }
        public string eve_sujeto_a_impagos_inmueble_principal { get; set; }
        public string eve_plazogaraje2 { get; set; }
        public string eve_mesesfianzatrastero2 { get; set; }
        public string eve_documentacion_pendiente_conv_3 { get; set; }
        public string eve_marca_lavavajillas { get; set; }
        public string eve_enseres_lavadorasecadora { get; set; }
        public string eve_descripcion_rechazo { get; set; }
        public string eve_ivatrastero1 { get; set; }
        public string eve_importebonificaciongaraje2 { get; set; }
        public string _eve_promocioninmueble3_value { get; set; }
        public string eve_fecha_fin_visado { get; set; }
        public string eve_ivaprinextrastero1 { get; set; }
        public string eve_cuentacorrientetitularprincipal { get; set; }
        public string _eve_inquilinoreferenciaid_value { get; set; }
        public string _eve_conviviente1_value { get; set; }
        public string eve_tres_meses_gratis { get; set; }
        public string eve_color_lavadora_secadora { get; set; }
        public string eve_mesesfianzainmuebleprincipal { get; set; }
        public string eve_importedelareservagaraje2 { get; set; }
        public string eve_asientoanulacion { get; set; }
        public string eve_importedelareservagaraje1 { get; set; }
        public string eve_cargaprinex1 { get; set; }
        public string eve_estado_cambio_titulares { get; set; }
        public string utcconversiontimezonecode { get; set; }
        public string eve_documentacion_recibida_conv_2 { get; set; }
        public string eve_documentacion_recibida_conv_3 { get; set; }
        public string eve_numero_rechazos_documentacion { get; set; }
        public string eve_documentacion_recibida_conv_1 { get; set; }
        public string eve_enseres_horno { get; set; }
        public string eve_carencia_comunidad_trastero1 { get; set; }
        public string eve_comunidadinmuebleprincipal { get; set; }
        public string eve_marca_horno { get; set; }
        public string eve_importe_bon { get; set; }
        public string eve_comentarios_financiero { get; set; }
        public string eve_estado_cambio_vivienda { get; set; }
        public string eve_color_induccion { get; set; }
        public string eve_fecha_comentariocomercializadora8 { get; set; }
        public string eve_fecha_comentariocomercializadora9 { get; set; }
        public string eve_color_lavadora { get; set; }
        public string eve_asientocontabilizado { get; set; }
        public string eve_fecha_comentariocomercializadora4 { get; set; }
        public string eve_fecha_comentariocomercializadora5 { get; set; }
        public string eve_fecha_comentariocomercializadora6 { get; set; }
        public string eve_fecha_comentariocomercializadora7 { get; set; }
        public string eve_comunidadtrastero2 { get; set; }
        public string eve_fecha_comentariocomercializadora1 { get; set; }
        public string eve_fecha_comentariocomercializadora2 { get; set; }
        public string eve_fecha_comentariocomercializadora3 { get; set; }
        public string eve_documentacion_visada_titular_1 { get; set; }
        public string eve_meses_permanencia_trastero_2 { get; set; }
        public string eve_requiere_datos_aval { get; set; }
        public string eve_periodicidadcomunidadgaraje2 { get; set; }
        public string eve_renta { get; set; }
        public string eve_ivapersonajuridicainmuebleprincipal { get; set; }
        public string eve_marca_microondas { get; set; }
        public string eve_garantiaadicionalinmuebleprincipal { get; set; }
        public string eve_operacionprinextrastero2 { get; set; }
        public string eve_estado_documentacion_titular_1 { get; set; }
        public string eve_sanitarios { get; set; }
        public string eve_usuario_envio_firma { get; set; }
        public string eve_enlace_cambio_titulares { get; set; }
        public string eve_plazoinmuebleprincipal { get; set; }
        public string eve_estado_documentacion_conv_2 { get; set; }
        public string eve_requiere_doc_aval { get; set; }
        public string eve_estado_documentacion_conv_3 { get; set; }
        public string eve_estado_documentacion_conv_1 { get; set; }
        public string eve_minusvalido_garaje_1 { get; set; }
        public string eve_fecha_contratacion { get; set; }
        public string eve_periodicidadibi { get; set; }
        public string eve_activacion_cambio_titulares { get; set; }
        public string eve_marca_lavadora_secadora { get; set; }
        public string _eve_avalista_value { get; set; }
        public string eve_ivatrastero2 { get; set; }
        public string eve_fecha_rechazo_documentacion { get; set; }
        public string eve_asnef { get; set; }
        public string eve_ivaprinextrastero2 { get; set; }
        public string overriddencreatedon { get; set; }
        public string eve_minusvalido_trastero_2 { get; set; }
        public string eve_nombre_apellidos { get; set; }
        public string eve_carencia_ibi_garaje1 { get; set; }
        public string eve_estado_bonificacion_propietario { get; set; }
        public string eve_color_frigorifico { get; set; }
        public string eve_sujeto_permanencia_inmueble_principal { get; set; }
        public string eve_importe_devolucion_parcial { get; set; }
        public string eve_scoring1 { get; set; }
        public string eve_documentacion_pendiente_conv_2 { get; set; }
        public string eve_enseres_frigorficoconcongeladorintegrado { get; set; }
        public string eve_ivaprinexgaraje1 { get; set; }
        public string eve_importebonificaciongaraje1 { get; set; }
        public string eve_meses_permanencia_garaje_2 { get; set; }
        public string eve_enseres_microondas { get; set; }
        public string eve_fecha_fijada_firma_anterior { get; set; }
        public string eve_direccion_postal_titulares { get; set; }
        public string eve_carencia_comunidad_trastero2 { get; set; }
        public string eve_apellidostitular { get; set; }
        public string _eve_promocioninmueble1_value { get; set; }
        public string eve_ivaprinexinmuebleprincipal { get; set; }
        public string eve_fecha_fijada_firma_adicional { get; set; }
        public string eve_bonificacion { get; set; }
        public string eve_notificacion_asnef { get; set; }
        public string eve_datos_contacto_propietario { get; set; }
        public string eve_carencia_ibi_inmuebleprincipal { get; set; }
        public string eve_periodicidadcomunidadinmuebleprincipal { get; set; }
        public string eve_operacionprinexinmuebleprincipal { get; set; }
        public string eve_mesesfianzagaraje2 { get; set; }
        public string eve_no_facturar_firma_contrato { get; set; }
        public string eve_documentacion_visada_titular_2 { get; set; }
        public string eve_rentagaraje2 { get; set; }
        public string eve_comentariofidere9 { get; set; }
        public string eve_revision_nota_titular_2 { get; set; }
        public string eve_revision_nota_titular_3 { get; set; }
        public string eve_comentariofidere8 { get; set; }
        public string _createdonbehalfby_value { get; set; }
        public string eve_revision_nota_titular_1 { get; set; }
        public string eve_comentariofidere5 { get; set; }
        public string eve_comentariofidere4 { get; set; }
        public string eve_numero_meses_corriente_de_pago_garaje_2 { get; set; }
        public string eve_comentariofidere7 { get; set; }
        public string eve_numero_meses_corriente_de_pago_garaje_1 { get; set; }
        public string eve_comentariofidere6 { get; set; }
        public string eve_estado_documentacion_titular_2 { get; set; }
        public string eve_comentariofidere1 { get; set; }
        public string eve_comentariofidere3 { get; set; }
        public string eve_comentariofidere2 { get; set; }
        public string eve_fecha_comentariocomercializadora10 { get; set; }
        public string eve_color_horno_microondas { get; set; }
        public string eve_documentacion_recibida_titular_2 { get; set; }
        public string eve_marca_campana { get; set; }
        public string eve_garantiaadicionalgaraje1 { get; set; }
        public string eve_dias_rechazo_documentacion_1 { get; set; }
        public string eve_importecotitular1 { get; set; }
        public string eve_marca_vitrocermica { get; set; }
        public string eve_importecotitular2 { get; set; }
        public string eve_enseres_lavavajillasintegrado { get; set; }
        public string _eve_conviviente2_value { get; set; }
        public string eve_plazotrastero1 { get; set; }
        public string eve_minusvalido_trastero_1 { get; set; }
        public string eve_scoringanio1 { get; set; }
        public string eve_carencia_ibi_trastero1 { get; set; }
        public string eve_bonificacion_recomendacion { get; set; }
        public string eve_numero_meses_corriente_de_pago_trastero_1 { get; set; }
        public string eve_numero_meses_corriente_de_pago_trastero_2 { get; set; }
        public string eve_leads_verano { get; set; }
        public string eve_periodicidadibitrastero2 { get; set; }
        public string eve_documentacion_pendiente_conv_1 { get; set; }
        public string eve_periodicidadibigaraje2 { get; set; }
        public string eve_marca_congelador { get; set; }
        public string eve_ivaprinexgaraje2 { get; set; }
        public string eve_color_horno { get; set; }
        public string eve_mesesgarantiagaraje1 { get; set; }
        public string eve_codigo_inquilino { get; set; }
        public string eve_importetitularprincipal { get; set; }
        public string eve_garantiaadicionaltrastero2 { get; set; }
        public string eve_marca_induccion { get; set; }
        public string eve_meses_permanencia_garaje_1 { get; set; }
        public string eve_enseres_lavadoraintegrada { get; set; }
        public string eve_ivapersonajuridicatrastero1 { get; set; }
        public string eve_bonificacion_campana { get; set; }
        public string eve_sujeto_a_impagos_trastero_2 { get; set; }
        public string eve_sujeto_a_impagos_trastero_1 { get; set; }
        public string eve_ivapersonajuridicagaraje1 { get; set; }
        public string eve_ivapersonajuridicatrastero2 { get; set; }
        public string eve_numero_expediente { get; set; }
        public string eve_ivapersonajuridicagaraje2 { get; set; }
        public string eve_nota_localizacion_titular_3 { get; set; }
        public string eve_mesesaplicaciongaraje1 { get; set; }
        public string eve_nota_localizacion_titular_2 { get; set; }
        public string eve_nota_localizacion_titular_1 { get; set; }
        public string eve_condiciones_firma_contrato { get; set; }
        public string eve_comunidadgaraje1 { get; set; }
        public string eve_marca_frigorifico { get; set; }
        public string eve_carencia_comunidad_garaje1 { get; set; }
        public string eve_titular_relevante_1 { get; set; }
        public string eve_fecha_envio_recordatorio_seguro { get; set; }
        public string eve_color_congelador { get; set; }
        public string eve_vivienda_contingencia_rotacion { get; set; }
        public string eve_rentagaraje1 { get; set; }
        public string eve_promocion_200 { get; set; }
        public string _eve_promocioninmueble5_value { get; set; }
        public string eve_motivo_cancelacion_reserva { get; set; }
        public string eve_comentarios_documentacion_viv_protegidas { get; set; }
        public string eve_sujeto_permanencia_trastero_2 { get; set; }
        public string eve_sujeto_permanencia_trastero_1 { get; set; }
        public string eve_mesesaplicaciontrastero1 { get; set; }
        public string eve_cotitular_relevante_2 { get; set; }
        public string eve_meses_permanencia_trastero_1 { get; set; }
        public string eve_cotitular_relevante_1 { get; set; }
        public Reserva() { }
        public Dynamics_Reservas Convertir2BD() 
        {
            Dynamics_Reservas dynamics_Reservas = new Dynamics_Reservas();
            dynamics_Reservas.OdataEtag = OdataEtag;
            dynamics_Reservas.eve_importebonificaciontrastero1 = eve_importebonificaciontrastero1;
            dynamics_Reservas.eve_periodicidadibitrastero1 = eve_periodicidadibitrastero1;
            dynamics_Reservas.eve_garantiaadicionaltrastero1 = eve_garantiaadicionaltrastero1;
            dynamics_Reservas.eve_lleida_request_id = eve_lleida_request_id;
            dynamics_Reservas.eve_fecha_fin = eve_fecha_fin;
            dynamics_Reservas.eve_dias_alta_suministro = eve_dias_alta_suministro;
            dynamics_Reservas.eve_empresa = eve_empresa;
            dynamics_Reservas.eve_fecha_alta_suministro = eve_fecha_alta_suministro;
            dynamics_Reservas.C_eve_pago_reserva_id_value = _eve_pago_reserva_id_value;
            dynamics_Reservas.eve_ibitrastero1 = eve_ibitrastero1;
            dynamics_Reservas.eve_nombretitular = eve_nombretitular;
            dynamics_Reservas.C_owningbusinessunit_value = _owningbusinessunit_value;
            dynamics_Reservas.eve_inquilinos_doc_subida = eve_inquilinos_doc_subida;
            dynamics_Reservas.C_owninguser_value = _owninguser_value;
            dynamics_Reservas.C_eve_promocioninmueble4_value = _eve_promocioninmueble4_value;
            dynamics_Reservas.statecode = statecode;
            dynamics_Reservas.eve_firmado = eve_firmado;
            dynamics_Reservas.eve_fecha_revision_asnef = eve_fecha_revision_asnef;
            dynamics_Reservas.eve_fecha_inicio = eve_fecha_inicio;
            dynamics_Reservas.C_ownerid_value = _ownerid_value;
            dynamics_Reservas.eve_alta_reserva_prinex = eve_alta_reserva_prinex;
            dynamics_Reservas.eve_alta_suministros = eve_alta_suministros;
            dynamics_Reservas.eve_fecha_primera_entrega_doc_inquilino = eve_fecha_primera_entrega_doc_inquilino;
            dynamics_Reservas.C_eve_titular_principal_value = _eve_titular_principal_value;
            dynamics_Reservas.eve_dias_primera_entrega_doc_inquilino = eve_dias_primera_entrega_doc_inquilino;
            dynamics_Reservas.eve_fecha_firma = eve_fecha_firma;
            dynamics_Reservas.timezoneruleversionnumber = timezoneruleversionnumber;
            dynamics_Reservas.statuscode = statuscode;
            dynamics_Reservas.createdon = createdon;
            dynamics_Reservas.versionnumber = versionnumber;
            dynamics_Reservas.eve_aprobado_por_tecnico = eve_aprobado_por_tecnico;
            dynamics_Reservas.eve_envio_notificacion_api = eve_envio_notificacion_api;
            dynamics_Reservas.eve_enlacerecuperacionreserva = eve_enlacerecuperacionreserva;
            dynamics_Reservas.eve_fecha_aprobacion_reserva = eve_fecha_aprobacion_reserva;
            dynamics_Reservas.eve_name = eve_name;
            dynamics_Reservas.modifiedon = modifiedon;
            dynamics_Reservas.eve_comunidadtrastero1 = eve_comunidadtrastero1;
            dynamics_Reservas.eve_campanya_empresas = eve_campanya_empresas;
            dynamics_Reservas.eve_fecha_aprobada_api = eve_fecha_aprobada_api;
            dynamics_Reservas.eve_rentaanio3 = eve_rentaanio3;
            dynamics_Reservas.eve_mesesfianzatrastero1 = eve_mesesfianzatrastero1;
            dynamics_Reservas.C_eve_cotitular1_value = _eve_cotitular1_value;
            dynamics_Reservas.C_eve_cotitular2_value = _eve_cotitular2_value;
            dynamics_Reservas.eve_mesesgarantiatrastero1 = eve_mesesgarantiatrastero1;
            dynamics_Reservas.eve_estado_adicional = eve_estado_adicional;
            dynamics_Reservas.C_eve_trastero_no_anejo_1_id_value = _eve_trastero_no_anejo_1_id_value;
            dynamics_Reservas.eve_reservaid = eve_reservaid;
            dynamics_Reservas.C_modifiedby_value = _modifiedby_value;
            dynamics_Reservas.eve_dias_gestion_reserva = eve_dias_gestion_reserva;
            dynamics_Reservas.eve_importedelareservatrastero1 = eve_importedelareservatrastero1;
            dynamics_Reservas.C_createdby_value = _createdby_value;
            dynamics_Reservas.eve_periodicidadcomunidadtrastero1 = eve_periodicidadcomunidadtrastero1;
            dynamics_Reservas.eve_gdpr = eve_gdpr;
            dynamics_Reservas.eve_dias_aprobada_tecnico = eve_dias_aprobada_tecnico;
            dynamics_Reservas.eve_rentatrastero1 = eve_rentatrastero1;
            dynamics_Reservas.eve_comunicacionfinalizada = eve_comunicacionfinalizada;
            dynamics_Reservas.eve_fecha_aprobada_tecnico = eve_fecha_aprobada_tecnico;
            dynamics_Reservas.eve_dias_revision_asnef = eve_dias_revision_asnef;
            dynamics_Reservas.eve_dias_aprobada_api = eve_dias_aprobada_api;
            dynamics_Reservas.C_eve_contrato_id_value = _eve_contrato_id_value;
            dynamics_Reservas.eve_operacionprinexgaraje1 = eve_operacionprinexgaraje1;
            dynamics_Reservas.eve_documentacion_recibida_titular_1 = eve_documentacion_recibida_titular_1;
            dynamics_Reservas.eve_comentarios_internos = eve_comentarios_internos;
            dynamics_Reservas.eve_tipoinquilinoprincipal = eve_tipoinquilinoprincipal;
            dynamics_Reservas.eve_periodicidadcomunidadgaraje1 = eve_periodicidadcomunidadgaraje1;
            dynamics_Reservas.eve_idprinextitularprincipal = eve_idprinextitularprincipal;
            dynamics_Reservas.eve_devolucion_reserva_extraordinaria = eve_devolucion_reserva_extraordinaria;
            dynamics_Reservas.eve_estado_documentacion_titular_3 = eve_estado_documentacion_titular_3;
            dynamics_Reservas.eve_carencia_comunidad_inmuebleprincipal = eve_carencia_comunidad_inmuebleprincipal;
            dynamics_Reservas.eve_marca_horno_microondas = eve_marca_horno_microondas;
            dynamics_Reservas.eve_carencia_ibi_trastero2 = eve_carencia_ibi_trastero2;
            dynamics_Reservas.importsequencenumber = importsequencenumber;
            dynamics_Reservas.eve_garantiaadicionalgaraje2 = eve_garantiaadicionalgaraje2;
            dynamics_Reservas.eve_reserva_contabilizada = eve_reserva_contabilizada;
            dynamics_Reservas.eve_enseres_frigorficoconcongelador = eve_enseres_frigorficoconcongelador;
            dynamics_Reservas.eve_sumatorio_enseres = eve_sumatorio_enseres;
            dynamics_Reservas.eve_periodicidadibigaraje1 = eve_periodicidadibigaraje1;
            dynamics_Reservas.eve_meses_corriente_pago_inmueble_principal = eve_meses_corriente_pago_inmueble_principal;
            dynamics_Reservas.eve_documentacion_visada = eve_documentacion_visada;
            dynamics_Reservas.eve_enseres_campanaextractora = eve_enseres_campanaextractora;
            dynamics_Reservas.eve_enseres_congelador = eve_enseres_congelador;
            dynamics_Reservas.eve_mesesgarantiagaraje2 = eve_mesesgarantiagaraje2;
            dynamics_Reservas.eve_comentarios_internos_api = eve_comentarios_internos_api;
            dynamics_Reservas.eve_comentario_interno_seguros = eve_comentario_interno_seguros;
            dynamics_Reservas.eve_mesesgarantiatrastero2 = eve_mesesgarantiatrastero2;
            dynamics_Reservas.eve_meses_apli = eve_meses_apli;
            dynamics_Reservas.C_eve_promocioninmueble2_value = _eve_promocioninmueble2_value;
            dynamics_Reservas.eve_agrupar_documentacion_total = eve_agrupar_documentacion_total;
            dynamics_Reservas.eve_black_friday = eve_black_friday;
            dynamics_Reservas.eve_enseres_lavadora = eve_enseres_lavadora;
            dynamics_Reservas.eve_comentario_bonificacion_recomendacion = eve_comentario_bonificacion_recomendacion;
            dynamics_Reservas.eve_carencia_ibi_garaje2 = eve_carencia_ibi_garaje2;
            dynamics_Reservas.eve_importedelareservatrastero2 = eve_importedelareservatrastero2;
            dynamics_Reservas.eve_idprinexcotitular2 = eve_idprinexcotitular2;
            dynamics_Reservas.eve_comercial_interno_asignado = eve_comercial_interno_asignado;
            dynamics_Reservas.eve_importe_oc_trastero_1 = eve_importe_oc_trastero_1;
            dynamics_Reservas.eve_importebonificacioninmuebleprincipal = eve_importebonificacioninmuebleprincipal;
            dynamics_Reservas.eve_importe_oc_trastero_2 = eve_importe_oc_trastero_2;
            dynamics_Reservas.C_eve_garaje_no_anejo_1_id_value = _eve_garaje_no_anejo_1_id_value;
            dynamics_Reservas.eve_ibigaraje1 = eve_ibigaraje1;
            dynamics_Reservas.eve_importe_oc_inmueble_principal = eve_importe_oc_inmueble_principal;
            dynamics_Reservas.eve_mesesaplicaciongaraje2 = eve_mesesaplicaciongaraje2;
            dynamics_Reservas.eve_scoring3 = eve_scoring3;
            dynamics_Reservas.eve_color_lavavajillas = eve_color_lavavajillas;
            dynamics_Reservas.eve_fecha_modificacion_cambio_vivienda = eve_fecha_modificacion_cambio_vivienda;
            dynamics_Reservas.eve_reserva_procedente_cambio_vivienda = eve_reserva_procedente_cambio_vivienda;
            dynamics_Reservas.eve_comunidadgaraje2 = eve_comunidadgaraje2;
            dynamics_Reservas.C_eve_inmueble_prinex1_value = _eve_inmueble_prinex1_value;
            dynamics_Reservas.eve_comentariocomercializadora10 = eve_comentariocomercializadora10;
            dynamics_Reservas.eve_enseres_hornomicroondas = eve_enseres_hornomicroondas;
            dynamics_Reservas.eve_importebonificaciontrastero2 = eve_importebonificaciontrastero2;
            dynamics_Reservas.eve_cargaprinex2 = eve_cargaprinex2;
            dynamics_Reservas.eve_bonificacion_recomendacion_propietario = eve_bonificacion_recomendacion_propietario;
            dynamics_Reservas.eve_ivainmuebleprincipal = eve_ivainmuebleprincipal;
            dynamics_Reservas.eve_sujeto_permanencia_garaje_1 = eve_sujeto_permanencia_garaje_1;
            dynamics_Reservas.eve_enviar_recordatorio_seguro_hogar = eve_enviar_recordatorio_seguro_hogar;
            dynamics_Reservas.eve_sujeto_permanencia_garaje_2 = eve_sujeto_permanencia_garaje_2;
            dynamics_Reservas.C_eve_garaje_no_anejo_2_id_value = _eve_garaje_no_anejo_2_id_value;
            dynamics_Reservas.eve_codigo_promocion = eve_codigo_promocion;
            dynamics_Reservas.eve_ivagaraje1 = eve_ivagaraje1;
            dynamics_Reservas.eve_color_campana = eve_color_campana;
            dynamics_Reservas.eve_meses_permanencia_inmueble_principal = eve_meses_permanencia_inmueble_principal;
            dynamics_Reservas.eve_enseres_induccion = eve_enseres_induccion;
            dynamics_Reservas.eve_hasta_fin_contrato_trastero_1 = eve_hasta_fin_contrato_trastero_1;
            dynamics_Reservas.eve_hasta_fin_contrato_trastero_2 = eve_hasta_fin_contrato_trastero_2;
            dynamics_Reservas.eve_mesesaplicacioninmuebleprincipal = eve_mesesaplicacioninmuebleprincipal;
            dynamics_Reservas.eve_operacionprinexgaraje2 = eve_operacionprinexgaraje2;
            dynamics_Reservas.eve_documentacion_pendiente_titular_1 = eve_documentacion_pendiente_titular_1;
            dynamics_Reservas.eve_documentacion_pendiente_titular_3 = eve_documentacion_pendiente_titular_3;
            dynamics_Reservas.eve_documentacion_pendiente_titular_2 = eve_documentacion_pendiente_titular_2;
            dynamics_Reservas.eve_sujeto_a_impagos_garaje_1 = eve_sujeto_a_impagos_garaje_1;
            dynamics_Reservas.eve_sujeto_a_impagos_garaje_2 = eve_sujeto_a_impagos_garaje_2;
            dynamics_Reservas.eve_color_vitroceramica = eve_color_vitroceramica;
            dynamics_Reservas.eve_enviar_contrato_reserva_firma = eve_enviar_contrato_reserva_firma;
            dynamics_Reservas.eve_mesesfianzagaraje1 = eve_mesesfianzagaraje1;
            dynamics_Reservas.eve_periodicidadcomunidad = eve_periodicidadcomunidad;
            dynamics_Reservas.eve_fecha_comentariofidere9 = eve_fecha_comentariofidere9;
            dynamics_Reservas.eve_fecha_comentariofidere8 = eve_fecha_comentariofidere8;
            dynamics_Reservas.eve_color_microondas = eve_color_microondas;
            dynamics_Reservas.eve_fecha_comentariofidere1 = eve_fecha_comentariofidere1;
            dynamics_Reservas.eve_marca_lavadora = eve_marca_lavadora;
            dynamics_Reservas.eve_fecha_comentariofidere3 = eve_fecha_comentariofidere3;
            dynamics_Reservas.eve_mesesfianza = eve_mesesfianza;
            dynamics_Reservas.eve_fecha_comentariofidere2 = eve_fecha_comentariofidere2;
            dynamics_Reservas.eve_fecha_comentariofidere5 = eve_fecha_comentariofidere5;
            dynamics_Reservas.eve_fecha_comentariofidere4 = eve_fecha_comentariofidere4;
            dynamics_Reservas.eve_fecha_comentariofidere7 = eve_fecha_comentariofidere7;
            dynamics_Reservas.eve_fecha_comentariofidere6 = eve_fecha_comentariofidere6;
            dynamics_Reservas.eve_comentariocomercializadora8 = eve_comentariocomercializadora8;
            dynamics_Reservas.eve_comentariocomercializadora9 = eve_comentariocomercializadora9;
            dynamics_Reservas.eve_fuerza_seguridad = eve_fuerza_seguridad;
            dynamics_Reservas.eve_agrupar_documentacion = eve_agrupar_documentacion;
            dynamics_Reservas.eve_documentacion_visada_titular_3 = eve_documentacion_visada_titular_3;
            dynamics_Reservas.eve_comentariocomercializadora1 = eve_comentariocomercializadora1;
            dynamics_Reservas.eve_comentariocomercializadora2 = eve_comentariocomercializadora2;
            dynamics_Reservas.eve_comentariocomercializadora3 = eve_comentariocomercializadora3;
            dynamics_Reservas.eve_comentariocomercializadora4 = eve_comentariocomercializadora4;
            dynamics_Reservas.eve_comentariocomercializadora5 = eve_comentariocomercializadora5;
            dynamics_Reservas.eve_importedelareservainmuebleprincipal = eve_importedelareservainmuebleprincipal;
            dynamics_Reservas.eve_comentariocomercializadora6 = eve_comentariocomercializadora6;
            dynamics_Reservas.eve_comentariocomercializadora7 = eve_comentariocomercializadora7;
            dynamics_Reservas.eve_plazotrastero2 = eve_plazotrastero2;
            dynamics_Reservas.eve_mesesgarantiainmuebleprincipal = eve_mesesgarantiainmuebleprincipal;
            dynamics_Reservas.eve_periodicidadibiinmuebleprincipal = eve_periodicidadibiinmuebleprincipal;
            dynamics_Reservas.C_eve_trastero_no_anejo_2_id_value = _eve_trastero_no_anejo_2_id_value;
            dynamics_Reservas.eve_comentario_cambio_vivienda = eve_comentario_cambio_vivienda;
            dynamics_Reservas.eve_estado_interno_adicional = eve_estado_interno_adicional;
            dynamics_Reservas.eve_comentarios_visado_contratos = eve_comentarios_visado_contratos;
            dynamics_Reservas.eve_ibitrastero2 = eve_ibitrastero2;
            dynamics_Reservas.eve_scoringanio3 = eve_scoringanio3;
            dynamics_Reservas.C_owningteam_value = _owningteam_value;
            dynamics_Reservas.eve_ibigaraje2 = eve_ibigaraje2;
            dynamics_Reservas.eve_enseres_congeladorintegrado = eve_enseres_congeladorintegrado;
            dynamics_Reservas.eve_usuario_envio_activacion_titulares = eve_usuario_envio_activacion_titulares;
            dynamics_Reservas.eve_rentatrastero2 = eve_rentatrastero2;
            dynamics_Reservas.eve_campana_revisada = eve_campana_revisada;
            dynamics_Reservas.eve_rentainmuebleprincipal = eve_rentainmuebleprincipal;
            dynamics_Reservas.C_eve_conviviente3_value = _eve_conviviente3_value;
            dynamics_Reservas.eve_mesesgarantia = eve_mesesgarantia;
            dynamics_Reservas.eve_minusvalido_inmueble_principal = eve_minusvalido_inmueble_principal;
            dynamics_Reservas.eve_estado_cambio_vivienda_anterior = eve_estado_cambio_vivienda_anterior;
            dynamics_Reservas.eve_usuario_doc_completa = eve_usuario_doc_completa;
            dynamics_Reservas.eve_comentariofidere10 = eve_comentariofidere10;
            dynamics_Reservas.eve_motivo_condiciones_ko = eve_motivo_condiciones_ko;
            dynamics_Reservas.eve_plazogaraje1 = eve_plazogaraje1;
            dynamics_Reservas.eve_ivagaraje2 = eve_ivagaraje2;
            dynamics_Reservas.eve_tipoinquilinocotitular1 = eve_tipoinquilinocotitular1;
            dynamics_Reservas.eve_tipoinquilinocotitular2 = eve_tipoinquilinocotitular2;
            dynamics_Reservas.eve_donde_ha_conocido = eve_donde_ha_conocido;
            dynamics_Reservas.C_modifiedonbehalfby_value = _modifiedonbehalfby_value;
            dynamics_Reservas.eve_comentario_campana = eve_comentario_campana;
            dynamics_Reservas.eve_tipo_campana = eve_tipo_campana;
            dynamics_Reservas.eve_carencia_comunidad_garaje2 = eve_carencia_comunidad_garaje2;
            dynamics_Reservas.eve_fecha_comentariofidere10 = eve_fecha_comentariofidere10;
            dynamics_Reservas.eve_ibiinmuebleprincipal = eve_ibiinmuebleprincipal;
            dynamics_Reservas.eve_importe_oc_garaje_2 = eve_importe_oc_garaje_2;
            dynamics_Reservas.eve_porcentajeivatxt = eve_porcentajeivatxt;
            dynamics_Reservas.eve_codigo_recuperacion = eve_codigo_recuperacion;
            dynamics_Reservas.eve_importe_oc_garaje_1 = eve_importe_oc_garaje_1;
            dynamics_Reservas.eve_hasta_fin_contrato_garaje_2 = eve_hasta_fin_contrato_garaje_2;
            dynamics_Reservas.eve_hasta_fin_contrato_garaje_1 = eve_hasta_fin_contrato_garaje_1;
            dynamics_Reservas.eve_nombre_y_apellidos_propietario = eve_nombre_y_apellidos_propietario;
            dynamics_Reservas.eve_enseres_vitrocermicainduccion = eve_enseres_vitrocermicainduccion;
            dynamics_Reservas.eve_documento_identidad_propietario = eve_documento_identidad_propietario;
            dynamics_Reservas.eve_periodicidadcomunidadtrastero2 = eve_periodicidadcomunidadtrastero2;
            dynamics_Reservas.eve_estado_interno = eve_estado_interno;
            dynamics_Reservas.eve_comentarios_nota_localizacion = eve_comentarios_nota_localizacion;
            dynamics_Reservas.eve_alquiler = eve_alquiler;
            dynamics_Reservas.eve_contabilidad = eve_contabilidad;
            dynamics_Reservas.eve_mesesaplicaciontrastero2 = eve_mesesaplicaciontrastero2;
            dynamics_Reservas.eve_observaciones_contratacion = eve_observaciones_contratacion;
            dynamics_Reservas.eve_iban = eve_iban;
            dynamics_Reservas.eve_comunidad = eve_comunidad;
            dynamics_Reservas.eve_dias_rechazo_documentacion = eve_dias_rechazo_documentacion;
            dynamics_Reservas.eve_documentacion_recibida_titular_3 = eve_documentacion_recibida_titular_3;
            dynamics_Reservas.eve_fecha_envio_manual_contrato = eve_fecha_envio_manual_contrato;
            dynamics_Reservas.eve_descuento_400 = eve_descuento_400;
            dynamics_Reservas.eve_campanya_navidad = eve_campanya_navidad;
            dynamics_Reservas.eve_enseres_lavadora_secadora_integrada = eve_enseres_lavadora_secadora_integrada;
            dynamics_Reservas.eve_fecha_ultima_activacion_cambio_titulares = eve_fecha_ultima_activacion_cambio_titulares;
            dynamics_Reservas.eve_minusvalido_garaje_2 = eve_minusvalido_garaje_2;
            dynamics_Reservas.eve_enseres_lavavajillas = eve_enseres_lavavajillas;
            dynamics_Reservas.eve_fecha_fijada_firma = eve_fecha_fijada_firma;
            dynamics_Reservas.eve_operacionprinextrastero1 = eve_operacionprinextrastero1;
            dynamics_Reservas.eve_operacion_contrato = eve_operacion_contrato;
            dynamics_Reservas.eve_nota_simple_titular_2 = eve_nota_simple_titular_2;
            dynamics_Reservas.eve_idprinexcotitular1 = eve_idprinexcotitular1;
            dynamics_Reservas.eve_nota_simple_titular_3 = eve_nota_simple_titular_3;
            dynamics_Reservas.eve_devolucion_reserva_realizada = eve_devolucion_reserva_realizada;
            dynamics_Reservas.eve_nota_simple_titular_1 = eve_nota_simple_titular_1;
            dynamics_Reservas.eve_bic = eve_bic;
            dynamics_Reservas.eve_hasta_fin_contrato_inmueble_principal = eve_hasta_fin_contrato_inmueble_principal;
            dynamics_Reservas.eve_bonificacion_extra_fin_mes = eve_bonificacion_extra_fin_mes;
            dynamics_Reservas.eve_sujeto_a_impagos_inmueble_principal = eve_sujeto_a_impagos_inmueble_principal;
            dynamics_Reservas.eve_plazogaraje2 = eve_plazogaraje2;
            dynamics_Reservas.eve_mesesfianzatrastero2 = eve_mesesfianzatrastero2;
            dynamics_Reservas.eve_documentacion_pendiente_conv_3 = eve_documentacion_pendiente_conv_3;
            dynamics_Reservas.eve_marca_lavavajillas = eve_marca_lavavajillas;
            dynamics_Reservas.eve_enseres_lavadorasecadora = eve_enseres_lavadorasecadora;
            dynamics_Reservas.eve_descripcion_rechazo = eve_descripcion_rechazo;
            dynamics_Reservas.eve_ivatrastero1 = eve_ivatrastero1;
            dynamics_Reservas.eve_importebonificaciongaraje2 = eve_importebonificaciongaraje2;
            dynamics_Reservas.C_eve_promocioninmueble3_value = _eve_promocioninmueble3_value;
            dynamics_Reservas.eve_fecha_fin_visado = eve_fecha_fin_visado;
            dynamics_Reservas.eve_ivaprinextrastero1 = eve_ivaprinextrastero1;
            dynamics_Reservas.eve_cuentacorrientetitularprincipal = eve_cuentacorrientetitularprincipal;
            dynamics_Reservas.C_eve_inquilinoreferenciaid_value = _eve_inquilinoreferenciaid_value;
            dynamics_Reservas.C_eve_conviviente1_value = _eve_conviviente1_value;
            dynamics_Reservas.eve_tres_meses_gratis = eve_tres_meses_gratis;
            dynamics_Reservas.eve_color_lavadora_secadora = eve_color_lavadora_secadora;
            dynamics_Reservas.eve_mesesfianzainmuebleprincipal = eve_mesesfianzainmuebleprincipal;
            dynamics_Reservas.eve_importedelareservagaraje2 = eve_importedelareservagaraje2;
            dynamics_Reservas.eve_asientoanulacion = eve_asientoanulacion;
            dynamics_Reservas.eve_importedelareservagaraje1 = eve_importedelareservagaraje1;
            dynamics_Reservas.eve_cargaprinex1 = eve_cargaprinex1;
            dynamics_Reservas.eve_estado_cambio_titulares = eve_estado_cambio_titulares;
            dynamics_Reservas.utcconversiontimezonecode = utcconversiontimezonecode;
            dynamics_Reservas.eve_documentacion_recibida_conv_2 = eve_documentacion_recibida_conv_2;
            dynamics_Reservas.eve_documentacion_recibida_conv_3 = eve_documentacion_recibida_conv_3;
            dynamics_Reservas.eve_numero_rechazos_documentacion = eve_numero_rechazos_documentacion;
            dynamics_Reservas.eve_documentacion_recibida_conv_1 = eve_documentacion_recibida_conv_1;
            dynamics_Reservas.eve_enseres_horno = eve_enseres_horno;
            dynamics_Reservas.eve_carencia_comunidad_trastero1 = eve_carencia_comunidad_trastero1;
            dynamics_Reservas.eve_comunidadinmuebleprincipal = eve_comunidadinmuebleprincipal;
            dynamics_Reservas.eve_marca_horno = eve_marca_horno;
            dynamics_Reservas.eve_importe_bon = eve_importe_bon;
            dynamics_Reservas.eve_comentarios_financiero = eve_comentarios_financiero;
            dynamics_Reservas.eve_estado_cambio_vivienda = eve_estado_cambio_vivienda;
            dynamics_Reservas.eve_color_induccion = eve_color_induccion;
            dynamics_Reservas.eve_fecha_comentariocomercializadora8 = eve_fecha_comentariocomercializadora8;
            dynamics_Reservas.eve_fecha_comentariocomercializadora9 = eve_fecha_comentariocomercializadora9;
            dynamics_Reservas.eve_color_lavadora = eve_color_lavadora;
            dynamics_Reservas.eve_asientocontabilizado = eve_asientocontabilizado;
            dynamics_Reservas.eve_fecha_comentariocomercializadora4 = eve_fecha_comentariocomercializadora4;
            dynamics_Reservas.eve_fecha_comentariocomercializadora5 = eve_fecha_comentariocomercializadora5;
            dynamics_Reservas.eve_fecha_comentariocomercializadora6 = eve_fecha_comentariocomercializadora6;
            dynamics_Reservas.eve_fecha_comentariocomercializadora7 = eve_fecha_comentariocomercializadora7;
            dynamics_Reservas.eve_comunidadtrastero2 = eve_comunidadtrastero2;
            dynamics_Reservas.eve_fecha_comentariocomercializadora1 = eve_fecha_comentariocomercializadora1;
            dynamics_Reservas.eve_fecha_comentariocomercializadora2 = eve_fecha_comentariocomercializadora2;
            dynamics_Reservas.eve_fecha_comentariocomercializadora3 = eve_fecha_comentariocomercializadora3;
            dynamics_Reservas.eve_documentacion_visada_titular_1 = eve_documentacion_visada_titular_1;
            dynamics_Reservas.eve_meses_permanencia_trastero_2 = eve_meses_permanencia_trastero_2;
            dynamics_Reservas.eve_requiere_datos_aval = eve_requiere_datos_aval;
            dynamics_Reservas.eve_periodicidadcomunidadgaraje2 = eve_periodicidadcomunidadgaraje2;
            dynamics_Reservas.eve_renta = eve_renta;
            dynamics_Reservas.eve_ivapersonajuridicainmuebleprincipal = eve_ivapersonajuridicainmuebleprincipal;
            dynamics_Reservas.eve_marca_microondas = eve_marca_microondas;
            dynamics_Reservas.eve_garantiaadicionalinmuebleprincipal = eve_garantiaadicionalinmuebleprincipal;
            dynamics_Reservas.eve_operacionprinextrastero2 = eve_operacionprinextrastero2;
            dynamics_Reservas.eve_estado_documentacion_titular_1 = eve_estado_documentacion_titular_1;
            dynamics_Reservas.eve_sanitarios = eve_sanitarios;
            dynamics_Reservas.eve_usuario_envio_firma = eve_usuario_envio_firma;
            dynamics_Reservas.eve_enlace_cambio_titulares = eve_enlace_cambio_titulares;
            dynamics_Reservas.eve_plazoinmuebleprincipal = eve_plazoinmuebleprincipal;
            dynamics_Reservas.eve_estado_documentacion_conv_2 = eve_estado_documentacion_conv_2;
            dynamics_Reservas.eve_requiere_doc_aval = eve_requiere_doc_aval;
            dynamics_Reservas.eve_estado_documentacion_conv_3 = eve_estado_documentacion_conv_3;
            dynamics_Reservas.eve_estado_documentacion_conv_1 = eve_estado_documentacion_conv_1;
            dynamics_Reservas.eve_minusvalido_garaje_1 = eve_minusvalido_garaje_1;
            dynamics_Reservas.eve_fecha_contratacion = eve_fecha_contratacion;
            dynamics_Reservas.eve_periodicidadibi = eve_periodicidadibi;
            dynamics_Reservas.eve_activacion_cambio_titulares = eve_activacion_cambio_titulares;
            dynamics_Reservas.eve_marca_lavadora_secadora = eve_marca_lavadora_secadora;
            dynamics_Reservas.C_eve_avalista_value = _eve_avalista_value;
            dynamics_Reservas.eve_ivatrastero2 = eve_ivatrastero2;
            dynamics_Reservas.eve_fecha_rechazo_documentacion = eve_fecha_rechazo_documentacion;
            dynamics_Reservas.eve_asnef = eve_asnef;
            dynamics_Reservas.eve_ivaprinextrastero2 = eve_ivaprinextrastero2;
            dynamics_Reservas.overriddencreatedon = overriddencreatedon;
            dynamics_Reservas.eve_minusvalido_trastero_2 = eve_minusvalido_trastero_2;
            dynamics_Reservas.eve_nombre_apellidos = eve_nombre_apellidos;
            dynamics_Reservas.eve_carencia_ibi_garaje1 = eve_carencia_ibi_garaje1;
            dynamics_Reservas.eve_estado_bonificacion_propietario = eve_estado_bonificacion_propietario;
            dynamics_Reservas.eve_color_frigorifico = eve_color_frigorifico;
            dynamics_Reservas.eve_sujeto_permanencia_inmueble_principal = eve_sujeto_permanencia_inmueble_principal;
            dynamics_Reservas.eve_importe_devolucion_parcial = eve_importe_devolucion_parcial;
            dynamics_Reservas.eve_scoring1 = eve_scoring1;
            dynamics_Reservas.eve_documentacion_pendiente_conv_2 = eve_documentacion_pendiente_conv_2;
            dynamics_Reservas.eve_enseres_frigorficoconcongeladorintegrado = eve_enseres_frigorficoconcongeladorintegrado;
            dynamics_Reservas.eve_ivaprinexgaraje1 = eve_ivaprinexgaraje1;
            dynamics_Reservas.eve_importebonificaciongaraje1 = eve_importebonificaciongaraje1;
            dynamics_Reservas.eve_meses_permanencia_garaje_2 = eve_meses_permanencia_garaje_2;
            dynamics_Reservas.eve_enseres_microondas = eve_enseres_microondas;
            dynamics_Reservas.eve_fecha_fijada_firma_anterior = eve_fecha_fijada_firma_anterior;
            dynamics_Reservas.eve_direccion_postal_titulares = eve_direccion_postal_titulares;
            dynamics_Reservas.eve_carencia_comunidad_trastero2 = eve_carencia_comunidad_trastero2;
            dynamics_Reservas.eve_apellidostitular = eve_apellidostitular;
            dynamics_Reservas.C_eve_promocioninmueble1_value = _eve_promocioninmueble1_value;
            dynamics_Reservas.eve_ivaprinexinmuebleprincipal = eve_ivaprinexinmuebleprincipal;
            dynamics_Reservas.eve_fecha_fijada_firma_adicional = eve_fecha_fijada_firma_adicional;
            dynamics_Reservas.eve_bonificacion = eve_bonificacion;
            dynamics_Reservas.eve_notificacion_asnef = eve_notificacion_asnef;
            dynamics_Reservas.eve_datos_contacto_propietario = eve_datos_contacto_propietario;
            dynamics_Reservas.eve_carencia_ibi_inmuebleprincipal = eve_carencia_ibi_inmuebleprincipal;
            dynamics_Reservas.eve_periodicidadcomunidadinmuebleprincipal = eve_periodicidadcomunidadinmuebleprincipal;
            dynamics_Reservas.eve_operacionprinexinmuebleprincipal = eve_operacionprinexinmuebleprincipal;
            dynamics_Reservas.eve_mesesfianzagaraje2 = eve_mesesfianzagaraje2;
            dynamics_Reservas.eve_no_facturar_firma_contrato = eve_no_facturar_firma_contrato;
            dynamics_Reservas.eve_documentacion_visada_titular_2 = eve_documentacion_visada_titular_2;
            dynamics_Reservas.eve_rentagaraje2 = eve_rentagaraje2;
            dynamics_Reservas.eve_comentariofidere9 = eve_comentariofidere9;
            dynamics_Reservas.eve_revision_nota_titular_2 = eve_revision_nota_titular_2;
            dynamics_Reservas.eve_revision_nota_titular_3 = eve_revision_nota_titular_3;
            dynamics_Reservas.eve_comentariofidere8 = eve_comentariofidere8;
            dynamics_Reservas.C_createdonbehalfby_value = _createdonbehalfby_value;
            dynamics_Reservas.eve_revision_nota_titular_1 = eve_revision_nota_titular_1;
            dynamics_Reservas.eve_comentariofidere5 = eve_comentariofidere5;
            dynamics_Reservas.eve_comentariofidere4 = eve_comentariofidere4;
            dynamics_Reservas.eve_numero_meses_corriente_de_pago_garaje_2 = eve_numero_meses_corriente_de_pago_garaje_2;
            dynamics_Reservas.eve_comentariofidere7 = eve_comentariofidere7;
            dynamics_Reservas.eve_numero_meses_corriente_de_pago_garaje_1 = eve_numero_meses_corriente_de_pago_garaje_1;
            dynamics_Reservas.eve_comentariofidere6 = eve_comentariofidere6;
            dynamics_Reservas.eve_estado_documentacion_titular_2 = eve_estado_documentacion_titular_2;
            dynamics_Reservas.eve_comentariofidere1 = eve_comentariofidere1;
            dynamics_Reservas.eve_comentariofidere3 = eve_comentariofidere3;
            dynamics_Reservas.eve_comentariofidere2 = eve_comentariofidere2;
            dynamics_Reservas.eve_fecha_comentariocomercializadora10 = eve_fecha_comentariocomercializadora10;
            dynamics_Reservas.eve_color_horno_microondas = eve_color_horno_microondas;
            dynamics_Reservas.eve_documentacion_recibida_titular_2 = eve_documentacion_recibida_titular_2;
            dynamics_Reservas.eve_marca_campana = eve_marca_campana;
            dynamics_Reservas.eve_garantiaadicionalgaraje1 = eve_garantiaadicionalgaraje1;
            dynamics_Reservas.eve_dias_rechazo_documentacion_1 = eve_dias_rechazo_documentacion_1;
            dynamics_Reservas.eve_importecotitular1 = eve_importecotitular1;
            dynamics_Reservas.eve_marca_vitrocermica = eve_marca_vitrocermica;
            dynamics_Reservas.eve_importecotitular2 = eve_importecotitular2;
            dynamics_Reservas.eve_enseres_lavavajillasintegrado = eve_enseres_lavavajillasintegrado;
            dynamics_Reservas.C_eve_conviviente2_value = _eve_conviviente2_value;
            dynamics_Reservas.eve_plazotrastero1 = eve_plazotrastero1;
            dynamics_Reservas.eve_minusvalido_trastero_1 = eve_minusvalido_trastero_1;
            dynamics_Reservas.eve_scoringanio1 = eve_scoringanio1;
            dynamics_Reservas.eve_carencia_ibi_trastero1 = eve_carencia_ibi_trastero1;
            dynamics_Reservas.eve_bonificacion_recomendacion = eve_bonificacion_recomendacion;
            dynamics_Reservas.eve_numero_meses_corriente_de_pago_trastero_1 = eve_numero_meses_corriente_de_pago_trastero_1;
            dynamics_Reservas.eve_numero_meses_corriente_de_pago_trastero_2 = eve_numero_meses_corriente_de_pago_trastero_2;
            dynamics_Reservas.eve_leads_verano = eve_leads_verano;
            dynamics_Reservas.eve_periodicidadibitrastero2 = eve_periodicidadibitrastero2;
            dynamics_Reservas.eve_documentacion_pendiente_conv_1 = eve_documentacion_pendiente_conv_1;
            dynamics_Reservas.eve_periodicidadibigaraje2 = eve_periodicidadibigaraje2;
            dynamics_Reservas.eve_marca_congelador = eve_marca_congelador;
            dynamics_Reservas.eve_ivaprinexgaraje2 = eve_ivaprinexgaraje2;
            dynamics_Reservas.eve_color_horno = eve_color_horno;
            dynamics_Reservas.eve_mesesgarantiagaraje1 = eve_mesesgarantiagaraje1;
            dynamics_Reservas.eve_codigo_inquilino = eve_codigo_inquilino;
            dynamics_Reservas.eve_importetitularprincipal = eve_importetitularprincipal;
            dynamics_Reservas.eve_garantiaadicionaltrastero2 = eve_garantiaadicionaltrastero2;
            dynamics_Reservas.eve_marca_induccion = eve_marca_induccion;
            dynamics_Reservas.eve_meses_permanencia_garaje_1 = eve_meses_permanencia_garaje_1;
            dynamics_Reservas.eve_enseres_lavadoraintegrada = eve_enseres_lavadoraintegrada;
            dynamics_Reservas.eve_ivapersonajuridicatrastero1 = eve_ivapersonajuridicatrastero1;
            dynamics_Reservas.eve_bonificacion_campana = eve_bonificacion_campana;
            dynamics_Reservas.eve_sujeto_a_impagos_trastero_2 = eve_sujeto_a_impagos_trastero_2;
            dynamics_Reservas.eve_sujeto_a_impagos_trastero_1 = eve_sujeto_a_impagos_trastero_1;
            dynamics_Reservas.eve_ivapersonajuridicagaraje1 = eve_ivapersonajuridicagaraje1;
            dynamics_Reservas.eve_ivapersonajuridicatrastero2 = eve_ivapersonajuridicatrastero2;
            dynamics_Reservas.eve_numero_expediente = eve_numero_expediente;
            dynamics_Reservas.eve_ivapersonajuridicagaraje2 = eve_ivapersonajuridicagaraje2;
            dynamics_Reservas.eve_nota_localizacion_titular_3 = eve_nota_localizacion_titular_3;
            dynamics_Reservas.eve_mesesaplicaciongaraje1 = eve_mesesaplicaciongaraje1;
            dynamics_Reservas.eve_nota_localizacion_titular_2 = eve_nota_localizacion_titular_2;
            dynamics_Reservas.eve_nota_localizacion_titular_1 = eve_nota_localizacion_titular_1;
            dynamics_Reservas.eve_condiciones_firma_contrato = eve_condiciones_firma_contrato;
            dynamics_Reservas.eve_comunidadgaraje1 = eve_comunidadgaraje1;
            dynamics_Reservas.eve_marca_frigorifico = eve_marca_frigorifico;
            dynamics_Reservas.eve_carencia_comunidad_garaje1 = eve_carencia_comunidad_garaje1;
            dynamics_Reservas.eve_titular_relevante_1 = eve_titular_relevante_1;
            dynamics_Reservas.eve_fecha_envio_recordatorio_seguro = eve_fecha_envio_recordatorio_seguro;
            dynamics_Reservas.eve_color_congelador = eve_color_congelador;
            dynamics_Reservas.eve_vivienda_contingencia_rotacion = eve_vivienda_contingencia_rotacion;
            dynamics_Reservas.eve_rentagaraje1 = eve_rentagaraje1;
            dynamics_Reservas.eve_promocion_200 = eve_promocion_200;
            dynamics_Reservas.C_eve_promocioninmueble5_value = _eve_promocioninmueble5_value;
            dynamics_Reservas.eve_motivo_cancelacion_reserva = eve_motivo_cancelacion_reserva;
            dynamics_Reservas.eve_comentarios_documentacion_viv_protegidas = eve_comentarios_documentacion_viv_protegidas;
            dynamics_Reservas.eve_sujeto_permanencia_trastero_2 = eve_sujeto_permanencia_trastero_2;
            dynamics_Reservas.eve_sujeto_permanencia_trastero_1 = eve_sujeto_permanencia_trastero_1;
            dynamics_Reservas.eve_mesesaplicaciontrastero1 = eve_mesesaplicaciontrastero1;
            dynamics_Reservas.eve_cotitular_relevante_2 = eve_cotitular_relevante_2;
            dynamics_Reservas.eve_meses_permanencia_trastero_1 = eve_meses_permanencia_trastero_1;
            dynamics_Reservas.eve_cotitular_relevante_1 = eve_cotitular_relevante_1;

            return dynamics_Reservas;
        }
    }
}
