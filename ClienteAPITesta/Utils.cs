﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ClienteAPITesta
{
    static class Utils
    {
        public static JObject MergeJsons(this JToken bd, JToken crm)
        {
            var merged = new JObject();
            if (JToken.DeepEquals(bd, crm)) return crm as JObject;

            switch (bd.Type)
            {
                case JTokenType.Object:
                    {
                        var mas = bd as JObject;
                        var testa = crm as JObject;
                        var addedKeys = mas.Properties().Select(c => c.Name).Except(testa.Properties().Select(c => c.Name));
                        var removedKeys = testa.Properties().Select(c => c.Name).Except(mas.Properties().Select(c => c.Name));
                        var unchangedKeys = mas.Properties().Where(c => JToken.DeepEquals(c.Value, crm[c.Name])).Select(c => c.Name);
                        foreach (var k in addedKeys)
                        {
                            merged[k] = bd[k];
                        }
                        foreach (var k in removedKeys)
                        {
                            merged[k] = crm[k];
                        }
                        foreach (var k in unchangedKeys)
                        {
                            merged[k] = crm[k];
                        }
                        var potentiallyModifiedKeys = mas.Properties().Select(c => c.Name).Except(addedKeys).Except(unchangedKeys);
                        foreach (var k in potentiallyModifiedKeys)
                        {
                            var foundDiff = MergeJsons(mas[k], testa[k]);
                            if (foundDiff.HasValues) merged[k] = testa[k];
                        }
                    }
                    break;
                case JTokenType.Array:
                    {
                        var mas = bd as JArray;
                        var testa = crm as JArray;
                        var plus = new JArray(mas.Except(testa, new JTokenEqualityComparer()));
                        var minus = new JArray(testa.Except(mas, new JTokenEqualityComparer()));
                        if (plus.HasValues) merged["mas"] = plus;
                        if (minus.HasValues) merged["testa"] = minus;
                    }
                    break;
                default:
                    merged["mas"] = bd;
                    merged["testa"] = crm;
                    break;
            }

            return merged;
        }

        public static IEnumerable<IEnumerable<T>> Batch<T>(this IEnumerable<T> source, int size)
        {
            T[] bucket = null;
            var count = 0;

            foreach (var item in source)
            {
                if (bucket == null)
                    bucket = new T[size];

                bucket[count++] = item;

                if (count != size)
                    continue;

                yield return bucket.Select(x => x);

                bucket = null;
                count = 0;
            }

            // Return the last bucket with all remaining elements
            if (bucket != null && count > 0)
            {
                Array.Resize(ref bucket, count);
                yield return bucket.Select(x => x);
            }
        }
    }
}
