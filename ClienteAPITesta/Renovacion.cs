﻿using System;
using Newtonsoft.Json;
using ModeloDatos;

namespace ClienteAPITesta
{
    public class Renovacion
    {
        [JsonProperty("@odata.etag")]
        public string OdataEtag { get; set; }
        public string eve_renta_neta_y1 { get; set; }
        public string eve_inquilino_2 { get; set; }
        public string eve_envio_burof_legal { get; set; }
        public string eve_nif_inquilino_2 { get; set; }
        public string eve_nif_inquilino_3 { get; set; }
        public string modifiedon { get; set; }
        public string _eve_contratorenovacionid_value { get; set; }
        public string _owninguser_value { get; set; }
        public string eve_cod_postal { get; set; }
        public string eve_tipo_envio_renovacion { get; set; }
        public string eve_ibi { get; set; }
        public string eve_recibidoburofaxopcioncompra { get; set; }
        public string eve_renovacionid { get; set; }
        public string eve_descripcionpromocion { get; set; }
        public string eve_activacionburofaxrecibido { get; set; }
        public string _ownerid_value { get; set; }
        public string eve_nombre { get; set; }
        public string eve_poblacion { get; set; }
        public string _eve_promocinrenovacinid_value { get; set; }
        public string eve_inquilino_1 { get; set; }
        public string eve_nif_inquilino_4 { get; set; }
        public string eve_fecha_envio_comunicacion { get; set; }
        public string eve_solicitarenvionotificaciones { get; set; }
        public string timezoneruleversionnumber { get; set; }
        public string eve_com_teo_12 { get; set; }
        public string createdon { get; set; }
        public string versionnumber { get; set; }
        public string eve_tipo_comunicacion { get; set; }
        public string eve_nif_inquilino_1 { get; set; }
        public string eve_periodo_carencia_ibi { get; set; }
        public string eve_telefono2 { get; set; }
        public string eve_tipo_plantilla { get; set; }
        public string eve_fecha_fin_prorroga { get; set; }
        public string eve_solicitarenviorenovacionsinpeticion { get; set; }
        public string eve_periodo_carencia_comunidad { get; set; }
        public string _eve_sociedadid_value { get; set; }
        public string eve_codigo_inmueble_principal { get; set; }
        public string eve_mes_facturacion_fidelizacion { get; set; }
        public string eve_fecha_fin_contrato { get; set; }
        public string _modifiedby_value { get; set; }
        public string eve_dif_fianza { get; set; }
        public string eve_email1 { get; set; }
        public string _createdby_value { get; set; }
        public string eve_numeroparticipantes { get; set; }
        public string statuscode { get; set; }
        public string eve_fechafirma { get; set; }
        public string eve_envioemailrenovacionsinpeticion { get; set; }
        public string eve_fecha_inicio_contrato { get; set; }
        public string eve_inquilino_3 { get; set; }
        public string eve_envioemailrenovacionopcioncompra { get; set; }
        public string eve_inquilino_4 { get; set; }
        public string eve_ubicacion { get; set; }
        public string statecode { get; set; }
        public string _owningbusinessunit_value { get; set; }
        public string eve_fianza { get; set; }
        public string eve_supuesto12 { get; set; }
        public string eve_camponumerico3 { get; set; }
        public string eve_apoderado_1 { get; set; }
        public string eve_nombreapellidos_c2 { get; set; }
        public string eve_apoderado_2 { get; set; }
        public string eve_apoderado_3 { get; set; }
        public string eve_apoderado_4 { get; set; }
        public string eve_apoderado_5 { get; set; }
        public string eve_renta_sin_iva_y2_gar1_12 { get; set; }
        public string eve_garantia { get; set; }
        public string eve_ibi_men_y3 { get; set; }
        public string eve_nif_c5 { get; set; }
        public string eve_supuesto10 { get; set; }
        public string eve_com_lista_gar2 { get; set; }
        public string eve_total_impagados { get; set; }
        public string importsequencenumber { get; set; }
        public string _eve_contratorenovacinid_value { get; set; }
        public string eve_renta_sin_iva_y1_gar3_12 { get; set; }
        public string eve_camponumerico5 { get; set; }
        public string utcconversiontimezonecode { get; set; }
        public string eve_parrafo_garantia { get; set; }
        public string overriddencreatedon { get; set; }
        public string eve_bonificacionmensual3 { get; set; }
        public string eve_rentas_ant_prop_50por { get; set; }
        public string eve_camponumerico2 { get; set; }
        public string eve_enlaceportalfidere { get; set; }
        public string eve_bonificaciontexto5 { get; set; }
        public string eve_bonificacionmensual6 { get; set; }
        public string eve_n_garaje_1 { get; set; }
        public string eve_telefonofidere { get; set; }
        public string eve_nombreapellidos_c5 { get; set; }
        public string eve_renta_sin_iva_y1_gar2_12 { get; set; }
        public string eve_ibi_men_y2 { get; set; }
        public string eve_dni_aval_1 { get; set; }
        public string eve_com_lista_gar1 { get; set; }
        public string eve_renta_neta_y5_12 { get; set; }
        public string eve_telefono5 { get; set; }
        public string eve_telefono4 { get; set; }
        public string eve_renta_sin_iva_y3_12 { get; set; }
        public string eve_renta_total_ga3 { get; set; }
        public string eve_telefono1 { get; set; }
        public string eve_renta_total_ga2 { get; set; }
        public string eve_renta_total_ga1 { get; set; }
        public string eve_telefono3 { get; set; }
        public string eve_rta_fidere_50por { get; set; }
        public string eve_fianza_gar2 { get; set; }
        public string eve_fianza_gar3 { get; set; }
        public string processid { get; set; }
        public string eve_renta_sin_iva_y1_gar1_12 { get; set; }
        public string eve_fianza_gar1 { get; set; }
        public string eve_bonificacionmensual7 { get; set; }
        public string eve_duraciondelcontrato { get; set; }
        public string eve_com_lista_gar3 { get; set; }
        public string eve_ryc_prop_y4 { get; set; }
        public string eve_nuevo_precio_oc { get; set; }
        public string eve_nombreapellidos_c4 { get; set; }
        public string eve_fecha_inicio_renovacion { get; set; }
        public string eve_com_lista { get; set; }
        public string traversedpath { get; set; }
        public string eve_ryc_prop_y1 { get; set; }
        public string eve_ryc_prop_y2 { get; set; }
        public string eve_ryc_prop_y3 { get; set; }
        public string eve_ibi_men_y1 { get; set; }
        public string eve_bonificaciontexto1 { get; set; }
        public string eve_numeroconciertofianza { get; set; }
        public string eve_cif { get; set; }
        public string eve_bonificaciontexto4 { get; set; }
        public string eve_bonificacionmensual1 { get; set; }
        public string eve_anejo_4 { get; set; }
        public string eve_anejo_5 { get; set; }
        public string eve_anejo_2 { get; set; }
        public string eve_m2_utiles_anejo_5 { get; set; }
        public string eve_anejo_3 { get; set; }
        public string eve_m2_utiles_anejo_4 { get; set; }
        public string eve_anejo_1 { get; set; }
        public string eve_m2_utiles_anejo_3 { get; set; }
        public string eve_com_teo_gar2_12 { get; set; }
        public string eve_m2_utiles_anejo_2 { get; set; }
        public string eve_m2_utiles_anejo_1 { get; set; }
        public string eve_campofecha1 { get; set; }
        public string eve_campofecha3 { get; set; }
        public string eve_campofecha2 { get; set; }
        public string eve_campofecha5 { get; set; }
        public string eve_campofecha4 { get; set; }
        public string eve_dni_aval_2 { get; set; }
        public string eve_bonificacionmensual4 { get; set; }
        public string eve_referenciacatastral { get; set; }
        public string eve_rentas_ant_prop { get; set; }
        public string eve_fecharenovacion { get; set; }
        public string eve_com_teo_gar3_12 { get; set; }
        public string eve_parentesco_c1 { get; set; }
        public string eve_parentesco_c2 { get; set; }
        public string eve_dif_fianza_gar1 { get; set; }
        public string eve_bonificaciontexto2 { get; set; }
        public string _owningteam_value { get; set; }
        public string eve_parentesco_c3 { get; set; }
        public string eve_dif_fianza_gar2 { get; set; }
        public string eve_renta_sin_iva_y4 { get; set; }
        public string eve_parentesco_c4 { get; set; }
        public string eve_dif_fianza_gar3 { get; set; }
        public string eve_numero_finca { get; set; }
        public string eve_parentesco_c5 { get; set; }
        public string eve_estimacion_renta { get; set; }
        public string eve_parrafo_aval { get; set; }
        public string eve_renta_neta_y4_12 { get; set; }
        public string eve_rentaycomunidad_y3 { get; set; }
        public string eve_renta_neta_y5 { get; set; }
        public string eve_renta_neta_y4 { get; set; }
        public string eve_renta_sin_iva_y1 { get; set; }
        public string _modifiedonbehalfby_value { get; set; }
        public string eve_m2_utiles_ga_3 { get; set; }
        public string eve_renta_neta_y3 { get; set; }
        public string eve_renta_neta_y2 { get; set; }
        public string eve_bonificacionmensual5 { get; set; }
        public string eve_com_lista_gar2_12 { get; set; }
        public string eve_fecha_fin_novacion { get; set; }
        public string eve_rentaycomunidad_y4 { get; set; }
        public string eve_inquilino_5 { get; set; }
        public string eve_m2_utiles_ga_4 { get; set; }
        public string eve_com_teo_gar1_12 { get; set; }
        public string eve_rentas_100por { get; set; }
        public string eve_rentaycomunidad_y1 { get; set; }
        public string _createdonbehalfby_value { get; set; }
        public string eve_bonificaciontexto3 { get; set; }
        public string eve_m2_utiles_ga_1 { get; set; }
        public string eve_renta_neta_y1_12 { get; set; }
        public string eve_rentas_fidere { get; set; }
        public string eve_observacion_renovacion { get; set; }
        public string eve_rentaycomunidad_y2 { get; set; }
        public string eve_fechanacimiento_c5 { get; set; }
        public string eve_m2_utiles_ga_2 { get; set; }
        public string eve_renta_sin_iva_y4_12 { get; set; }
        public string eve_registro { get; set; }
        public string eve_renta_sin_iva_y4_gar3_12 { get; set; }
        public string eve_m2_utiles_vi { get; set; }
        public string eve_promocion { get; set; }
        public string eve_email4 { get; set; }
        public string eve_com_lista_12 { get; set; }
        public string eve_precio_max_califi { get; set; }
        public string eve_nif_inquilino_5 { get; set; }
        public string eve_renta_sin_iva_y4_gar2 { get; set; }
        public string eve_renta_sin_iva_y4_gar3 { get; set; }
        public string eve_renta_sin_iva_y4_gar1 { get; set; }
        public string eve_dif_precio_max_50por { get; set; }
        public string eve_renta_sin_iva_y4_gar2_12 { get; set; }
        public string eve_email2 { get; set; }
        public string eve_m2_utiles_tr_2 { get; set; }
        public string eve_renta_sin_iva_y3 { get; set; }
        public string eve_fechanacimiento_c4 { get; set; }
        public string eve_renta_sin_iva_y2_gar2 { get; set; }
        public string eve_renta_sin_iva_y2_gar3 { get; set; }
        public string eve_anioibi2 { get; set; }
        public string eve_renta_sin_iva_y2_gar1 { get; set; }
        public string eve_renta_neta_y3_12 { get; set; }
        public string eve_nif_c3 { get; set; }
        public string eve_email5 { get; set; }
        public string eve_m2_utiles_tr_1 { get; set; }
        public string eve_fechanacimiento_c1 { get; set; }
        public string eve_gsqm { get; set; }
        public string eve_renta_sin_iva_y4_gar1_12 { get; set; }
        public string eve_renta_sin_iva_y3_gar3_12 { get; set; }
        public string eve_supuesto7 { get; set; }
        public string eve_supuesto6 { get; set; }
        public string eve_supuesto5 { get; set; }
        public string eve_supuesto4 { get; set; }
        public string eve_supuesto3 { get; set; }
        public string eve_renta_sin_iva_y1_gar2 { get; set; }
        public string eve_supuesto2 { get; set; }
        public string eve_nif_apoderado_1 { get; set; }
        public string eve_supuesto1 { get; set; }
        public string eve_renta_sin_iva_y1_gar3 { get; set; }
        public string eve_email3 { get; set; }
        public string eve_nif_apoderado_2 { get; set; }
        public string eve_nif_apoderado_3 { get; set; }
        public string eve_renta_sin_iva_y1_gar1 { get; set; }
        public string eve_m2_utiles_tr_3 { get; set; }
        public string eve_nif_apoderado_4 { get; set; }
        public string eve_fechanacimiento_c3 { get; set; }
        public string eve_renta_sin_iva_y2 { get; set; }
        public string eve_nif_apoderado_5 { get; set; }
        public string eve_supuesto9 { get; set; }
        public string eve_aval_personal_2 { get; set; }
        public string eve_anioibi1 { get; set; }
        public string eve_supuesto8 { get; set; }
        public string eve_idcontrato { get; set; }
        public string eve_aval_personal_1 { get; set; }
        public string eve_operacion { get; set; }
        public string eve_mesdeactualizacion { get; set; }
        public string _stageid_value { get; set; }
        public string eve_n_garaje_2 { get; set; }
        public string eve_renta_sin_iva_y1_12 { get; set; }
        public string eve_n_trastero_2 { get; set; }
        public string eve_com_lista_gar3_12 { get; set; }
        public string eve_renta_sin_iva_y3_gar2_12 { get; set; }
        public string eve_fecha_fin_renovacion { get; set; }
        public string eve_emailcontactofidere { get; set; }
        public string eve_coef_par { get; set; }
        public string eve_renta_sin_iva_y2_12 { get; set; }
        public string eve_nif_c2 { get; set; }
        public string eve_n_garaje_4 { get; set; }
        public string eve_campotexto5 { get; set; }
        public string eve_renta_sin_iva_y2_gar3_12 { get; set; }
        public string eve_fechanacimiento_c2 { get; set; }
        public string eve_campotexto4 { get; set; }
        public string eve_supuesto11 { get; set; }
        public string eve_campotexto1 { get; set; }
        public string eve_campotexto3 { get; set; }
        public string eve_camponumerico4 { get; set; }
        public string eve_campotexto2 { get; set; }
        public string eve_renta_sin_iva_y3_gar1_12 { get; set; }
        public string eve_bonificaciontotal { get; set; }
        public string eve_com_lista_gar1_12 { get; set; }
        public string eve_nif_c1 { get; set; }
        public string eve_n_garaje_3 { get; set; }
        public string eve_nombreapellidos_c3 { get; set; }
        public string eve_renta_neta_y2_12 { get; set; }
        public string eve_n_trastero_3 { get; set; }
        public string eve_camponumerico1 { get; set; }
        public string eve_fechamodificacionregistro { get; set; }
        public string eve_nif_c4 { get; set; }
        public string eve_rentas_desc_50por { get; set; }
        public string eve_renta_sin_iva_y3_gar2 { get; set; }
        public string eve_renta_sin_iva_y2_gar2_12 { get; set; }
        public string eve_renta_sin_iva_y3_gar3 { get; set; }
        public string eve_renta_sin_iva_y3_gar1 { get; set; }
        public string eve_anyos_novacion { get; set; }
        public string eve_bonificacionmensual2 { get; set; }
        public string eve_nombreapellidos_c1 { get; set; }
        public string eve_n_trastero_1 { get; set; }
        public Renovacion() { }

        public Dynamics_Renovaciones Convertir2BD()
        {
            Dynamics_Renovaciones d_renovacion = new Dynamics_Renovaciones();
            d_renovacion.OdataEtag = OdataEtag;
            d_renovacion.eve_renta_neta_y1 = eve_renta_neta_y1;
            d_renovacion.eve_inquilino_2 = eve_inquilino_2;
            d_renovacion.eve_envio_burof_legal = eve_envio_burof_legal;
            d_renovacion.eve_nif_inquilino_2 = eve_nif_inquilino_2;
            d_renovacion.eve_nif_inquilino_3 = eve_nif_inquilino_3;
            d_renovacion.modifiedon = modifiedon;
            d_renovacion.C_eve_contratorenovacionid_value = _eve_contratorenovacionid_value;
            d_renovacion.C_owninguser_value = _owninguser_value;
            d_renovacion.eve_cod_postal = eve_cod_postal;
            d_renovacion.eve_tipo_envio_renovacion = eve_tipo_envio_renovacion;
            d_renovacion.eve_ibi = eve_ibi;
            d_renovacion.eve_recibidoburofaxopcioncompra = eve_recibidoburofaxopcioncompra;
            d_renovacion.eve_renovacionid = eve_renovacionid;
            d_renovacion.eve_descripcionpromocion = eve_descripcionpromocion;
            d_renovacion.eve_activacionburofaxrecibido = eve_activacionburofaxrecibido;
            d_renovacion.C_ownerid_value = _ownerid_value;
            d_renovacion.eve_nombre = eve_nombre;
            d_renovacion.eve_poblacion = eve_poblacion;
            d_renovacion.C_eve_promocinrenovacinid_value = _eve_promocinrenovacinid_value;
            d_renovacion.eve_inquilino_1 = eve_inquilino_1;
            d_renovacion.eve_nif_inquilino_4 = eve_nif_inquilino_4;
            d_renovacion.eve_fecha_envio_comunicacion = eve_fecha_envio_comunicacion;
            d_renovacion.eve_solicitarenvionotificaciones = eve_solicitarenvionotificaciones;
            d_renovacion.timezoneruleversionnumber = timezoneruleversionnumber;
            d_renovacion.eve_com_teo_12 = eve_com_teo_12;
            d_renovacion.createdon = createdon;
            d_renovacion.versionnumber = versionnumber;
            d_renovacion.eve_tipo_comunicacion = eve_tipo_comunicacion;
            d_renovacion.eve_nif_inquilino_1 = eve_nif_inquilino_1;
            d_renovacion.eve_periodo_carencia_ibi = eve_periodo_carencia_ibi;
            d_renovacion.eve_telefono2 = eve_telefono2;
            d_renovacion.eve_tipo_plantilla = eve_tipo_plantilla;
            d_renovacion.eve_fecha_fin_prorroga = eve_fecha_fin_prorroga;
            d_renovacion.eve_solicitarenviorenovacionsinpeticion = eve_solicitarenviorenovacionsinpeticion;
            d_renovacion.eve_periodo_carencia_comunidad = eve_periodo_carencia_comunidad;
            d_renovacion.C_eve_sociedadid_value = _eve_sociedadid_value;
            d_renovacion.eve_codigo_inmueble_principal = eve_codigo_inmueble_principal;
            d_renovacion.eve_mes_facturacion_fidelizacion = eve_mes_facturacion_fidelizacion;
            d_renovacion.eve_fecha_fin_contrato = eve_fecha_fin_contrato;
            d_renovacion.C_modifiedby_value = _modifiedby_value;
            d_renovacion.eve_dif_fianza = eve_dif_fianza;
            d_renovacion.eve_email1 = eve_email1;
            d_renovacion.C_createdby_value = _createdby_value;
            d_renovacion.eve_numeroparticipantes = eve_numeroparticipantes;
            d_renovacion.statuscode = statuscode;
            d_renovacion.eve_fechafirma = eve_fechafirma;
            d_renovacion.eve_envioemailrenovacionsinpeticion = eve_envioemailrenovacionsinpeticion;
            d_renovacion.eve_fecha_inicio_contrato = eve_fecha_inicio_contrato;
            d_renovacion.eve_inquilino_3 = eve_inquilino_3;
            d_renovacion.eve_envioemailrenovacionopcioncompra = eve_envioemailrenovacionopcioncompra;
            d_renovacion.eve_inquilino_4 = eve_inquilino_4;
            d_renovacion.eve_ubicacion = eve_ubicacion;
            d_renovacion.statecode = statecode;
            d_renovacion.C_owningbusinessunit_value = _owningbusinessunit_value;
            d_renovacion.eve_fianza = eve_fianza;
            d_renovacion.eve_supuesto12 = eve_supuesto12;
            d_renovacion.eve_camponumerico3 = eve_camponumerico3;
            d_renovacion.eve_apoderado_1 = eve_apoderado_1;
            d_renovacion.eve_nombreapellidos_c2 = eve_nombreapellidos_c2;
            d_renovacion.eve_apoderado_2 = eve_apoderado_2;
            d_renovacion.eve_apoderado_3 = eve_apoderado_3;
            d_renovacion.eve_apoderado_4 = eve_apoderado_4;
            d_renovacion.eve_apoderado_5 = eve_apoderado_5;
            d_renovacion.eve_renta_sin_iva_y2_gar1_12 = eve_renta_sin_iva_y2_gar1_12;
            d_renovacion.eve_garantia = eve_garantia;
            d_renovacion.eve_ibi_men_y3 = eve_ibi_men_y3;
            d_renovacion.eve_nif_c5 = eve_nif_c5;
            d_renovacion.eve_supuesto10 = eve_supuesto10;
            d_renovacion.eve_com_lista_gar2 = eve_com_lista_gar2;
            d_renovacion.eve_total_impagados = eve_total_impagados;
            d_renovacion.importsequencenumber = importsequencenumber;
            d_renovacion.C_eve_contratorenovacinid_value = _eve_contratorenovacinid_value;
            d_renovacion.eve_renta_sin_iva_y1_gar3_12 = eve_renta_sin_iva_y1_gar3_12;
            d_renovacion.eve_camponumerico5 = eve_camponumerico5;
            d_renovacion.utcconversiontimezonecode = utcconversiontimezonecode;
            d_renovacion.eve_parrafo_garantia = eve_parrafo_garantia;
            d_renovacion.overriddencreatedon = overriddencreatedon;
            d_renovacion.eve_bonificacionmensual3 = eve_bonificacionmensual3;
            d_renovacion.eve_rentas_ant_prop_50por = eve_rentas_ant_prop_50por;
            d_renovacion.eve_camponumerico2 = eve_camponumerico2;
            d_renovacion.eve_enlaceportalfidere = eve_enlaceportalfidere;
            d_renovacion.eve_bonificaciontexto5 = eve_bonificaciontexto5;
            d_renovacion.eve_bonificacionmensual6 = eve_bonificacionmensual6;
            d_renovacion.eve_n_garaje_1 = eve_n_garaje_1;
            d_renovacion.eve_telefonofidere = eve_telefonofidere;
            d_renovacion.eve_nombreapellidos_c5 = eve_nombreapellidos_c5;
            d_renovacion.eve_renta_sin_iva_y1_gar2_12 = eve_renta_sin_iva_y1_gar2_12;
            d_renovacion.eve_ibi_men_y2 = eve_ibi_men_y2;
            d_renovacion.eve_dni_aval_1 = eve_dni_aval_1;
            d_renovacion.eve_com_lista_gar1 = eve_com_lista_gar1;
            d_renovacion.eve_renta_neta_y5_12 = eve_renta_neta_y5_12;
            d_renovacion.eve_telefono5 = eve_telefono5;
            d_renovacion.eve_telefono4 = eve_telefono4;
            d_renovacion.eve_renta_sin_iva_y3_12 = eve_renta_sin_iva_y3_12;
            d_renovacion.eve_renta_total_ga3 = eve_renta_total_ga3;
            d_renovacion.eve_telefono1 = eve_telefono1;
            d_renovacion.eve_renta_total_ga2 = eve_renta_total_ga2;
            d_renovacion.eve_renta_total_ga1 = eve_renta_total_ga1;
            d_renovacion.eve_telefono3 = eve_telefono3;
            d_renovacion.eve_rta_fidere_50por = eve_rta_fidere_50por;
            d_renovacion.eve_fianza_gar2 = eve_fianza_gar2;
            d_renovacion.eve_fianza_gar3 = eve_fianza_gar3;
            d_renovacion.processid = processid;
            d_renovacion.eve_renta_sin_iva_y1_gar1_12 = eve_renta_sin_iva_y1_gar1_12;
            d_renovacion.eve_fianza_gar1 = eve_fianza_gar1;
            d_renovacion.eve_bonificacionmensual7 = eve_bonificacionmensual7;
            d_renovacion.eve_duraciondelcontrato = eve_duraciondelcontrato;
            d_renovacion.eve_com_lista_gar3 = eve_com_lista_gar3;
            d_renovacion.eve_ryc_prop_y4 = eve_ryc_prop_y4;
            d_renovacion.eve_nuevo_precio_oc = eve_nuevo_precio_oc;
            d_renovacion.eve_nombreapellidos_c4 = eve_nombreapellidos_c4;
            d_renovacion.eve_fecha_inicio_renovacion = eve_fecha_inicio_renovacion;
            d_renovacion.eve_com_lista = eve_com_lista;
            d_renovacion.traversedpath = traversedpath;
            d_renovacion.eve_ryc_prop_y1 = eve_ryc_prop_y1;
            d_renovacion.eve_ryc_prop_y2 = eve_ryc_prop_y2;
            d_renovacion.eve_ryc_prop_y3 = eve_ryc_prop_y3;
            d_renovacion.eve_ibi_men_y1 = eve_ibi_men_y1;
            d_renovacion.eve_bonificaciontexto1 = eve_bonificaciontexto1;
            d_renovacion.eve_numeroconciertofianza = eve_numeroconciertofianza;
            d_renovacion.eve_cif = eve_cif;
            d_renovacion.eve_bonificaciontexto4 = eve_bonificaciontexto4;
            d_renovacion.eve_bonificacionmensual1 = eve_bonificacionmensual1;
            d_renovacion.eve_anejo_4 = eve_anejo_4;
            d_renovacion.eve_anejo_5 = eve_anejo_5;
            d_renovacion.eve_anejo_2 = eve_anejo_2;
            d_renovacion.eve_m2_utiles_anejo_5 = eve_m2_utiles_anejo_5;
            d_renovacion.eve_anejo_3 = eve_anejo_3;
            d_renovacion.eve_m2_utiles_anejo_4 = eve_m2_utiles_anejo_4;
            d_renovacion.eve_anejo_1 = eve_anejo_1;
            d_renovacion.eve_m2_utiles_anejo_3 = eve_m2_utiles_anejo_3;
            d_renovacion.eve_com_teo_gar2_12 = eve_com_teo_gar2_12;
            d_renovacion.eve_m2_utiles_anejo_2 = eve_m2_utiles_anejo_2;
            d_renovacion.eve_m2_utiles_anejo_1 = eve_m2_utiles_anejo_1;
            d_renovacion.eve_campofecha1 = eve_campofecha1;
            d_renovacion.eve_campofecha3 = eve_campofecha3;
            d_renovacion.eve_campofecha2 = eve_campofecha2;
            d_renovacion.eve_campofecha5 = eve_campofecha5;
            d_renovacion.eve_campofecha4 = eve_campofecha4;
            d_renovacion.eve_dni_aval_2 = eve_dni_aval_2;
            d_renovacion.eve_bonificacionmensual4 = eve_bonificacionmensual4;
            d_renovacion.eve_referenciacatastral = eve_referenciacatastral;
            d_renovacion.eve_rentas_ant_prop = eve_rentas_ant_prop;
            d_renovacion.eve_fecharenovacion = eve_fecharenovacion;
            d_renovacion.eve_com_teo_gar3_12 = eve_com_teo_gar3_12;
            d_renovacion.eve_parentesco_c1 = eve_parentesco_c1;
            d_renovacion.eve_parentesco_c2 = eve_parentesco_c2;
            d_renovacion.eve_dif_fianza_gar1 = eve_dif_fianza_gar1;
            d_renovacion.eve_bonificaciontexto2 = eve_bonificaciontexto2;
            d_renovacion.C_owningteam_value = _owningteam_value;
            d_renovacion.eve_parentesco_c3 = eve_parentesco_c3;
            d_renovacion.eve_dif_fianza_gar2 = eve_dif_fianza_gar2;
            d_renovacion.eve_renta_sin_iva_y4 = eve_renta_sin_iva_y4;
            d_renovacion.eve_parentesco_c4 = eve_parentesco_c4;
            d_renovacion.eve_dif_fianza_gar3 = eve_dif_fianza_gar3;
            d_renovacion.eve_numero_finca = eve_numero_finca;
            d_renovacion.eve_parentesco_c5 = eve_parentesco_c5;
            d_renovacion.eve_estimacion_renta = eve_estimacion_renta;
            d_renovacion.eve_parrafo_aval = eve_parrafo_aval;
            d_renovacion.eve_renta_neta_y4_12 = eve_renta_neta_y4_12;
            d_renovacion.eve_rentaycomunidad_y3 = eve_rentaycomunidad_y3;
            d_renovacion.eve_renta_neta_y5 = eve_renta_neta_y5;
            d_renovacion.eve_renta_neta_y4 = eve_renta_neta_y4;
            d_renovacion.eve_renta_sin_iva_y1 = eve_renta_sin_iva_y1;
            d_renovacion.C_modifiedonbehalfby_value = _modifiedonbehalfby_value;
            d_renovacion.eve_m2_utiles_ga_3 = eve_m2_utiles_ga_3;
            d_renovacion.eve_renta_neta_y3 = eve_renta_neta_y3;
            d_renovacion.eve_renta_neta_y2 = eve_renta_neta_y2;
            d_renovacion.eve_bonificacionmensual5 = eve_bonificacionmensual5;
            d_renovacion.eve_com_lista_gar2_12 = eve_com_lista_gar2_12;
            d_renovacion.eve_fecha_fin_novacion = eve_fecha_fin_novacion;
            d_renovacion.eve_rentaycomunidad_y4 = eve_rentaycomunidad_y4;
            d_renovacion.eve_inquilino_5 = eve_inquilino_5;
            d_renovacion.eve_m2_utiles_ga_4 = eve_m2_utiles_ga_4;
            d_renovacion.eve_com_teo_gar1_12 = eve_com_teo_gar1_12;
            d_renovacion.eve_rentas_100por = eve_rentas_100por;
            d_renovacion.eve_rentaycomunidad_y1 = eve_rentaycomunidad_y1;
            d_renovacion.C_createdonbehalfby_value = _createdonbehalfby_value;
            d_renovacion.eve_bonificaciontexto3 = eve_bonificaciontexto3;
            d_renovacion.eve_m2_utiles_ga_1 = eve_m2_utiles_ga_1;
            d_renovacion.eve_renta_neta_y1_12 = eve_renta_neta_y1_12;
            d_renovacion.eve_rentas_fidere = eve_rentas_fidere;
            d_renovacion.eve_observacion_renovacion = eve_observacion_renovacion;
            d_renovacion.eve_rentaycomunidad_y2 = eve_rentaycomunidad_y2;
            d_renovacion.eve_fechanacimiento_c5 = eve_fechanacimiento_c5;
            d_renovacion.eve_m2_utiles_ga_2 = eve_m2_utiles_ga_2;
            d_renovacion.eve_renta_sin_iva_y4_12 = eve_renta_sin_iva_y4_12;
            d_renovacion.eve_registro = eve_registro;
            d_renovacion.eve_renta_sin_iva_y4_gar3_12 = eve_renta_sin_iva_y4_gar3_12;
            d_renovacion.eve_m2_utiles_vi = eve_m2_utiles_vi;
            d_renovacion.eve_promocion = eve_promocion;
            d_renovacion.eve_email4 = eve_email4;
            d_renovacion.eve_com_lista_12 = eve_com_lista_12;
            d_renovacion.eve_precio_max_califi = eve_precio_max_califi;
            d_renovacion.eve_nif_inquilino_5 = eve_nif_inquilino_5;
            d_renovacion.eve_renta_sin_iva_y4_gar2 = eve_renta_sin_iva_y4_gar2;
            d_renovacion.eve_renta_sin_iva_y4_gar3 = eve_renta_sin_iva_y4_gar3;
            d_renovacion.eve_renta_sin_iva_y4_gar1 = eve_renta_sin_iva_y4_gar1;
            d_renovacion.eve_dif_precio_max_50por = eve_dif_precio_max_50por;
            d_renovacion.eve_renta_sin_iva_y4_gar2_12 = eve_renta_sin_iva_y4_gar2_12;
            d_renovacion.eve_email2 = eve_email2;
            d_renovacion.eve_m2_utiles_tr_2 = eve_m2_utiles_tr_2;
            d_renovacion.eve_renta_sin_iva_y3 = eve_renta_sin_iva_y3;
            d_renovacion.eve_fechanacimiento_c4 = eve_fechanacimiento_c4;
            d_renovacion.eve_renta_sin_iva_y2_gar2 = eve_renta_sin_iva_y2_gar2;
            d_renovacion.eve_renta_sin_iva_y2_gar3 = eve_renta_sin_iva_y2_gar3;
            d_renovacion.eve_anioibi2 = eve_anioibi2;
            d_renovacion.eve_renta_sin_iva_y2_gar1 = eve_renta_sin_iva_y2_gar1;
            d_renovacion.eve_renta_neta_y3_12 = eve_renta_neta_y3_12;
            d_renovacion.eve_nif_c3 = eve_nif_c3;
            d_renovacion.eve_email5 = eve_email5;
            d_renovacion.eve_m2_utiles_tr_1 = eve_m2_utiles_tr_1;
            d_renovacion.eve_fechanacimiento_c1 = eve_fechanacimiento_c1;
            d_renovacion.eve_gsqm = eve_gsqm;
            d_renovacion.eve_renta_sin_iva_y4_gar1_12 = eve_renta_sin_iva_y4_gar1_12;
            d_renovacion.eve_renta_sin_iva_y3_gar3_12 = eve_renta_sin_iva_y3_gar3_12;
            d_renovacion.eve_supuesto7 = eve_supuesto7;
            d_renovacion.eve_supuesto6 = eve_supuesto6;
            d_renovacion.eve_supuesto5 = eve_supuesto5;
            d_renovacion.eve_supuesto4 = eve_supuesto4;
            d_renovacion.eve_supuesto3 = eve_supuesto3;
            d_renovacion.eve_renta_sin_iva_y1_gar2 = eve_renta_sin_iva_y1_gar2;
            d_renovacion.eve_supuesto2 = eve_supuesto2;
            d_renovacion.eve_nif_apoderado_1 = eve_nif_apoderado_1;
            d_renovacion.eve_supuesto1 = eve_supuesto1;
            d_renovacion.eve_renta_sin_iva_y1_gar3 = eve_renta_sin_iva_y1_gar3;
            d_renovacion.eve_email3 = eve_email3;
            d_renovacion.eve_nif_apoderado_2 = eve_nif_apoderado_2;
            d_renovacion.eve_nif_apoderado_3 = eve_nif_apoderado_3;
            d_renovacion.eve_renta_sin_iva_y1_gar1 = eve_renta_sin_iva_y1_gar1;
            d_renovacion.eve_m2_utiles_tr_3 = eve_m2_utiles_tr_3;
            d_renovacion.eve_nif_apoderado_4 = eve_nif_apoderado_4;
            d_renovacion.eve_fechanacimiento_c3 = eve_fechanacimiento_c3;
            d_renovacion.eve_renta_sin_iva_y2 = eve_renta_sin_iva_y2;
            d_renovacion.eve_nif_apoderado_5 = eve_nif_apoderado_5;
            d_renovacion.eve_supuesto9 = eve_supuesto9;
            d_renovacion.eve_aval_personal_2 = eve_aval_personal_2;
            d_renovacion.eve_anioibi1 = eve_anioibi1;
            d_renovacion.eve_supuesto8 = eve_supuesto8;
            d_renovacion.eve_idcontrato = eve_idcontrato;
            d_renovacion.eve_aval_personal_1 = eve_aval_personal_1;
            d_renovacion.eve_operacion = eve_operacion;
            d_renovacion.eve_mesdeactualizacion = eve_mesdeactualizacion;
            d_renovacion.C_stageid_value = _stageid_value;
            d_renovacion.eve_n_garaje_2 = eve_n_garaje_2;
            d_renovacion.eve_renta_sin_iva_y1_12 = eve_renta_sin_iva_y1_12;
            d_renovacion.eve_n_trastero_2 = eve_n_trastero_2;
            d_renovacion.eve_com_lista_gar3_12 = eve_com_lista_gar3_12;
            d_renovacion.eve_renta_sin_iva_y3_gar2_12 = eve_renta_sin_iva_y3_gar2_12;
            d_renovacion.eve_fecha_fin_renovacion = eve_fecha_fin_renovacion;
            d_renovacion.eve_emailcontactofidere = eve_emailcontactofidere;
            d_renovacion.eve_coef_par = eve_coef_par;
            d_renovacion.eve_renta_sin_iva_y2_12 = eve_renta_sin_iva_y2_12;
            d_renovacion.eve_nif_c2 = eve_nif_c2;
            d_renovacion.eve_n_garaje_4 = eve_n_garaje_4;
            d_renovacion.eve_campotexto5 = eve_campotexto5;
            d_renovacion.eve_renta_sin_iva_y2_gar3_12 = eve_renta_sin_iva_y2_gar3_12;
            d_renovacion.eve_fechanacimiento_c2 = eve_fechanacimiento_c2;
            d_renovacion.eve_campotexto4 = eve_campotexto4;
            d_renovacion.eve_supuesto11 = eve_supuesto11;
            d_renovacion.eve_campotexto1 = eve_campotexto1;
            d_renovacion.eve_campotexto3 = eve_campotexto3;
            d_renovacion.eve_camponumerico4 = eve_camponumerico4;
            d_renovacion.eve_campotexto2 = eve_campotexto2;
            d_renovacion.eve_renta_sin_iva_y3_gar1_12 = eve_renta_sin_iva_y3_gar1_12;
            d_renovacion.eve_bonificaciontotal = eve_bonificaciontotal;
            d_renovacion.eve_com_lista_gar1_12 = eve_com_lista_gar1_12;
            d_renovacion.eve_nif_c1 = eve_nif_c1;
            d_renovacion.eve_n_garaje_3 = eve_n_garaje_3;
            d_renovacion.eve_nombreapellidos_c3 = eve_nombreapellidos_c3;
            d_renovacion.eve_renta_neta_y2_12 = eve_renta_neta_y2_12;
            d_renovacion.eve_n_trastero_3 = eve_n_trastero_3;
            d_renovacion.eve_camponumerico1 = eve_camponumerico1;
            d_renovacion.eve_fechamodificacionregistro = eve_fechamodificacionregistro;
            d_renovacion.eve_nif_c4 = eve_nif_c4;
            d_renovacion.eve_rentas_desc_50por = eve_rentas_desc_50por;
            d_renovacion.eve_renta_sin_iva_y3_gar2 = eve_renta_sin_iva_y3_gar2;
            d_renovacion.eve_renta_sin_iva_y2_gar2_12 = eve_renta_sin_iva_y2_gar2_12;
            d_renovacion.eve_renta_sin_iva_y3_gar3 = eve_renta_sin_iva_y3_gar3;
            d_renovacion.eve_renta_sin_iva_y3_gar1 = eve_renta_sin_iva_y3_gar1;
            d_renovacion.eve_anyos_novacion = eve_anyos_novacion;
            d_renovacion.eve_bonificacionmensual2 = eve_bonificacionmensual2;
            d_renovacion.eve_nombreapellidos_c1 = eve_nombreapellidos_c1;
            d_renovacion.eve_n_trastero_1 = eve_n_trastero_1;
            return d_renovacion;
        }
    }
}
