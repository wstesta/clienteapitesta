﻿using System;
using Newtonsoft.Json;
using ModeloDatos;

namespace ClienteAPITesta
{
    public class Campanya_Comercial
    {
        [JsonProperty("@odata.etag")]
        public string OdataEtag { get; set; }
        public string eve_fecha_contacto { get; set; }
        public string eve_reasignacion { get; set; }
        public string _eve_inquilino_principal_id_value { get; set; }
        public string createdon { get; set; }
        public string eve_name { get; set; }
        public string importsequencenumber { get; set; }
        public string statecode { get; set; }
        public string eve_estado_recuperacion_adicional { get; set; }
        public string eve_fecha_cambio_motivo_1 { get; set; }
        public string eve_fecha_cambio_motivo_2 { get; set; }
        public string eve_tipo_recuperacion { get; set; }
        public string _ownerid_value { get; set; }
        public string modifiedon { get; set; }
        public string _eve_codigo_contrato_id_value { get; set; }
        public string eve_comentarios_motivo_finalizacion { get; set; }
        public string versionnumber { get; set; }
        public string eve_motivo_inquilino { get; set; }
        public string timezoneruleversionnumber { get; set; }
        public string eve_comentarios_estado_recuperacion { get; set; }
        public string _modifiedby_value { get; set; }
        public string eve_tipo_envio_renovacion { get; set; }
        public string statuscode { get; set; }
        public string eve_estado_recuperacion { get; set; }
        public string eve_fecha_envio_renovacion_historico { get; set; }
        public string eve_motivo_finalizacion_contrato_anterior_2 { get; set; }
        public string eve_campanya_comercialid { get; set; }
        public string eve_motivo_finalizacion_contrato_anterior_1 { get; set; }
        public string _createdby_value { get; set; }
        public string _owningbusinessunit_value { get; set; }
        public string eve_motivo_finalizacion_contrato_adicional { get; set; }
        public string _owninguser_value { get; set; }
        public string eve_equipo_encargado_recuperacion { get; set; }
        public string eve_motivo_no_recuperacion_1 { get; set; }
        public string eve_cierre_automatico_recuperacion { get; set; }
        public string eve_otros_motivos_i { get; set; }
        public string eve_email_recuperacion_inquilino_2 { get; set; }
        public string eve_fecha_fin_contrato { get; set; }
        public string eve_cierre_expediente { get; set; }
        public string eve_fecha_finalizacion_recuperacion { get; set; }
        public string eve_otros_motivos_ii { get; set; }
        public string eve_inquilino_recuperacion_3 { get; set; }
        public string overriddencreatedon { get; set; }
        public string eve_inquilino_recuperacion_1 { get; set; }
        public string eve_fecha_cierre_automatico { get; set; }
        public string _modifiedonbehalfby_value { get; set; }
        public string eve_email_recuperacion_inquilino_3 { get; set; }
        public string eve_codigo_postal_destino { get; set; }
        public string eve_poblacion_destino { get; set; }
        public string eve_email_recuperacion_inquilino_1 { get; set; }
        public string eve_clasificacion_cambio_vivienda { get; set; }
        public string utcconversiontimezonecode { get; set; }
        public string eve_motivo_no_recuperacion_2 { get; set; }
        public string _createdonbehalfby_value { get; set; }
        public string eve_telefono_recuperacion_1 { get; set; }
        public string eve_telefono_recuperacion_3 { get; set; }
        public string eve_telefono_recuperacion_2 { get; set; }
        public string eve_inquilino_recuperacion_2 { get; set; }
        public string _owningteam_value { get; set; }
        public Campanya_Comercial() {}
        public Dynamics_Campanya_Comercial Convertir2BD() {
            Dynamics_Campanya_Comercial d_cc = new Dynamics_Campanya_Comercial();
            d_cc.OdataEtag = OdataEtag;
            d_cc.eve_fecha_contacto = eve_fecha_contacto;
            d_cc.eve_reasignacion = eve_reasignacion;
            d_cc.C_eve_inquilino_principal_id_value = _eve_inquilino_principal_id_value;
            d_cc.createdon = createdon;
            d_cc.eve_name = eve_name;
            d_cc.importsequencenumber = importsequencenumber;
            d_cc.statecode = statecode;
            d_cc.eve_estado_recuperacion_adicional = eve_estado_recuperacion_adicional;
            d_cc.eve_fecha_cambio_motivo_1 = eve_fecha_cambio_motivo_1;
            d_cc.eve_fecha_cambio_motivo_2 = eve_fecha_cambio_motivo_2;
            d_cc.eve_tipo_recuperacion = eve_tipo_recuperacion;
            d_cc.C_ownerid_value = _ownerid_value;
            d_cc.modifiedon = modifiedon;
            d_cc.C_eve_codigo_contrato_id_value = _eve_codigo_contrato_id_value;
            d_cc.eve_comentarios_motivo_finalizacion = eve_comentarios_motivo_finalizacion;
            d_cc.versionnumber = versionnumber;
            d_cc.eve_motivo_inquilino = eve_motivo_inquilino;
            d_cc.timezoneruleversionnumber = timezoneruleversionnumber;
            d_cc.eve_comentarios_estado_recuperacion = eve_comentarios_estado_recuperacion;
            d_cc.C_modifiedby_value = _modifiedby_value;
            d_cc.eve_tipo_envio_renovacion = eve_tipo_envio_renovacion;
            d_cc.statuscode = statuscode;
            d_cc.eve_estado_recuperacion = eve_estado_recuperacion;
            d_cc.eve_fecha_envio_renovacion_historico = eve_fecha_envio_renovacion_historico;
            d_cc.eve_motivo_finalizacion_contrato_anterior_2 = eve_motivo_finalizacion_contrato_anterior_2;
            d_cc.eve_campanya_comercialid = eve_campanya_comercialid;
            d_cc.eve_motivo_finalizacion_contrato_anterior_1 = eve_motivo_finalizacion_contrato_anterior_1;
            d_cc.C_createdby_value = _createdby_value;
            d_cc.C_owningbusinessunit_value = _owningbusinessunit_value;
            d_cc.eve_motivo_finalizacion_contrato_adicional = eve_motivo_finalizacion_contrato_adicional;
            d_cc.C_owninguser_value = _owninguser_value;
            d_cc.eve_equipo_encargado_recuperacion = eve_equipo_encargado_recuperacion;
            d_cc.eve_motivo_no_recuperacion_1 = eve_motivo_no_recuperacion_1;
            d_cc.eve_cierre_automatico_recuperacion = eve_cierre_automatico_recuperacion;
            d_cc.eve_otros_motivos_i = eve_otros_motivos_i;
            d_cc.eve_email_recuperacion_inquilino_2 = eve_email_recuperacion_inquilino_2;
            d_cc.eve_fecha_fin_contrato = eve_fecha_fin_contrato;
            d_cc.eve_cierre_expediente = eve_cierre_expediente;
            d_cc.eve_fecha_finalizacion_recuperacion = eve_fecha_finalizacion_recuperacion;
            d_cc.eve_otros_motivos_ii = eve_otros_motivos_ii;
            d_cc.eve_inquilino_recuperacion_3 = eve_inquilino_recuperacion_3;
            d_cc.overriddencreatedon = overriddencreatedon;
            d_cc.eve_inquilino_recuperacion_1 = eve_inquilino_recuperacion_1;
            d_cc.eve_fecha_cierre_automatico = eve_fecha_cierre_automatico;
            d_cc.C_modifiedonbehalfby_value = _modifiedonbehalfby_value;
            d_cc.eve_email_recuperacion_inquilino_3 = eve_email_recuperacion_inquilino_3;
            d_cc.eve_codigo_postal_destino = eve_codigo_postal_destino;
            d_cc.eve_poblacion_destino = eve_poblacion_destino;
            d_cc.eve_email_recuperacion_inquilino_1 = eve_email_recuperacion_inquilino_1;
            d_cc.eve_clasificacion_cambio_vivienda = eve_clasificacion_cambio_vivienda;
            d_cc.utcconversiontimezonecode = utcconversiontimezonecode;
            d_cc.eve_motivo_no_recuperacion_2 = eve_motivo_no_recuperacion_2;
            d_cc.C_createdonbehalfby_value = _createdonbehalfby_value;
            d_cc.eve_telefono_recuperacion_1 = eve_telefono_recuperacion_1;
            d_cc.eve_telefono_recuperacion_3 = eve_telefono_recuperacion_3;
            d_cc.eve_telefono_recuperacion_2 = eve_telefono_recuperacion_2;
            d_cc.eve_inquilino_recuperacion_2 = eve_inquilino_recuperacion_2;
            d_cc.C_owningteam_value = _owningteam_value;
            return d_cc;
        }
    }
}
