﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Configuration;

namespace ClienteAPITesta
{
    class Email
    {
        private static SmtpClient _cliente;
        private static string _host;
        private static int _port;
        private static bool _ssl;
        private static string _user_email;
        private static string _pass_email;
        public static string _destinatarios;
        public static string _asunto;
        private void ReadConfigEmail()
        {
            _host = ReadSetting("EmailHost", 1);
            _port = int.Parse(ReadSetting("EmailPort", 1));
            _ssl = Boolean.Parse(ReadSetting("Ssl", 1));
            _user_email = ReadSetting("EmailUser", 1);
            _pass_email = ReadSetting("EmailPass", 0);
            _destinatarios = ReadSetting("Destinatarios", 1);
            _asunto = ReadSetting("Asunto", 1);
        }
        private string ReadSetting(string key, int log)
        {
            //si log 0 no hay log, si otra cosa sí hay log
            string result = string.Empty;
            try
            {
                var appSettings = ConfigurationManager.AppSettings;
                result = appSettings[key] ?? "Not Found";
                if (log != 0)
                {
                    Program.logger.Info($"Info Setting: {key} value: {result}");
                }
            }
            catch (ConfigurationErrorsException e)
            {
                Program.logger.Error($"Exception: {e.Message}");
            }
            return result;
        }
        public void InicializarGestorCorreo()
        {
            if (null == _host)
            {
                ReadConfigEmail();
            }

            _cliente = new SmtpClient(_host, _port)
            {
                EnableSsl = _ssl,
                Port = _port,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(_user_email, _pass_email)
            };
        }
        public void EnviarCorreo(string destinatario, string asunto, string mensaje, bool esHtlm = false)
        {
            MailMessage email = new MailMessage(_user_email, destinatario, asunto, mensaje);
            email.IsBodyHtml = esHtlm;
            try
            {
                _cliente.Send(email);
                Program.logger.Info($"Email enviado desde {_user_email}: {destinatario} {asunto} {mensaje}");
            }
            catch (Exception e)
            {
                Program.logger.Error($"Exception: {e.Message}");
            }
            finally
            {
                _cliente.Dispose();
            }
        }
        private static string CrearCuerpoMensaje(List<string> archivos_FTP)
        {
            string mensaje_email = string.Empty;
            
            return mensaje_email;
        }
    }
}
