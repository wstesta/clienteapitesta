﻿using System;
using Newtonsoft.Json;
using ModeloDatos;

namespace ClienteAPITesta
{
    public class Inmueble_anejo_comercial
    {
        [JsonProperty("@odata.etag")]
        public string OdataEtag { get; set; }
        public string _owningbusinessunit_value { get; set; }
        public string eve_relacionvigente { get; set; }
        public string _ownerid_value { get; set; }
        public Nullable<bool> statuscode { get; set; }
        public string importsequencenumber { get; set; }
        public string eve_name { get; set; }
        public string _eve_codigoinmuebleprincipalid_value { get; set; }
        public string _eve_codigoinmuebleanejocomercialid_value { get; set; }
        public string modifiedon { get; set; }
        public string _owninguser_value { get; set; }
        public string _modifiedby_value { get; set; }
        public Nullable<int> versionnumber { get; set; }
        public string eve_inmuebleanejoscomercialid { get; set; }
        public string createdon { get; set; }
        public Nullable<bool> statecode { get; set; }
        public string _createdby_value { get; set; }
        public string overriddencreatedon { get; set; }
        public string eve_idanejocomercial { get; set; }
        public string emailaddress { get; set; }
        public string _modifiedonbehalfby_value { get; set; }
        public string eve_comentarios_anejos_comerciales { get; set; }
        public string _eve_codigopromocioninmuebleprincid_value { get; set; }
        public string utcconversiontimezonecode { get; set; }
        public string eve_idanejocomercial0 { get; set; }
        public string _createdonbehalfby_value { get; set; }
        public string _eve_codigopromocionanejocomercialid_value { get; set; }
        public string _owningteam_value { get; set; }
        public string timezoneruleversionnumber { get; set; }
        public Inmueble_anejo_comercial() { }
        public Dynamics_InmuebleAnejoComercial Convertir2BD()
        {
            Dynamics_InmuebleAnejoComercial dynamics_InmuebleAnejoComercial = new Dynamics_InmuebleAnejoComercial();
            dynamics_InmuebleAnejoComercial.odataetag = OdataEtag;
            dynamics_InmuebleAnejoComercial.C_owningbusinessunit_value = _owningbusinessunit_value;
            dynamics_InmuebleAnejoComercial.eve_relacionvigente = eve_relacionvigente;
            dynamics_InmuebleAnejoComercial.C_ownerid_value = _ownerid_value;
            dynamics_InmuebleAnejoComercial.statuscode = statuscode;
            dynamics_InmuebleAnejoComercial.importsequencenumber = importsequencenumber;
            dynamics_InmuebleAnejoComercial.eve_name = eve_name;
            dynamics_InmuebleAnejoComercial.C_eve_codigoinmuebleprincipalid_value = _eve_codigoinmuebleprincipalid_value;
            dynamics_InmuebleAnejoComercial.C_eve_codigoinmuebleanejocomercialid_value = _eve_codigoinmuebleanejocomercialid_value;
            dynamics_InmuebleAnejoComercial.modifiedon = modifiedon;
            dynamics_InmuebleAnejoComercial.C_owninguser_value = _owninguser_value;
            dynamics_InmuebleAnejoComercial.C_modifiedby_value = _modifiedby_value;
            dynamics_InmuebleAnejoComercial.versionnumber = versionnumber;
            dynamics_InmuebleAnejoComercial.eve_inmuebleanejoscomercialid = eve_inmuebleanejoscomercialid;
            dynamics_InmuebleAnejoComercial.createdon = createdon;
            dynamics_InmuebleAnejoComercial.statecode = statecode;
            dynamics_InmuebleAnejoComercial.C_createdby_value = _createdby_value;
            dynamics_InmuebleAnejoComercial.overriddencreatedon = overriddencreatedon;
            dynamics_InmuebleAnejoComercial.eve_idanejocomercial = eve_idanejocomercial;
            dynamics_InmuebleAnejoComercial.emailaddress = emailaddress;
            dynamics_InmuebleAnejoComercial.C_modifiedonbehalfby_value = _modifiedonbehalfby_value;
            dynamics_InmuebleAnejoComercial.eve_comentarios_anejos_comerciales = eve_comentarios_anejos_comerciales;
            dynamics_InmuebleAnejoComercial.C_eve_codigopromocioninmuebleprincid_value = _eve_codigopromocioninmuebleprincid_value;
            dynamics_InmuebleAnejoComercial.utcconversiontimezonecode = utcconversiontimezonecode;
            dynamics_InmuebleAnejoComercial.eve_idanejocomercial0 = eve_idanejocomercial0;
            dynamics_InmuebleAnejoComercial.C_createdonbehalfby_value = _createdonbehalfby_value;
            dynamics_InmuebleAnejoComercial.C_eve_codigopromocionanejocomercialid_value = _eve_codigopromocionanejocomercialid_value;
            dynamics_InmuebleAnejoComercial.C_owningteam_value = _owningteam_value;
            dynamics_InmuebleAnejoComercial.timezoneruleversionnumber = timezoneruleversionnumber;
            return dynamics_InmuebleAnejoComercial;
        }
    }
}
